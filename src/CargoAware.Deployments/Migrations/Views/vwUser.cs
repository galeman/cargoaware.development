﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630150956)]
    public sealed class vwUser : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwUser') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwUser AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwUser 
                AS
                   SELECT
                        u.Id,
                        u.Name,
                        u.Email,
                        u.Username,
                        u.LastLogIn,
                        u.LastPasswordChange,
                        u.LockedUntil,
                        u.LockReason,
                        u.IsArchived,
                        up.Permission AS Uri
                    FROM dbo.[User] u
                    LEFT JOIN dbo.UserPermission up ON up.UserId = u.Id;");
        }
    }
}
