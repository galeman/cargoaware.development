﻿using System;

namespace CargoAware.Services.Infrastructure
{
    public sealed class DomainException : Exception
    {
        public DomainException(string message) : base(message)
        {
        }

        public DomainException(string message, params object[] args) : base(String.Format(message, args))
        {
        }
    }
}