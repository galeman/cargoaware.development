﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150115223567)]
    public sealed class Event : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Event")
                  .WithColumn("Timestamp").AsCustom("DateTimeOffset(0)").NotNullable()
                  .WithColumn("EventSource").AsByte().NotNullable()
                  .WithColumn("EventName").AsAnsiString(255).NotNullable()
                  .WithColumn("Message").AsAnsiString(int.MaxValue).NotNullable();
        }
    }
}