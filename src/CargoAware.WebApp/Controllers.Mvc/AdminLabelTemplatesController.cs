﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.LabelTemplate;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("admin/labeltemplates")]
    public partial class AdminLabelTemplatesController : AppMvcController
    {
        private static Lazy<LabelTemplateRepository> _LabelTemplateRepository;


        #region // Constructor + DI
        // ==================================================

        static AdminLabelTemplatesController()
        {
            Inject(Repositories.ForLabelTemplates);
        }

        private static void Inject(Lazy<LabelTemplateRepository> dependency)
        {
            _LabelTemplateRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.AdminLabelTemplates()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        [GET("addedit")]
        public virtual ActionResult AddEdit(Boolean adding, long[] ids)
        {
            if (!User.HasPermission(Permissions.User.AdminLabelTemplates()))
            {
                throw UnauthorizedException();
            }

            var model = new AddEditLabelTemplatesModel
            {
                Adding = adding,
                LabelTemplates = _LabelTemplateRepository.Value.GetLabelTemplates(User.CurrentLocation.Id).Data
            };

            if (!adding)
            {
                if (ids != null && ids.Length != 0)
                {
                    model.Details = model.LabelTemplates
                        .Where(x => ids.Any(id => x.Id == id))
                        .Select(x => new AddEditLabelTemplatesDetailsModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Type = x.Type,
                            Language = x.Language,
                            Code = x.Code,
                            Version = x.Version,
                            IsActive = x.IsActive
                        });
                }
            }

            return View(model);
        }

        // ==================================================
        #endregion
    }
}