﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(201506300602)]
    public sealed class vwTagRead : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwTagRead') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwTagRead AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwTagRead 
                AS
                   SELECT
                        tr.Id,
                        tr.Epc,
                        tr.LocationId,
                        tr.RegionManagerId,
                        tr.X,
                        tr.Y,
                        tr.Z,
                        tr.Rssi,
                        tr.Readers,
                        tr.[Timestamp],
                        tr.LocationZoneId,
                        --tr.IsProcessed,
                        lz.Name AS LocationZoneName,
                        tr.FixedLocationRead,
                        s.Id AS ShipmentId
                        --COALESCE(s.TagReadSegmentStart, DATEADD(day, -1, MAX(tr.[Timestamp]) OVER (PARTITION BY tr.Epc))) AS CalculatedTagReadSegmentStart
                    FROM dbo.TagRead tr
                        INNER JOIN dbo.Shipment s ON tr.Epc = s.AirWaybillTagEpc OR tr.Epc = s.UldTagEpc
                        LEFT JOIN dbo.LocationZone lz ON tr.LocationZoneId = lz.Id;");
        }
    }
}
