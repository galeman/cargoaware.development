﻿using CargoAware.Services.Common;
using System;

namespace CargoAware.Services.Location
{
    public sealed class LocationDocument
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public AxisDirection AxisDirection { get; set; }
        public OriginLocation OriginLocation { get; set; }
        public Guid ApiKey { get; set; }
        public Boolean IsArchived { get; set; }

        internal long LocationId { set { Id = value; } }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as LocationDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}