﻿using System;

namespace CargoAware.JobService.Infrastructure
{
    public interface IJobEx //: IJob
    {
        Boolean ContinueProcessing { get; set; }
        void Execute();
    }
}