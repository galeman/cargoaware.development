﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Printer;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/printers")]
    public class PrintersApiController : AppApiController
    {
        private static Lazy<PrinterRepository> _PrinterRepository;


        #region // Constructor + DI
        // ==================================================

        static PrintersApiController()
        {
            Inject(Repositories.ForPrinters);
        }

        private static void Inject(Lazy<PrinterRepository> dependency)
        {
            _PrinterRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<PrinterDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.AdminPrinters()))
            {
                throw UnauthorizedException();
            }

            var result = _PrinterRepository.Value.GetPrinters(User.CurrentLocation.Id, request.GetApplyMethod());
            return result;
        }

        [POST("create"), HttpPost]
        public HttpResponseMessage Create(IEnumerable<AddEditPrintersDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminPrinters()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (String.IsNullOrWhiteSpace(detail.Name) ||
                    !detail.Type.HasValue ||
                    String.IsNullOrWhiteSpace(detail.Address) ||
                    detail.Port == 0)
                {
                    continue;
                }

                PrinterService.CreatePrinter(new PrinterDocument
                {
                    LocationId = locationId,
                    Name = detail.Name,
                    Type = detail.Type.Value,
                    Address = detail.Address,
                    Port = detail.Port,
                    ExternalId = detail.ExternalId,
                    UldDefault = detail.UldDefault,
                });
            }

            return SuccessResponse(true);
        }

        [POST("update"), HttpPost]
        public HttpResponseMessage Update(IEnumerable<AddEditPrintersDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminPrinters()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (detail.Id == 0 ||
                    String.IsNullOrWhiteSpace(detail.Name) ||
                    !detail.Type.HasValue ||
                    String.IsNullOrWhiteSpace(detail.Address) ||
                    detail.Port == 0)
                {
                    continue;
                }

                PrinterService.UpdatePrinter(new PrinterDocument
                {
                    Id = detail.Id,
                    LocationId = locationId,
                    Name = detail.Name,
                    Type = detail.Type.Value,
                    Address = detail.Address,
                    Port = detail.Port,
                    ExternalId = detail.ExternalId,
                    UldDefault = detail.UldDefault,
                });
            }

            return SuccessResponse(true);
        }

        [POST("archive"), HttpPost]
        public HttpResponseMessage Archive([FromBody] long id)
        {
            if (!User.HasPermission(Permissions.User.AdminPrinters()))
            {
                throw UnauthorizedException();
            }

            PrinterService.ArchivePrinter(new PrinterDocument
            {
                Id = id
            });

            return SuccessResponse(true);
        }

        // ==================================================
        #endregion
    }
}