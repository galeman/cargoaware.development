﻿using System.Data.Common;

namespace CargoAware.Services.Infrastructure.Repositories
{
    public interface IDbConnectionFactory
    {
        DbConnection OpenDbConnection();
        DbConnection CreateDbConnection();
    }
}