﻿// franwell.js
// (c) 2012-2015 Franwell, Inc.

// requires - jQuery.js, jQuery.validationEngine.js, Spin.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};
    franwell.$ = franwell.$ || root.jQuery;


    // =========================================================
    // readCookie : reads the specified cookie from the document
    // =========================================================

    franwell.readCookie = franwell.readCookie || function (name, useCache) {
        if (useCache == null) { useCache = true; }
        if (franwell.cookies && useCache) { return franwell.cookies[name]; }

        franwell.cookies = {};
        var docCookies = document.cookie.split('; ');

        for (var i = docCookies.length - 1; i >= 0; i--) {
            var cookieData = docCookies[i].split('=');
            franwell.cookies[cookieData[0]] = cookieData[1];
        }

        return franwell.cookies[name];
    };


    // ==============================================================
    // replaceClass : TODO Description
    // --------------------------------------------------------------
    //  - oldClass: "Old" selector that will be used to find elements
    //  - newClass: Selector that will replace the newSelector
    // ==============================================================

    franwell.replaceClass = franwell.replaceClass || function (oldClass, newClass) {
        return franwell.$('.' + oldClass)
            .removeClass(oldClass)
            .addClass(newClass);
    };


    // ======================================================================================
    // singleSelectCheckboxes : Ensures that only one checkbox can be selected among siblings
    // --------------------------------------------------------------------------------------
    //  - levelsUp: How many parent levels to backtrack to group siblings
    // ======================================================================================

    franwell.singleSelectCheckboxes = franwell.singleSelectCheckboxes || function (levelsUp) {
        if (levelsUp == null || typeof levelsUp !== 'number') levelsUp = 0;

        var jsClass = '.js-singleselect-checkbox';

        franwell.$(jsClass).change(function () {
            if (this.checked) {
                var $searchTarget = franwell.$(this).parent();
                // start at "1" because we always start one level up
                for (var i = 1; i < levelsUp; i++) {
                    var $newTarget = $searchTarget.parent();
                    if ($newTarget.length !== 0) {
                        $searchTarget = $newTarget;
                    }
                }

                $searchTarget
                    .find(jsClass + ':checked')
                    .not(this)
                    .prop('checked', false);
            }
        });
    };


    // ===========================================================
    // getRecursiveProperty : Recursively finds a property's value
    // ===========================================================

    franwell.getRecursiveProperty = franwell.getRecursiveProperty || function (obj, properties) {
        var props = properties.split('.'),
            result = null;

        for (var i = 0, icnt = props.length; i < icnt; i++) {
            if (typeof obj[props[i]] !== 'undefined') {
                result = obj = obj[props[i]];
            } else {
                break;
            }
        }

        return result;
    };


    // ========================================
    // startSpinner : Shows a "loading" spinner
    // ========================================

    franwell.startSpinner = franwell.startSpinner || function () {
        var $body = franwell.$(document.body),
            $data = $body.data();

        if (typeof $data.spinner === 'undefined') {
            if ($body.children('#spinnerBackground').length === 0) {
                franwell.$('<div id="spinnerBackground">')
                    .css({
                        backgroundColor: '#000',
                        position: 'fixed',
                        top: 0,
                        bottom: 0,
                        left: 0,
                        right: 0,
                        opacity: 0.6,
                        zIndex: 20000
                    })
                    .appendTo($body);
            }

            var options = {
                lines: 11, // The number of lines to draw
                length: 30, // The length of each line
                width: 11, // The line thickness
                radius: 30, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#FFF', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 60, // Afterglow percentage
                shadow: true, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: 'auto', // Top position relative to parent in px
                left: 'auto' // Left position relative to parent in px
            };
            $data.spinner = new Spinner(options).spin($body.get(0));
            return true;
        } else {
            return false;
        }
    };


    // ===============================
    // stopSpinner : Stops the spinner
    // ===============================

    franwell.stopSpinner = franwell.stopSpinner || function () {
        var $body = franwell.$(document.body),
            $spinnerBackground = $body.children('#spinnerBackground'),
            $data = $body.data();

        if (typeof $data.spinner !== 'undefined') {
            $data.spinner.stop();
            delete $data.spinner;
        }
        if ($spinnerBackground.length !== 0) {
            $spinnerBackground.remove();
        }
    };


    // =============================================================================
    // validationEngine: Sets up the Form Validation Engine with some default values
    // =============================================================================

    franwell.validationEngine = franwell.validationEngine || function ($form) {
        if (!franwell.$.validationEngineLanguage.allRules.notPoBox) {
            franwell.$.validationEngineLanguage.allRules.notPoBox = {
                func: function (field) {
                    // RegEx exists in code-behind, please update that one if changing this one
                    var pattern = new RegExp(/\b[p]*(ost)*\.*\s*[o|0]*(ffice)*\.*\s*b[o|0]x[\b\s\d_]+/i);
                    var match = pattern.exec(field.val());
                    return (match == null);
                },
                alertText: '* P.O. Box addresses are not permitted'
            };
        }

        $form.validationEngine({
            autoHidePrompt: true,
            autoHideDelay: 3000,
            promptPosition: "topLeft"
        });
    };


    // =================================================================
    // ServerErrors : Opens an error alert with useful error information
    // =================================================================

    franwell.ServerErrors = franwell.ServerErrors || function (errorResponse) {
        var alertTemplate =
                '<div class="alert alert-error">' +
                    '<a class="close" data-dismiss="alert">×</a>' +
                    '<p>{{message}} {{exceptionTypeTemplate}}</p>' +
                    '{{exceptionMessageTemplate}}' +
                    '{{stackTraceTemplate}}' +
                '</div>',
            exceptionTypeTemplate =
                '({{exceptionType}})',
            exceptionMessageTemplate =
                '<p>{{exceptionMessage}}</p>',
            stackTraceTemplate =
                '<ul class="js-panelbar-new">' +
                    '<li>' +
                        '<span class="muted"><small>(view details)</small></span>' +
                        '<div><pre style="max-height: 400px; overflow-y: scroll;">{{stackTrace}}</pre></div>' +
                    '</li>' +
                '</ul>',

            message = typeof errorResponse === 'string' ? errorResponse : errorResponse.Message,
            exceptionType = errorResponse.ExceptionType || errorResponse.ClassName || null,
            exceptionMessage = errorResponse.ExceptionMessage || null,
            stackTrace = errorResponse.StackTrace || errorResponse.RemoteStackTraceString || errorResponse.StackTraceString || null,

            result = alertTemplate
                .replace('{{message}}', message || 'An error has occurred (' + (new Date()).toString() + ')')
                .replace('{{exceptionTypeTemplate}}',
                    exceptionType ? exceptionTypeTemplate.replace('{{exceptionType}}', exceptionType) :
                    '')
                .replace('{{exceptionMessageTemplate}}',
                    exceptionMessage ? exceptionMessageTemplate.replace('{{exceptionMessage}}', exceptionMessage) :
                    '')
                .replace('{{stackTraceTemplate}}',
                    stackTrace ? stackTraceTemplate.replace('{{stackTrace}}', stackTrace) :
                    '');

        franwell.$('#user-alerts').append(result);
        franwell.$('.js-panelbar-new')
            .removeClass('js-panelbar-new')
            .addClass('js-panelbar')
            .kendoPanelBar();
    };


    // ==============================================================================
    // MessageAlerts : Opens an alert with a message, and optionally, a type of alert
    // ==============================================================================

    franwell.MessageAlerts = franwell.MessageAlerts || function (message, type) {
        var template =
            '<div class="alert alert-{{type}}">' +
                '<a class="close" data-dismiss="alert">×</a>' +
                '<p>{{message}}</p>' +
            '</div>';

        if (type == null) type = 'info';

        var result = template
            .replace('{{type}}', type)
            .replace('{{message}}', message);

        franwell.$('#user-alerts').append(result);
    };


    // ==========================================================================================================
    // MonitorUserSession : Continously monitors the current user's session and alerts when it is about to expire
    // ==========================================================================================================

    franwell.MonitorUserSession = franwell.MonitorUserSession || function (timeoutCookieName, sessionDuration, noopUrl, logOutUrl) {
        var $sessionTimeoutAlert = franwell.$('#session_timeout_alert'),
            $sessionTimeleft = franwell.$('#session_timeleft'),
            $extendSession = franwell.$('#extend_session'),
            $logoutSession = franwell.$('#logout_session'),
            $sessionTimeoutBar = franwell.$('#session_timeout_bar'),
            performingLogOut = false,

            hideSessionTimeoutAlert = function () { $sessionTimeoutAlert.hide(300); },

            performLogOut = function () {
                if (performingLogOut) return;
                franwell.startSpinner();
                window.location.href = logOutUrl;
                performingLogOut = true;
            };

        $extendSession.click(function () {
            franwell.submitData({ url: noopUrl, useSpinner: false });
            hideSessionTimeoutAlert();
        });

        $logoutSession.click(function () {
            performLogOut();
            hideSessionTimeoutAlert();
        });

        setInterval(function () {
            var utcNow = moment.utc(),
                utcSessionTimeout = moment.utc(franwell.readCookie(timeoutCookieName, false)),
                timeLeftMs = utcSessionTimeout.diff(utcNow),
                sessionPercent = 100 - Math.max(timeLeftMs * 100 / sessionDuration, 0),
                sessionTimedOut = sessionPercent > 99;

            // Less than three minutes left
            if (!performingLogOut && !sessionTimedOut && timeLeftMs < 180000) {
                if (!$sessionTimeoutAlert.data('dismissed')) {
                    $sessionTimeleft.text(utcSessionTimeout.fromNow(true));
                    if (!$sessionTimeoutAlert.is(':visible')) {
                        $sessionTimeoutAlert.show(500);
                    }
                }
            } else {
                if ($sessionTimeoutAlert.data('dismissed')) $sessionTimeoutAlert.removeData('dismissed');
                if ($sessionTimeoutAlert.is(':visible')) hideSessionTimeoutAlert();
            }

            // User has timed out - log off
            if (sessionTimedOut) performLogOut();

            if (!window.lastSessionPercent) window.lastSessionPercent = 0;
            if (window.lastSessionPercent < 100) {
                window.lastSessionPercent = sessionPercent;
                $sessionTimeoutBar
                    .css('background-color',
                        sessionPercent < 50 ? '#0E0' :
                        sessionPercent < 75 ? '#E90' :
                        '#E00')
                    .width(sessionPercent.toString() + '%');
            }
        }, 2000);
    };


    // ============================================================================================
    // UpdateableTimers : Sets off a timer that updates all elements with a data-datetime attribute
    // ============================================================================================

    franwell.UpdateableTimers = franwell.UpdateableTimers || function () {
        franwell.$('.js-updateable-timestamp').each(function () {
            var $this = franwell.$(this),
                datetime = $this.data('datetime');
            if (datetime) {
                $this.text(franwell.formatTimeFromNow(datetime));
            }
        });

        if (window.updateableTimersTimeout) {
            clearTimeout(window.updateableTimersTimeout);
        }
        window.updateableTimersTimeout = setTimeout(franwell.UpdateableTimers, 10000);
    };


    // ==================================================================================
    // errorResponseHandler : TODO Description
    // ==================================================================================

    franwell.errorResponseHandler = franwell.errorResponseHandler || function (response) {
        var errorResponse = response.responseJSON || response;

        window.log(errorResponse);
        switch (response.status) {
            case 0:
                // If the error is 0, ignore it...
                break;
            case 401:
                // If the session has expired, reload the window and the user will be redirected to the log-in page
                location.reload();
                break;
            case 500:
                franwell.ServerErrors(errorResponse);
                break;
            default:
                franwell.MessageAlerts(response.statusText, 'error');
        }
    };


    // =======================================================
    // submitData : Helper for submitting data to the server
    // -------------------------------------------------------
    // Options:
    //  - url: the URL
    //  - method: HTTP method to use (default: 'POST')
    //  - data: data to pass on along with the request
    //  - dataType: data type name
    //  - contentType: content type name of data going across
    //  - success: event handler to call on success
    //  - error: event handler to call on error
    //  - useSpinner: boolean whether to use the UI spinner (default: true)
    // =======================================================

    franwell.submitData = franwell.submitData || function (options) {
        options = franwell.$.extend(true, {
            url: '',
            method: 'POST',
            success: function () { },
            error: franwell.errorResponseHandler,
            useSpinner: true
        }, options || {});


        var usingSpinner = (options.useSpinner ? franwell.startSpinner() : false);
        franwell.$.ajax({
            contentType: options.contentType,
            data: options.data,
            dataType: options.dataType,
            url: options.url,
            type: options.method
        })
        .done(options.success)
        .fail(options.error)
        .always(function () {
            if (usingSpinner) {
                franwell.stopSpinner();
            }
        });
    };


    // =================================================================
    // submitJson : Helper for submitting JSON to the server
    // -----------------------------------------------------------------
    // Options: same options as submitData, except the following:
    //  - data: anything in "data" will be stringified (JSON.stringify)
    //  - dataType: set to "json"
    //  - contentType: set to "application/json"
    // =================================================================

    franwell.submitJson = franwell.submitJson || function (options) {
        options = franwell.$.extend(true, options || {}, {
            contentType: 'application/json',
            dataType: 'json'
        });
        if (options.data != null && typeof options.data !== 'string') {
            options.data = JSON.stringify(options.data);
        }

        franwell.submitData(options);
    };


    // ======================================================
    // $.franwellActivateForm : Sets up validation for a form
    // ======================================================

    franwell.$.fn.franwellActivateForm = franwell.$.fn.franwellActivateForm || function () {
        this.each(function () {
            var $this = franwell.$(this);
            franwell.validationEngine($this);
            $this.submit(function () {
                var validationResult = $this.validationEngine('validate');
                if (!validationResult) {
                    return false;
                }
                // this method causes a full page reload, so we don't worry about stopping the spinner.
                franwell.startSpinner();
                return true;
            });
        });
        return this;
    };


    // ====================================================================================
    // $.franwellActivateAjaxForm : Sets up validation and AJAX-style submission for a form
    // ------------------------------------------------------------------------------------
    // Options : Options hash passed to $.ajax
    // ====================================================================================

    franwell.$.fn.franwellActivateAjaxForm = franwell.$.fn.franwellActivateAjaxForm || function (options) {
        // Fail if nothing is selected.
        if (!this.length) {
            window.log('franwellActivateAjaxForm: No element selected.');
            return this;
        }

        var ajaxOptions = franwell.$.extend(true, {
            error: franwell.errorResponseHandler
        }, options);

        this.each(function () {
            var $this = franwell.$(this);
            franwell.validationEngine($this);
            $this.submit(function () {
                var validationResult = $this.validationEngine('validate');
                if (validationResult) {
                    $this.franwellAjaxSubmit(ajaxOptions);
                }
                return false;
            });
        });

        return this;
    };


    // ===================================================
    // $.franwellAjaxSubmit : Submits a form via AJAX
    // ---------------------------------------------------
    // Options: same options as submitJson, except the following:
    //  - url: if not specified, will be taken from the form's action attribute
    //  - method: if not specified, will be taken from the form's method attribute
    // ===================================================

    franwell.$.fn.franwellAjaxSubmit = franwell.$.fn.franwellAjaxSubmit || function (options) {
        // Fail if nothing is selected.
        if (!this.length) {
            window.log('franwellAjaxSubmit: No element selected.');
            return this;
        }

        var $form = this,

            // Grab the URL from the form element.
            action = $form.attr('action'),
            url = options.url || typeof action === 'string'
                ? franwell.$.trim(action)
                : window.location.href || '',
            method = options.method || $form.attr('method') || 'POST',

            // Serialize the form values
            data = $form.serializeObject().model,

            // Build the final options hash.
            ajaxOptions = franwell.$.extend(true, {
                url: url,
                type: method,
                data: data
            }, options);

        // Finally, submit the data.
        franwell.submitJson(ajaxOptions);
    };
})();