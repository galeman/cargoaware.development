﻿namespace CargoAware.Services.Common
{
    public enum EntityVoidedState : byte
    {
        All,
        NonVoidedOnly,
        VoidedOnly
    }
}