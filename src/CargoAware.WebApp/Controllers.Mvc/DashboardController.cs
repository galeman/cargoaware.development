﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("dashboard")]
    public partial class DashboardController : AppMvcController
    {
        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            return View();
        }

        // ==================================================
        #endregion
    }
}