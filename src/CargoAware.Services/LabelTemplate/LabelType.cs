﻿namespace CargoAware.Services.LabelTemplate
{
    public enum LabelType : byte
    {
        AirWaybill,
        ULD
    }
}