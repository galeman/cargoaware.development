﻿using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace CargoAware.Web.Infrastructure.Extensions
{
    public static class HttpExtensions
    {
        //public static string ToUrl(
        //    this ActionResult result,
        //    RouteCollection routeCollection,
        //    RequestContext requestContext)
        //{
        //    var callInfo = result.GetT4MVCResult();
        //    return UrlHelper.GenerateUrl(
        //        null,
        //        null,
        //        null,
        //        callInfo.RouteValueDictionary,
        //        routeCollection,
        //        requestContext,
        //        false);
        //}

        public static string ToUrl(this RouteValueDictionary routeValues)
        {
            var httpContext = new HttpContextWrapper(HttpContext.Current);
            var routeData = new RouteData();
            var requestContext = new RequestContext(httpContext, routeData);
            var result = UrlHelper.GenerateUrl(
                null,
                null,
                null,
                routeValues,
                RouteTable.Routes,
                requestContext,
                false);
            return result;
        }

        public static bool IsModal(this HttpContextBase context)
        {
            var isModal = context.Request.QueryString["isModal"];
            return isModal != null && isModal == "true";
        }
    }
}