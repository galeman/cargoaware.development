﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Infrastructure.Implementations
{
    public static class Generators
    {
        private static IEnumerable<char> RandomChars
        {
            get
            {
                while (true)
                {
                    yield return (char)Random.Next(0, 255);
                }
            }
        }

        internal static Func<string> RandomString(int count, Func<char, bool> predicate)
        {
            return () => new string(RandomChars.Where(predicate).Take(count).ToArray());
        }

        public static readonly Func<Guid> GuidComb = () =>
        {
            var guidArray = Guid.NewGuid().ToByteArray();

            var baseDate = new DateTime(1900, 1, 1);
            var now = DateTime.UtcNow;

            var days = new TimeSpan(now.Ticks - baseDate.Ticks);
            var msecs = now.TimeOfDay;

            var daysArray = BitConverter.GetBytes(days.Days);
            var msecsArray = BitConverter.GetBytes((long)(msecs.TotalMilliseconds / 3.333333));

            Array.Reverse(daysArray);
            Array.Reverse(msecsArray);

            Array.Copy(daysArray, daysArray.Length - 2, guidArray, guidArray.Length - 6, 2);
            Array.Copy(msecsArray, msecsArray.Length - 4, guidArray, guidArray.Length - 4, 4);

            return new Guid(guidArray);
        };

        private static readonly Random Random = new Random((int)DateTime.UtcNow.Ticks);
    }
}