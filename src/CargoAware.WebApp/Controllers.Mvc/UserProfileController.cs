﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.User;
using CargoAware.Web.Infrastructure.Filters;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("user/profile")]
    public partial class UserProfileController : AppMvcController
    {
        private static Lazy<UserRepository> _UserRepository;


        #region // Constructor + DI
        // ==================================================

        static UserProfileController()
        {
            Inject(Repositories.ForUsers);
        }

        private static void Inject(Lazy<UserRepository> dependency)
        {
            _UserRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            var user = _UserRepository.Value.GetAuthorizationDetailsById(User.Id);
            var model = new UserProfileModel
            {
                Username = user.Username,
                FullName = user.Name,
                Email = user.Email,
                SecurityQuestion = user.SecurityQuestion,
                SecurityAnswer = user.SecurityAnswer,

                MustSetPassword = User.LogInState.HasFlag(LogInState.MustSetPassword)
            };
            return View(model);
        }

        // ==================================================
        #endregion
    }
}