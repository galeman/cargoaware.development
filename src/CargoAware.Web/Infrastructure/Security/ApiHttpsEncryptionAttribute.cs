﻿using System;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace CargoAware.Web.Infrastructure.Security
{
    public sealed class ApiHttpsEncryptionAttribute : AuthorizeAttribute
    {
        protected override Boolean IsAuthorized(HttpActionContext actionContext)
        {
            // An API call is authorized if it doesn't have to be redirected to https
            var httpsUsage = Helper.ValidateHttpsUsage(actionContext.Request.RequestUri);
            return !httpsUsage.Redirect;
        }
    }
}