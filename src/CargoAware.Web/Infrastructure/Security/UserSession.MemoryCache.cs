using CargoAware.Services.Infrastructure;
using System;
using System.Web;

namespace CargoAware.Web.Infrastructure.Security
{
    public static partial class UserSession
    {
        private static readonly Func<long, String> GetUserPrincipalCacheName =
            userId => String.Format("UserPrincipal.{0}", userId);


        #region // Public Static Methods
        // ==================================================

        public static void RemoveFromCache()
        {
            var user = HttpContext.Current.User as UserPrincipal;
            if (user == null)
            {
                return;
            }

            RemoveUserPrincipalCache(user.Id);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        private static UserPrincipal GetUserPrincipalFromCache(long userId)
        {
            var result = MemoryCacheService.GetOrAdd(
                GetUserPrincipalCacheName(userId),
                () => UserPrincipal.FromAuthorizationDetails(_UserRepository.Value.GetAuthorizationDetailsById(userId)),
                TimeSpan.FromMinutes(5),
                true);
            return result;
        }

        private static void RemoveUserPrincipalCache(long userId)
        {
            MemoryCacheService.Remove(GetUserPrincipalCacheName(userId));
        }

        // ==================================================
        #endregion
    }
}