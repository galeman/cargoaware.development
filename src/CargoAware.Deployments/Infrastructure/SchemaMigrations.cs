using FluentMigrator.VersionTableInfo;
using System;

namespace CargoAware.Deployments.Infrastructure
{
    [VersionTableMetaData]
    public sealed class SchemaMigrations : IVersionTableMetaData
    {
        public string SchemaName
        {
            get { return String.Empty; }
        }

        public string TableName
        {
            get { return "SchemaMigrations"; }
        }

        public string ColumnName
        {
            get { return "Version"; }
        }

        public string DescriptionColumnName
        {
            get { return "Description"; }
        }

        public string UniqueIndexName
        {
            get { return "UC_SchemaMigrations"; }
        }

        public string AppliedOnColumnName
        {
            get { return "AppliedOn"; }
        }
    }
}