﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150608033132)]
    public class AddUldToPPrintJob_20150608033132 : AutoReversingMigration
    {
        public override void Up()
        {
            if (!Schema.Table("PrintJob").Column("Uld").Exists())
            {
                Alter.Table("PrintJob")
                     .AddColumn("Uld").AsAnsiString(255).NotNullable().SetExistingRowsTo("");
            }
            if (!Schema.Table("PrintJobData").Column("Uld").Exists())
            {
                Alter.Table("PrintJobData")
                     .AddColumn("Uld").AsAnsiString(255).NotNullable().SetExistingRowsTo("");
            }
        }
    }
}