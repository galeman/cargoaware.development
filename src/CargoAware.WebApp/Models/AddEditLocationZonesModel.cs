﻿using CargoAware.Services.Common;
using CargoAware.Services.LocationZone;
using System;
using System.Collections.Generic;

namespace CargoAware.WebApp.Models
{
    public class AddEditLocationZonesModel
    {
        public Boolean Adding { get; set; }
        public IEnumerable<LocationZoneDocument> LocationZones { get; set; }
        public IEnumerable<AddEditLocationZonesDetailsModel> Details { get; set; }
    }

    public class AddEditLocationZonesDetailsModel
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public String LmsZoneId { get; set; }
        public Boolean AutoUpdateLms { get; set; }
        public int LmsUpdateDelaySeconds { get; set; }
        public String EnvoyZoneId { get; set; }
        public IEnumerable<Point3D> Points { get; set; }
    }
}