﻿using CargoAware.Web.Infrastructure.Extensions;
using CargoAware.Web.Infrastructure.Security;
using System;
using System.Security.Principal;
using System.Web.Mvc;
using System.Web.Routing;

namespace CargoAware.Web.Infrastructure.Controllers
{
    [MvcHttpsEncryption]
    public abstract class BaseMvcController<TPrincipal> : Controller where TPrincipal : class, IPrincipal
    {
        protected new TPrincipal User
        {
            get { return base.User as TPrincipal; }
        }

        protected abstract RouteValueDictionary FrontPageRouteValues { get; }

        protected UnauthorizedAccessException UnauthorizedException()
        {
            if ((User.Identity.IsAuthenticated) && (FrontPageRouteValues != null))
            {
                Response.RedirectToRoute(FrontPageRouteValues);
                Response.End();
            }

            return new UnauthorizedAccessException();
        }

        protected ViewResult GetIFrameView(String title, String url)
        {
            var result = View("IFrame");
            var viewBag = result.ViewBag;

            viewBag.Title = title;
            viewBag.IFrameUrl = url;

            return result;
        }

        protected override void OnException(ExceptionContext filterContext)
        {
            if (filterContext.ExceptionHandled)
            {
                return;
            }

            //filterContext.RequestContext.HttpContext.Request.Url.AbsolutePath
            ExceptionHelper.LogControllerException(
                User,
                filterContext.RouteData.Values.ToUrl(),
                filterContext.Exception);

            //filterContext.ExceptionHandled = true;
            base.OnException(filterContext);
        }
    }
}