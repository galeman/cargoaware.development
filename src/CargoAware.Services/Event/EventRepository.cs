﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Linq;

namespace CargoAware.Services.Event
{
    public sealed class EventRepository : RepositoryBase
    {
        public EventRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public Paged<EventDocument> GetEvents(
            EventSource? eventSource = null, DateTimeOffset? rangeStart = null, DateTimeOffset? rangeEnd = null)
        {
            var result = QueryEvents(builder =>
            {
                if (eventSource.HasValue)
                {
                    builder.Where("e.EventSource = @eventSource", new { eventSource });
                }
                if (rangeStart.HasValue)
                {
                    builder.Where("e.Timestamp >= @rangeStart", new { rangeStart });
                }
                if (rangeEnd.HasValue)
                {
                    builder.Where("e.Timestamp <= @rangeEnd", new { rangeEnd });
                }
            });
            return result;
        }

        /// <summary>
        /// Requires: Timestamp, EventSource, EventName, Message
        /// </summary>
        public void Insert(EventDocument entity)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
INSERT INTO Event (
    Timestamp,
    EventSource,
    EventName,
    Message
) VALUES (
    @Timestamp,
    @EventSource,
    @EventName,
    @Message
);", entity);
            }
        }

        private Paged<EventDocument> QueryEvents(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        e.Timestamp,
        e.EventSource,
        e.EventName,
        e.Message,
        DENSE_RANK() OVER (ORDER BY e.Id ASC) AS FirstRow,
        DENSE_RANK() OVER (ORDER BY e.Id DESC) AS LastRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.Event e
    /**where**/
)
SELECT * FROM cte
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("e.Timestamp DESC");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<EventDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<EventDocument>(data.Distinct(), total);
            }
        }
    }
}