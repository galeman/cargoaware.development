﻿using System.Web.Mvc;
using CargoAware.Web.Infrastructure.Security;

namespace CargoAware.Web.Infrastructure
{
    public abstract class ApplicationViewBase : WebViewPage
    {
        protected new UserPrincipal User
        {
            get { return UserPrincipal.FromPrincipal(base.User); }
        }
    }

    public abstract class ApplicationViewBase<TModel> : WebViewPage<TModel>
    {
        protected new UserPrincipal User
        {
            get { return UserPrincipal.FromPrincipal(base.User); }
        }
    }
}