﻿using CommandLine;
using CommandLine.Text;
using System.Text;

namespace CargoAware.JobService.Infrastructure
{
    public sealed class Options
    {
        [Option('a', "action", HelpText = "The action to perform.", Required = true)]
        public string ActionToPerform { get; set; }

        [Option('f', "file", HelpText = "Input file to process.", Required = false)]
        public string InputFile { get; set; }

        [Option('p', "parameter", HelpText = "Parameter for action.", Required = false)]
        public string Parameter { get; set; }

        [Option('v', "verbose", HelpText = "Print details during execution.")]
        public bool Verbose { get; set; }

        [HelpOption]
        public string GetUsage()
        {
            var sb = new StringBuilder();

            sb.AppendLine(HelpText.AutoBuild(this));
            sb.AppendLine("Valid actions:");

            foreach (var kvp in JobService.GetJobUsageExamples())
            {
                sb.AppendLine(kvp.Key + ":");
                sb.AppendLine(kvp.Value);
                sb.AppendLine();
            }

            return sb.ToString();
        }
    }
}