﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20140411160400)]
    public sealed class LogInAttempts : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("LogInAttempt")
                  .WithColumn("Username").AsAnsiString(255).Indexed().NotNullable()
                  .WithColumn("Host").AsAnsiString(45).NotNullable()
                  .WithColumn("Reason").AsAnsiString(100).NotNullable()
                  .WithColumn("AttemptDateTime").AsDateTime().NotNullable()
                  .WithColumn("Succeeded").AsBoolean().NotNullable();
        }
    }
}