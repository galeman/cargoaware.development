﻿using System;

namespace CargoAware.Services.Infrastructure.Implementations
{
    public static class Extensions
    {
        internal static Func<T, bool> Except<T>(this Func<T, bool> predicate, Func<T, bool> exception)
        {
            return arg => predicate(arg) && !exception(arg);
        }

        internal static Func<T, bool> Or<T>(this Func<T, bool> predicate, Func<T, bool> alternative)
        {
            return arg => predicate(arg) || alternative(arg);
        }
    }
}