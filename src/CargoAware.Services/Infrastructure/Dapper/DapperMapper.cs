﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Infrastructure.Dapper
{
    public static class DapperMapper
    {
        public static Func<TParent, TChild, TParent> CreateMap<TParent, TChild>()
            where TParent : class, IAssociate<TChild>
        {
            var cache = new List<TParent>();
            return (parent, child) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                existing.Associate(child);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, TParent> CreateMap<TParent, TChild1, TChild2>()
            where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                existing.Associate(child1);
                existing.Associate(child2);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, TChild3, TParent> CreateMap<TParent, TChild1, TChild2, TChild3>()
            where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>, IAssociate<TChild3>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2, child3) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                existing.Associate(child1);
                existing.Associate(child2);
                existing.Associate(child3);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, TChild3, TChild4, TParent> CreateMap
            <TParent, TChild1, TChild2, TChild3, TChild4>()
            where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>, IAssociate<TChild3>, IAssociate<TChild4>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2, child3, child4) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                existing.Associate(child1);
                existing.Associate(child2);
                existing.Associate(child3);
                existing.Associate(child4);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, TChild3, TChild4, TChild5, TParent> CreateMap
            <TParent, TChild1, TChild2, TChild3, TChild4, TChild5>()
            where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>, IAssociate<TChild3>, IAssociate<TChild4>,
                IAssociate<TChild5>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2, child3, child4, child5) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                existing.Associate(child1);
                existing.Associate(child2);
                existing.Associate(child3);
                existing.Associate(child4);
                existing.Associate(child5);

                return existing;
            };
        }

        public static Func<TRecord, long, long, TRecord> CreatePagedMap<TRecord>(Action<long> setTotal)
        {
            return (record, firstRow, lastRow) =>
            {
                setTotal(firstRow + lastRow - 1);
                return record;
            };
        }

        public static Func<TParent, TChild, long, long, TParent> CreatePagedMap<TParent, TChild>(Action<long> setTotal)
            where TParent : class, IAssociate<TChild>
        {
            var cache = new List<TParent>();
            return (parent, child, firstRow, lastRow) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                setTotal(firstRow + lastRow - 1);
                existing.Associate(child);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, long, long, TParent> CreatePagedMap<TParent, TChild1, TChild2>(
            Action<long> setTotal) where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2, firstRow, lastRow) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                setTotal(firstRow + lastRow - 1);
                existing.Associate(child1);
                existing.Associate(child2);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, TChild3, long, long, TParent> CreatePagedMap
            <TParent, TChild1, TChild2, TChild3>(Action<long> setTotal)
            where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>, IAssociate<TChild3>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2, child3, firstRow, lastRow) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                setTotal(firstRow + lastRow - 1);
                existing.Associate(child1);
                existing.Associate(child2);
                existing.Associate(child3);

                return existing;
            };
        }

        public static Func<TParent, TChild1, TChild2, TChild3, TChild4, long, long, TParent> CreatePagedMap
            <TParent, TChild1, TChild2, TChild3, TChild4>(Action<long> setTotal)
            where TParent : class, IAssociate<TChild1>, IAssociate<TChild2>, IAssociate<TChild3>, IAssociate<TChild4>
        {
            var cache = new List<TParent>();
            return (parent, child1, child2, child3, child4, firstRow, lastRow) =>
            {
                var existing = cache.FirstOrDefault(x => x.Equals(parent));
                if (existing == null)
                {
                    existing = parent;
                    cache.Add(parent);
                }

                setTotal(firstRow + lastRow - 1);
                existing.Associate(child1);
                existing.Associate(child2);
                existing.Associate(child3);
                existing.Associate(child4);

                return existing;
            };
        }
    }
}