﻿using CargoAware.Services.Common;
using Microsoft.SqlServer.Types;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.LocationZone
{
    public sealed class LocationZoneDocument
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public String Name { get; set; }
        public String LmsZoneId { get; set; }
        public Boolean AutoUpdateLms { get; set; }
        public int LmsUpdateDelaySeconds { get; set; }
        public String EnvoyZoneId { get; set; }
        public IEnumerable<Point3D> Points
        {
            get
            {
                if (SpatialData == null)
                {
                    yield break;
                }

                for (var i = 1; i < SpatialData.STNumPoints(); i++)
                {
                    var spatialPoint = SpatialData.STPointN(i);
                    yield return new Point3D
                    {
                        X = (Decimal)spatialPoint.STX.Value,
                        Y = (Decimal)spatialPoint.STY.Value,
                        Z = (Decimal)(spatialPoint.HasZ ? spatialPoint.Z.Value : 0)
                    };
                }
            }
        }
        public String PointsString
        {
            get
            {
                var result = "(" + String.Join(") (", Points.Select(x => x.X + ", " + x.Y)) + ")";
                return result;
            }
        }
        public DateTimeOffset? ArchivedDateTimeUtc { get; set; }

        internal long ZoneId { set { Id = value; } }
        internal SqlGeometry SpatialData { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as LocationZoneDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}