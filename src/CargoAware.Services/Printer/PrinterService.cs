﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using System;

namespace CargoAware.Services.Printer
{
    public static class PrinterService
    {
        private static Lazy<PrinterRepository> _PrinterRepository;


        static PrinterService()
        {
            Inject(Repositories.ForPrinters);
        }

        private static void Inject(Lazy<PrinterRepository> dependency)
        {
            _PrinterRepository = dependency;
        }


        /// <param name="message">Required: LocationId, Name, Type, ExternalId, Address, Port</param>
        public static PrinterDocument CreatePrinter(PrinterDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            message.Id = 0;

            // Step 3: Does this command pass business rules?

            var sameNamePrinterExists = _PrinterRepository.Value.DoesPrinterWithSameNameExist(message.LocationId, message.Name);
            if (sameNamePrinterExists)
            {
                throw new DomainException("The Printer \"{0}\" already exists.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            long printerId;
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrinterRepository.Value.InsertOrUpdate(message);
                printerId = _PrinterRepository.Value.GetPrinterIdByName(message.Name);
                scope.Complete();
            }

            var result = _PrinterRepository.Value.GetPrinterById(printerId);
            return result;
        }

        /// <param name="message">Required: Id, Name, Type, ExternalId, Address, Port</param>
        public static void UpdatePrinter(PrinterDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var existingPrinter = _PrinterRepository.Value.GetPrinterById(message.Id);
            if (existingPrinter == null)
            {
                throw new DomainException("The specified Printer, \"{0}\", does not exist.", message.Name);
            }

            var sameNamePrinterExists = _PrinterRepository.Value.DoesPrinterWithSameNameExist(message.LocationId, message.Name, message.Id);
            if (sameNamePrinterExists)
            {
                throw new DomainException("A Printer named \"{0}\" already exists.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrinterRepository.Value.InsertOrUpdate(message);
                scope.Complete();
            }
        }

        public static void UpdatePrinterStatus(long printerId, String status)
        {
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrinterRepository.Value.UpdatePrinterStatus(printerId, status);
                scope.Complete();
            }
        }

        /// <param name="message">Required: Id</param>
        public static void ArchivePrinter(PrinterDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var doesPrinterExist = _PrinterRepository.Value.GetPrinterById(message.Id);
            if (doesPrinterExist == null)
            {
                throw new DomainException("The specified Printer does not exist.");
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrinterRepository.Value.Archive(message.Id);
                scope.Complete();
            }
        }
    }
}