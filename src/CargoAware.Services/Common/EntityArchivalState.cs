﻿namespace CargoAware.Services.Common
{
    public enum EntityArchivalState : byte
    {
        All,
        ActiveOnly,
        ArchivedOnly
    }
}