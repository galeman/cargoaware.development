﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Linq;

namespace CargoAware.Services.RegionManager
{
    public sealed class RegionManagerRepository : RepositoryBase
    {
        public RegionManagerRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public bool DoesRegionWithSameRegionIdExist(long locationId, Guid regionId, long? excludedId = null)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var sql = "SELECT TOP (1) 1 FROM RegionManagerRegion WHERE LocationId = @locationId AND RegionId = @regionId";

                if (excludedId.HasValue)
                {
                    sql = sql + " AND Id <> @excludedId";
                }

                return db.QueryBoolean(sql, new { locationId, regionId, excludedId });
            }
        }

        public RegionManagerRegionDocument GetRegionById(long id)
        {
            var result = QueryRegions(builder => builder.Where("rmr.Id = @id", new { id })).Data;
            return result.FirstOrDefault();
        }

        public long GetIdByRegionId(Guid regionId)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM RegionManagerRegion
/**where**/;");

                builder.Where("RegionId = @regionId", new { regionId });

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        public Paged<RegionManagerRegionDocument> GetRegions(long locationId, Action<SqlBuilder> query = null)
        {
            var result = QueryRegions(builder =>
            {
                builder.Where("rmr.LocationId = @locationId", new { locationId });

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        internal void InsertOrUpdate(RegionManagerRegionDocument entity)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE RegionManagerRegion
SET Name = @Name,
    RegionId = @RegionId
WHERE Id = @Id;
IF @@ERROR = 0 AND @@ROWCOUNT = 0
    INSERT INTO RegionManagerRegion (
        LocationId,
        Name,
        RegionId
    ) VALUES (
        @LocationId,
        @Name,
        @RegionId
    );", entity);

                scope.Complete();
            }
        }

        internal void Archive(long id)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
DELETE FROM RegionManagerRegion
WHERE Id = @id;", new { id });

                scope.Complete();
            }
        }

        private Paged<RegionManagerRegionDocument> QueryRegions(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        rmr.Id,
        rmr.LocationId,
        rmr.Name,
        rmr.RegionId,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwRegionManagerRegion rmr
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("rmr.Name, rmr.RegionId, rmr.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<RegionManagerRegionDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<RegionManagerRegionDocument>(data.Distinct(), total);
            }
        }
    }
}