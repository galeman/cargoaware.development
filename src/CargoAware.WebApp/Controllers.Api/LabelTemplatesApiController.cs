﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.LabelTemplate;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/labeltemplates")]
    public class LabelTemplatesApiController : AppApiController
    {
        private static Lazy<LabelTemplateRepository> _LabelTemplateRepository;


        #region // Constructor + DI
        // ==================================================

        static LabelTemplatesApiController()
        {
            Inject(Repositories.ForLabelTemplates);
        }

        private static void Inject(Lazy<LabelTemplateRepository> dependency)
        {
            _LabelTemplateRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<LabelTemplateDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.AdminLabelTemplates()))
            {
                throw UnauthorizedException();
            }

            var result = _LabelTemplateRepository.Value.GetLabelTemplates(User.CurrentLocation.Id, request.GetApplyMethod());
            return result;
        }

        [POST("create"), HttpPost]
        public HttpResponseMessage Create(IEnumerable<AddEditLabelTemplatesDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminLabelTemplates()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (String.IsNullOrWhiteSpace(detail.Name) ||
                    !detail.Type.HasValue ||
                    !detail.Language.HasValue ||
                    String.IsNullOrWhiteSpace(detail.Code))
                {
                    continue;
                }

                LabelTemplateService.CreateLabelTemplate(new LabelTemplateDocument
                {
                    LocationId = locationId,
                    Name = detail.Name,
                    Type = detail.Type.Value,
                    Language = detail.Language.Value,
                    Code = detail.Code,
                    IsActive = detail.IsActive
                });
            }

            return SuccessResponse(true);
        }

        [POST("update"), HttpPost]
        public HttpResponseMessage Update(IEnumerable<AddEditLabelTemplatesDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminLabelTemplates()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (detail.Id == 0 ||
                    String.IsNullOrWhiteSpace(detail.Name) ||
                    !detail.Type.HasValue ||
                    !detail.Language.HasValue ||
                    String.IsNullOrWhiteSpace(detail.Code))
                {
                    continue;
                }

                LabelTemplateService.UpdateLabelTemplate(new LabelTemplateDocument
                {
                    Id = detail.Id,
                    LocationId = locationId,
                    Name = detail.Name,
                    Type = detail.Type.Value,
                    Language = detail.Language.Value,
                    Code = detail.Code,
                    IsActive = detail.IsActive
                });
            }

            return SuccessResponse(true);
        }

        [POST("archive"), HttpPost]
        public HttpResponseMessage Archive([FromBody] long id)
        {
            if (!User.HasPermission(Permissions.User.AdminLabelTemplates()))
            {
                throw UnauthorizedException();
            }

            LabelTemplateService.ArchiveLabelTemplate(new LabelTemplateDocument
            {
                Id = id
            });

            return SuccessResponse(true);
        }

        // ==================================================
        #endregion
    }
}