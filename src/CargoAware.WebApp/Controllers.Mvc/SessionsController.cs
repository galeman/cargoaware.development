﻿using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.User;
using CargoAware.Web.Infrastructure.Filters;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Infrastructure.Menus;
using CargoAware.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    public partial class SessionsController : AppMvcController
    {
        private const String IncorrectSecurityAnswer = "The security answer provided is incorrect. Please try again.";


        private static Lazy<UserRepository> _UserRepository;


        #region // Constructor + DI
        // ==================================================

        static SessionsController()
        {
            Inject(Repositories.ForUsers);
        }

        private static void Inject(Lazy<UserRepository> dependency)
        {
            _UserRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult FrontPage()
        {
            var pageMenu = PageMenus.NavigationMenu(User);
            var url = GetHomePage(pageMenu);
            return Redirect(url);
        }

        [AllowAnonymous]
        [GET("log-in")]
        public virtual ActionResult LogInWithPassword()
        {
            return View(new LogInModel());
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [POST("log-in")]
        public virtual ActionResult LogInWithPassword(LogInModel model)
        {
            if (Request.IsAuthenticated)
            {
                LogOut();
            }

            var result = UserSession.Authenticate(
                //command => Execute(command),
                ValidationType.Password,
                model.LogIn,
                model.Password);
            if (!result.Successful)
            {
                ModelState.AddModelError("authenticate", result.ErrorMessage);
                return View(model);
            }

            LogInState logInState = 0;

            var userAuth = result.AuthorizationDetails;
            if (userAuth.IsPasswordExpired())
            {
                logInState |= LogInState.MustSetPassword;
            }

            UserSession.Update(userAuth, logInState);
            return RedirectToAction(MVC.Sessions.FrontPage());
        }

        [AllowAnonymous]
        [GET("log-out")]
        public virtual ActionResult LogOut()
        {
            UserSession.LogOut();
            return RedirectToAction(MVC.Sessions.FrontPage());
        }

        [AllowAnonymous]
        [GET("log-in/first-time")]
        public virtual ActionResult FirstTimeUsers()
        {
            return View();
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [POST("log-in/first-time")]
        public virtual ActionResult FirstTimeUsers(String key)
        {
            if (Request.IsAuthenticated)
            {
                LogOut();
            }

            var credentialResult = UserSession.Authenticate(ValidationType.Key, key: key);
            if (!credentialResult.Successful)
            {
                ModelState.AddModelError("authenticate", credentialResult.ErrorMessage);
                return View(model: key);
            }

            var logInState = LogInState.MustSetPassword;

            var userAuth = credentialResult.AuthorizationDetails;

            UserSession.Update(userAuth, logInState);
            return RedirectToAction(MVC.Sessions.FrontPage());
        }

        [AllowAnonymous]
        [GET("log-in/key/{key}")]
        public virtual ActionResult LogInWithKey(String key)
        {
            if (Request.IsAuthenticated)
            {
                LogOut();
            }

            var credentialResult = UserSession.Authenticate(ValidationType.Key, key: key);
            if (!credentialResult.Successful)
            {
                ModelState.AddModelError("authenticate", credentialResult.ErrorMessage);
                return View(viewName: "FirstTimeUsers", model: key);
            }

            var logInState = LogInState.MustSetPassword;

            var userAuth = credentialResult.AuthorizationDetails;

            UserSession.Update(userAuth, logInState);
            return RedirectToAction(MVC.Sessions.FrontPage());
        }

        [AllowAnonymous]
        [GET("password/reset")]
        public virtual ActionResult ResetPassword()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction(MVC.Sessions.FrontPage());
            }

            return View();
        }

        [AllowAnonymous]
        [POST("password/reset")]
        public virtual ActionResult ResetPassword(String username)
        {
            var credentialResult = UserSession.ValidateCredentials(ValidationType.Username, username);

            if (!credentialResult.Successful)
            {
                ModelState.AddModelError("username", credentialResult.ErrorMessage);
                return View();
            }

            return RedirectToAction(MVC.Sessions.ResetPasswordQuestion(username));
        }

        [AllowAnonymous]
        [GET("password/reset/question/{username}")]
        public virtual ActionResult ResetPasswordQuestion(String username)
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction(MVC.Sessions.FrontPage());
            }

            var credentialResult = UserSession.ValidateCredentials(ValidationType.Username, username);
            var userId = credentialResult.AuthorizationDetails.Id;

            if (!credentialResult.Successful)
            {
                ModelState.AddModelError("username", credentialResult.ErrorMessage);
                return View();
            }

            var question = _UserRepository.Value.GetSecurityQuestion(userId);
            return View(model: question);
        }

        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [POST("password/reset/question/{username}")]
        public virtual ActionResult ResetPasswordQuestion(String username, String answer)
        {
            var credentialResult = UserSession.ValidateCredentials(ValidationType.Username, username);
            var userId = credentialResult.AuthorizationDetails.Id;

            if (!credentialResult.Successful)
            {
                ModelState.AddModelError("username", credentialResult.ErrorMessage);
                return View();
            }

            if (_UserRepository.Value.CheckSecurityAnswer(userId, answer))
            {
                // TODO : Bring functionality over from Metrc
                //Execute(new ResetPassword(userId, userId));
                return RedirectToAction(MVC.Sessions.ResetPasswordSuccess());
            }

            ModelState.AddModelError("answer", IncorrectSecurityAnswer);

            var question = _UserRepository.Value.GetSecurityQuestion(userId);
            return View(model: question);
        }

        [AllowAnonymous]
        [GET("password/reset/success")]
        public virtual ActionResult ResetPasswordSuccess()
        {
            return View();
        }

        [GET("session/change-location/{id}")]
        public virtual ActionResult ChangeLocation(long id)
        {
            var location = User.Locations.FirstOrDefault(x => x.Id == id);
            if (location == null)
            {
                throw UnauthorizedException();
            }

            UserSession.Update(location);

            if (Request.Url != null &&
                Request.UrlReferrer != null &&
                String.Equals(Request.Url.Authority, Request.UrlReferrer.Authority, StringComparison.OrdinalIgnoreCase))
            {
                return Redirect(Request.UrlReferrer.ToString());
            }
            return RedirectToAction(MVC.Sessions.FrontPage());
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private static String GetHomePage(IEnumerable<IMenuNode> menus)
        {
            return GetHomePageRecursive(menus);
        }

        private static String GetHomePageRecursive(IEnumerable<IMenuNode> menus)
        {
            foreach (var node in menus)
            {
                DropdownMenu menu;
                MenuLink link;

                if ((menu = node as DropdownMenu) != null)
                {
                    if (!menu.Visible)
                    {
                        continue;
                    }

                    var subLink = GetHomePageRecursive(menu);
                    return subLink;
                }
                else if ((link = node as MenuLink) != null)
                {
                    if (!link.Visible)
                    {
                        continue;
                    }

                    return link.Url;
                }
            }

            return null;
        }

        // ==================================================
        #endregion
    }
}