﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.DataProcessing;
using CargoAware.Services.Shipment;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/shipments")]
    public class ShipmentsApiController : AppApiController
    {
        private static Lazy<ShipmentRepository> _ShipmentRepository;


        #region // Constructor + DI
        // ==================================================

        static ShipmentsApiController()
        {
            Inject(Repositories.ForShipments);
        }

        private static void Inject(Lazy<ShipmentRepository> dependency)
        {
            _ShipmentRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<ShipmentDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.ViewShipments()) &&
                !User.HasPermission(Permissions.User.ManageShipments()))
            {
                throw UnauthorizedException();
            }

            var result = _ShipmentRepository.Value.GetShipments(
                User.HasPermission(Permissions.User.ManageShipments())
                    ? EntityVisibleState.All
                    : EntityVisibleState.VisibleOnly,
                request.GetApplyMethod());
            return result;
        }

        [POST("tagreads/awb"), HttpPost]
        public Paged<TagReadDocument> GetPagedTagReads(long id, KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.ViewShipments()) &&
                !User.HasPermission(Permissions.User.ManageShipments()))
            {
                throw UnauthorizedException();
            }

            var result = _ShipmentRepository.Value.GetTagReads(id, request.GetApplyMethod());
            return result;
        }

        [POST("tagreads/awb/grouped"), HttpPost]
        public HttpResponseMessage GetTagReads(IEnumerable<long> ids, int playbackTrail = 10, SmoothingFormula smoothing = SmoothingFormula.None)
        {
            if (!User.HasPermission(Permissions.User.ViewShipments()) &&
                !User.HasPermission(Permissions.User.ManageShipments()))
            {
                throw UnauthorizedException();
            }

            playbackTrail = Math.Abs(playbackTrail); // Ensure it's a positive number

            var shipments = ids.Select(x => new
            {
                ShipmentId = x,
                TagReadsByLocation = _ShipmentRepository.Value.GetTagReads(x).Data.GroupBy(y => y.LocationId)
            }).ToArray();

            var locationsShipments = new Dictionary<long, IDictionary<long, IEnumerable<TagReadDto>>>();
            var locationsTimestamps = new Dictionary<long, SortedDictionary<long, IDictionary<long, IList<TagReadDto>>>>();
            var locationsTimestampRange = new Dictionary<long, TimestampRange>();

            var epoch = new DateTime(1970, 1, 1);
            Func<DateTimeOffset, long> getSecondsSinceEpoch = timestamp =>
            {
                var secondsSinceEpoch = (long)Math.Round(timestamp.Subtract(epoch).TotalSeconds, MidpointRounding.AwayFromZero);
                return secondsSinceEpoch;
            };

            foreach (var shipment in shipments)
            {
                var shipmentId = shipment.ShipmentId;

                foreach (var tagReads in shipment.TagReadsByLocation)
                {
                    var locationId = tagReads.Key;
                    TagReadDocument[] sortedTagReads;

                    switch (smoothing)
                    {
                        case SmoothingFormula.SimpleMovingAverage:
                            sortedTagReads = SimpleMovingAverage.Filter(tagReads).ToArray();
                            break;
                        case SmoothingFormula.ExponentialMovingAverage:
                            sortedTagReads = ExponentialMovingAverage.Filter(tagReads).ToArray();
                            break;
                        case SmoothingFormula.Median:
                            sortedTagReads = Median.Filter(tagReads).ToArray();
                            break;
                        case SmoothingFormula.TakacsTimeBased:
                            var tags = _ShipmentRepository.Value.GetTagReadsProcessed(locationId).Data.OrderBy(y => y.LocationId);
                            sortedTagReads = tags.ToArray();
                            break;
                        case SmoothingFormula.TakacsTimeBasedWithAverage:
                            sortedTagReads = TakacsTimeBased.Filter(tagReads, applyAverage: true).ToArray();
                            break;
                        case SmoothingFormula.TakacsPointsBased:
                            sortedTagReads = TakacsPointsBased.Filter(tagReads).ToArray();
                            break;
                        case SmoothingFormula.TakacsPointsBasedWithAverage:
                            sortedTagReads = TakacsPointsBased.Filter(tagReads, applyAverage: true).ToArray();
                            break;
                        default:
                            sortedTagReads = tagReads.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
                            break;
                    }
                    sortedTagReads = TimeCompressor.Filter(sortedTagReads).ToArray();

                    // shipments by location

                    // [locationId] <-- we are here
                    // -- [shipmentId]
                    // -- -- [tagRead]
                    IDictionary<long, IEnumerable<TagReadDto>> locationShipments;
                    if (!locationsShipments.TryGetValue(locationId, out locationShipments))
                    {
                        locationShipments = new Dictionary<long, IEnumerable<TagReadDto>>();
                        locationsShipments[locationId] = locationShipments;
                    }

                    // [locationId]
                    // -- [shipmentId] <-- we are here
                    // -- -- [tagRead] <-- adding these
                    Debug.Assert(!locationShipments.ContainsKey(shipmentId), "locationShipments.ContainsKey(shipment.ShipmentId)");
                    locationShipments[shipmentId] = sortedTagReads.Select(x => new TagReadDto { X = x.X, Y = x.Y, Z = x.Z });

                    // shipments by location and timestamp

                    // [locationId] <-- we are here
                    // -- [timestamp(seconds)]
                    // -- -- [shipmentId]
                    // -- -- -- [tagRead]
                    SortedDictionary<long, IDictionary<long, IList<TagReadDto>>> locationTimestamps;
                    if (!locationsTimestamps.TryGetValue(locationId, out locationTimestamps))
                    {
                        locationTimestamps = new SortedDictionary<long, IDictionary<long, IList<TagReadDto>>>();
                        locationsTimestamps[locationId] = locationTimestamps;
                    }

                    for (var i = 0; i < sortedTagReads.Length; i++)
                    {
                        var tagRead = sortedTagReads[i];

                        // [locationId]
                        // -- [timestamp(seconds)] <-- we are here
                        // -- -- [shipmentId]
                        // -- -- -- [tagRead]
                        IDictionary<long, IList<TagReadDto>> timestampShipments;
                        var epochTimestamp1 = getSecondsSinceEpoch(tagRead.Timestamp) / 60;
                        if (!locationTimestamps.TryGetValue(epochTimestamp1, out timestampShipments))
                        {
                            timestampShipments = new Dictionary<long, IList<TagReadDto>>();
                            locationTimestamps[epochTimestamp1] = timestampShipments;
                        }

                        // [locationId]
                        // -- [timestamp(seconds)]
                        // -- -- [shipmentId] <-- we are here
                        // -- -- -- [tagRead]
                        IList<TagReadDto> shipmentTagReads;
                        if (!timestampShipments.TryGetValue(shipmentId, out shipmentTagReads))
                        {
                            shipmentTagReads = new List<TagReadDto>();
                            timestampShipments[shipmentId] = shipmentTagReads;
                        }

                        // [locationId]
                        // -- [timestamp(seconds)]
                        // -- -- [shipmentId] <-- we are here
                        // -- -- -- [tagRead] <-- adding these
                        ((List<TagReadDto>)shipmentTagReads).AddRange(sortedTagReads
                            .Skip(Math.Max(i - playbackTrail, 0))
                            .Take(playbackTrail)
                            .Select(x => new TagReadDto { X = x.X, Y = x.Y, Z = x.Z }));
                    }

                    // timestamps min/max

                    TimestampRange timestampRange;
                    if (!locationsTimestampRange.TryGetValue(locationId, out timestampRange))
                    {
                        timestampRange = new TimestampRange();
                        locationsTimestampRange[locationId] = timestampRange;
                    }

                    var epochTimestamp2 = getSecondsSinceEpoch(sortedTagReads.First().Timestamp) / 60;
                    if (epochTimestamp2 < timestampRange.Minimum)
                    {
                        timestampRange.Minimum = epochTimestamp2;
                    }

                    epochTimestamp2 = getSecondsSinceEpoch(sortedTagReads.Last().Timestamp) / 60;
                    if (epochTimestamp2 > timestampRange.Maximum)
                    {
                        timestampRange.Maximum = epochTimestamp2;
                    }
                }
            }


            // fill the missing timestamps (seconds)

            // [locationId] <-- we are here
            // -- [timestamp(seconds)]
            // -- -- [shipmentId]
            // -- -- -- [tagRead]
            foreach (var locationTimestamps in locationsTimestamps)
            {
                var timestampRange = locationsTimestampRange[locationTimestamps.Key];
                var lastKnownLocations = new Dictionary<long, IList<TagReadDto>>();

                for (var i = timestampRange.Minimum; i <= timestampRange.Maximum; i++)
                {
                    IDictionary<long, IList<TagReadDto>> timestampShipments;
                    // [locationId]
                    // -- [timestamp(seconds)] <-- we are here
                    // -- -- [shipmentId]
                    // -- -- -- [tagRead]
                    if (locationTimestamps.Value.TryGetValue(i, out timestampShipments))
                    {
                        foreach (var lastKnownLocation in lastKnownLocations.Where(x => !timestampShipments.ContainsKey(x.Key)))
                        {
                            timestampShipments[lastKnownLocation.Key] = lastKnownLocation.Value.ToArray();
                        }
                        foreach (var shipmentTagReads in timestampShipments)
                        {
                            lastKnownLocations[shipmentTagReads.Key] = shipmentTagReads.Value
                                .Skip(Math.Max(shipmentTagReads.Value.Count - playbackTrail, 0))
                                .Take(playbackTrail)
                                .ToArray();
                        }
                    }
                    else
                    {
                        locationTimestamps.Value[i] = lastKnownLocations.ToDictionary(x => x.Key, x => x.Value);
                    }
                }
            }

            var result = new
            {
                ByLocation = locationsShipments,
                ByLocationAndTimestamp = locationsTimestamps,
                //TimestampRange = locationsTimestampRange
            };
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        // ==================================================
        #endregion


        #region // Private Classes
        // ==================================================

        private class TagReadDto
        {
            public Decimal X { get; set; }
            public Decimal Y { get; set; }
            public Decimal Z { get; set; }
        }

        private class TimestampRange
        {
            public TimestampRange()
            {
                Minimum = long.MaxValue;
                Maximum = long.MinValue;
            }

            public long Minimum { get; set; }
            public long Maximum { get; set; }
        }

        // ==================================================
        #endregion
    }
}