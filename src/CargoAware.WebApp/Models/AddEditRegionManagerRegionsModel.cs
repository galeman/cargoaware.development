﻿using CargoAware.Services.RegionManager;
using System;
using System.Collections.Generic;

namespace CargoAware.WebApp.Models
{
    public class AddEditRegionManagerRegionsModel
    {
        public Boolean Adding { get; set; }
        public IEnumerable<RegionManagerRegionDocument> Regions { get; set; }
        public IEnumerable<AddEditRegionManagerRegionsDetailsModel> Details { get; set; }
    }

    public class AddEditRegionManagerRegionsDetailsModel
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public Guid RegionId { get; set; }
    }
}