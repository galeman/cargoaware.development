﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(2015070392600)]
    public sealed class vwTagReadProcessed : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwTagReadProcessed') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwTagReadProcessed AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwTagReadProcessed 
                AS
                   SELECT
                        tr.Id,
                        tr.Epc,
                        tr.LocationId,
                        tr.RegionManagerId,
                        tr.X,
                        tr.Y,
                        tr.Z,
                        tr.Rssi,
                        tr.Readers,
                        tr.[Timestamp],
                        tr.LocationZoneId,
                        lz.Name AS LocationZoneName,
                        tr.FixedLocationRead,
                        s.Id AS ShipmentId
                        --COALESCE(s.TagReadSegmentStart, DATEADD(day, -1, MAX(tr.[Timestamp]) OVER (PARTITION BY tr.Epc))) AS CalculatedTagReadSegmentStart
                    FROM dbo.TagRead tr
                        INNER JOIN dbo.Shipment s ON tr.Epc = s.AirWaybillTagEpc OR tr.Epc = s.UldTagEpc
                        LEFT JOIN dbo.LocationZone lz ON tr.LocationZoneId = lz.Id;");
        }
    }
}
