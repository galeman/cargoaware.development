﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630150343)]
    public sealed class vwRegionManagerRegion : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwRegionManagerRegion') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwRegionManagerRegion AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwRegionManagerRegion 
                AS
                   SELECT
                        rmr.Id,
                        rmr.LocationId,
                        rmr.Name,
                        rmr.RegionId
                    FROM dbo.RegionManagerRegion rmr;");
        }
    }
}
