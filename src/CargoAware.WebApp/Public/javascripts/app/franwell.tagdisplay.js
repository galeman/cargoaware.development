﻿// franwell.bootstrap.js
// (c) 2012-2014 Franwell, Inc.

// requires franwell.format.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};


    franwell.getTagLocation = franwell.getTagLocation || function (config, point, adjustToCanvas) {
        if (adjustToCanvas == null) adjustToCanvas = true;

        var result = {
            X: config.swapCoordinates && adjustToCanvas ? point.Y : point.X,
            Y: config.swapCoordinates && adjustToCanvas ? point.X : point.Y,
            Z: point.Z,
            Timestamp: point.Timestamp
        };
        if (adjustToCanvas) {
            result.X = (result.X + config.xAdjustment) * config.scale;
            result.Y = ((result.Y + config.yAdjustment) * config.scale) + 11; // +11 because of image
            if (config.invertX) {
                result.X = config.width - result.X;
            }
            if (config.invertY) {
                result.Y = config.height - result.Y;
            }
        }

        return result;
    };


    franwell.displayTagData = franwell.displayTagData || function (config, data, $awbCanvas, $summary, $firstRead, $lastRead) {
        if (!data || data.length === 0) {
            return;
        }

        var i, icnt, x, xcnt,
            canvasContext = $awbCanvas.get(0).getContext('2d'),
            containedInArea = function (point, area) {
                return point.X >= area.X1 && point.X <= area.X2 &&
                    point.Y >= area.Y1 && point.Y <= area.Y2;
            },
            firstBreakdown,
            firstStorage,
            loc,
            rawLoc,
            $descFields;

        canvasContext.drawImage(config.backgroundImage, 0, 0);
        canvasContext.beginPath();

        for (i = 0, icnt = data.length; i < icnt; i++) {
            loc = franwell.getTagLocation(config, data[i]);
            rawLoc = franwell.getTagLocation(config, data[i], false);

            if (!firstBreakdown) {
                for (x = 0, xcnt = config.breakdowns.length; x < xcnt; x++) {
                    if (containedInArea(loc, config.breakdowns[x])) {
                        firstBreakdown = rawLoc;
                        break;
                    }
                }
            }
            if (!firstStorage) {
                for (x = 0, xcnt = config.storages.length; x < xcnt; x++) {
                    if (containedInArea(loc, config.storages[x])) {
                        firstStorage = rawLoc;
                        break;
                    }
                }
            }

            if (i === 0) { canvasContext.moveTo(loc.X, loc.Y); }
            else { canvasContext.lineTo(loc.X, loc.Y); }
            canvasContext.arc(loc.X, loc.Y, 2, 0, Math.PI * 2, true);
        }

        canvasContext.stroke();

        $descFields = $summary.find('dd');
        $descFields.eq(0).html(franwell.formatNumber(data.length));
        $descFields.eq(1).html(firstBreakdown
            ? franwell.formatDateTime(firstBreakdown.Timestamp)
            : franwell.isNull());
        $descFields.eq(2).html(firstStorage
            ? franwell.formatDateTime(firstStorage.Timestamp)
            : franwell.isNull());

        rawLoc = franwell.getTagLocation(config, data[0], false);
        $descFields = $firstRead.find('dd');
        $descFields.eq(0).html(franwell.formatDateTime(rawLoc.Timestamp));
        $descFields.eq(1).html(franwell.formatNumber(rawLoc.X, 2));
        $descFields.eq(2).html(franwell.formatNumber(rawLoc.Y, 2));

        rawLoc = franwell.getTagLocation(config, data[data.length - 1], false);
        $descFields = $lastRead.find('dd');
        $descFields.eq(0).html(franwell.formatDateTime(rawLoc.Timestamp));
        $descFields.eq(1).html(franwell.formatNumber(rawLoc.X, 2));
        $descFields.eq(2).html(franwell.formatNumber(rawLoc.Y, 2));
    };


    franwell.getAnimationFrameIndex = franwell.getAnimationFrameIndex || function ($awbCanvas) {
        var index = $awbCanvas.data('currentAnimationFrame');
        return index;
    };


    franwell.setAnimationFrameIndex = franwell.setAnimationFrameIndex || function (index, $awbCanvas) {
        if (index) {
            $awbCanvas.data('currentAnimationFrame', index);
        } else {
            $awbCanvas.removeData('currentAnimationFrame');
        }
    };


    franwell.drawCurrentAnimationFrame = franwell.drawCurrentAnimationFrame || function (config, data, $awbCanvas, frameDrawnCallback) {
        var canvasContext = $awbCanvas.get(0).getContext('2d'),
            loc1,
            loc2,
            currentFrameIndex;

        currentFrameIndex = franwell.getAnimationFrameIndex($awbCanvas);
        if (!currentFrameIndex) {
            currentFrameIndex = 1;
        } else if (currentFrameIndex === data.length - 1) {
            franwell.stopAnimation($awbCanvas);
            currentFrameIndex = null;
        } else {
            currentFrameIndex++;
        }
        franwell.setAnimationFrameIndex(currentFrameIndex, $awbCanvas);

        if (!currentFrameIndex) {
            return;
        }

        loc1 = franwell.getTagLocation(config, data[currentFrameIndex - 1]);
        loc2 = franwell.getTagLocation(config, data[currentFrameIndex]);

        canvasContext.drawImage(config.backgroundImage, 0, 0);
        canvasContext.beginPath();
        canvasContext.moveTo(loc1.X, loc1.Y);
        canvasContext.arc(loc1.X, loc1.Y, 2, 0, Math.PI * 2, true);
        canvasContext.lineTo(loc2.X, loc2.Y);
        canvasContext.arc(loc2.X, loc2.Y, 2, 0, Math.PI * 2, true);
        canvasContext.stroke();

        if (typeof frameDrawnCallback === 'function') {
            frameDrawnCallback();
        }
    };


    franwell.playAnimation = franwell.playAnimation || function (config, data, $awbCanvas, frameDrawnCallback) {
        if (!data || data.length === 0 || data.length === 1) {
            return;
        }

        var timerHandle,
            drawFrame = function () { franwell.drawCurrentAnimationFrame(config, data, $awbCanvas, frameDrawnCallback); };

        // draw first frame immediately
        drawFrame();
        // start timer
        timerHandle = setInterval(drawFrame, 500);
        $awbCanvas.data('animationTimer', timerHandle);
    };


    franwell.stopAnimation = franwell.stopAnimation || function ($awbCanvas) {
        var timerHandle = $awbCanvas.data('animationTimer');
        if (!timerHandle) {
            return;
        }

        clearInterval(timerHandle);
        $awbCanvas.removeData('animationTimer');
    };
})();