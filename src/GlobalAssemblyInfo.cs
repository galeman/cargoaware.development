using System.Reflection;
using System.Resources;
using System.Runtime.InteropServices;

[assembly: AssemblyCompany("Franwell, Inc.")]
[assembly: AssemblyProduct("CargoAware Web Application")]
[assembly: AssemblyCopyright("Copyright 2013-2015 Franwell, Inc.")]
[assembly: AssemblyVersion("1.0.0")]
[assembly: AssemblyFileVersion("1.0.0")]
[assembly: NeutralResourcesLanguage("en")]
[assembly: AssemblyInformationalVersion("developer-build")]
[assembly: ComVisible(false)]