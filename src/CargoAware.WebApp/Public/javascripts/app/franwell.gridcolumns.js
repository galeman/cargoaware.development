﻿// franwell.gridcolumns.js
// (c) 2012-2015 Franwell, Inc.

// requires - franwell.js, franwell.format.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {};
    franwell.gridcolumns = franwell.gridcolumns || {};
    franwell.gridschemas = franwell.gridschemas || {};


    // Label Templates
    franwell.gridcolumns.labelTemplates = franwell.gridcolumns.labelTemplates || [
        { title: 'Label Tepmlate', field: 'Name' },
        { title: 'Type', field: 'Type', filterable: false },
        { title: 'Language', field: 'Language', filterable: false },
        { title: 'Version', field: 'Version' },
        {
            title: 'Active', field: 'IsActive',
            template: '#= IsActive ? "Yes" : "No" #'
        }
    ];
    franwell.gridschemas.labelTemplates = franwell.gridschemas.labelTemplates || {
        fields: {
            Version: { type: 'number' },
            IsActive: { type: 'boolean' }
        }
    };


    // Locations
    franwell.gridcolumns.locations = franwell.gridcolumns.locations || [
        { title: 'Location', field: 'Name' },
        { title: 'Axis', field: 'AxisDirection', filterable: false },
        { title: 'Origin', field: 'OriginLocation', filterable: false },
        { title: 'API Key', field: 'ApiKey' }
    ];


    // Location Zones
    franwell.gridcolumns.locationZones = franwell.gridcolumns.locationZones || [
        { title: 'Zone', field: 'Name' },
        { title: 'LMS Zone', field: 'LmsZoneId' },
        {
            title: 'Auto Update LMS', field: 'AutoUpdateLms',
            template: '#= AutoUpdateLms ? "Yes" : "No" #'
        },
        { title: 'Envoy Zone', field: 'EnvoyZoneId' },
        {
            title: 'Points', field: 'PointsString', filterable: false, sortable: false
        }
    ];
    franwell.gridschemas.locationZones = franwell.gridschemas.locationZones || {
        fields: {
            AutoUpdateLms: { type: 'boolean' }
        }
    };


    // Print Jobs
    franwell.gridcolumns.printJobs = franwell.gridcolumns.printJobs || [
        { title: 'Type', field: 'LabelType', filterable: false },
        {
            title: 'Created', field: 'CreatedDateTimeUtc',
            filterable: { ui: 'datetimepicker' },
            template: '#= franwell.formatDateTime(CreatedDateTimeUtc, true, true) #'
        },
        {
            title: 'Submitted', field: 'SubmittedDateTimeUtc',
            filterable: { ui: 'datetimepicker' },
            template: '#= franwell.formatDateTime(SubmittedDateTimeUtc, true, true) #'
        },
        {
            title: 'Cancel Request', field: 'CancelRequestDateTimeUtc',
            filterable: { ui: 'datetimepicker' },
            template: '#= franwell.formatDateTime(CancelRequestDateTimeUtc, true, true) #'
        },
        {
            title: 'Cancelled', field: 'CancelledDateTimeUtc',
            filterable: { ui: 'datetimepicker' },
            template: '#= franwell.formatDateTime(CancelledDateTimeUtc, true, true) #'
        },
        { title: 'Address', field: 'PrinterAddress' },
        { title: 'Port', field: 'PrinterPort' },
        { title: 'External Id.', field: 'PrinterExternalId' },
        { title: 'Air Waybill', field: 'AirWaybill' },
        { title: 'Start Piece', field: 'StartPiece' },
        { title: 'End Piece', field: 'EndPiece' }
    ];
    franwell.gridschemas.printJobs = franwell.gridschemas.printJobs || {
        fields: {
            CreatedDateTimeUtc: { type: 'date' },
            SubmittedDateTimeUtc: { type: 'date' },
            CancelRequestDateTimeUtc: { type: 'date' },
            CancelledDateTimeUtc: { type: 'date' },
            PrinterPort: { type: 'number' },
            StartPiece: { type: 'number' },
            EndPiece: { type: 'number' }
        }
    };


    // Printers
    franwell.gridcolumns.printers = franwell.gridcolumns.printers || [
        { title: 'Printer', field: 'Name' },
        { title: 'Type', field: 'Type', filterable: false },
        { title: 'Address', field: 'Address' },
        { title: 'Port', field: 'Port' },
        { title: 'External Id.', field: 'ExternalId' },
        {
            title: 'ULD Default', field: 'UldDefault',
            template: '#= UldDefault ? "Yes" : "No" #'
        },
        { title: 'Status', field: 'Status' }
    ];
    franwell.gridschemas.printers = franwell.gridschemas.printers || {
        fields: {
            Port: { type: 'number' }
        }
    };


    // Region Manager Regions
    franwell.gridcolumns.regionManagerRegions = franwell.gridcolumns.regionManagerRegions || [
        { title: 'Region', field: 'Name' },
        { title: 'ID', field: 'RegionId' }
    ];


    // Shipments
    franwell.gridcolumns.shipments = franwell.gridcolumns.shipments || [
        { title: 'Flight', field: 'FlightNumber' },
        {
            title: 'Flight Date', field: 'FlightDate',
            template: '#= franwell.formatDate(FlightDate) #'
        },
        { title: 'Air Waybill', field: 'AirWaybill' },
        {
            title: 'Air Waybill Tag', field: 'AirWaybillTagEpc'
            //template: '#= AirWaybillTagEpc.substr(AirWaybillTagEpc.length - 10) #'
        },
        {
            title: 'AWB Scan', field: 'AirWaybillTagDateTime',
            template: '#= franwell.formatDateTime(AirWaybillTagDateTime, true) #'
        },
        { title: 'ULD', field: 'Uld' },
        {
            title: 'ULD Tag', field: 'UldTagEpc',
            template: '#= UldTagEpc.substr(UldTagEpc.length - 10) #'
        },
        {
            title: 'ULD Scan', field: 'UldTagDateTime',
            template: '#= franwell.formatDateTime(UldTagDateTime, true) #'
        },
        { title: 'Note', field: 'Note' }
    ];
    franwell.gridschemas.shipments = franwell.gridschemas.shipments || {
        fields: {
            FlightDate: { type: 'date' },
            AirWaybillTagDateTime: { type: 'date' },
            UldTagDateTime: { type: 'date' }
        }
    };


    // Tag Reads
    franwell.gridcolumns.tagReads = franwell.gridcolumns.tagReads || [
        {
            title: 'Timestamp', field: 'Timestamp',
            filterable: { ui: 'datetimepicker' },
            template: '#= franwell.formatDateTime(Timestamp, false, true, true) #'
        },
        { title: 'X', field: 'X' },
        { title: 'Y', field: 'Y' },
        { title: 'Z', field: 'Z' },
        { title: 'Location Zone', field: 'LocationZoneName' },
        { title: 'Readers', field: 'Readers' }
    ];
    franwell.gridschemas.tagReads = franwell.gridschemas.tagReads || {
        fields: {
            Timestamp: { type: 'date' },
            X: { type: 'number' },
            Y: { type: 'number' },
            Z: { type: 'number' }
        }
    };



    // MyMits stuff
    // ==================================================
    // ==================================================
    // ==================================================
    // ==================================================
    // ==================================================
    // Adjustment Reasons
    franwell.gridcolumns.adjustmentReasons = franwell.gridcolumns.adjustmentReasons || [
        { title: 'Adjustment Reason', field: 'Name' }
    ];


    // Credit Cards
    franwell.gridcolumns.creditCards = franwell.gridcolumns.creditCards || [
        {
            title: 'Number', field: 'CardNumber',
            template: '<img alt="#= CardType #" title="#= CardType #" src="#= ImageUrl #" /> #= CardNumber #'
        },
        { title: 'Cardholder', field: 'CardholderName' },
        { title: 'Expiration', template: '#= ExpirationMonth #/#= ExpirationYear #' },
        {
            title: 'Expired', field: 'IsExpired',
            template: '#= IsExpired ? "Yes" : "No" #'
        },
        {
            title: 'Tag Orders', field: 'IsValidForTagOrder',
            template: '#= IsValidForTagOrder ? "Enabled" : "" #'
        },
        {
            title: 'Support', field: 'IsSubscriptionToken',
            template: '#= IsSubscriptionToken ? "Active" : "" #'
        }
    ];


    // Data Import
    franwell.gridcolumns.dataImport = franwell.gridcolumns.dataImport || [
        { title: 'File', field: 'FileName' },
        {
            title: 'Uploaded', field: 'UploadDateTime',
            template: '#= franwell.formatDateTime(UploadDateTime, true) #'
        },
        {
            title: 'Entries', field: 'TotalRows',
            template: '#= franwell.formatNumber(TotalRows) #'
        },
        { title: 'Status', field: 'Status' },
        { title: 'User', field: 'UserName' }
    ];
    franwell.gridschemas.dataImport = franwell.gridschemas.dataImport || {
        fields: {
            'UploadDateTime': { type: 'date' },
            'TotalRows': { type: 'number' }
        }
    };
    franwell.gridcolumns.dataImportErrors = franwell.gridcolumns.dataImportErrors || [
        {
            title: 'Row', field: 'Row',
            template: '#= franwell.formatNumber(Row + 1, 0, false) #'
        },
        { title: 'Message', field: 'Message' }
    ];
    franwell.gridschemas.dataImportErrors = franwell.gridschemas.dataImportErrors || {
        fields: {
            'Row': { type: 'number' }
        }
    };


    // Employees
    franwell.gridcolumns.employees = franwell.gridcolumns.employees || [
        { title: 'License No.', field: 'License.Number', width: '100px' },
        { title: 'Full Name', field: 'FullName' },
        { title: 'E-mail', field: 'User.Email' },
        { title: 'Type', field: 'License.EmployeeTypeName' },
        {
            title: 'Granted', field: 'License.StartDate', width: '90px',
            template: '#= franwell.formatDate(License.StartDate) #'
        },
        {
            title: 'Expires', field: 'License.EndDate', width: '90px',
            template: '#= franwell.formatDate(License.EndDate) #'
        },
        {
            title: 'Locked', field: 'SelectedFacilityEmployee.IsBlocked', width: '80px',
            template: '#= SelectedFacilityEmployee.IsBlocked ? "Yes" : "No" #'
        },
        {
            title: 'Manager', field: 'SelectedFacilityEmployee.IsManager', width: '80px',
            template: '#= SelectedFacilityEmployee.IsManager ? "Yes" : "No" #'
        }
    ];
    franwell.gridschemas.employees = franwell.gridschemas.employees || {
        fields: {
            'License.StartDate': { type: 'date' },
            'License.EndDate': { type: 'date' },
            'SelectedFacilityEmployee.IsBlocked': { type: 'boolean' },
            'SelectedFacilityEmployee.IsManager': { type: 'boolean' }
        }
    };


    // Excise Taxes
    franwell.gridcolumns.exciseTaxes = franwell.gridcolumns.exciseTaxes || [
        {
            title: 'Start Date', field: 'StartDate',
            template: '#= franwell.formatDate(StartDate) #'
        },
        {
            title: 'End Date', field: 'EndDate',
            template: '#= franwell.formatDate(EndDate) #'
        },
        {
            title: 'Percent', field: 'Percent',
            template: '#= franwell.formatNumber(Percent, 2, false, true) # %'
        },
        { title: 'Product Category', field: 'ProductCategoryName' },
        {
            title: 'Avg. Market Price', field: 'AverageMarketPrice',
            template: '#= franwell.formatCurrency(AverageMarketPrice) #'
        },
        {
            title: 'Quantity', field: 'Quantity',
            template: '#= franwell.formatQuantity(Quantity) # #= UnitOfMeasureAbbreviation #'
        }
    ];
    franwell.gridschemas.exciseTaxes = franwell.gridschemas.exciseTaxes || {
        fields: {
            StartDate: { type: 'date' },
            EndDate: { type: 'date' },
            Percent: { type: 'number' },
            AverageMarketPrice: { type: 'number' },
            Quantity: { type: 'number' }
        }
    };


    // Event History
    franwell.gridcolumns.eventHistory = franwell.gridcolumns.eventHistory || [
        { title: 'Description', field: 'Description', filterable: false, sortable: false },
        { title: 'Employee', field: 'UserName', filterable: false, sortable: false, width: '20%' },
        {
            title: 'Date', field: 'ActualDate', filterable: false, sortable: false, width: '110px',
            template: '#= franwell.formatDate(ActualDate) #'
        },
        {
            title: 'Reported', field: 'RecordedDateTime', filterable: false, sortable: false, width: '180px',
            // Date/time format intentionally separated with a space
            template: '#= franwell.formatDateTime(RecordedDateTime) #'
        }
    ];


    // Facilities
    franwell.gridcolumns.facilities = franwell.gridcolumns.facilities || [
        { title: 'Facility', field: 'Name' },
        { title: 'License No.', field: 'License.Number', width: '100px' },
        { title: 'Type', field: 'FacilityType.Name' },
        {
            title: 'Granted', field: 'License.StartDate', width: '110px',
            template: '#= franwell.formatDate(License.StartDate) #'
        },
        {
            title: 'Expires', field: 'License.EndDate', width: '110px',
            template: '#= franwell.formatDate(License.EndDate) #'
        },
        { title: 'Home', field: 'HomePage' },
        {
            title: 'Locked', field: 'IsBlocked', width: '80px',
            template: '#= IsBlocked ? "Yes" : "No" #'
        },
        {
            title: 'Manager', field: 'IsManager', width: '80px',
            template: '#= IsManager ? "Yes" : "No" #'
        }
    ];
    franwell.gridschemas.facilities = franwell.gridschemas.facilities || {
        fields: {
            'License.StartDate': { type: 'date' },
            'License.EndDate': { type: 'date' },
            'IsBlocked': { type: 'boolean' },
            'IsManager': { type: 'boolean' }
        }
    };


    // Harvest Batches
    franwell.gridcolumns.harvestBatches = franwell.gridcolumns.harvestBatches || [
        { title: 'Harvest Batch', field: 'Name' },
        {
            title: 'Type', field: 'HarvestType',
            template: '#= ' +
                'HarvestType === "WholePlant" ? "Harvest" :' +
                'HarvestType === "Product" ? "Manicure" :' +
                'HarvestType #'
        },
        { title: 'Room', field: 'DryingRoomName' },
        {
            title: 'Wet Weight', field: 'TotalWetWeight', width: '110px',
            template: '#= franwell.formatQuantity(TotalWetWeight) # #= UnitOfWeightAbbreviation #'
        },
        {
            title: 'Batch Date', field: 'HarvestStartDate', width: '110px',
            template: '#= franwell.formatDate(HarvestStartDate) #'
        }
    ];
    franwell.gridschemas.harvestBatches = franwell.gridschemas.harvestBatches || {
        fields: {
            TotalWetWeight: { type: 'number' },
            HarvestStartDate: { type: 'date' }
        }
    };


    // Immature Plants
    franwell.gridcolumns.immaturePlants = franwell.gridcolumns.immaturePlants || [
        { title: 'Group', field: 'Name' },
        { title: 'Strain', field: 'StrainName' },
        { title: 'Type', field: 'TypeName', width: '150px' },
        {
            title: 'Plants', field: 'Count', width: '90px',
            template: '#= franwell.formatNumber(Count) #'
        },
        {
            title: 'Group Date', field: 'PlantedDate', width: '110px',
            template: '#= franwell.formatDate(PlantedDate) #'
        }
    ];
    franwell.gridschemas.immaturePlants = franwell.gridschemas.immaturePlants || {
        fields: {
            Count: { type: 'number' },
            PlantedDate: { type: 'date' }
        }
    };


    // Industry Users
    franwell.gridcolumns.industryUsers = franwell.gridcolumns.industryUsers || [
        { title: 'License No.', field: 'License.Number', width: '100px' },
        { title: 'Full Name', field: 'FullName' },
        { title: 'E-mail', field: 'User.Email' },
        { title: 'Type', field: 'License.EmployeeTypeName' },
        {
            title: 'Granted', field: 'License.StartDate', width: '110px',
            template: '#= franwell.formatDate(License.StartDate) #'
        },
        {
            title: 'Expires', field: 'License.EndDate', width: '110px',
            template: '#= franwell.formatDate(License.EndDate) #'
        }
    ];
    franwell.gridschemas.industryUsers = franwell.gridschemas.industryUsers || {
        fields: {
            'License.StartDate': { type: 'date' },
            'License.EndDate': { type: 'date' }
        }
    };


    // Items
    franwell.gridcolumns.items = franwell.gridcolumns.items || [
        { title: 'Item', field: 'Name' },
        { title: 'Category', field: 'ProductCategoryName' },
        { title: 'Strain', field: 'StrainName' },
        { title: 'Unit of Measure', field: 'UnitOfMeasureName' }
    ];


    // Item Categories
    franwell.gridcolumns.itemCategories = franwell.gridcolumns.itemCategories || [
        { title: 'Item Category', field: 'Name' },
        { title: 'Quantity Type', field: 'QuantityType' },
        {
            title: 'Strain Required', field: 'RequiresStrain',
            template: '#= RequiresStrain ? "Yes" : "No" #'
        }
    ];
    franwell.gridschemas.itemCategories = franwell.gridschemas.itemCategories || {
        fields: {
            RequiresStrain: { type: 'boolean' }
        }
    };


    // Licensees
    franwell.gridcolumns.licensees = franwell.gridcolumns.licensees || [
        { title: 'Facility', field: 'Name' },
        { title: 'License No.', field: 'License.Number', width: '100px' },
        { title: 'Type', field: 'FacilityType.Name' },
        {
            title: 'Granted', field: 'License.StartDate', width: '110px',
            template: '#= franwell.formatDate(License.StartDate) #'
        },
        {
            title: 'Expires', field: 'License.EndDate', width: '110px',
            template: '#= franwell.formatDate(License.EndDate) #'
        }
    ];
    franwell.gridschemas.licensees = franwell.gridschemas.licensees || {
        fields: {
            'License.StartDate': { type: 'date' },
            'License.EndDate': { type: 'date' }
        }
    };


    // Manifests
    franwell.gridcolumns.manifests = franwell.gridcolumns.manifests || [
        { title: 'Manifest', field: 'ManifestNumber' },
        { title: 'License', field: 'ShipperFacilityLicenseNumber' },
        { title: 'Origin', field: 'ShipperFacilityName' },
        { title: 'Driver', field: 'DriverName' },
        {
            title: 'Vehicle Info', field: 'VehicleInfo', width: '170px',
            template: '<small>Make: #= VehicleMake #<br />Model: #= VehicleModel #<br />Lic. Plate: #= VehicleLicensePlateNumber #</small>'
        },
        { title: 'Stops', field: 'ShipmentDeliveriesCount', width: '75px' },
        { title: 'Packages', field: 'ShipmentPackagesCount', width: '100px' },
        { title: 'Employee', field: 'CreatedByUserName' },
        {
            title: 'Date Created', field: 'CreatedDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(CreatedDateTime, true) #'
        },
        {
            title: 'Voided', field: 'IsVoided', width: '75px',
            template: '#= IsVoided ? "Yes" : "No" #'
        }
    ];
    franwell.gridschemas.manifests = franwell.gridschemas.manifests || {
        fields: {
            ShipmentDeliveriesCount: { type: 'number' },
            ShipmentPackagesCount: { type: 'number' },
            IsVoided: { type: 'boolean' },
            CreatedDateTime: { type: 'date' }
        }
    };


    // MyLO Businesses
    franwell.gridcolumns.myLoBusiness = franwell.gridcolumns.myLoBusiness || [
        { title: "Name", field: "FullName" },
        { title: "License No.", field: "LicenseNumber", width: '100px' },
        { title: "License Type", field: "LicenseType" },
        {
            title: 'Granted', field: 'EffectiveDate', width: '110px',
            template: '#= franwell.formatDate(EffectiveDate) #'
        },
        {
            title: 'Expires', field: 'ExpirationDate', width: '110px',
            template: '#= franwell.formatDate(ExpirationDate) #'
        }
    ];
    franwell.gridschemas.myLoBusiness = franwell.gridschemas.myLoBusiness || {
        fields: {
            EffectiveDate: { type: 'date' },
            ExpirationDate: { type: 'date' }
        }
    };


    // MyLO Occupational
    franwell.gridcolumns.myLoOccupational = franwell.gridcolumns.myLoOccupational || [
        { title: "License No.", field: "LicenseNumber", width: '100px' },
        { title: "Name", field: "FullName" },
        { title: "First Name", field: "FirstName" },
        { title: "Middle Name", field: "MiddleName" },
        { title: "Last Name", field: "LastName" },
        { title: "License Type", field: "LicenseType" },
        {
            title: 'Granted', field: 'EffectiveDate', width: '110px',
            template: '#= franwell.formatDate(EffectiveDate) #'
        },
        {
            title: 'Expires', field: 'ExpirationDate', width: '110px',
            template: '#= franwell.formatDate(ExpirationDate) #'
        }
    ];
    franwell.gridschemas.myLoOccupational = franwell.gridschemas.myLoOccupational || {
        fields: {
            EffectiveDate: { type: 'date' },
            ExpirationDate: { type: 'date' }
        }
    };


    // Packages
    franwell.gridcolumns.packages = franwell.gridcolumns.packages || [
        { title: 'Tag', field: 'Label' },
        { title: 'Item', field: 'ProductName' },
        {
            title: 'Quantity', field: 'Quantity', width: '110px',
            template: '#= franwell.formatQuantity(Quantity) # #= UnitOfMeasureAbbreviation #'
        },
        {
            title: 'Date', field: 'PackagedDate', width: '110px',
            template: '#= franwell.formatDate(PackagedDate) #'
        }
    ];
    franwell.gridschemas.packages = franwell.gridschemas.packages || {
        fields: {
            Quantity: { type: 'number' },
            PackagedDate: { type: 'date' }
        }
    };


    // Patients
    franwell.gridcolumns.patients = franwell.gridcolumns.patients || [
        { title: 'License No.', field: 'LicenseNumber', width: '100px' },
        {
            title: 'Granted', field: 'LicenseEffectiveStartDate', width: '110px',
            template: '#= franwell.formatDate(LicenseEffectiveStartDate) #'
        },
        {
            title: 'Expires', field: 'LicenseEffectiveEndDate', width: '110px',
            template: '#= franwell.formatDate(LicenseEffectiveEndDate) #'
        },
        { title: 'Recom\'d Plants', field: 'RecommendedPlants', width: '145px' },
        { title: 'Recom\'d Ounces', field: 'RecommendedSmokableQuantity', width: '150px' },
        {
            title: 'Registered', field: 'RegistrationDate', width: '110px',
            template: '#= franwell.formatDate(RegistrationDate) #'
        }
    ];
    franwell.gridschemas.patients = franwell.gridschemas.patients || {
        fields: {
            LicenseEffectiveStartDate: { type: 'date' },
            LicenseEffectiveEndDate: { type: 'date' },
            RecommendedPlants: { type: 'number' },
            RecommendedSmokableQuantity: { type: 'number' },
            RegistrationDate: { type: 'date' }
        }
    };


    // Plants
    franwell.gridcolumns.plants = franwell.gridcolumns.plants || [
        { title: 'Tag', field: 'Label' },
        { title: 'Strain', field: 'StrainName' },
        { title: 'Room', field: 'RoomName' },
        // the "Phase" column is removed by some phase-specific grids
        { title: 'Phase', field: 'GrowthPhaseName', width: '110px' },
        {
            title: 'Batch Date', field: 'PlantedDate', width: '110px',
            template: '#= franwell.formatDate(PlantedDate) #'
        }
    ];
    franwell.gridschemas.plants = franwell.gridschemas.plants || {
        fields: {
            PlantedDate: { type: 'date' }
        }
    };


    // Relationships
    franwell.gridcolumns.relationships = franwell.gridcolumns.relationships || [
        { title: 'Name', field: 'FullName' },
        { title: 'License No.', field: 'LicenseNumber', width: '100px' },
        { title: 'License Type', field: 'LicenseType' },
        {
            title: 'Granted', field: 'EffectiveDate', width: '110px',
            template: '#= franwell.formatDate(EffectiveDate) #'
        },
        {
            title: 'Expires', field: 'ExpirationDate', width: '110px',
            template: '#= franwell.formatDate(ExpirationDate) #'
        }
    ];
    franwell.gridschemas.relationships = franwell.gridschemas.relationships || {
        fields: {
            EffectiveDate: { type: 'date' },
            ExpirationDate: { type: 'date' }
        }
    };


    // Sales
    franwell.gridcolumns.sales = franwell.gridcolumns.sales || [
        {
            title: 'Sales Date', field: 'SalesDate',
            template: '#= franwell.formatDate(SalesDate) #'
        },
        { title: 'Transactions', field: 'TotalTransactions' },
        { title: 'Packages', field: 'TotalPackages' },
        { title: 'Total', field: 'TotalPrice', format: '{0:c}' }
    ];
    franwell.gridschemas.sales = franwell.gridschemas.sales || {
        fields: {
            SalesDate: { type: 'date' },
            TotalTransactions: { type: 'number' },
            TotalPackages: { type: 'number' },
            TotalPrice: { type: 'number' }
        }
    };
    franwell.gridcolumns.salesTransactions = franwell.gridcolumns.salesTransactions || [
        { title: 'Package', field: 'PackageLabel' },
        { title: 'Item', field: 'ProductName' },
        {
            title: 'Quantity', field: 'QuantitySold', width: '110px',
            template: '#= franwell.formatQuantity(QuantitySold) # #= UnitOfMeasureAbbreviation #'
        },
        { title: 'Total', field: 'TotalPrice', format: '{0:c}', width: '85px' }
    ];
    franwell.gridschemas.salesTransactions = franwell.gridschemas.salesTransactions || {
        fields: {
            QuantitySold: { type: 'number' },
            TotalPrice: { type: 'number' }
        }
    };


    // Sales Taxes
    franwell.gridcolumns.salesTaxes = franwell.gridcolumns.salesTaxes || [
        {
            title: 'Start Date', field: 'StartDate',
            template: '#= franwell.formatDate(StartDate) #'
        },
        {
            title: 'End Date', field: 'EndDate',
            template: '#= franwell.formatDate(EndDate) #'
        },
        {
            title: 'Percent', field: 'Percent',
            template: '#= franwell.formatNumber(Percent, 2, false, true) # %'
        }
    ];
    franwell.gridschemas.salesTaxes = franwell.gridschemas.salesTaxes || {
        fields: {
            StartDate: { type: 'date' },
            EndDate: { type: 'date' },
            Percent: { type: 'number' }
        }
    };


    // Strains
    franwell.gridcolumns.strains = franwell.gridcolumns.strains || [
        { title: 'Strain', field: 'Name' },
        { title: 'Testing', field: 'TestingStatus' },
        { title: 'THC', field: 'ThcLevel', width: '15%' },
        { title: 'CBD', field: 'CbdLevel', width: '15%' },
        { title: 'Genetics', field: 'Genetics', width: '20%', filterable: false, sortable: false }
    ];
    franwell.gridschemas.strains = franwell.gridschemas.strains || {
        fields: {
            ThcLevel: { type: 'number' },
            CbdLevel: { type: 'number' }
        }
    };


    // Tag Orders
    franwell.gridcolumns.tagOrders = franwell.gridcolumns.tagOrders || [
        { title: 'Order Number', field: 'OrderNumber', width: '140px' },
        {
            title: 'Order Date', field: 'OrderDate', width: '120px',
            filterable: { ui: 'datetimepicker' },
            template: '#= franwell.formatDateTime(OrderDate, true) #'
        },
        { title: 'Status', field: 'Status' },
        //{
        //    title: 'Expedited', field: 'ExpeditedServiceRequested',
        //    template: '#= ExpeditedServiceRequested ? "Yes" : "No" #'
        //},
        {
            title: 'Total', field: 'OrderAmount',
            template: '#= franwell.formatCurrency(OrderAmount) #'
        },
        {
            title: 'Paid', field: 'PaymentAmount',
            template: '#= franwell.formatCurrency(PaymentAmount) #'
        },
        { title: 'Tracking Number', field: 'TrackingNumber' },
        { title: 'User', field: 'OrderedByName' }
    ];
    franwell.gridschemas.tagOrders = franwell.gridschemas.tagOrders || {
        fields: {
            OrderDate: { type: 'date' },
            ExpeditedServiceRequested: { type: 'boolean' },
            OrderAmount: { type: 'number' },
            PaymentAmount: { type: 'number' }
        }
    };
    franwell.gridcolumns.tagOrdersDetails = franwell.gridcolumns.tagOrdersDetails || [
        { title: 'Type', field: 'TagType' },
        {
            title: 'Quantity', field: 'Quantity',
            template: '#= franwell.formatNumber(Quantity) #'
        },
        {
            title: 'Unit Price', field: 'UnitPrice',
            template: '#= franwell.formatCurrency(UnitPrice) #'
        },
        {
            title: 'Total', filterable: false, sortable: false,
            template: '#= franwell.formatCurrency(Quantity * UnitPrice) #'
        }
    ];
    franwell.gridschemas.tagOrdersDetails = franwell.gridschemas.tagOrdersDetails || {
        fields: {
            Quantity: { type: 'number' },
            UnitPrice: { type: 'number' }
        }
    };


    // Transfers
    franwell.gridcolumns.incomingTransfers = franwell.gridcolumns.incomingTransfers || [
        { title: 'Manifest', field: 'ManifestNumber' },
        { title: 'License', field: 'ShipperFacilityLicenseNumber' },
        { title: 'Origin', field: 'ShipperFacilityName' },
        { title: 'Driver', field: 'DriverName' },
        {
            title: 'Vehicle Info', field: 'VehicleInfo', width: '170px',
            template: '<small>Make: #= VehicleMake #<br />Model: #= VehicleModel #<br />Lic. Plate: #= VehicleLicensePlateNumber #</small>'
        },
        { title: 'Packages', field: 'DeliveryPackagesCount', width: '100px' },
        {
            title: 'Est. Departure', field: 'EstimatedDepartureDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(EstimatedDepartureDateTime, true) #'
        },
        {
            title: 'Est. Arrival', field: 'EstimatedArrivalDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(EstimatedArrivalDateTime, true) #'
        },
        {
            title: 'Received', field: 'ReceivedDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(ReceivedDateTime, true) #'
        }
    ];
    franwell.gridschemas.incomingTransfers = franwell.gridschemas.incomingTransfers || {
        fields: {
            ShipmentDeliveriesCount: { type: 'number' },
            ShipmentPackagesCount: { type: 'number' },
            DeliveryPackagesCount: { type: 'number' },
            EstimatedDepartureDateTime: { type: 'date' },
            EstimatedArrivalDateTime: { type: 'date' },
            ReceivedDateTime: { type: 'date' }
        }
    };
    franwell.gridcolumns.outgoingTransfers = franwell.gridcolumns.outgoingTransfers || [
        { title: 'Manifest', field: 'ManifestNumber' },
        { title: 'Driver', field: 'DriverName' },
        {
            title: 'Vehicle Info', field: 'VehicleInfo', width: '170px',
            template: '<small>Make: #= VehicleMake #<br />Model: #= VehicleModel #<br />Lic. Plate: #= VehicleLicensePlateNumber #</small>'
        },
        { title: 'Stops', field: 'ShipmentDeliveriesCount', width: '75px' },
        { title: 'Packages', field: 'ShipmentPackagesCount', width: '100px' },
        { title: 'Employee', field: 'CreatedByUserName' },
        {
            title: 'Date Created', field: 'CreatedDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(CreatedDateTime, true) #'
        }
    ];
    franwell.gridschemas.outgoingTransfers = franwell.gridschemas.outgoingTransfers || {
        fields: {
            ShipmentDeliveriesCount: { type: 'number' },
            ShipmentPackagesCount: { type: 'number' },
            CreatedDateTime: { type: 'date' }
        }
    };
    franwell.gridcolumns.transferDestinations = franwell.gridcolumns.transferDestinations || [
        { title: 'Destination', field: 'RecipientFacilityName' },
        { title: 'License No.', field: 'RecipientFacilityLicenseNumber', width: '100px' },
        { title: 'Type', field: 'ShipmentType' },
        {
            title: 'Est. Departure', field: 'EstimatedDepartureDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(EstimatedDepartureDateTime, true) #'
        },
        {
            title: 'Est. Arrival', field: 'EstimatedArrivalDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(EstimatedArrivalDateTime, true) #'
        },
        {
            title: 'Received', field: 'ReceivedDateTime', filterable: { ui: 'datetimepicker' }, width: '120px',
            template: '#= franwell.formatDateTime(ReceivedDateTime, true) #'
        },
        { title: 'Packages', field: 'ShipmentPackagesCount', width: '100px' }
    ];
    franwell.gridschemas.transferDestinations = franwell.gridschemas.transferDestinations || {
        fields: {
            EstimatedDepartureDateTime: { type: 'date' },
            EstimatedArrivalDateTime: { type: 'date' },
            ReceivedDateTime: { type: 'date' },
            ShipmentPackagesCount: { type: 'number' }
        }
    };
    franwell.gridcolumns.transferPackages = franwell.gridcolumns.transferPackages || [
        { title: 'Package', field: 'PackageLabel' },
        { title: 'Item', field: 'ProductName' },
        {
            title: 'Shipped Qty.', field: 'ShippedQuantity', width: '120px',
            template: '#= ShippedQuantity ? franwell.formatQuantity(ShippedQuantity) + " " + ShippedUnitOfMeasureAbbreviation : "" #'
        },
        {
            title: 'Received Qty.', field: 'ReceivedQuantity', width: '130px',
            template: '#= ReceivedQuantity ? franwell.formatQuantity(ReceivedQuantity) + " " + ReceivedUnitOfMeasureAbbreviation : "" #'
        }
    ];
    franwell.gridschemas.transferPackages = franwell.gridschemas.transferPackages || {
        fields: {
            ShippedQuantity: { type: 'number' },
            ReceivedQuantity: { type: 'number' }
        }
    };


    // Users
    franwell.gridcolumns.users = franwell.gridcolumns.users || [
        { title: 'Username', field: 'Username' },
        { title: 'Name', field: 'Name' },
        { title: 'E-mail', field: 'Email' },
        { title: 'Home', field: 'HomePage' },
        {
            title: 'Locked Until', field: 'LockedUntil', width: '150px',
            template: '#= LockedUntil == null ? "" : LockedUntil >= new Date(3000, 1) ? "Permanently" : franwell.formatDateTime(LockedUntil) #'
        }
    ];
    franwell.gridschemas.users = franwell.gridschemas.users || {
        fields: {
            LockedUntil: { type: 'date' }
        }
    };
})();