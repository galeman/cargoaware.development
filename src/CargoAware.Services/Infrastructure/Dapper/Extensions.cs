﻿using CargoAware.Services.Common;
using Dapper;
using System;
using System.Data;

namespace CargoAware.Services.Infrastructure.Dapper
{
    internal static class Extensions
    {
        public static bool QueryBoolean(this IDbConnection db, string sql, dynamic param = null)
        {
            var results = SqlMapper.Query<int>(db, sql, param);
            return results.Count > 0 && results[0] > 0;
        }

        public static Paged<T> QueryPaged<T>(this IDbConnection db, string sql, dynamic param = null)
        {
            long total = 0;
            Func<T, long, long, T> map = (record, firstRow, lastRow) =>
            {
                total = firstRow + lastRow - 1;
                return record;
            };

            try
            {
                var data = SqlMapper.Query<T, long, long, T>(db, sql, map, @param, splitOn: "FirstRow,LastRow");
                return new Paged<T>(data, total);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debug.WriteLine(ex.Message);
                throw;
            }
        }

        public static T QueryScalar<T>(this IDbConnection db, string sql, dynamic param = null)
        {
            var results = SqlMapper.Query<T>(db, sql, param);
            return results.Count == 1 ? results[0] : default(T);
        }
    }
}