﻿namespace CargoAware.Services.Common
{
    public enum EntityVisibleState : byte
    {
        All,
        NonVisibleOnly,
        VisibleOnly
    }
}