﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.RegionManager;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("admin/regionmanager/regions")]
    public partial class AdminRegionManagerRegionsController : AppMvcController
    {
        private static Lazy<RegionManagerRepository> _RegionManagerRepository;


        #region // Constructor + DI
        // ==================================================

        static AdminRegionManagerRegionsController()
        {
            Inject(Repositories.ForRegionManager);
        }

        private static void Inject(Lazy<RegionManagerRepository> dependency)
        {
            _RegionManagerRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.AdminRegionManagerRegions()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        [GET("addedit")]
        public virtual ActionResult AddEdit(Boolean adding, long[] ids)
        {
            if (!User.HasPermission(Permissions.User.AdminRegionManagerRegions()))
            {
                throw UnauthorizedException();
            }

            var model = new AddEditRegionManagerRegionsModel
            {
                Adding = adding,
                Regions = _RegionManagerRepository.Value.GetRegions(User.CurrentLocation.Id).Data
            };

            if (!adding)
            {
                if (ids != null && ids.Length != 0)
                {
                    model.Details = model.Regions
                        .Where(x => ids.Any(id => x.Id == id))
                        .Select(x => new AddEditRegionManagerRegionsDetailsModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            RegionId = x.RegionId
                        });
                }
            }

            return View(model);
        }

        // ==================================================
        #endregion
    }
}