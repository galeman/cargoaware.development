﻿using CargoAware.Services.Common;
using System;

namespace CargoAware.Services.Event
{
    public sealed class EventDocument
    {
        public DateTimeOffset Timestamp { get; set; }
        public EventSource EventSource { get; set; }
        public String EventName { get; set; }
        public String Message { get; set; }
    }
}