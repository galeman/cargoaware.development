﻿using CsvHelper;
using CsvHelper.Configuration;
using System;
using System.IO;
using System.Linq;

namespace CargoAware.WebApp.Infrastructure.DataImport
{
    public static class Reader
    {
        public struct RecordsResult<T>
        {
            public T[] Records { get; set; }
            public String ProperFileExtension { get; set; }
        };


        public static RecordsResult<T> GetRecords<T>(String filename, CsvConfiguration configuration)
        {
            RecordsResult<T> result;

            using (var reader = File.OpenText(filename))
            using (var csvReader = new CsvReader(reader, configuration))
            {
                var records = csvReader.GetRecords<T>();
                try
                {
                    result = new RecordsResult<T>
                    {
                        Records = records.ToArray(),
                        ProperFileExtension = ".csv"
                    };
                }
                catch (NullReferenceException)
                {
                    // NullReferenceException is apparently thrown when the file is in an invalid format
                    throw new InvalidDataException("The uploaded file does not contain a valid CSV format. Please ensure you are uploading the correct file.");
                }
            }

            return result;
        }
    }
}