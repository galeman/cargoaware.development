﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Helpers;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CargoAware.Web.Infrastructure.Security
{
    public class ApiValidateAntiForgeryToken : ActionFilterAttribute
    {
        public const String TokenName = "ApiVerificationToken";

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var cookieToken = "";
            var formToken = "";

            IEnumerable<String> tokenHeaders;
            if (actionContext.Request.Headers.TryGetValues(TokenName, out tokenHeaders))
            {
                var tokens = tokenHeaders.First().Split(':');
                if (tokens.Length == 2)
                {
                    cookieToken = tokens[0];
                    formToken = tokens[1];
                }
            }
            AntiForgery.Validate(cookieToken, formToken);

            base.OnActionExecuting(actionContext);
        }
    }
}