﻿using System;

namespace CargoAware.WebApp.Models.Imports
{
    public class RegionManagerImportModel
    {
        public String Epc { get; set; }
        public Decimal X { get; set; }
        public Decimal Y { get; set; }
        public Decimal Z { get; set; }
        public DateTime Timestamp { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as RegionManagerImportModel;
            return other != null &&
                String.Equals(Epc, other.Epc) &&
                X == other.X &&
                Y == other.Y &&
                Z == other.Z &&
                Timestamp == other.Timestamp;
        }

        public override int GetHashCode()
        {
            var result = Epc.GetHashCode();
            result = (result * 397) ^ X.GetHashCode();
            result = (result * 397) ^ Y.GetHashCode();
            result = (result * 397) ^ Z.GetHashCode();
            result = (result * 397) ^ Timestamp.GetHashCode();
            return result;
        }
    }
}