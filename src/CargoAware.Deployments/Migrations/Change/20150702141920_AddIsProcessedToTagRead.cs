﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150702141920)]
    public class AddIsProcessedToTagRead_20150702141920 : AutoReversingMigration
    {
        public override void Up()
        {
            if (!Schema.Table("TagRead").Column("IsProcessed").Exists())
            {
                Alter.Table("TagRead")
                     .AddColumn("IsProcessed").AsBoolean().NotNullable().SetExistingRowsTo(false);
            }
        }
    }
}