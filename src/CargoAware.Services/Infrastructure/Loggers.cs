﻿using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;
using System;
using System.Linq;

namespace CargoAware.Services.Infrastructure
{
    public static class Loggers
    {
        private const String LogPattern = "%d [%t] %-5p %m%n";
        private static readonly PatternLayout PatternLayout;


        #region // Constructor
        // ==================================================

        static Loggers()
        {
            var hierarchy = (Hierarchy)LogManager.GetRepository();


            PatternLayout = new PatternLayout
            {
                ConversionPattern = LogPattern
            };
            PatternLayout.ActivateOptions();


            var tracer = new TraceAppender
            {
                Layout = PatternLayout
            };
            tracer.ActivateOptions();
            hierarchy.Root.AddAppender(tracer);


#if (DEBUG)
            hierarchy.Root.Level = Level.All;
#else
            hierarchy.Root.Level = Level.Warn;
#endif
            hierarchy.Configured = true;
        }

        // ==================================================
        #endregion


        // For additional loggers/logs, please create a new Get<___>Logger() method
        #region // Logger Methods
        // ==================================================

        public static ILog GetExceptionLogger()
        {
            var result = GetLogger("ExceptionLogger", "Exception.log");
            return result;
        }

        public static ILog GetApiControllerLogger()
        {
            var result = GetLogger("ApiControllerLogger", "ApiController.log", Level.All);
            return result;
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private static ILog GetLogger(String name, String logFileName, Level loggingLevel = null)
        {
            var result = LogManager.GetLogger(name);
            var logger = (Logger)result.Logger;

            if (loggingLevel != null)
            {
                logger.Level = loggingLevel;
            }

            var appenders = logger.Appenders.OfType<RollingFileAppender>();
            if (!appenders.Any())
            {
                var roller = new RollingFileAppender
                {
                    AppendToFile = true,
                    Layout = PatternLayout,
                    File = @"Logs\" + logFileName,
                    MaxFileSize = 1048576, // 1 mb
                    MaxSizeRollBackups = 10,
                    Name = name,
                    PreserveLogFileNameExtension = true,
                    RollingStyle = RollingFileAppender.RollingMode.Size,
                    StaticLogFileName = true,
                };
                roller.ActivateOptions();
                logger.AddAppender(roller);
            }

            return result;
        }

        // ==================================================
        #endregion
    }
}