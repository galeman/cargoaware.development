﻿using CargoAware.Services.User;
using CargoAware.Web.Infrastructure.Extensions;
using HtmlTags;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Infrastructure.Menus
{
    public sealed class MenuLink : IMenuNode
    {
        private readonly Boolean _Permitted;
        private Boolean _Visible;


        public MenuLink(
            String text,
            ActionResult action,
            IEnumerable<PermissionKey> permissions = null,
            params String[] permissionUris)
            : this(text, action.GetT4MVCResult().RouteValueDictionary.ToUrl(), permissions, permissionUris) { }

        public MenuLink(
            String text,
            String url,
            IEnumerable<PermissionKey> permissions = null,
            params String[] permissionUris)
        {
            Text = text;
            Url = url;
            AnchorAttributes = new Dictionary<String, String>();
            ListItemAttributes = new Dictionary<String, String>();
            _Permitted = (
                (permissions == null) ||
                (permissionUris == null) ||
                (permissionUris.Length == 0) ||
                (permissionUris.Any(x => permissions.AnyPermits(new PermissionKey(x)))));
            _Visible = _Permitted;
        }


        public String Url { get; private set; }
        /// <summary>
        /// Always perform an HTML Escape when outputting this text.
        /// </summary>
        public String Text { get; private set; }
        /// <summary>
        /// Always perform an HTML Escape when outputting this text.
        /// </summary>
        public IDictionary<String, String> AnchorAttributes { get; private set; }
        /// <summary>
        /// Always perform an HTML Escape when outputting this text.
        /// </summary>
        public IDictionary<String, String> ListItemAttributes { get; private set; }
        /// <summary>
        /// Always perform an HTML Escape on all user-input being assigned to this property.
        /// </summary>
        public String PrependHtml { get; set; }
        /// <summary>
        /// Always perform an HTML Escape on all user-input being assigned to this property.
        /// </summary>
        public String AppendHtml { get; set; }
        public Boolean OpenNewPage { get; set; }
        public Boolean Selected { get; set; }
        public Boolean Visible
        {
            get { return _Visible; }
            set { if (_Permitted) _Visible = value; }
        }


        public HtmlTag ToHtmlTag()
        {
            return ToListItem();
        }

        public HtmlTag ToListItem()
        {
            if (!Visible)
            {
                return null;
            }

            var link = new HtmlTag("a")
                .Attr("href", Url)
                .Attr("target", OpenNewPage ? "_blank" : "_top")
                .AppendHtml(PrependHtml)
                .AppendHtml(Encoder.HtmlEncode(Text))
                .AppendHtml(AppendHtml);
            if (AnchorAttributes != null)
            {
                foreach (var attribute in AnchorAttributes)
                {
                    link.Attr(attribute.Key, attribute.Value);
                }
            }

            var listItem = new HtmlTag("li")
                .Append(link);
            if (Selected)
            {
                listItem.AddClass("active active-page");
            }
            if (ListItemAttributes != null)
            {
                foreach (var attribute in ListItemAttributes)
                {
                    listItem.Attr(attribute.Key, attribute.Value);
                }
            }

            return listItem;
        }
    }
}