﻿using CargoAware.WebApp.Models.Imports;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;

namespace CargoAware.WebApp.Infrastructure.DataImport
{
    public sealed class RegionManagerImportCsvClassMap : BaseCsvClassMap<RegionManagerImportModel>
    {
        public RegionManagerImportCsvClassMap()
        {
            Map(m => m.Epc).Name("tag_id").TypeConverter<StringConverter>();
            Map(m => m.X).Name("x_location").TypeConverter<DecimalConverter>();
            Map(m => m.Y).Name("y_location").TypeConverter<DecimalConverter>();
            Map(m => m.Z).Name("z_location").TypeConverter<DecimalConverter>();
            Map(m => m.Timestamp).Name("locate_time").TypeConverter<DateTimeConverter>();
        }

        public static CsvConfiguration CsvConfiguration
        {
            get
            {
                var result = GetNewCsvConfiguration();
                result.HasHeaderRecord = true;
                result.RegisterClassMap<RegionManagerImportCsvClassMap>();
                return result;
            }
        }
    }
}