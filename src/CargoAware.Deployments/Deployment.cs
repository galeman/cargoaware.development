﻿using CargoAware.Deployments.Infrastructure;
using System;

namespace CargoAware.Deployments
{
    public static class Deployment
    {
        public static void CleanViewDatabase(String connectionString)
        {
            new CleanRelationalDb(connectionString).Execute();
        }

        public static void MigrateViewDatabase(String connectionString)
        {
            new MigrateRelationalDb(connectionString, typeof(MigrateRelationalDb).Assembly).Execute();
        }
    }
}