﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150302095948)]
    public sealed class PrintJob : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("PrintJob")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("CreatedDateTimeUtc").AsCustom("datetimeoffset(0)").NotNullable()
                  .WithColumn("SubmittedDateTimeUtc").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("CancelRequestDateTimeUtc").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("CancelledDateTimeUtc").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("LabelType").AsByte().NotNullable()
                  .WithColumn("PrinterAddress").AsAnsiString(255).NotNullable()
                  .WithColumn("PrinterPort").AsInt16().NotNullable()
                  .WithColumn("PrinterExternalId").AsAnsiString(50).NotNullable().Indexed()
                  .WithColumn("AirWaybill").AsAnsiString(255).NotNullable()
                  .WithColumn("StartPiece").AsInt16().NotNullable()
                  .WithColumn("EndPiece").AsInt16().NotNullable()
                  .WithColumn("Uld").AsAnsiString(255).NotNullable();
        }
    }
}