﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Shipment
{
    public sealed class ShipmentRepository : RepositoryBase
    {
        public ShipmentRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public Paged<ShipmentDocument> GetShipments(
            EntityVisibleState visibleState = EntityVisibleState.VisibleOnly, Action<SqlBuilder> query = null)
        {
            return QueryShipments(builder =>
            {
                //switch (visibleState)
                //{
                //    case EntityVisibleState.NonVisibleOnly:
                //        builder.Where("s.IsVisible = 0");
                //        break;
                //    case EntityVisibleState.VisibleOnly:
                //        builder.Where("s.IsVisible = 1");
                //        break;
                //}

                if (query != null)
                {
                    query(builder);
                }
            });
        }

        public Paged<TagReadDocument> GetTagReads(long shipmentId, Action<SqlBuilder> query = null)
        {
            var result = QueryTagReads(builder =>
            {
                builder.Where("tr.ShipmentId = @shipmentId", new { shipmentId });

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }
        internal void InsertShipment(ShipmentDocument shipment)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
INSERT INTO dbo.Shipment
(
    FlightNumber,
    FlightDate,
    AirWaybill,
    AirWaybillTagEpc,
    AirWaybillTagDateTime,
    Uld,
    UldTagEpc,
    UldTagDateTime,
    OpenContainerDateTime,
    CloseContainerDateTime,
    Note,
    TagReadSegmentStart,
    TagReadSegmentEnd
) VALUES (
    @FlightNumber,
    @FlightDate,
    @AirWaybill,
    @AirWaybillTagEpc,
    @AirWaybillTagDateTime,
    @Uld,
    @UldTagEpc,
    @UldTagDateTime,
    @OpenContainerDateTime,
    @CloseContainerDateTime,
    @Note,
    @TagReadSegmentStart,
    @TagReadSegmentEnd
);", shipment);

                scope.Complete();
            }
        }

        internal void InsertTagReads(IEnumerable<TagReadDocument> tagReads)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                foreach (var tagRead in tagReads)
                {
                    String sql;

                    if (tagRead.FixedLocationRead)
                    {
                        sql = @"
DECLARE @centerPoint geometry;

SELECT TOP (1)
    @centerPoint = SpatialData.STCentroid()
FROM LocationZone
WHERE Id = @LocationZoneId;


INSERT INTO dbo.TagRead
(
    Epc,
    LocationId,
    RegionManagerId,
    X,
    Y,
    Z,
    Rssi,
    Readers,
    Timestamp,
    LocationZoneId,
    FixedLocationRead,
    IsProcessed,
) VALUES (
    @Epc,
    @LocationId,
    @RegionManagerId,
    @centerPoint.STX,
    @centerPoint.STY,
    0,
    @Rssi,
    @Readers,
    @Timestamp,
    @LocationZoneId,
    @FixedLocationRead,
    0
);";
                    }
                    else
                    {
                        sql = @"
INSERT INTO dbo.TagRead
(
    Epc,
    LocationId,
    RegionManagerId,
    X,
    Y,
    Z,
    Rssi,
    Readers,
    Timestamp,
    LocationZoneId,
    FixedLocationRead,
    IsProcessed
) VALUES (
    @Epc,
    @LocationId,
    @RegionManagerId,
    @X,
    @Y,
    @Z,
    @Rssi,
    @Readers,
    @Timestamp,
    (
        SELECT TOP (1) Id
        FROM LocationZone
        WHERE
            LocationId = @LocationId AND
            (ArchivedDateTimeUtc IS NULL OR ArchivedDateTimeUtc >= @Timestamp) AND
            SpatialData.STIntersects(geometry::STGeomFromText('POINT(' + CAST(@X AS varchar(50)) + ' ' + CAST(@Y AS varchar(50)) + ')', 0)) = 1
    ),
    @FixedLocationRead,
    0
);";
                    }

                    db.Execute(sql, tagRead);
                }

                scope.Complete();
            }
        }

        private Paged<ShipmentDocument> QueryShipments(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        s.Id,
        s.FlightNumber,
        s.FlightDate,
        s.AirWaybill,
        s.AirWaybillTagEpc,
        s.AirWaybillTagDateTime,
        s.Uld,
        s.UldTagEpc,
        s.UldTagDateTime,
        s.OpenContainerDateTime,
        s.CloseContainerDateTime,
        s.Note,
        s.TagReadSegmentStart,
        s.TagReadSegmentEnd,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwShipment s
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("s.FlightDate DESC, s.AirWaybill, s.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<ShipmentDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<ShipmentDocument>(data.Distinct(), total);
            }
        }
        private Paged<TagReadDocument> QueryTagReads(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        tr.Id,
        tr.Epc,
        tr.LocationId,
        tr.RegionManagerId,
        tr.X,
        tr.Y,
        tr.Z,
        tr.Rssi,
        tr.Readers,
        tr.[Timestamp],
        tr.LocationZoneId,
        tr.LocationZoneName,
        tr.FixedLocationRead,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
        --COALESCE(s.TagReadSegmentStart, DATEADD(day, -1, MAX(tr.[Timestamp]) OVER (PARTITION BY tr.Epc))) AS CalculatedTagReadSegmentStart
    FROM dbo.vwTagRead tr
    /**where**/
    /**having**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
-- WHERE [Timestamp] >= CalculatedTagReadSegmentStart
ORDER BY _row;");

                //builder.Where("(tr.TagReadSegmentStart IS NULL OR tr.[Timestamp] >= tr.TagReadSegmentStart)");
                //builder.Where("(tr.TagReadSegmentEnd IS NULL OR tr.[Timestamp] <= tr.TagReadSegmentEnd)");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("tr.[Timestamp], tr.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<TagReadDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<TagReadDocument>(data.Distinct(), total);
            }
        }

        #region Processed TagReads
        public Paged<TagReadDocument> GetTagReadsFromTable(long shipmentId, Action<SqlBuilder> query = null)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
DECLARE @Epc varchar(255)
DECLARE @T TABLE(Id Bigint,
   EPC varchar(255),
   LocationId bigint,
   RegionManagerId varchar(255),
   X float,
   Y float,
   Z float,
   Rssi smallint,
   Readers varchar(512),
[Timestamp] datetimeoffset,
LocationZoneId bigint,
LocationZoneName varchar(255),
FixedLocationRead bit,
IsProcessed bit)

SELECT TOP (1)
       @Epc = T.Epc
FROM TagRead AS T
INNER JOIN dbo.Shipment AS S
       ON T.Epc = S.AirWaybillTagEpc
       OR T.Epc = S.UldTagEpc
LEFT JOIN dbo.LocationZone AS LZ
       ON T.LocationZoneId = LZ.Id
WHERE (EPC <> @Epc
AND @Epc IS NOT NULL)
OR (@Epc IS NULL
AND IsProcessed = 0)
AND s.Id = @shipmentId 
ORDER BY T.Epc,T.LocationId, T.[Timestamp]

INSERT INTO @T
SELECT TOP (100)
       T.Id,
       T.Epc,
       T.LocationId,
       T.RegionManagerId,
       T.X,
       T.Y,
       T.Z,
       T.Rssi,
       T.Readers,
       T.[Timestamp],
       T.LocationZoneId,
       lz.Name AS LocationZoneName,
       T.FixedLocationRead,
       T.IsProcessed       
FROM TagRead AS T
INNER JOIN dbo.Shipment AS s
       ON T.Epc = s.AirWaybillTagEpc
       OR T.Epc = s.UldTagEpc
LEFT JOIN dbo.LocationZone AS lz
       ON T.LocationZoneId = lz.Id
WHERE s.Id = @shipmentId AND T.EPC = @Epc
AND T.IsProcessed = 0

ORDER BY T.Epc, T.LocationId, T.[Timestamp]

;WITH Cte AS(
SELECT
Id,
EPC,
LocationId,
RegionManagerId,
X,
Y,
Z,
Rssi,
Readers,
[Timestamp],
LocationZoneId,
LocationZoneName,
FixedLocationRead,
IsProcessed
FROM @T 
UNION ALL 
SELECT TOP (50)
       T.Id,
       T.Epc,
       T.LocationId,
       T.RegionManagerId,
       T.X,
       T.Y,
       T.Z,
       T.Rssi,
       T.Readers,
       T.[Timestamp],
       T.LocationZoneId,
       lz.Name AS LocationZoneName,
       T.FixedLocationRead,
       1
FROM TagReadProcessed AS T
INNER JOIN dbo.Shipment AS S
       ON T.Epc = S.AirWaybillTagEpc
       OR T.Epc = S.UldTagEpc
LEFT JOIN dbo.LocationZone AS lz
       ON T.LocationZoneId = lz.Id
WHERE s.Id = @shipmentId AND T.EPC = @Epc
ORDER BY Epc, T.LocationId, T.[Timestamp] DESC
),_pagination AS(
SELECT DENSE_RANK() OVER (ORDER BY Id ASC) AS FirstRow,
  DENSE_RANK() OVER (ORDER BY Id DESC) AS LastRow,
          DENSE_RANK() OVER (Order BY Epc,LocationId, Timestamp) AS _row
FROM cte 
),_totals AS (
SELECT COUNT_BIG(DISTINCT _row) AS LastRow
FROM _pagination
),FirstRow AS (
SELECT FirstRow
FROM _pagination
)
SELECT * FROM Cte
CROSS APPLY FirstRow,_totals
/**orderby**/");

                //builder.Where("s.Id = @shipmentId AND T.EPC = @Epc", new { shipmentId });
                builder.OrderBy("[Timestamp], Id");
                builder.ColumnAliases.Add("LocationZoneName", "lz.Name");
                builder.AddParameters(new { shipmentId });

                if (query != null)
                {
                    query(builder);
                }
                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<TagReadDocument>(i => total = i),
                    selector.Parameters,
                    buffered: false, //TODO: HA-GA Probar con o sin esta propiedad para ver si funciona correctamente.
                    splitOn: "FirstRow,LastRow");
                return new Paged<TagReadDocument>(data.Distinct(), total);
            }
        }

        private Paged<TagReadDocument> QueryTagReadsFromTable(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        tr.Id,
        tr.Epc,
        tr.LocationId,
        tr.RegionManagerId,
        tr.X,
        tr.Y,
        tr.Z,
        tr.Rssi,
        tr.Readers,
        tr.[Timestamp],
        tr.LocationZoneId,
        lz.Name AS LocationZoneName,
        tr.FixedLocationRead,
        DENSE_RANK() OVER (ORDER BY tr.Id ASC) AS FirstRow,
        DENSE_RANK() OVER (ORDER BY tr.Id DESC) AS LastRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
        --COALESCE(s.TagReadSegmentStart, DATEADD(day, -1, MAX(tr.[Timestamp]) OVER (PARTITION BY tr.Epc))) AS CalculatedTagReadSegmentStart
    FROM dbo.TagRead tr
        INNER JOIN dbo.Shipment s ON tr.Epc = s.AirWaybillTagEpc OR tr.Epc = s.UldTagEpc
        LEFT JOIN dbo.LocationZone lz ON tr.LocationZoneId = lz.Id
    /**where**/
    /**having**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
-- WHERE [Timestamp] >= CalculatedTagReadSegmentStart
ORDER BY _row;");

                //builder.Where("(tr.TagReadSegmentStart IS NULL OR tr.[Timestamp] >= tr.TagReadSegmentStart)");
                //builder.Where("(tr.TagReadSegmentEnd IS NULL OR tr.[Timestamp] <= tr.TagReadSegmentEnd)");
                builder.ColumnAliases.Add("LocationZoneName", "lz.Name");
                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("tr.[Timestamp], tr.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<TagReadDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<TagReadDocument>(data.Distinct(), total);
            }
        }

        internal void InsertProcessedTagReads(IEnumerable<TagReadDocument> tagReads)
        {
            //TODO:HA Verify X,Y,Z
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                foreach (var tagRead in tagReads)
                {
                    String sql;
                    sql = @"
DECLARE @centerPoint geometry;

SELECT TOP (1)
    @centerPoint = SpatialData.STCentroid()
FROM LocationZone
WHERE Id = @LocationZoneId;

IF NOT EXISTS(SELECT TOP(1) TRP.Id FROM dbo.TagReadProcessed AS TRP WHERE TRP.Id = @id)
BEGIN
INSERT INTO dbo.TagReadProcessed
(
    id,
    Epc,
    LocationId,
    RegionManagerId,
    X,
    Y,
    Z,
    Rssi,
    Readers,
    Timestamp,
    LocationZoneId,
    FixedLocationRead
) VALUES (
    @id,
    @Epc,
    @LocationId,
    @RegionManagerId,
    @X,
    @Y,
    @Z,
    @Rssi,
    @Readers,
    @Timestamp,
    @LocationZoneId,
    @FixedLocationRead
)
END
ELSE
BEGIN
    UPDATE dbo.TagReadProcessed
    SET epc = @Epc,
        locationid = @LocationId,
        regionManagerId = @RegionManagerId,
        x =  @X,
        y = @Y,
        z =  @Z,
        rssi = @Rssi,
        readers =  @Readers,
        [timestamp] =  @Timestamp,
        locationZoneId =  @LocationZoneId,
        fixedLocationRead =  @FixedLocationRead
     WHERE id = @id
END
";

                    db.Execute(sql, tagRead);
                }

                scope.Complete();
            }
        }

        internal void UpdateTagReadsProcessed(IEnumerable<TagReadDocument> tagReads)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                foreach (var tagRead in tagReads)
                {
                    String sql;
                    sql = @"
UPDATE dbo.TagRead
SET IsProcessed = 1
WHERE id = @id;";

                    db.Execute(sql, tagRead);
                }
                scope.Complete();
            }
        }

        public Paged<TagReadDocument> GetTagReadsProcessed(long shipmentId, Action<SqlBuilder> query = null)
        {
            var result = QueryProcessedTagReads(builder =>
            {
                builder.Where("tr.ShipmentId = @shipmentId", new { shipmentId });

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        private Paged<TagReadDocument> QueryProcessedTagReads(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        tr.Id,
        tr.Epc,
        tr.LocationId,
        tr.RegionManagerId,
        tr.X,
        tr.Y,
        tr.Z,
        tr.Rssi,
        tr.Readers,
        tr.[Timestamp],
        tr.LocationZoneId,
        tr.LocationZoneName,
        tr.FixedLocationRead,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
        --COALESCE(s.TagReadSegmentStart, DATEADD(day, -1, MAX(tr.[Timestamp]) OVER (PARTITION BY tr.Epc))) AS CalculatedTagReadSegmentStart
    FROM dbo.TagReadProcessed tr
    /**where**/
    /**having**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
-- WHERE [Timestamp] >= CalculatedTagReadSegmentStart
ORDER BY _row;");

                builder.Where("(tr.TagReadSegmentStart IS NULL OR tr.[Timestamp] >= tr.TagReadSegmentStart)");
                builder.Where("(tr.TagReadSegmentEnd IS NULL OR tr.[Timestamp] <= tr.TagReadSegmentEnd)");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("tr.[Timestamp], tr.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<TagReadDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<TagReadDocument>(data.Distinct(), total);
            }
        }
        #endregion
    }
}