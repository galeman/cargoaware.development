﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630144015)]
    public sealed class vwLocation : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwLocation') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwLocation AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwLocation 
                AS
                   SELECT
                        l.Id,
                        l.Name,
                        l.AxisDirection,
                        l.OriginLocation,
                        l.ApiKey,
                        l.IsArchived
                    FROM dbo.Location l;");
        }
    }
}

