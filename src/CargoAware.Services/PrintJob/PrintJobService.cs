﻿using System.Globalization;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using CargoAware.Services.KeyValue;
using CargoAware.Services.LabelTemplate;
using CargoAware.Services.Printer;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.PrintJob
{
    public static class PrintJobService
    {
        private static Lazy<KeyValueRepository> _KeyValueRepository;
        private static Lazy<PrinterRepository> _PrinterRepository;
        private static Lazy<PrintJobRepository> _PrintJobRepository;


        static PrintJobService()
        {
            Inject(Repositories.ForKeyValues);
            Inject(Repositories.ForPrinters);
            Inject(Repositories.ForPrintJobs);
        }

        private static void Inject(Lazy<KeyValueRepository> dependency)
        {
            _KeyValueRepository = dependency;
        }

        private static void Inject(Lazy<PrinterRepository> dependency)
        {
            _PrinterRepository = dependency;
        }

        private static void Inject(Lazy<PrintJobRepository> dependency)
        {
            _PrintJobRepository = dependency;
        }


        /// <param name="printJobData">Required: AirWaybill, Destination, Pieces, Origin, Weight, UnitOfMeasure, Service, HandlingCodes, TransfersInfo, TransfersDateTime</param>
        /// <param name="labelCount">Required</param>
        /// <param name="printerId">printerId or externalId required</param>
        /// <param name="externalId">printerId or externalId required</param>
        public static Tuple<PrintJobDocument, List<PrintJobDataDocument>> PrintAirWaybillLabels(PrintJobDataDocument printJobData, int labelCount, long printerId = 0, String externalId = null)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            long numericAwb;
            if (!long.TryParse(printJobData.AirWaybill, out numericAwb))
            {
                throw new DomainException("Invalid Air Waybill number specified {0}", printJobData.AirWaybill);
            }

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            PrinterDocument printer = null;
            if (printerId != 0)
            {
                printer = _PrinterRepository.Value.GetPrinterById(printerId);
            }
            else if (!String.IsNullOrWhiteSpace(externalId))
            {
                printer = _PrinterRepository.Value.GetPrintersByExternalId(externalId).FirstOrDefault();
            }

            if (printer == null)
            {
                throw new DomainException("Unable to print the Air Waybill label because no printer was specified, or the specified printer doesn't exist.");
            }

            //var location = _LocationRepository.Value.GetLocationById(printer.LocationId);
            var airlinePrefix = _KeyValueRepository.Value.GetKeyValue(
                KeyValueRepository.KnownApplications.CargoAware,
                KeyValueRepository.KnownKeys.AirlinePrefix).Value;
            var lastAirWaybill = _PrintJobRepository.Value.GetLastPrintJob(printJobData.AirWaybill);
            var startPiece = (Int16)(lastAirWaybill != null ? lastAirWaybill.EndPiece + 1 : 1);
            var endPiece = (Int16)(startPiece + labelCount - 1);

            var printJob = new PrintJobDocument
            {
                LocationId = printer.LocationId,
                LabelType = LabelType.AirWaybill,
                PrinterAddress = printer.Address,
                PrinterPort = printer.Port,
                PrinterExternalId = printer.ExternalId,
                AirWaybill = printJobData.AirWaybill,
                StartPiece = startPiece,
                EndPiece = endPiece,
                Uld = "",
            };
            var labels = new List<PrintJobDataDocument>(labelCount);

            for (var i = startPiece; i <= endPiece; i++)
            {
                labels.Add(new PrintJobDataDocument
                {
                    Epc = AirWaybillFormatting.GetEpc(airlinePrefix, printJobData.AirWaybill, i),
                    Barcode = printJobData.AirWaybill + i.ToString("00000"),
                    PieceNumber = i,
                    AirWaybill = printJobData.AirWaybill,
                    Uld = "",
                    Destination = printJobData.Destination,
                    Pieces = printJobData.Pieces,
                    Origin = printJobData.Origin,
                    Weight = printJobData.Weight,
                    UnitOfMeasure = printJobData.UnitOfMeasure,
                    Service = printJobData.Service,
                    HandlingCodes = printJobData.HandlingCodes,
                    TransfersInfo = printJobData.TransfersInfo,
                    TransfersDateTime = printJobData.TransfersDateTime
                });
            }

            // Step 3: Does this command pass business rules?

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrintJobRepository.Value.Insert(printJob, labels);
                scope.Complete();
            }

            return Tuple.Create(printJob, labels);
        }

        /// <param name="printJobData">Required: ULD, Destination, Origin</param>
        /// <param name="locationId">Required</param>
        public static Tuple<PrintJobDocument, List<PrintJobDataDocument>> PrintUldLabel(PrintJobDataDocument printJobData, long locationId)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            var printer = _PrinterRepository.Value.GetUldPrintersByLocationId(locationId).FirstOrDefault();
            if (printer == null)
            {
                throw new DomainException("Unable to print the ULD label because there is no default ULD printer setup.");
            }

            var airlinePrefix = _KeyValueRepository.Value.GetKeyValue(
                KeyValueRepository.KnownApplications.CargoAware,
                KeyValueRepository.KnownKeys.AirlinePrefix).Value;
            var uldCounter = _KeyValueRepository.Value.GetKeyValue(
                KeyValueRepository.KnownApplications.CargoAware,
                KeyValueRepository.KnownKeys.UldCounter);
            long serialNumber;
            if (uldCounter == null || !long.TryParse(uldCounter.Value, out serialNumber))
            {
                serialNumber = 1;
            }

            var printJob = new PrintJobDocument
            {
                LocationId = printer.LocationId,
                LabelType = LabelType.ULD,
                PrinterAddress = printer.Address,
                PrinterPort = printer.Port,
                PrinterExternalId = printer.ExternalId,
                AirWaybill = "",
                StartPiece = 0,
                EndPiece = 0,
                Uld = printJobData.Uld,
            };

            var labels = new List<PrintJobDataDocument>
            {
                new PrintJobDataDocument
                {
                    Epc = UldFormatting.GetEpc(airlinePrefix, serialNumber),
                    Barcode = printJobData.Uld,
                    PieceNumber = 0,
                    AirWaybill = "",
                    Uld = printJobData.Uld,
                    Destination = printJobData.Destination,
                    Pieces = 0,
                    Origin = printJobData.Origin,
                    Weight = 0,
                    UnitOfMeasure = "",
                    Service = "",
                    HandlingCodes = "",
                    TransfersInfo = new String[0],
                    TransfersDateTime = new String[0]
                }
            };

            // Step 3: Does this command pass business rules?

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _KeyValueRepository.Value.SetKeyValue(
                    KeyValueRepository.KnownApplications.CargoAware,
                    KeyValueRepository.KnownKeys.UldCounter,
                    (serialNumber + 1).ToString(CultureInfo.InvariantCulture));
                _PrintJobRepository.Value.Insert(printJob, labels);
                scope.Complete();
            }

            return Tuple.Create(printJob, labels);
        }

        public static void PrintJobsSubmitted(params long[] printJobIds)
        {
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrintJobRepository.Value.PrintJobsSubmitted(printJobIds);
                scope.Complete();
            }
        }

        public static void PrintJobDataPrinted(long printJobDataId, DateTime printedDateTime)
        {
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrintJobRepository.Value.PrintJobDataPrinted(printJobDataId, printedDateTime);
                scope.Complete();
            }
        }

        /// <param name="message">Required: Id</param>
        public static void CancelPrintJob(PrintJobDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var doesPrintJobExist = _PrintJobRepository.Value.GetPrintJobById(message.Id);
            if (doesPrintJobExist == null)
            {
                throw new DomainException("The specified PrintJob does not exist.");
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrintJobRepository.Value.CancelPrintJob(message.Id);
                scope.Complete();
            }
        }

        public static void PrintJobsCancelled(params long[] printJobIds)
        {
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _PrintJobRepository.Value.PrintJobsCancelled(printJobIds);
                scope.Complete();
            }
        }
    }
}