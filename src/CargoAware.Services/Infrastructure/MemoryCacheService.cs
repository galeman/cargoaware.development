﻿using System;
using System.Runtime.Caching;

namespace CargoAware.Services.Infrastructure
{
    public static class MemoryCacheService
    {
        public static T Get<T>(String name)
           where T : class
        {
            return (MemoryCache.Default.Get(name) as T);
        }

        public static T GetOrAdd<T>(
            String name, Func<T> value, TimeSpan cacheTime, Boolean slidingExpiration = false,
            Boolean removableOnLowMemory = true)
            where T : class
        {
            var result = Get<T>(name);
            if (result == null)
            {
                result = value();
                if (result != null)
                {
                    Set(name, result, cacheTime, slidingExpiration, removableOnLowMemory);
                }
            }
            return result;
        }

        public static void Set(String name, dynamic value, TimeSpan cacheTime, Boolean slidingExpiration = false, Boolean removableOnLowMemory = true)
        {
            var cacheItem = new CacheItem(name, value);
            var cacheItemPolicy = new CacheItemPolicy();

            if (cacheTime == TimeSpan.Zero)
            {
                cacheItemPolicy.AbsoluteExpiration = ObjectCache.InfiniteAbsoluteExpiration;
            }
            else if (slidingExpiration)
            {
                cacheItemPolicy.SlidingExpiration = cacheTime;
            }
            else
            {
                cacheItemPolicy.AbsoluteExpiration = new DateTimeOffset(DateTime.Now + cacheTime);
            }
            if (!removableOnLowMemory)
            {
                cacheItemPolicy.Priority = CacheItemPriority.NotRemovable;
            }

            MemoryCache.Default.Set(cacheItem, cacheItemPolicy);
        }

        public static void Remove(String name)
        {
            MemoryCache.Default.Remove(name);
        }
    }
}