﻿using System;
using System.Collections.Generic;

namespace CargoAware.Services.PrintJob
{
    public class PrintJobDataDocument
    {
        public long Id { get; set; }
        public String Epc { get; set; }
        public String Barcode { get; set; }
        public Int16 PieceNumber { get; set; }
        public String AirWaybill { get; set; }
        public String Uld { get; set; }
        public String Destination { get; set; }
        public Int16 Pieces { get; set; }
        public String Origin { get; set; }
        public Double Weight { get; set; }
        public String UnitOfMeasure { get; set; }
        public String Service { get; set; }
        public String HandlingCodes { get; set; }
        public IEnumerable<String> TransfersInfo { get; set; }
        public IEnumerable<String> TransfersDateTime { get; set; }
        public DateTime? PrintedDateTime { get; set; }

        internal long PrintJobDataId { set { Id = value; } }
        internal String AirWaybillDapper { set { AirWaybill = value; } }
        internal String UldDapper { set { Uld = value; } }
        internal String TransfersInfoAsString
        {
            get { return String.Join("||", TransfersInfo); }
            set { TransfersInfo = value.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries); }
        }
        internal String TransfersDateTimeAsString
        {
            get { return String.Join("||", TransfersDateTime); }
            set { TransfersDateTime = value.Split(new[] { "||" }, StringSplitOptions.RemoveEmptyEntries); }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as PrintJobDataDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}