﻿using System;

namespace CargoAware.Services.Shipment
{
    public sealed class ShipmentDocument
    {
        public long Id { get; set; }
        public String FlightNumber { get; set; }
        public DateTime FlightDate { get; set; }
        public String AirWaybill { get; set; }
        public String AirWaybillTagEpc { get; set; }
        public DateTimeOffset? AirWaybillTagDateTime { get; set; }
        public String Uld { get; set; }
        public String UldTagEpc { get; set; }
        public DateTimeOffset? UldTagDateTime { get; set; }
        public DateTimeOffset? OpenContainerDateTime { get; set; }
        public DateTimeOffset? CloseContainerDateTime { get; set; }
        public String Note { get; set; }
        public DateTimeOffset? TagReadSegmentStart { get; set; }
        public DateTimeOffset? TagReadSegmentEnd { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as ShipmentDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}