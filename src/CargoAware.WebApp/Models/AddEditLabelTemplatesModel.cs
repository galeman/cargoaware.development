﻿using CargoAware.Services.LabelTemplate;
using System;
using System.Collections.Generic;

namespace CargoAware.WebApp.Models
{
    public class AddEditLabelTemplatesModel
    {
        public Boolean Adding { get; set; }
        public IEnumerable<LabelTemplateDocument> LabelTemplates { get; set; }
        public IEnumerable<AddEditLabelTemplatesDetailsModel> Details { get; set; }
    }

    public class AddEditLabelTemplatesDetailsModel
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public LabelType? Type { get; set; }
        public TemplateLanguage? Language { get; set; }
        public String Code { get; set; }
        public long Version { get; set; }
        public Boolean IsActive { get; set; }
    }
}