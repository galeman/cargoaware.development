﻿using CargoAware.Services.DataProcessing;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using CargoAware.Services.Location;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Shipment
{
    public static class ShipmentService
    {
        private static Lazy<LocationRepository> _LocationRepository;
        private static Lazy<ShipmentRepository> _ShipmentRepository;


        static ShipmentService()
        {
            Inject(Repositories.ForLocations);
            Inject(Repositories.ForShipments);
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        private static void Inject(Lazy<ShipmentRepository> dependency)
        {
            _ShipmentRepository = dependency;
        }


        /// <param name="shipment">
        /// <para>Requires: FlightNumber, FlightDate, AirWaybill, AirWaybillTagEpc, Uld, UldTagEpc, Note</para>
        /// <para>Optional: AirWaybillTagDateTime, UldTagDateTime, OpenContainerDateTime, CloseContainerDateTime, TagReadSegmentStart, TagReadSegmentEnd</para>
        /// </param>
        public static void CreateShipment(ShipmentDocument shipment)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            shipment.Id = 0;

            // Step 3: Does this command pass business rules? Always.

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _ShipmentRepository.Value.InsertShipment(shipment);
                scope.Complete();
            }
        }

        /// <param name="apiKey">Required</param>
        /// <param name="tagReads">
        /// <para>Requires: Epc, X, Y, Z, Rssi, Readers, Timestamp</para>
        /// <para>Optional: RegionManagerId</para>
        /// </param>
        public static void InsertTagReads(String apiKey, IEnumerable<TagReadDocument> tagReads)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);

            // Step 3: Does this command pass business rules?

            if (location == null)
            {
                throw new DomainException("The specified API Key does not exist.");
            }

            // Step 4: Update the view in a single transaction.

            InsertTagReads(location.Id, tagReads);
        }

        /// <param name="locationId">Required</param>
        /// <param name="tagReads">
        /// <para>Requires: Epc, X, Y, Z, Rssi, Readers, Timestamp</para>
        /// <para>Optional: RegionManagerId</para>
        /// </param>
        public static void InsertTagReads(long locationId, IEnumerable<TagReadDocument> tagReads)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            var tagReadsArray = tagReads.ToArray();

            foreach (var tagRead in tagReadsArray)
            {
                tagRead.LocationId = locationId;
            }

            // Step 3: Does this command pass business rules?

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _ShipmentRepository.Value.InsertTagReads(tagReadsArray);
                scope.Complete();
            }
        }

        #region Process tagReads
        public static void InsertTagReadsProcessed(String apiKey)
        {
            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);
            if (location == null)
            {
                throw new DomainException("The specified API Key does not exist.");
            }

            //TODO: HA-GA:
            // - Obetener los tagreads de 150 en 150 por epc
            // -- filtarlos
            // -- SKIP 51
            // -- insertar filtrados en tagreadsprocessed
            //-- actualizaar los proceseados en tagread IsProcessed = 1
            // Obtener los siguiente 150
            var i = 0;
            do
            {
                var tagsNotP = _ShipmentRepository.Value.GetTagReadsFromTable(location.Id);
                i = tagsNotP.Data.Count;
                var processedTags = TakacsTimeBased.Filter(tagsNotP.Data, applyAverage: true).ToArray();
                InsertUpdateTagReadsProcessed(location.Id, processedTags.Skip(51), tagsNotP.Data);
            } while (i > 0);
        }

        /// <summary>
        /// Inserts the processed tags to the TagReadProcessed table and updates the value of all the tags to IsProcessed
        /// </summary>
        /// <param name="locationId"></param>
        /// <param name="processedTags"></param>
        /// <param name="rawTagReads"></param>
        public static void InsertUpdateTagReadsProcessed(long locationId, IEnumerable<TagReadDocument> processedTags, IEnumerable<TagReadDocument> rawTagReads)
        {
            var tagReadsArray = processedTags.ToArray();
            foreach (var tagRead in tagReadsArray)
            {
                tagRead.LocationId = locationId;
            }
            //It is do it like this to use the same transaction so if one operation fails no changes are maid
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _ShipmentRepository.Value.InsertProcessedTagReads(tagReadsArray);
                _ShipmentRepository.Value.UpdateTagReadsProcessed(rawTagReads);
                scope.Complete();
            }
        }
        #endregion
    }
}