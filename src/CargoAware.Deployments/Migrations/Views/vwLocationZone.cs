﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630144926)]
    public sealed class vwLocationZone : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwLocationZone') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwLocationZone AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwLocationZone 
                AS
                   SELECT
                        lz.Id,
                        lz.LocationId,
                        lz.Name,
                        lz.LmsZoneId,
                        lz.AutoUpdateLms,
                        lz.LmsUpdateDelaySeconds,
                        lz.EnvoyZoneId,
                        lz.SpatialData,
                        lz.ArchivedDateTimeUtc
                    FROM dbo.LocationZone lz;");
        }
    }
}
