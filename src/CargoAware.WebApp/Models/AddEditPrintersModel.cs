﻿using CargoAware.Services.Printer;
using System;
using System.Collections.Generic;

namespace CargoAware.WebApp.Models
{
    public class AddEditPrintersModel
    {
        public Boolean Adding { get; set; }
        public IEnumerable<PrinterDocument> Printers { get; set; }
        public IEnumerable<AddEditPrintersDetailsModel> Details { get; set; }
    }

    public class AddEditPrintersDetailsModel
    {
        public long Id { get; set; }
        public String Name { get; set; }
        public PrinterType? Type { get; set; }
        public String Address { get; set; }
        public Int16 Port { get; set; }
        public String ExternalId { get; set; }
        public Boolean UldDefault { get; set; }
    }
}