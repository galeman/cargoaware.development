﻿using System;
using System.Web.Helpers;

namespace CargoAware.Web.Infrastructure.Security
{
    public static class AntiForgeryHelper
    {
        public static String GetTokenHeaderValue()
        {
            String cookieToken;
            String formToken;

            AntiForgery.GetTokens(null, out cookieToken, out formToken);
            var result = String.Format("{0}:{1}", cookieToken, formToken);
            return result;
        }
    }
}