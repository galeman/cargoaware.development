﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Event;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.LocationZone;
using CargoAware.Services.Shipment;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api.ExternalServices
{
    [RoutePrefix("envoy")]
    public sealed class EnvoyApiController : ApiController
    {
        private static Lazy<EventRepository> _EventRepository;
        private static Lazy<LocationZoneRepository> _LocationZoneRepository;


        #region // Constructor + DI
        // ==================================================

        static EnvoyApiController()
        {
            Inject(Repositories.ForEvents);
            Inject(Repositories.ForLocationZones);
        }

        private static void Inject(Lazy<EventRepository> dependency)
        {
            _EventRepository = dependency;
        }

        private static void Inject(Lazy<LocationZoneRepository> dependency)
        {
            _LocationZoneRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [GET(""), HttpGet] // used for testing
        [POST(""), HttpPost]
        public async Task<HttpResponseMessage> Index(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return
                TryProcessTagReads(requestBody) // ||
                // TryProcess__(requestBody) // ||
                // TryProcess__(requestBody)
                    ? Request.CreateResponse(HttpStatusCode.OK)
                    : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("tagreads"), HttpGet] // used for testing
        [POST("tagreads"), HttpPost]
        public async Task<HttpResponseMessage> TagReads(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return TryProcessTagReads(requestBody)
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private Boolean TryProcessTagReads(String requestBody)
        {
            try
            {
                var tagReads = JsonConvert.DeserializeObject<EnvoyTagReadsDto>(requestBody);

                var locationZoneId = _LocationZoneRepository.Value.GetLocationZoneIdByEnvoyZone(tagReads.LogicalDevice);
                if (locationZoneId == 0)
                {
                    throw new DomainException("The Envoy Zone \"{0}\" does not exist.", tagReads.LogicalDevice);
                }

                _EventRepository.Value.Insert(new EventDocument
                {
                    Timestamp = DateTimeOffset.Now,
                    EventSource = EventSource.Envoy,
                    EventName = "Envoy/TagReads",
                    Message = requestBody
                });
                ShipmentService.InsertTagReads(tagReads.DeviceName, tagReads.Data.Select(x => new TagReadDocument
                {
                    Epc = x.Tag,
                    Rssi = (Int16)x.Rssi,
                    Readers = "",
                    Timestamp = tagReads.Timestamp,
                    LocationZoneId = locationZoneId,
                    FixedLocationRead = true
                }));
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        // ==================================================
        #endregion


        #region // DTO Classes
        // ==================================================

        public class EnvoyTagReadsDto
        {
            public String DeviceName { get; set; }
            [JsonConverter(typeof(EnvoyDateTimeConverter))]
            public DateTime Timestamp { get; set; }
            public String LogicalDevice { get; set; }
            public IEnumerable<TagReadDto> Data { get; set; }

            public class TagReadDto
            {
                public String Tag { get; set; }
                public Double Rssi { get; set; }
            }
        }

        // ==================================================
        #endregion


        #region // Private Classes
        // ==================================================

        public class EnvoyDateTimeConverter : IsoDateTimeConverter
        {
            public EnvoyDateTimeConverter()
            {
                DateTimeFormat = "yyyy-MM-ddTHH:mm:ss.fffzzz";
            }
        }

        // ==================================================
        #endregion
    }
}