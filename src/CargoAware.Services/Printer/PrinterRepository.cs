﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Printer
{
    public sealed class PrinterRepository : RepositoryBase
    {
        public PrinterRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public bool DoesPrinterWithSameNameExist(long locationId, String name, long? excludedId = null)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var sql = "SELECT TOP (1) 1 FROM Printer WHERE LocationId = @locationId AND Name = @name";

                if (excludedId.HasValue)
                {
                    sql = sql + " AND Id <> @excludedId";
                }

                return db.QueryBoolean(sql, new { locationId, name, excludedId });
            }
        }

        public PrinterDocument GetPrinterById(long id)
        {
            var result = QueryPrinters(builder => builder.Where("pr.Id = @id", new { id })).Data;
            return result.FirstOrDefault();
        }

        public long GetPrinterIdByName(String name)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM Printer
/**where**/;");

                builder.Where("Name = @name", new { name });

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        public Paged<PrinterDocument> GetPrinters(long locationId, Action<SqlBuilder> query = null)
        {
            var result = QueryPrinters(builder =>
            {
                builder.Where("pr.LocationId = @locationId", new { locationId });

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        public IEnumerable<PrinterDocument> GetPrintersByExternalId(String externalId)
        {
            var result = QueryPrinters(builder => builder.Where("pr.ExternalId = @externalId", new { externalId })).Data;
            return result;
        }

        public IEnumerable<PrinterDocument> GetUldPrintersByLocationId(long locationId)
        {
            var result = QueryPrinters(builder => builder.Where("pr.LocationId = @locationId AND pr.UldDefault = 1", new { locationId })).Data;
            return result;
        }

        internal void InsertOrUpdate(PrinterDocument entity)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE Printer
SET Name = @Name,
    [Type] = @Type,
    Address = @Address,
    Port = @Port,
    ExternalId = @ExternalId,
    UldDefault = @UldDefault
WHERE Id = @Id;
IF @@ERROR = 0 AND @@ROWCOUNT = 0
    INSERT INTO Printer (
        LocationId,
        Name,
        [Type],
        Address,
        Port,
        Status,
        ExternalId,
        UldDefault
    ) VALUES (
        @LocationId,
        @Name,
        @Type,
        @Address,
        @Port,
        'Unknown',
        @ExternalId,
        @UldDefault
    );", entity);

                scope.Complete();
            }
        }

        internal void UpdatePrinterStatus(long id, String status)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE Printer
SET Status = @status
WHERE Id = @id;", new { id, status });

                scope.Complete();
            }
        }

        internal void Archive(long id)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
DELETE FROM Printer
WHERE Id = @id;", new { id });

                scope.Complete();
            }
        }

        private Paged<PrinterDocument> QueryPrinters(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        pr.Id,
        pr.LocationId,
        pr.Name,
        pr.[Type],
        pr.Address,
        pr.Port,
        pr.Status,
        pr.ExternalId,
        pr.UldDefault,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwPrinter pr
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("pr.Name, pr.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<PrinterDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<PrinterDocument>(data.Distinct(), total);
            }
        }
    }
}