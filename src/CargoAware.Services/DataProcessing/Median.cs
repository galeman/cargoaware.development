﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Shipment;
using MathNet.Numerics.Statistics;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.DataProcessing
{
    public static class Median
    {
        /// <summary>
        /// Returns a copy of the data processed ordered by Timestamp, then by Id.
        /// </summary>
        /// <param name="data">Data to filter.</param>
        /// <param name="samples">Number of samples to consider for the Median calculation.</param>
        public static IEnumerable<TagReadDocument> Filter(IEnumerable<TagReadDocument> data, int samples = 10)
        {
            var sortedDataArray = data.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
            var result = sortedDataArray.Select(x => x.CopyCommonPropertiesTo<TagReadDocument>()).ToArray();

            if (samples < 2)
            {
                samples = 2;
            }

            //var filter = new OnlineMedianFilter(samples);
            //var medianXaxis = filter.ProcessSamples(result.Select(x => (Double)x.X).ToArray());
            //var medianYaxis = filter.ProcessSamples(result.Select(x => (Double)x.Y).ToArray());

            //MathNet.Numerics.Statistics.Statistics.
            //var pointsToIgnore = samples >> 1; // divided by 2
            //Array.Copy(result, pointsToIgnore, result, 0, result.Length - pointsToIgnore);
            //Array.Resize(ref result, result.Length - pointsToIgnore);

            //for (var i = 0; i < result.Length; i++)
            //{
            //    result[i].X = (Decimal)medianXaxis[i + pointsToIgnore];
            //    result[i].Y = (Decimal)medianYaxis[i + pointsToIgnore];
            //}

            // Skip the first value
            for (var i = 1; i < result.Length; i++)
            {
                //  1 :  1 - 10 = Min(-9|0) = 0, Min(2|10) = 2
                //  2 :  2 - 10 = Min(-8|0) = 0, Min(3|10) = 3
                //  ...
                //  9 :  9 - 10 = Min(-1|0) = 0, Min(10|10) = 10
                // 15 : 15 - 10 = Min(5|0)  = 5, Min(15|10) = 10

                var startingPoint = Math.Max(i - samples, 0);
                var tagReadsInWindow = sortedDataArray
                    .Skip(startingPoint)
                    .Take(Math.Min(i + 1, samples)) // including the current entry
                    .ToArray();
                var xValues = tagReadsInWindow.Select(x => (Double)x.X).ToArray();
                var yValues = tagReadsInWindow.Select(y => (Double)y.Y).ToArray();
                var xMedian = xValues.Median();
                var yMedian = yValues.Median();

                var entry = result[i];
                entry.X = (Decimal)xMedian;
                entry.Y = (Decimal)yMedian;
            }

            return result;
        }
    }
}