﻿namespace CargoAware.Services.Common
{
    public enum EventSource : byte
    {
        // ReSharper disable InconsistentNaming
        JobService,
        ITCS,
        Envoy,
        LMS,
        iLynx
        // ReSharper restore InconsistentNaming
    }
}