﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150301142132)]
    public sealed class Printer : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("Printer")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("Name").AsAnsiString(255).NotNullable()
                  .WithColumn("Type").AsByte().NotNullable()
                  .WithColumn("Address").AsAnsiString(255).NotNullable()
                  .WithColumn("Port").AsInt16().NotNullable()
                  .WithColumn("Status").AsAnsiString(255).NotNullable()
                  .WithColumn("ExternalId").AsAnsiString(50).NotNullable().Indexed()
                  .WithColumn("UldDefault").AsBoolean().NotNullable();
        }
    }
}