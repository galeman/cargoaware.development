﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.LocationZone
{
    public static class LocationZoneService
    {
        private static Lazy<LocationZoneRepository> _LocationZoneRepository;


        static LocationZoneService()
        {
            Inject(Repositories.ForLocationZones);
        }

        private static void Inject(Lazy<LocationZoneRepository> dependency)
        {
            _LocationZoneRepository = dependency;
        }


        /// <param name="message">
        /// Required: LocationId, Name,
        ///     LmsZoneId (can be blank, must not be null), AutoUpdateLms, LmsUpdateDelaySeconds (can be zero)
        ///     EnvoyZoneId (can be blank, must not be null)
        /// </param>
        /// <param name="points">Required: X, Y</param>
        public static LocationZoneDocument CreateLocationZone(LocationZoneDocument message, IEnumerable<Point3D> points)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            message.Id = 0;
            var pointsList = points.ToList();

            // Step 3: Does this command pass business rules?

            var sameNameLocationZoneExists = _LocationZoneRepository.Value.DoesLocationZoneWithSameNameExist(message.LocationId, message.Name);
            if (sameNameLocationZoneExists)
            {
                throw new DomainException("The Location Zone \"{0}\" already exists.", message.Name);
            }

            if (pointsList.Count < 3)
            {
                throw new DomainException("Location Zone \"{0}\" requires a minimum of three Points.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            var firstPoint = pointsList.First();
            if (firstPoint != pointsList.Last())
            {
                pointsList.Add(firstPoint);
            }

            long locationZoneId;
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LocationZoneRepository.Value.InsertOrUpdate(message, pointsList);
                locationZoneId = _LocationZoneRepository.Value.GetLocationZoneIdByName(message.Name);
                scope.Complete();
            }

            var result = _LocationZoneRepository.Value.GetLocationZoneById(locationZoneId);
            return result;
        }

        /// <param name="message">
        /// Required: Id, Name,
        ///     LmsZoneId (can be blank, must not be null), AutoUpdateLms, LmsUpdateDelaySeconds (can be zero)
        ///     EnvoyZoneId (can be blank, must not be null)
        /// </param>
        /// <param name="points">Required: X, Y</param>
        public static void UpdateLocationZone(LocationZoneDocument message, IEnumerable<Point3D> points)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            var pointsList = points.ToList();

            // Step 3: Does this command pass business rules?

            var existingLocationZone = _LocationZoneRepository.Value.GetLocationZoneById(message.Id);
            if (existingLocationZone == null)
            {
                throw new DomainException("The specified Location Zone, \"{0}\", does not exist.", message.Name);
            }

            var sameNameLocationZoneExists = _LocationZoneRepository.Value.DoesLocationZoneWithSameNameExist(message.LocationId, message.Name, message.Id);
            if (sameNameLocationZoneExists)
            {
                throw new DomainException("A Location Zone named \"{0}\" already exists.", message.Name);
            }

            if (pointsList.Count < 3)
            {
                throw new DomainException("Location Zone \"{0}\" requires a minimum of three Points.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            var firstPoint = pointsList.First();
            if (firstPoint != pointsList.Last())
            {
                pointsList.Add(firstPoint);
            }

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LocationZoneRepository.Value.InsertOrUpdate(message, pointsList);
                scope.Complete();
            }
        }

        /// <param name="message">Required: Id</param>
        public static void ArchiveLocationZone(LocationZoneDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var doesLocationZoneExist = _LocationZoneRepository.Value.GetLocationZoneById(message.Id);
            if (doesLocationZoneExist == null)
            {
                throw new DomainException("The specified Location Zone does not exist.");
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LocationZoneRepository.Value.Archive(message.Id);
                scope.Complete();
            }
        }
    }
}