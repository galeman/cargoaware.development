﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace CargoAware.Services.Infrastructure
{
    public static class Security
    {
        public static readonly Func<Byte[], String, String> Decrypt =
            DecryptToken("^~6k$;lxaf1Z&:N9U@ANY&^#%'+n5+[d", "AE>c+>3,ROucOO/.Q");

        public static readonly Func<String, String, Byte[]> Encrypt =
            EncryptToken("^~6k$;lxaf1Z&:N9U@ANY&^#%'+n5+[d", "AE>c+>3,ROucOO/.Q");

        public static readonly Func<String, String, Byte[]> Hash =
            HashKey("4K=dO?;aa?itp{G&wQVXez9}");


        #region // Private Methods
        // ==================================================

        private static Func<String, String, Byte[]> HashKey(String key)
        {
            var encoding = new UTF8Encoding();
            var hmac = new HMACSHA1(encoding.GetBytes(key));
            return (password, salt) =>
            {
                var combined = String.Concat(password, salt);
                var toHash = encoding.GetBytes(combined);
                var hash = hmac.ComputeHash(toHash);
                return hash;
            };
        }

        private static Func<String, String, Byte[]> EncryptToken(String key, String vector)
        {
            var encoding = new UTF8Encoding();
            var encryptor = new RijndaelManaged().CreateEncryptor(encoding.GetBytes(key), encoding.GetBytes(vector));
            return (token, salt) =>
            {
                var combined = String.Concat(salt, token);
                var toEncrypt = encoding.GetBytes(combined);
                Byte[] encrypted;
                using (var ms = new MemoryStream())
                using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                {
                    cs.Write(toEncrypt, 0, toEncrypt.Length);
                    cs.FlushFinalBlock();
                    ms.Position = 0;
                    encrypted = new Byte[ms.Length];
                    ms.Read(encrypted, 0, encrypted.Length);
                }
                return encrypted;
            };
        }

        private static Func<Byte[], String, String> DecryptToken(String key, String vector)
        {
            var encoding = new UTF8Encoding();
            var decryptor = new RijndaelManaged().CreateDecryptor(encoding.GetBytes(key), encoding.GetBytes(vector));
            return (encrypted, salt) =>
            {
                String combined;
                using (var ms = new MemoryStream())
                using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Write))
                {
                    cs.Write(encrypted, 0, encrypted.Length);
                    cs.FlushFinalBlock();
                    ms.Position = 0;
                    var decrypted = new Byte[ms.Length];
                    ms.Read(decrypted, 0, decrypted.Length);
                    ms.Close();
                    combined = encoding.GetString(decrypted);
                }
                if (!combined.StartsWith(salt))
                {
                    throw new InvalidOperationException("Salts do not match.");
                }
                return combined.Substring(salt.Length, combined.Length - salt.Length);
            };
        }

        // ==================================================
        #endregion
    }
}