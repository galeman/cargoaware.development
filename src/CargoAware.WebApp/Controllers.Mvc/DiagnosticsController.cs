﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("diagnostics")]
    public partial class DiagnosticsController : AppMvcController
    {
        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.Diagnostics()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        // ==================================================
        #endregion
    }
}