﻿using System;

namespace CargoAware.Services.PrintJob
{
    public static class UldFormatting
    {
        public static String GetEpc(String airlinePrefix, long serialNumber)
        {
            const String Version = "01";
            var result = String.Format("CAF{0}{1}{2:0000000000000000}", Version, airlinePrefix, serialNumber);
            return result;
        }
    }
}