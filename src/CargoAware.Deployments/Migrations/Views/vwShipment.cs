﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630150542)]
    public sealed class vwShipment : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwShipment') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwShipment AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwShipment 
                AS
                   SELECT
                        s.Id,
                        s.FlightNumber,
                        s.FlightDate,
                        s.AirWaybill,
                        s.AirWaybillTagEpc,
                        s.AirWaybillTagDateTime,
                        s.Uld,
                        s.UldTagEpc,
                        s.UldTagDateTime,
                        s.OpenContainerDateTime,
                        s.CloseContainerDateTime,
                        s.Note,
                        s.TagReadSegmentStart,
                        s.TagReadSegmentEnd
                    FROM dbo.Shipment s;");
        }
    }
}
