﻿using CargoAware.Services.Infrastructure.Implementations;
using System;

namespace CargoAware.Services.Infrastructure
{
    public static class Generate
    {
        public static readonly Func<String> NextLogInKey =
            Generators.RandomString(32, Predicates.CharIsAlphaOrNumber.Or(c => c == '-'));

        public static readonly Func<String> NextSalt =
            Generators.RandomString(12, Predicates.CharIsLetterNumberOrSpecial);

        public static readonly Func<String> NextToken =
            Generators.RandomString(32, Predicates.CharIsLetterNumberOrSpecial.Except(c => c == '|'));
    }
}