﻿using CargoAware.JobService.Infrastructure;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using CommandLine;
using log4net;
using System;
using System.Configuration.Install;
using System.IO;
using System.Reflection;
using System.ServiceProcess;

namespace CargoAware.JobService
{
    static class Program
    {
        private static readonly ILog _Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        static void Main(String[] args)
        {
            try
            {
                log4net.Config.XmlConfigurator.Configure();
                _Log.Info("Starting CargoAware Jobs Service.");

                if (args.Length > 0)
                {
                    switch (args[0].ToLower())
                    {
                        case "-i":
                        case "/i":
                            _Log.Info("Attempting to install CargoAware Job Service as a service.");
                            ManagedInstallerClass.InstallHelper(new[] { Assembly.GetExecutingAssembly().Location });
                            break;
                        case "-u":
                        case "/u":
                            _Log.Info("Attempting to uninstall CargoAware Job Service.");
                            ManagedInstallerClass.InstallHelper(new[] { "/u", Assembly.GetExecutingAssembly().Location });
                            break;
                    }
                }

                IsRunningAsService = !Environment.UserInteractive;

                HostEngineInternally();
                ParseCommandArguments(args);

                if (IsRunningAsService)
                {
                    _Log.Info("Running CargoAware Job Service in Service mode.");
                    ServiceBase.Run(new JobService());
                }
                else
                {
                    _Log.Info("Running CargoAware Job Service in Interactive mode.");
                    var job = new JobService();

                    Console.CancelKeyPress += (s, e) =>
                    {
                        e.Cancel = true;
                        job.StopJobs();
                    };

                    job.Start(args);
                    job.Stop();

                    Console.WriteLine("Please press any key to continue.");
                    Console.ReadKey();
                }
            }
            catch (Exception ex)
            {
                WriteOutput(_Log.Fatal, "Unhandled Error." + Environment.NewLine + ex);

                if (Environment.UserInteractive)
                {
                    WriteOutput(_Log.Error, ex.Message);
                }
            }
        }

        public static Boolean IsRunningAsService { get; private set; }

        public static void WriteOutput(Action<Object> log, String message)
        {
            if (string.IsNullOrEmpty(message))
            {
                return;
            }

            if (log != null)
            {
                log(message);
            }

            Console.WriteLine("[{0:yyyyMMdd HH:mm:ss.fff}] {1}", DateTime.Now, message);
        }

        public static void ClearLastConsoleLine()
        {
            var currentCursorLine = Console.CursorTop;
            var windowWidth = Console.WindowWidth - 1;

            Console.SetCursorPosition(0, currentCursorLine);
            Console.Write(new String(' ', windowWidth));
            Console.SetCursorPosition(0, currentCursorLine);
        }

        public static Options CommandLineOptions { get; private set; }

        public static DateTime GetBuildTimestamp()
        {
            // ReSharper disable InconsistentNaming
            const int c_PeHeaderOffset = 60;
            const int c_LinkerTimestampOffset = 8;
            // ReSharper restore InconsistentNaming

            var filePath = Assembly.GetCallingAssembly().Location;
            var buildTimestampBytes = new byte[2048];

            using (var executable = new FileStream(filePath, FileMode.Open, FileAccess.Read))
            {
                executable.Read(buildTimestampBytes, 0, 2048);
            }

            var i = BitConverter.ToInt32(buildTimestampBytes, c_PeHeaderOffset);
            var secondsSince1970 = BitConverter.ToInt32(buildTimestampBytes, i + c_LinkerTimestampOffset);
            var buildTimestamp = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            buildTimestamp = buildTimestamp.AddSeconds(secondsSince1970);
            buildTimestamp = buildTimestamp.ToLocalTime();
            return buildTimestamp;
        }

        private static void HostEngineInternally()
        {
            //#if (DEBUG)
            //            var log = Loggers.GetExceptionLogger();
            //            SystemEvents.Subscribe(Observer.Create<EventMessage>(log.Warn));
            //#endif

            //            var emailSenderHandler = new EmailSender();
            //            Engine.Initialize(emailSenderHandler);
            Client.ViewStore = () => new RepositoryConnectionFactory(ConnectionStrings.ViewStore);
        }

        private static void ParseCommandArguments(String[] args)
        {
            CommandLineOptions = new Options();

            if (!IsRunningAsService && !Parser.Default.ParseArguments(args, CommandLineOptions))
            {
                Environment.Exit(0);
            }
        }
    }
}