﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150608034344)]
    public class AddUldDefaultToPPrinter_20150608034344 : AutoReversingMigration
    {
        public override void Up()
        {
            if (!Schema.Table("Printer").Column("UldDefault").Exists())
            {
                Alter.Table("Printer")
                     .AddColumn("UldDefault").AsBoolean().NotNullable().SetExistingRowsTo(false);
            }
        }
    }
}