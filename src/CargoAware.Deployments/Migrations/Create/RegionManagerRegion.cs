﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150208194789)]
    public sealed class RegionManagerRegion : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("RegionManagerRegion")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("Name").AsAnsiString(255).NotNullable()
                  .WithColumn("RegionId").AsGuid().NotNullable();
        }
    }
}