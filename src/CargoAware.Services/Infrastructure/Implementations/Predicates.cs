﻿using System;

namespace CargoAware.Services.Infrastructure.Implementations
{
    internal static class Predicates
    {
        public static Func<char, bool> CharIsAlphaOrNumber
        {
            get { return c => (c >= 48 && c <= 57) || (c >= 65 && c <= 90) || (c >= 97 && c <= 122); }
        }

        public static Func<char, bool> CharIsLetterNumberOrSpecial
        {
            get { return c => char.IsLetterOrDigit(c) || char.IsPunctuation(c) || char.IsSymbol(c); }
        }
    }
}