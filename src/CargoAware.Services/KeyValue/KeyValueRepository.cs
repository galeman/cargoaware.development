﻿using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;

namespace CargoAware.Services.KeyValue
{
    public sealed class KeyValueRepository : RepositoryBase
    {
        public static class KnownApplications
        {
            public const String CargoAware = "CARGOAWARE";
            public const String JobService = "JOBSERVICE";
        }

        public static class KnownKeys
        {
            public const String AirlinePrefix = "AIRLINE_PREFIX";
            public const String UldCounter = "ULD_COUNTER";
        }

        public KeyValueRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public void DeleteKeyValue(String app, String key)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
DELETE FROM KeyValue
WHERE Application = @app AND [Key] = @key;",
                    new { app, key });
            }
        }

        public KeyValueDocument GetKeyValue(String app, String key)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var result = db.Query<KeyValueDocument>(@"
SELECT TOP (1)
    Application,
    [Key],
    Reference,
    [Value],
    BinaryValue,
    LastWriteDateTime AS LastWriteDateTimeSetter
FROM KeyValue
WHERE Application = @app AND [Key] = @key;",
                    new { app, key }).FirstOrDefault();

                return result;
            }
        }

        public IEnumerable<KeyValueDocument> GetAllKeyValues(String app)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var result = db.Query<KeyValueDocument>(@"
SELECT
    Application,
    [Key],
    [Reference],
    [Value],
    BinaryValue,
    LastWriteDateTime AS LastWriteDateTimeSetter
FROM KeyValue
WHERE Application = @app
ORDER BY [Key];",
                    new { app });

                return result;
            }
        }

        public void SetKeyValue(String app, String key, String value, bool requireNewTransaction = false)
        {
            SetKeyValue(app, key, null, value, null, requireNewTransaction);
        }

        public void SetKeyValue(String app, String key, byte[] binaryValue, bool requireNewTransaction = false)
        {
            SetKeyValue(app, key, null, null, binaryValue, requireNewTransaction);
        }

        public void SetKeyValue(String app, String key, String reference, String value, bool requireNewTransaction = false)
        {
            SetKeyValue(app, key, reference, value, null, requireNewTransaction);
        }

        public void SetKeyValue(
            String app, String key, String reference, String value,
            byte[] binaryValue, bool requireNewTransaction = false)
        {
            using (var tx = new TransactionScope(requireNewTransaction ? TransactionScopeOption.RequiresNew : TransactionScopeOption.Required, TransactionOptionsDefault))
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
MERGE KeyValue AS Target
USING (VALUES (@app, @key, @reference, @value, @binaryValue))
    AS source ([Application], [Key], [Reference], [Value], [BinaryValue])
    ON Target.[Application] = @App AND Target.[Key] = @Key
WHEN MATCHED THEN
    UPDATE
    SET [Value] = source.[Value],
        [Reference] = source.[Reference],
        [BinaryValue] = source.[BinaryValue],
        [LastWriteDateTime] = GETUTCDATE()
WHEN NOT MATCHED THEN
    INSERT ([Application], [Key], [Reference], [Value], [BinaryValue], [LastWriteDateTime])
    VALUES (@app, @key, @reference, @value, @binaryValue, GETUTCDATE());",
                    new { app, key, reference, value, binaryValue });
                tx.Complete();
            }
        }
    }
}