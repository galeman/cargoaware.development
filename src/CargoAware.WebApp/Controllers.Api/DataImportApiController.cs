﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using CargoAware.Services.Shipment;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Infrastructure.DataImport;
using CargoAware.WebApp.Models.Imports;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/dataimport")]
    public sealed class DataImportApiController : AppApiController
    {
        #region // API Commands
        // ==================================================

        [POST("regionmanager"), HttpPost]
        //public async Task<HttpResponseMessage> Sales() // .NET 4.5
        public Task<HttpResponseMessage> RegionManager(String flightNumber, DateTime? flightDate, String airWaybill, String uld, String uldTagEpc) // .NET 4
        {
            if (!User.HasPermission(Permissions.User.DataImport()))
            {
                throw UnauthorizedException();
            }

            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            var importFileDirectory = GetImportDirectory(locationId, userId, ImportExportDataType.RegionManager);
            var processedImportDirectory = GetProcessedDirectory(locationId, userId, ImportExportDataType.RegionManager);

            var baseShipmentDocument = new ShipmentDocument
            {
                FlightNumber = flightNumber,
                FlightDate = flightDate.HasValue ? flightDate.Value : DateTime.Now,
                AirWaybill = airWaybill,
                // AirWaybillTagEpc = set during the save process
                Uld = uld ?? "",
                UldTagEpc = uldTagEpc ?? "",
                Note = ""
            };

            var result = ReceiveFilesAsync(Request, importFileDirectory, importFile =>
            {
                var errors = new List<Tuple<int, String>>();

                var importResult = Reader.GetRecords<RegionManagerImportModel>(
                    importFile.LocalFileName,
                    RegionManagerImportCsvClassMap.CsvConfiguration);

                if (importResult.Records.Length == 0)
                {
                    throw new DomainException("The uploaded file does not contain any entries.");
                }

                using (var transaction = RepositoryBase.GetNewTransactionScope())
                {
                    ProcessRegionManagerFile(
                        baseShipmentDocument,
                        locationId,
                        importResult.Records,
                        (row, message) => errors.Add(Tuple.Create(row, message)));

                    File.Move(importFile.LocalFileName, Path.Combine(
                        processedImportDirectory,
                        String.Format("DataUpload-{0:yyyy-MM-dd_HHmmss-fff}{1}", DateTime.Now, importResult.ProperFileExtension)));

                    transaction.Complete();
                }
            });
            return result;
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private static String GetImportDirectory(long locationId, long userId, ImportExportDataType dataType)
        {
            var result = Path.Combine(
                HttpContext.Current.Server.MapPath("~/App_Data/DataImport"),
                String.Format("Location-{0}", locationId),
                String.Format("User-{0}", userId),
                dataType.ToString());
            if (!Directory.Exists(result))
            {
                Directory.CreateDirectory(result);
            }
            return result;
        }

        private static String GetProcessedDirectory(long locationId, long userId, ImportExportDataType dataType)
        {
            var result = Path.Combine(
                GetImportDirectory(locationId, userId, dataType),
                "Processed");
            if (!Directory.Exists(result))
            {
                Directory.CreateDirectory(result);
            }
            return result;
        }

        //private static async Task<HttpResponseMessage> ReceiveFilesAsync( // .NET 4.5
        private static Task<HttpResponseMessage> ReceiveFilesAsync( // .NET 4
            HttpRequestMessage request,
            String dataImportDirectory,
            Action<MultipartFileData> work)
        {
            var dataStreamProvider = new MultipartFormDataStreamProvider(dataImportDirectory);

            // .NET 4.5
            //try
            //{
            //    await Request.Content.ReadAsMultipartAsync(provider);

            //    foreach (var file in provider.FileData)
            //    {
            //       // work here
            //    }
            //    return Request.CreateResponse(HttpStatusCode.OK);
            //}
            //catch (Exception e)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            //}

            var task = request.Content
                .ReadAsMultipartAsync(dataStreamProvider)
                .ContinueWith(x =>
                {
                    if (x.IsFaulted || x.IsCanceled)
                    {
#if (DEBUG)
                        var response = request.CreateErrorResponse(HttpStatusCode.InternalServerError, x.Exception);
#else
                        var response = request.CreateErrorResponse(HttpStatusCode.InternalServerError, x.Exception.Message);
#endif
                        throw new HttpResponseException(response);
                    }

                    try
                    {
                        foreach (var file in dataStreamProvider.FileData)
                        {
                            work(file);
                        }
                        return request.CreateResponse(HttpStatusCode.OK);
                    }
                    catch (Exception ex)
                    {
                        if (ex is HttpResponseException)
                        {
                            throw;
                        }

#if (DEBUG)
                        var response = request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex);
#else
                        var response = request.CreateErrorResponse(HttpStatusCode.InternalServerError, ex.Message);
#endif
                        throw new HttpResponseException(response);
                    }
                });
            return task;
        }

        // ==================================================
        #endregion


        #region // Import Processors
        // ==================================================

        private static Boolean ProcessRegionManagerFile(
            ShipmentDocument baseShipmentDocument,
            long locationId,
            IEnumerable<RegionManagerImportModel> importData,
            Action<int, String> error)
        {
            var importDataByTag = importData.GroupBy(x => x.Epc);

            foreach (var tagGroup in importDataByTag)
            {
                var sortedTagReads = tagGroup.OrderBy(x => x.Timestamp).ToArray();

                baseShipmentDocument.AirWaybillTagEpc = tagGroup.Key;
                baseShipmentDocument.TagReadSegmentStart = sortedTagReads.First().Timestamp;
                baseShipmentDocument.TagReadSegmentEnd = sortedTagReads.Last().Timestamp;

                ShipmentService.CreateShipment(baseShipmentDocument);
                ShipmentService.InsertTagReads(
                    locationId,
                    tagGroup.Select(x => new TagReadDocument
                    {
                        Epc = x.Epc,
                        LocationId = locationId,
                        X = x.X,
                        Y = x.Y,
                        Z = x.Z,
                        Timestamp = x.Timestamp
                    }));
            }
            return true;
        }

        // ==================================================
        #endregion
    }
}