﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Event;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.PrintJob;
using CargoAware.Services.Shipment;
using CargoAware.WebApp.Models.ExternalServices;
using System;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api.ExternalServices
{
    [RoutePrefix("lms")]
    public sealed class LmsApiController : ApiController
    {
        private static Lazy<EventRepository> _EventRepository;


        #region // Constructor + DI
        // ==================================================

        static LmsApiController()
        {
            Inject(Repositories.ForEvents);
        }

        private static void Inject(Lazy<EventRepository> dependency)
        {
            _EventRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [GET(""), HttpGet] // used for testing
        [POST(""), HttpPost]
        public async Task<HttpResponseMessage> CatchAll(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return
                TryProcessPrintRequest(requestBody) // ||
                // TryProcess__(requestBody) // ||
                // TryProcess__(requestBody)
                    ? Request.CreateResponse(HttpStatusCode.OK)
                    : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("printxml"), HttpGet] // used for testing
        [POST("printxml"), HttpPost]
        public async Task<HttpResponseMessage> PrintXml(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return TryProcessPrintRequest(requestBody)
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("cargoxml/xfsu"), HttpGet] // used for testing
        [POST("cargoxml/xfsu"), HttpPost]
        public async Task<HttpResponseMessage> CargoXmlXfsu(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();

            _EventRepository.Value.Insert(new EventDocument
            {
                Timestamp = DateTimeOffset.Now,
                EventSource = EventSource.LMS,
                EventName = "LMS/CargoXml/XFSU",
                Message = requestBody
            });
            File.WriteAllText(
                Path.Combine(@"C:\Writable", "LmsCargoXmlXfsu " + DateTime.Now.ToString("yyyy-MM-dd HHmmss fff") + ".txt"),
                requestBody);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private Boolean TryProcessPrintRequest(String requestBody)
        {
            LmsPrintXmlModel printXml;
            if (!XmlMessage.TryParse(requestBody, out printXml))
            {
                return false;
            }

            _EventRepository.Value.Insert(new EventDocument
            {
                Timestamp = DateTimeOffset.Now,
                EventSource = EventSource.LMS,
                EventName = "LMS/PrintXml",
                Message = requestBody
            });

            int labelCount;
            if (!int.TryParse(printXml.PrintInfo.NumberOfLabels, out labelCount))
            {
                labelCount = printXml.AirWaybillInfo.Details.Pieces.Total;
            }

            var printJob = PrintJobService.PrintAirWaybillLabels(new PrintJobDataDocument
            {
                AirWaybill = printXml.AirWaybillInfo.Details.AirWaybillNumber,
                Destination = printXml.AirWaybillInfo.Details.Routing.Destination.Station,
                Pieces = (Int16)printXml.AirWaybillInfo.Details.Pieces.Total,
                Origin = printXml.AirWaybillInfo.Details.Routing.Origin.Station,
                Weight = printXml.AirWaybillInfo.Details.Pieces.Weight,
                UnitOfMeasure = printXml.AirWaybillInfo.Details.Pieces.WeightUnitOfMeasure,
                Service = printXml.AirWaybillInfo.Details.ProductDescription,
                HandlingCodes = String.Join("  ", printXml.AirWaybillInfo.Details.HandlingCodes.Codes),
                TransfersInfo = printXml.BookingInfo.Details.Segments.Select(x =>
                    String.Format("{0}  BKD  {1}",
                    x.Destination.Station,
                    x.FlightInfo.FlightNumber)),
                TransfersDateTime = printXml.BookingInfo.Details.Segments.Select(x =>
                    String.Format("{0} {1}",
                    x.FlightInfo.FlightDate,
                    x.FlightInfo.Departure != null && !String.IsNullOrWhiteSpace(x.FlightInfo.Departure.Time)
                        ? x.FlightInfo.Departure.Time.InsertOnce(2, ":")
                        : ""))
            }, labelCount, externalId: printXml.PrinterId.ToString(CultureInfo.InvariantCulture));

            var labels = printJob.Item2;
            foreach (var label in labels)
            {
                ShipmentService.CreateShipment(new ShipmentDocument
                {
                    FlightNumber = String.Join(",", printXml.BookingInfo.Details.Segments.Select(x => x.FlightInfo.FlightNumber)),
                    FlightDate = DateTime.Today, //printXml.BookingInfo.Details.Segments.First(x => x.FlightInfo.FlightDate),
                    AirWaybill = printXml.AirWaybillInfo.Details.AirWaybillNumber,
                    AirWaybillTagEpc = label.Epc,
                    Uld = "",
                    UldTagEpc = "",
                    Note = ""
                });
            }

            return true;
        }

        // ==================================================
        #endregion
    }
}