﻿using CargoAware.Services.Common;
using CargoAware.Services.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.DataProcessing
{
    public static class TimeCompressor
    {
        /// <summary>
        /// Returns a copy of the processed data ordered by Timestamp, then by Id.
        /// </summary>
        /// <param name="data">Data to filter.</param>
        public static IEnumerable<TagReadDocument> Filter(IEnumerable<TagReadDocument> data)
        {
            var sortedDataArray = data.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
            var result = new List<TagReadDocument>(sortedDataArray.Length / 60 + 1);

            if (sortedDataArray.Length != 0)
            {
                var firstTimestamp = sortedDataArray.First().Timestamp;
                var lastTimestamp = sortedDataArray.Last().Timestamp;
                //var totalMinutes = (int)Math.Ceiling((lastTimestamp - firstTimestamp).TotalMinutes);

                var timeblockStart = firstTimestamp;
                while (timeblockStart <= lastTimestamp)
                {
                    var timeblockEnd = timeblockStart.AddMinutes(1).AddSeconds(-timeblockStart.Second);
                    var tagReadsInTimeSlice = sortedDataArray
                        .SkipWhile(x => x.Timestamp < timeblockStart)
                        .TakeWhile(x => x.Timestamp <= timeblockEnd) // include current entry
                        .Select(x => new Point3D { X = x.X, Y = x.Y, Z = x.Z })
                        .ToArray();
                    if (tagReadsInTimeSlice.Length != 0)
                    {
                        var averageOfTimeSlice = tagReadsInTimeSlice.Aggregate((r1, r2) =>
                            new Point3D { X = r1.X + r2.X, Y = r1.Y + r2.Y });
                        averageOfTimeSlice.X /= tagReadsInTimeSlice.Length;
                        averageOfTimeSlice.Y /= tagReadsInTimeSlice.Length;

                        var entry = new TagReadDocument
                        {
                            X = averageOfTimeSlice.X,
                            Y = averageOfTimeSlice.Y,
                            Timestamp = timeblockEnd
                        };
                        result.Add(entry);
                    }
                    timeblockStart = timeblockEnd;
                }
            }

            return result;
        }
    }
}