using System.ComponentModel;
using System.Configuration;
using System.Linq;

namespace CargoAware.Services.Infrastructure
{
    public static class ConfigurationExtensions
    {
        public static T GetOrDefault<T>(this KeyValueConfigurationCollection settings, string key, T @default)
        {
            if (!settings.AllKeys.Any(k => k.Equals(key)))
            {
                return @default;
            }
            return (T)TypeDescriptor.GetConverter(typeof(T)).ConvertFromString(settings[key].Value);
        }

        public static void Set<T>(this KeyValueConfigurationCollection settings, string key, T value)
        {
            if (!settings.AllKeys.Any(k => k.Equals(key)))
            {
                settings.Add(key, value.ToString());
            }
            else
            {
                settings[key].Value = value.ToString();
            }
        }
    }
}