﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630145922)]
    public sealed class vwPrinter : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwPrinter') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwPrinter AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwPrinter
                AS
                   SELECT
                        pr.Id,
                        pr.LocationId,
                        pr.Name,
                        pr.[Type],
                        pr.Address,
                        pr.Port,
                        pr.Status,
                        pr.ExternalId,
                        pr.UldDefault
                    FROM dbo.Printer pr;");
        }
    }
}
