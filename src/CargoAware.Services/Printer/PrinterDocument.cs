﻿using System;

namespace CargoAware.Services.Printer
{
    public class PrinterDocument
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public String Name { get; set; }
        public PrinterType Type { get; set; }
        public String Address { get; set; }
        public Int16 Port { get; set; }
        public String Status { get; set; }
        public String ExternalId { get; set; }
        public Boolean UldDefault { get; set; }
    }
}