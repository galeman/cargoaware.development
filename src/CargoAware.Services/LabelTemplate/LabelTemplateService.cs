﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using System;

namespace CargoAware.Services.LabelTemplate
{
    public static class LabelTemplateService
    {
        private static Lazy<LabelTemplateRepository> _LabelTemplateRepository;


        static LabelTemplateService()
        {
            Inject(Repositories.ForLabelTemplates);
        }

        private static void Inject(Lazy<LabelTemplateRepository> dependency)
        {
            _LabelTemplateRepository = dependency;
        }


        /// <param name="message">Required: LocationId, Name, Type, Language, Code, IsActive</param>
        public static LabelTemplateDocument CreateLabelTemplate(LabelTemplateDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            message.Id = 0;

            // Step 3: Does this command pass business rules?

            var sameNameLabelTemplateExists = _LabelTemplateRepository.Value.DoesLabelTemplateWithSameNameExist(message.LocationId, message.Name);
            if (sameNameLabelTemplateExists)
            {
                throw new DomainException("The Label Template \"{0}\" already exists.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            long labelTemplateId;
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LabelTemplateRepository.Value.InsertOrUpdate(message);
                labelTemplateId = _LabelTemplateRepository.Value.GetLabelTemplateIdByName(message.Name);
                scope.Complete();
            }

            var result = _LabelTemplateRepository.Value.GetLabelTemplateById(labelTemplateId);
            return result;
        }

        /// <param name="message">Required: Id, Name, Type, Language, Code, IsActive</param>
        public static void UpdateLabelTemplate(LabelTemplateDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var existingLabelTemplate = _LabelTemplateRepository.Value.GetLabelTemplateById(message.Id);
            if (existingLabelTemplate == null)
            {
                throw new DomainException("The specified Label Template, \"{0}\", does not exist.", message.Name);
            }

            var sameNameLabelTemplateExists = _LabelTemplateRepository.Value.DoesLabelTemplateWithSameNameExist(message.LocationId, message.Name, message.Id);
            if (sameNameLabelTemplateExists)
            {
                throw new DomainException("A Label Template named \"{0}\" already exists.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            message.LocationId = existingLabelTemplate.LocationId;

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LabelTemplateRepository.Value.InsertOrUpdate(message);
                scope.Complete();
            }
        }

        /// <param name="message">Required: Id</param>
        public static void ArchiveLabelTemplate(LabelTemplateDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var doesLabelTemplateExist = _LabelTemplateRepository.Value.GetLabelTemplateById(message.Id);
            if (doesLabelTemplateExist == null)
            {
                throw new DomainException("The specified Label Template does not exist.");
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LabelTemplateRepository.Value.Archive(message.Id);
                scope.Complete();
            }
        }
    }
}