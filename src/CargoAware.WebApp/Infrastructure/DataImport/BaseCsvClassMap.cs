﻿using System.Globalization;
using CsvHelper.Configuration;

namespace CargoAware.WebApp.Infrastructure.DataImport
{
    public abstract class BaseCsvClassMap<T> : CsvClassMap<T>
    {
        protected static CsvConfiguration GetNewCsvConfiguration()
        {
            return new CsvConfiguration
            {
                AllowComments = false,
                DetectColumnCountChanges = true,
                IgnoreHeaderWhiteSpace = true,
                IsHeaderCaseSensitive = false,
                SkipEmptyRecords = true,
                ThrowOnBadData = true,
                TrimFields = false,
                TrimHeaders = true,
                WillThrowOnMissingField = true,
            };
        }
    }
}