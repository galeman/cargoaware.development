﻿using System;

namespace CargoAware.Services.Common
{
    public struct Point3D
    {
        public Decimal X { get; set; }
        public Decimal Y { get; set; }
        public Decimal Z { get; set; }

        public override bool Equals(Object obj)
        {
            if (!(obj is Point3D))
            {
                return false;
            }
            var other = (Point3D)obj;
            return other.X.Equals(X) &&
                   other.Y.Equals(Y) &&
                   other.Z.Equals(Z);
        }

        public override int GetHashCode()
        {
            var result = X.GetHashCode();
            result = (result * 397) ^ Y.GetHashCode();
            result = (result * 397) ^ Z.GetHashCode();
            return result;
        }

        public static bool operator ==(Point3D left, Point3D right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Point3D left, Point3D right)
        {
            return !left.Equals(right);
        }
    }
}