﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Location;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/locations")]
    public class LocationsApiController : AppApiController
    {
        private static Lazy<LocationRepository> _LocationRepository;


        #region // Constructor + DI
        // ==================================================

        static LocationsApiController()
        {
            Inject(Repositories.ForLocations);
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<LocationDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.AdminLocations()))
            {
                throw UnauthorizedException();
            }

            var result = _LocationRepository.Value.GetLocations(query: request.GetApplyMethod());
            return result;
        }

        [POST("create"), HttpPost]
        public HttpResponseMessage Create(IEnumerable<AddEditLocationsDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminLocations()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            foreach (var detail in model)
            {
                if (String.IsNullOrWhiteSpace(detail.Name) ||
                    !detail.AxisDirection.HasValue ||
                    !detail.OriginLocation.HasValue)
                {
                    continue;
                }

                LocationService.CreateLocation(new LocationDocument
                {
                    Name = detail.Name,
                    AxisDirection = detail.AxisDirection.Value,
                    OriginLocation = detail.OriginLocation.Value,
                    ApiKey = Guid.NewGuid()
                });
            }

            UserSession.RemoveFromCache();

            return SuccessResponse(true);
        }

        [POST("update"), HttpPost]
        public HttpResponseMessage Update(IEnumerable<AddEditLocationsDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminLocations()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            foreach (var detail in model)
            {
                if (detail.Id == 0 ||
                    String.IsNullOrWhiteSpace(detail.Name) ||
                    !detail.AxisDirection.HasValue ||
                    !detail.OriginLocation.HasValue)
                {
                    continue;
                }

                LocationService.UpdateLocation(new LocationDocument
                {
                    Id = detail.Id,
                    Name = detail.Name,
                    AxisDirection = detail.AxisDirection.Value,
                    OriginLocation = detail.OriginLocation.Value,
                    ApiKey = detail.GenerateNewApiKey ? Guid.NewGuid() : Guid.Empty
                });
            }

            UserSession.RemoveFromCache();

            return SuccessResponse(true);
        }

        [POST("archive"), HttpPost]
        public HttpResponseMessage Archive([FromBody] long id)
        {
            if (!User.HasPermission(Permissions.User.AdminLocations()))
            {
                throw UnauthorizedException();
            }

            LocationService.ArchiveLocation(new LocationDocument
            {
                Id = id
            });

            UserSession.RemoveFromCache();

            return SuccessResponse(true);
        }

        // ==================================================
        #endregion
    }
}