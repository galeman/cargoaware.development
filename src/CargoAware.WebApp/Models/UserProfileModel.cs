﻿using System;

namespace CargoAware.WebApp.Models
{
    public class UserProfileModel
    {
        public String Username { get; set; }
        public String FullName { get; set; }
        public String Email { get; set; }
        public String Password { get; set; }
        public String PasswordConfirmation { get; set; }
        public String SecurityQuestion { get; set; }
        public String SecurityAnswer { get; set; }

        public Boolean MustSetPassword { get; set; }
    }
}