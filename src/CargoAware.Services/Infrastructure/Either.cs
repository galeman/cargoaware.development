﻿// ==========================================================
// Either.cs
// ==========================================================
// Copyright 2012-2013 Adam W. McKinley
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
// http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
// ==========================================================

using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Infrastructure
{
    public abstract class Either<TLeft, TRight>
    {
        public bool IsLeft()
        {
            return this is Left<TLeft, TRight>;
        }

        public bool IsRight()
        {
            return this is Right<TLeft, TRight>;
        }

        public TLeft FromLeft()
        {
            var left = this as Left<TLeft, TRight>;
            if (left == null)
            {
                throw new NotLeftException();
            }
            return left.Value;
        }

        public TRight FromRight()
        {
            var right = this as Right<TLeft, TRight>;
            if (right == null)
            {
                throw new NotRightException();
            }
            return right.Value;
        }

        public Either<TResultLeft, TResultRight> Select<TResultLeft, TResultRight>(
            Func<TLeft, TResultLeft> leftFunc, Func<TRight, TResultRight> rightFunc)
        {
            if (this is Left<TLeft, TRight>)
            {
                return new Left<TResultLeft, TResultRight>(leftFunc(FromLeft()));
            }
            return new Right<TResultLeft, TResultRight>(rightFunc(FromRight()));
        }

        public Either<TResult, TRight> Select<TResult>(Func<TLeft, TResult> func)
        {
            if (this is Left<TLeft, TRight>)
            {
                return new Left<TResult, TRight>(func(FromLeft()));
            }
            return new Right<TResult, TRight>(FromRight());
        }

        public Either<TLeft, TResult> Select<TResult>(Func<TRight, TResult> func)
        {
            if (this is Left<TLeft, TRight>)
            {
                return new Left<TLeft, TResult>(FromLeft());
            }
            return new Right<TLeft, TResult>(func(FromRight()));
        }

        public static implicit operator TLeft(Either<TLeft, TRight> either)
        {
            var left = either as Left<TLeft, TRight>;
            return left != null ? left.Value : default(TLeft);
        }

        public static implicit operator TRight(Either<TLeft, TRight> either)
        {
            var right = either as Right<TLeft, TRight>;
            return right != null ? right.Value : default(TRight);
        }

        public static implicit operator Either<TLeft, TRight>(TLeft value)
        {
            return new Left<TLeft, TRight>(value);
        }

        public static implicit operator Either<TLeft, TRight>(TRight value)
        {
            return new Right<TLeft, TRight>(value);
        }
    }

    public sealed class Left<TLeft, TRight> : Either<TLeft, TRight>
    {
        public Left(TLeft value)
        {
            if (value == null)
            {
                throw new Exception(string.Format("Left<{0}, {1}> must not contain null.", typeof(TLeft).Name, typeof(TRight).Name));
            }
            this.value = value;
        }

        public TLeft Value
        {
            get { return value; }
        }

        private readonly TLeft value;
    }

    public sealed class Right<TLeft, TRight> : Either<TLeft, TRight>
    {
        public Right(TRight value)
        {
            if (value == null)
            {
                throw new Exception(string.Format("Right<{0}, {1}> must not contain null.", typeof(TLeft).Name, typeof(TRight).Name));
            }
            this.value = value;
        }

        public TRight Value
        {
            get { return value; }
        }

        private readonly TRight value;
    }

    public sealed class NotLeftException : InvalidOperationException
    {
        public NotLeftException()
            : base("Either is not left.")
        {
        }
    }

    public sealed class NotRightException : InvalidOperationException
    {
        public NotRightException()
            : base("Either is not right.")
        {
        }
    }

    public sealed class NeitherLeftNorRightException : InvalidOperationException
    {
        public NeitherLeftNorRightException()
            : base("Either is neither left nor right.")
        {
        }
    }

    public static class Extensions
    {
        public static TResult Either<TLeft, TRight, TResult>
            (this Either<TLeft, TRight> either, Func<TLeft, TResult> ifLeft, Func<TRight, TResult> ifRight)
        {
            var left = either as Left<TLeft, TRight>;
            if (left != null)
            {
                return ifLeft(left.Value);
            }

            var right = either as Right<TLeft, TRight>;
            if (right != null)
            {
                return ifRight(right.Value);
            }

            throw new NeitherLeftNorRightException();
        }

        public static IEnumerable<TLeft> Lefts<TLeft, TRight>(this IEnumerable<Either<TLeft, TRight>> eithers)
        {
            return eithers.OfType<Left<TLeft, TRight>>().Select(left => left.Value);
        }

        public static IEnumerable<TRight> Rights<TLeft, TRight>(this IEnumerable<Either<TLeft, TRight>> eithers)
        {
            return eithers.OfType<Right<TLeft, TRight>>().Select(right => right.Value);
        }

        public static Tuple<IEnumerable<TLeft>, IEnumerable<TRight>> PartitionEithers<TLeft, TRight>
            (this IEnumerable<Either<TLeft, TRight>> eithers)
        {
            var array = eithers.ToArray();
            return new Tuple<IEnumerable<TLeft>, IEnumerable<TRight>>(array.Lefts(), array.Rights());
        }
    }
}