﻿namespace CargoAware.Services.Common
{
    public enum AxisDirection : byte
    {
        Normal,
        Inverted
    }
}