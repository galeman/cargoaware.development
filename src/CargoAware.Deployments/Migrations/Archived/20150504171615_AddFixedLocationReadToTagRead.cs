﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150504171615)]
    public class AddFixedLocationReadToTagRead_20150504171615 : AutoReversingMigration
    {
        public override void Up()
        {
            Alter.Table("TagRead")
                 .AddColumn("FixedLocationRead").AsBoolean().NotNullable().SetExistingRowsTo(false);
        }
    }
}