﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150302202530)]
    public sealed class PrintJobData : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("PrintJobData")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("PrintJobId").AsInt64().NotNullable().Indexed()
                  .WithColumn("Epc").AsAnsiString(255).NotNullable().Indexed()
                  .WithColumn("Barcode").AsAnsiString(255).NotNullable().Indexed()
                  .WithColumn("PieceNumber").AsInt16().NotNullable()
                  .WithColumn("AirWaybill").AsAnsiString(255).NotNullable()
                  .WithColumn("Uld").AsAnsiString(255).NotNullable()
                  .WithColumn("Destination").AsAnsiString(255).NotNullable()
                  .WithColumn("Pieces").AsInt16().NotNullable()
                  .WithColumn("Origin").AsAnsiString(255).NotNullable()
                  .WithColumn("Weight").AsDouble().NotNullable()
                  .WithColumn("UnitOfMeasure").AsAnsiString(255).NotNullable()
                  .WithColumn("Service").AsAnsiString(255).NotNullable()
                  .WithColumn("HandlingCodes").AsAnsiString(255).NotNullable()
                  .WithColumn("TransfersInfo").AsAnsiString(512).NotNullable()
                  .WithColumn("TransfersDateTime").AsAnsiString(512).NotNullable()
                  .WithColumn("PrintedDateTime").AsDateTime().Nullable();
        }
    }
}