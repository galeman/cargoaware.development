﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.LabelTemplate;
using CargoAware.Services.Location;
using CargoAware.Services.Printer;
using CargoAware.Services.PrintJob;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api.ExternalServices
{
    [RoutePrefix("printservice")]
    public sealed class PrintServiceApiController : ApiController
    {
        private static Lazy<LabelTemplateRepository> _LabelTemplateRepository;
        private static Lazy<LocationRepository> _LocationRepository;
        private static Lazy<PrinterRepository> _PrinterRepository;
        private static Lazy<PrintJobRepository> _PrintJobRepository;


        #region // Constructor + DI
        // ==================================================

        static PrintServiceApiController()
        {
            Inject(Repositories.ForLabelTemplates);
            Inject(Repositories.ForLocations);
            Inject(Repositories.ForPrinters);
            Inject(Repositories.ForPrintJobs);
        }

        private static void Inject(Lazy<LabelTemplateRepository> dependency)
        {
            _LabelTemplateRepository = dependency;
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        private static void Inject(Lazy<PrinterRepository> dependency)
        {
            _PrinterRepository = dependency;
        }

        private static void Inject(Lazy<PrintJobRepository> dependency)
        {
            _PrintJobRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [GET("labeltemplates"), HttpGet]
        [POST("labeltemplates"), HttpPost]
        public IEnumerable<LabelTemplateDto> LabelTemplates([FromUri] String apiKey)
        {
            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);
            var labelTemplates = _LabelTemplateRepository.Value.GetLabelTemplates(location.Id).Data;
            var result = labelTemplates.Select(x => new LabelTemplateDto
            {
                Code = x.Code,
                Name = x.Name,
                Version = (int)x.Version,
                PrinterLanguage = x.Language.ToString()
            });
            return result;
        }

        [GET("printjobs"), HttpGet]
        [POST("printjobs"), HttpPost]
        public IEnumerable<PrintJobDto> PrintJobs([FromUri] String apiKey)
        {
            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);

            var labelTemplateLookup = new ConcurrentDictionary<LabelType, LabelTemplateDocument>();
            foreach (var labelType in Enum.GetValues(typeof(LabelType)))
            {
                var labelTemplate = _LabelTemplateRepository.Value.GetActiveLabelTemplate(location.Id, (LabelType)labelType);
                if (labelTemplate == null)
                {
                    continue;
                }

                labelTemplateLookup[(LabelType)labelType] = labelTemplate;
            }

            if (labelTemplateLookup.Count == 0)
            {
                throw new DomainException("No labels have been defined.");
            }

            var printJobs = _PrintJobRepository.Value.GetPrintJobs(
                location.Id,
                EntitySubmittedState.NonSubmittedOnly,
                EntityCancelledState.NonCancelledOnly).Data;
            var result = new List<PrintJobDto>(printJobs.Count);
            foreach (var printJob in printJobs)
            {
                LabelTemplateDocument labelTemplate;
                if (!labelTemplateLookup.TryGetValue(printJob.LabelType, out labelTemplate))
                {
                    throw new DomainException("There is no Active Label Template of type {0}", printJob.LabelType.ToString());
                }

                result.Add(new PrintJobDto
                {
                    PrintingRequestId = printJob.Id,
                    IPAddress = printJob.PrinterAddress,
                    PortNumber = printJob.PrinterPort,
                    TemplateName = labelTemplate.Name,
                    TemplateVersionNumber = (int)labelTemplate.Version,
                    TagData = GetPrintJobDtoTagData(printJob)
                });
            }

            PrintJobService.PrintJobsSubmitted(printJobs.Select(x => x.Id).ToArray());

            return result;
        }

        [GET("cancelledprintjobs"), HttpGet]
        [POST("cancelledprintjobs"), HttpPost]
        public IEnumerable<CancelPrintJobDto> CancelledPrintJobs([FromUri] String apiKey)
        {
            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);

            var cancelledPrintJobs = _PrintJobRepository.Value.GetPrintJobs(
                location.Id,
                cancelRequestState: EntityCancelledState.CancelledOnly,
                cancelledState: EntityCancelledState.NonCancelledOnly).Data;
            var result = cancelledPrintJobs.Select(printJob => new CancelPrintJobDto
            {
                PrintingRequestId = printJob.Id,
                IPAddress = printJob.PrinterAddress,
                PortNumber = printJob.PrinterPort
            });

            PrintJobService.PrintJobsCancelled(cancelledPrintJobs.Select(x => x.Id).ToArray());

            return result;
        }

        [GET("printjobresults"), HttpGet]
        [POST("printjobresults"), HttpPost]
        public HttpResponseMessage PrintJobResults([FromUri] String apiKey, PrintServiceData<PrintJobResultDto> printJobResults)
        {
            foreach (var printJobResult in printJobResults.Data)
            {
                String printJobDataIdString;
                long printJobDataId;
                if (!printJobResult.Identifier.TryGetValue("PrintJobDataId", out printJobDataIdString) ||
                    !long.TryParse(printJobDataIdString, out printJobDataId))
                {
                    continue;
                }

                PrintJobService.PrintJobDataPrinted(printJobDataId, printJobResult.PrintedDateTime);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [GET("printerstatus"), HttpGet]
        [POST("printerstatus"), HttpPost]
        public IEnumerable<PrinterInfoDto> PrinterStatus([FromUri] String apiKey, PrintServiceData<PrinterStatusDto> printerStatuses)
        {
            //var requestBody = Request.Content.ReadAsStringAsync().Result;
            //var printerStatuses = JsonConvert.DeserializeObject<PrintServiceData<PrinterStatusDto>>(requestBody);
            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);
            var printers = _PrinterRepository.Value.GetPrinters(location.Id).Data;

            foreach (var printerStatus in printerStatuses.Data)
            {
                var address = printerStatus.Address;
                var status =
                    String.Format(
                        "{0}: {1}",
                        printerStatus.Status ? "Good" : "Error",
                        String.Join(", ", printerStatus.ErrorList))
                    .Trim()
                    .TrimEnd(':');
                foreach (var printer in printers.Where(x => String.Equals(String.Format("{0}:{1}", x.Address, x.Port), address)))
                {
                    PrinterService.UpdatePrinterStatus(printer.Id, status);
                }
            }

            var result = printers.Select(x => new PrinterInfoDto
            {
                IPAddress = x.Address,
                PortNumber = x.Port
            });
            return result;
        }

        [GET("printqueue"), HttpGet]
        [POST("printqueue"), HttpPost]
        public HttpResponseMessage PrintQueue([FromUri] String apiKey)
        {
            //var requestBody = Request.Content.ReadAsStringAsync().Result;
            //var printJobResults = JsonConvert.DeserializeObject<PrintServiceData<PrintJobResultDto>>(requestBody);
            //foreach (var printJobResult in printJobResults.Data)
            //{
            //    String printJobDataIdString;
            //    long printJobDataId;
            //    if (!printJobResult.Identifier.TryGetValue("PrintJobDataId", out printJobDataIdString) ||
            //        !long.TryParse(printJobDataIdString, out printJobDataId))
            //    {
            //        continue;
            //    }

            //    PrintJobService.PrintJobDataPrinted(printJobDataId, printJobResult.PrintedDateTime);
            //}
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private String[] GetFixedFieldCount(IEnumerable<String> values, int count)
        {
            var valuesArray = values.ToArray();
            if (valuesArray.Length != count)
            {
                Array.Resize(ref valuesArray, count);
            }
            for (var i = 0; i < count; i++)
            {
                if (valuesArray[i] == null)
                {
                    valuesArray[i] = "";
                }
            }
            return valuesArray;
        }

        private IEnumerable<Dictionary<String, String>> GetPrintJobDtoTagData(PrintJobDocument printJob)
        {
            IEnumerable<Dictionary<String, String>> result = null;

            switch (printJob.LabelType)
            {
                case LabelType.AirWaybill:
                    result = GetAirWaybillTagData(printJob);
                    break;
                case LabelType.ULD:
                    result = GetUldTagData(printJob);
                    break;
            }

            return result;
        }

        private IEnumerable<Dictionary<String, String>> GetAirWaybillTagData(PrintJobDocument printJob)
        {
            var result = printJob.LabelData.Select(printJobData =>
            {
                var service = GetFixedFieldCount(printJobData.Service.Split('/'), 2);
                var transfersInfo = GetFixedFieldCount(printJobData.TransfersInfo, 5);
                var transfersDateTime = GetFixedFieldCount(printJobData.TransfersDateTime, 5);

                return new Dictionary<String, String>
                {
                    { "PrintJobDataId", printJobData.Id.ToString(CultureInfo.InvariantCulture) },
                    { "epc", printJobData.Epc },
                    { "barcode", printJobData.Barcode },
                    { "air_waybill", printJobData.AirWaybill.InsertOnce(3, "-") },
                    { "destination", printJobData.Destination },
                    { "pieces", printJobData.Pieces.ToString(CultureInfo.InvariantCulture) },
                    { "origin", printJobData.Origin },
                    { "weight", String.Format("{0:0.0}{1}", printJobData.Weight, printJobData.UnitOfMeasure) },
                    { "service_1", service[0] },
                    { "service_2", service[1] },
                    { "handling_codes", printJobData.HandlingCodes },
                    { "transfer_info_1", transfersInfo[0] },
                    { "transfer_info_2", transfersInfo[1] },
                    { "transfer_info_3", transfersInfo[2] },
                    { "transfer_info_4", transfersInfo[3] },
                    { "transfer_info_5", transfersInfo[4] },
                    { "transfer_datetime_1", transfersDateTime[0] },
                    { "transfer_datetime_2", transfersDateTime[1] },
                    { "transfer_datetime_3", transfersDateTime[2] },
                    { "transfer_datetime_4", transfersDateTime[3] },
                    { "transfer_datetime_5", transfersDateTime[4] },
                };
            });
            return result;
        }

        private IEnumerable<Dictionary<String, String>> GetUldTagData(PrintJobDocument printJob)
        {
            var result = printJob.LabelData.Select(printJobData => new Dictionary<String, String>
            {
                { "PrintJobDataId", printJobData.Id.ToString(CultureInfo.InvariantCulture) },
                { "epc", printJobData.Epc },
                { "barcode", printJobData.Barcode },
                { "uld", printJobData.Uld },
                { "destination", printJobData.Destination },
                { "origin", printJobData.Origin },
            });
            return result;
        }

        // ==================================================
        #endregion


        #region // DTO Classes
        // ==================================================

        public class LabelTemplateDto
        {
            public String Code { get; set; }
            public String Name { get; set; }
            public Int32 Version { get; set; }
            public String PrinterLanguage { get; set; }
        }

        public class PrinterInfoDto
        {
            // ReSharper disable InconsistentNaming
            public String IPAddress { get; set; }
            // ReSharper restore InconsistentNaming
            public Int32 PortNumber { get; set; }
        }

        public class PrintJobDto : PrinterInfoDto
        {
            public Int64 PrintingRequestId { get; set; }
            public String TemplateName { get; set; }
            public Int32 TemplateVersionNumber { get; set; }
            public IEnumerable<Dictionary<String, String>> TagData { get; set; }
        }

        public class CancelPrintJobDto
        {
            // ReSharper disable InconsistentNaming
            public String IPAddress { get; set; }
            // ReSharper restore InconsistentNaming
            public Int32 PortNumber { get; set; }
            public Int64 PrintingRequestId { get; set; }
        }

        public class PrintServiceData<T>
        {
            public IEnumerable<T> Data { get; set; }
        }

        public class PrintJobResultDto
        {
            public Dictionary<String, String> Identifier { get; set; }
            public DateTime PrintedDateTime { get; set; }
            public Boolean Success { get; set; }
            public String Returned { get; set; }
        }

        public class PrinterStatusDto
        {
            public String Address { get; set; }
            public Boolean Status { get; set; }
            public String[] ErrorList { get; set; }
        }

        // ==================================================
        #endregion
    }
}