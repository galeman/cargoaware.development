﻿using CargoAware.Services.Infrastructure;
using System;
using System.Security.Principal;

namespace CargoAware.Web.Infrastructure
{
    public static class ExceptionHelper
    {
        public static void LogControllerException(IPrincipal userPrincipal, String url, Exception exception)
        {
            var userInformation = userPrincipal.Identity.Name;

            var log = Loggers.GetExceptionLogger();
            log.Error(String.Format("User: {0} ({1})", userInformation, url), exception);
        }
    }
}