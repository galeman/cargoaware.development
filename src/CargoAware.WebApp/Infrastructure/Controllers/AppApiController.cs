using CargoAware.Web.Infrastructure.Controllers;
using CargoAware.Web.Infrastructure.Security;

namespace CargoAware.WebApp.Infrastructure.Controllers
{
    [ApiValidateAntiForgeryToken]
    public abstract class AppApiController : BaseApiController<UserPrincipal>
    {
    }
}