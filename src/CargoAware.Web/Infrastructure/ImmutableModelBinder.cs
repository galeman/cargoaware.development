﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Fasterflect;

namespace CargoAware.Web.Infrastructure
{
    public sealed class ImmutableModelBinder : DefaultModelBinder
    {
        protected override Object CreateModel(
            ControllerContext controllerContext,
            ModelBindingContext bindingContext,
            Type modelType)
        {
            var ctor = modelType.Constructors(Flags.InstancePublic).FirstOrDefault();
            if (ctor == null)
            {
                throw new InvalidOperationException(String.Format(
                    "Cannot bind POST submission to {0}: there is no public constructor.",
                    modelType.Name));
            }

            var arguments = new List<Object>();
            foreach (var parameter in ctor.Parameters())
            {
                var owned = parameter;
                try
                {
                    var metadata = bindingContext.PropertyMetadata.Single(pair =>
                        String.Equals(pair.Key, owned.Name, StringComparison.InvariantCultureIgnoreCase));

                    // For array form values, sometimes the key is "name", and other times it is "name[]".
                    var result = bindingContext.ValueProvider.GetValue(metadata.Key) ??
                                 bindingContext.ValueProvider.GetValue(String.Format("{0}[]", metadata.Key));

                    if (result != null)
                    {
                        arguments.Add(result.ConvertTo(owned.ParameterType));
                    }
                    else if (owned.ParameterType.IsArray)
                    {
                        arguments.Add(owned.ParameterType.CreateInstance(0));
                    }
                }
                catch (NullReferenceException)
                {
                    throw new InvalidOperationException(String.Format(
                        "Cannot bind POST submission to {0}: missing {1}.",
                        modelType.Name,
                        owned.Name));
                }
                catch (InvalidOperationException ex)
                {
                    throw new InvalidOperationException(String.Format(
                        "Cannot bind POST submission to {0}.",
                        modelType.Name),
                        ex);
                }
            }

            return ctor.CreateInstance(arguments.ToArray());
        }
    }
}