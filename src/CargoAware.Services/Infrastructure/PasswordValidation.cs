﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.Infrastructure
{
    public static class PasswordValidation
    {
        public static string GetComplexityRegex()
        {
            var components = new List<string>();

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreLowercaseLetters)
            {
                components.Add("(?=.*[a-z]+)");
            }

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreNumbers)
            {
                components.Add("(?=.*[0-9]+)");
            }

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreSymbols)
            {
                components.Add("(?=.*[^A-Za-z0-9]+)");
            }

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreUppercaseLetters)
            {
                components.Add("(?=.*[A-Z]+)");
            }

            return String.Format(@"^.*{0}.*$", String.Concat(components));
        }

        public static string GetMinimumLengthMessage()
        {
            return String.Format(
                "Password must have a minimum length of {0}.", AppSettings.Instance.MinimumPasswordLength);
        }

        public static string GetSimpleMessage()
        {
            var conditions = new List<string>();

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreUppercaseLetters)
            {
                conditions.Add("one upper case letter");
            }

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreLowercaseLetters)
            {
                conditions.Add("one lower case letter");
            }

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreNumbers)
            {
                conditions.Add("one digit");
            }

            if (AppSettings.Instance.PasswordsMustIncludeOneOrMoreSymbols)
            {
                conditions.Add("one special symbol");
            }

            switch (conditions.Count)
            {
                case 0:
                    return "Password may contain any characters.";

                case 1:
                    return String.Format("Password must contain a minimum of {0}.", conditions[0]);

                default: // 2 or more
                    var joined = String.Join(", ", conditions.Take(conditions.Count - 1));
                    var last = conditions.Last();
                    return String.Format("Password must contain a minimum of {0} and {1}.", joined, last);
            }
        }
    }
}