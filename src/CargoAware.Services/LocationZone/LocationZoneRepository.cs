﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.LocationZone
{
    public sealed class LocationZoneRepository : RepositoryBase
    {
        public LocationZoneRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public bool DoesLocationZoneWithSameNameExist(long locationId, String name, long? excludedId = null)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var sql = "SELECT TOP (1) 1 FROM LocationZone WHERE ArchivedDateTimeUtc IS NULL AND LocationId = @locationId AND Name = @name";

                if (excludedId.HasValue)
                {
                    sql = sql + " AND Id <> @excludedId";
                }

                return db.QueryBoolean(sql, new { locationId, name, excludedId });
            }
        }

        public LocationZoneDocument GetLocationZoneById(long id)
        {
            var result = QueryLocationZones(builder => builder.Where("lz.Id = @id", new { id })).Data;
            return result.FirstOrDefault();
        }

        public long GetLocationZoneIdByName(String name)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM LocationZone
/**where**/;");

                builder.Where("Name = @name", new { name });

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        public long GetLocationZoneIdByEnvoyZone(String envoyZone)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM LocationZone
/**where**/;");

                builder.Where("EnvoyZoneId = @envoyZone", new { envoyZone });

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        public Paged<LocationZoneDocument> GetLocationZones(
            long locationId, EntityArchivalState archivalState = EntityArchivalState.ActiveOnly, Action<SqlBuilder> query = null)
        {
            var result = QueryLocationZones(builder =>
            {
                builder.Where("lz.LocationId = @locationId", new { locationId });

                switch (archivalState)
                {
                    case EntityArchivalState.ActiveOnly:
                        builder.Where("lz.ArchivedDateTimeUtc IS NULL");
                        break;
                    case EntityArchivalState.ArchivedOnly:
                        builder.Where("lz.ArchivedDateTimeUtc IS NOT NULL");
                        break;
                }

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        internal void InsertOrUpdate(LocationZoneDocument entity, IEnumerable<Point3D> points)
        {
            var spatialData = String.Format("POLYGON (({0}))", String.Join(", ", points.Select(x => x.X + " " + x.Y)));

            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE LocationZone
SET Name = @Name,
    LmsZoneId = @LmsZoneId,
    AutoUpdateLms = @AutoUpdateLms,
    LmsUpdateDelaySeconds = @LmsUpdateDelaySeconds,
    EnvoyZoneId = @EnvoyZoneId,
    SpatialData = @spatialData
WHERE Id = @Id;
IF @@ERROR = 0 AND @@ROWCOUNT = 0
    INSERT INTO LocationZone (
        LocationId,
        Name,
        LmsZoneId,
        AutoUpdateLms,
        LmsUpdateDelaySeconds,
        EnvoyZoneId,
        SpatialData
    ) VALUES (
        @LocationId,
        @Name,
        @LmsZoneId,
        @AutoUpdateLms,
        @LmsUpdateDelaySeconds,
        @EnvoyZoneId,
        @spatialData
    );", new
       {
           entity.Id,
           entity.LocationId,
           entity.Name,
           entity.LmsZoneId,
           entity.AutoUpdateLms,
           entity.LmsUpdateDelaySeconds,
           entity.EnvoyZoneId,
           spatialData
       });

                scope.Complete();
            }
        }

        internal void Archive(long id)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE LocationZone
SET ArchivedDateTimeUtc = GETUTCDATE()
WHERE Id = @id;", new { id });

                scope.Complete();
            }
        }

        private Paged<LocationZoneDocument> QueryLocationZones(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        lz.Id,
        lz.LocationId,
        lz.Name,
        lz.LmsZoneId,
        lz.AutoUpdateLms,
        lz.LmsUpdateDelaySeconds,
        lz.EnvoyZoneId,
        lz.SpatialData,
        lz.ArchivedDateTimeUtc,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwLocationZone lz
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("lz.Name, lz.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<LocationZoneDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<LocationZoneDocument>(data.Distinct(), total);
            }
        }
    }
}