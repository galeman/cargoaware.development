﻿using System;

namespace CargoAware.Services.PrintJob
{
    public static class AirWaybillFormatting
    {
        public static String GetEpc(String airlinePrefix, String airWaybill, int pieceNumber)
        {
            const String Version = "00";
            var result = String.Format("CAF{0}{1}{2}{3:00000}", Version, airlinePrefix, airWaybill, pieceNumber);
            return result;
        }
    }
}