﻿using System.Threading;
using System.Web.Http.Filters;

namespace CargoAware.Web.Infrastructure.Filters
{
    public class ExceptionHandlerFilter : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            ExceptionHelper.LogControllerException(
                Thread.CurrentPrincipal,
                actionExecutedContext.Request.RequestUri.AbsolutePath,
                actionExecutedContext.Exception);

            base.OnException(actionExecutedContext);
        }
    }
}