﻿namespace CargoAware.Services.Common
{
    public enum SmoothingFormula : byte
    {
        None,
        SimpleMovingAverage,
        ExponentialMovingAverage,
        Median,
        TakacsTimeBased,
        TakacsPointsBased,
        TakacsTimeBasedWithAverage,
        TakacsPointsBasedWithAverage,
    }
}