using CargoAware.Web.Infrastructure.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.Web.Infrastructure.Filters
{
    [Flags]
    public enum LogInState
    {
        //MustAcceptLegal = 1 << 0,
        MustSetPassword = 1 << 1,
        //MustEnterSudoPassword = 1 << 2,
    }

    public sealed class LogInStateFilter : ActionFilterAttribute
    {
        private readonly Tuple<LogInState, ActionResult>[] _Actions;


        public LogInStateFilter(params Tuple<LogInState, ActionResult>[] actions)
        {
            _Actions = actions;
        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var user = filterContext.HttpContext.User as UserPrincipal;
            if (user == null)
            {
                return;
            }

            var logInState = user.LogInState;
            var nextAction = _Actions.FirstOrDefault(x => logInState.HasFlag(x.Item1));
            if (nextAction == null)
            {
                return;
            }

            var redirect = RedirectTo(nextAction.Item2);
            if (Matches(redirect.RouteValues, filterContext.RouteData.Values))
            {
                return;
            }

            redirect.ExecuteResult(filterContext);
        }


        public static LogInState ModifyLogInState(LogInState @base, LogInState? statesToAdd = null, LogInState? statesToRemove = null)
        {
            var newLogInState = @base;

            if (statesToAdd.HasValue)
            {
                newLogInState |= statesToAdd.Value;
            }
            if (statesToRemove.HasValue)
            {
                newLogInState &= ~statesToRemove.Value;
            }

            return newLogInState;
        }

        private static Boolean Matches(IDictionary<String, Object> redirect, IDictionary<String, Object> current)
        {
            return Equals(current["controller"], redirect["controller"]) &&
                   Equals(current["action"], redirect["action"]);
        }

        private static RedirectToRouteResult RedirectTo(ActionResult result)
        {
            var callInfo = result.GetT4MVCResult();
            return new RedirectToRouteResult(callInfo.RouteValueDictionary);
        }
    }
}