﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150303135948)]
    public sealed class CreateKeyValue : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("KeyValue")
                  .WithColumn("Application").AsAnsiString(255).NotNullable()
                  .WithColumn("Key").AsAnsiString(255).NotNullable()
                  .WithColumn("Reference").AsAnsiString(int.MaxValue).Nullable()
                  .WithColumn("Value").AsAnsiString(int.MaxValue).Nullable()
                  .WithColumn("BinaryValue").AsBinary(int.MaxValue).Nullable()
                  .WithColumn("LastWriteDateTime").AsDateTime().NotNullable().WithDefault(SystemMethods.CurrentUTCDateTime);

            Create.PrimaryKey("PK_KeyValue").OnTable("KeyValue").Columns(new[] { "Application", "Key" });
        }
    }
}