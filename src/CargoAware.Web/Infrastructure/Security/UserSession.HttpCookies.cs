using CargoAware.Web.Infrastructure.Filters;
using CargoAware.Web.Infrastructure.Json.Net;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace CargoAware.Web.Infrastructure.Security
{
    public static partial class UserSession
    {
        private const String UpdateAuthenticationHttpCookieFlagName = "UpdateAuthenticationHttpCookie";


        #region // Public Static Methods
        // ==================================================

        public static void RestoreFromHttpCookie()
        {
            var context = HttpContext.Current;

            var httpAuthCookie = context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (httpAuthCookie == null)
            {
                LogOut();
                return;
            }

            var authCookie = FormsAuthentication.Decrypt(httpAuthCookie.Value);
            if (authCookie == null)
            {
                LogOut();
                return;
            }

            var cookieData = JsonConvert.DeserializeObject<HttpCookieData>(
                authCookie.UserData,
                new JsonSerializerSettings
                {
                    ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                    ContractResolver = new AppJsonContractResolver()
                });

            if (cookieData.UserId == 0)
            {
                LogOut();
                return;
            }

            var cachedUser = GetUserPrincipalFromCache(cookieData.UserId);
            if (!String.Equals(context.User.Identity.Name, cachedUser.Identity.Name, StringComparison.OrdinalIgnoreCase))
            {
                LogOut();
                return;
            }

            var currentLocation =
                cachedUser.Locations.FirstOrDefault(x => x.Id == cookieData.CurrentLocation) ??
                (cachedUser.Locations.Any() ? cachedUser.Locations.ElementAt(0) : null);

            cachedUser.Update(cookieData.LogInState);
            cachedUser.Update(currentLocation);

            var cookieTimestamp = new DateTime(cookieData.Timestamp);
            context.Items[UpdateAuthenticationHttpCookieFlagName] =
                DateTime.UtcNow.Subtract(cookieTimestamp).TotalSeconds > 5;
            ApplyPrincipal(cachedUser);
        }

        public static void UpdateAuthenticationHttpCookie(
            UserPrincipal userPrincipal,
            Boolean persistSession = false,
            Boolean onlyIfNecessary = false)
        {
            var context = HttpContext.Current;

            if (onlyIfNecessary)
            {
                var shouldUpdate = context.Items[UpdateAuthenticationHttpCookieFlagName] as Boolean?;
                if ((shouldUpdate.HasValue) && (!shouldUpdate.Value))
                {
                    return;
                }
            }

            // Cookie time works based on the local date/time, and not UTC
            var now = DateTime.Now;
            var expiration = persistSession
                ? now.AddYears(1)
                : now.Add(FormsAuthentication.Timeout);

            var cookieData = new HttpCookieData
            {
                UserId = userPrincipal.Id,
                LogInState = userPrincipal.LogInState,
                CurrentLocation = userPrincipal.CurrentLocation != null ? userPrincipal.CurrentLocation.Id : 0,
                Timestamp = DateTime.UtcNow.Ticks
            };
            var serializedCookieData = JsonConvert.SerializeObject(cookieData, Formatting.None);

            var authenticationCookie = new FormsAuthenticationTicket(
                2,
                userPrincipal.Username,
                now,
                expiration,
                persistSession,
                serializedCookieData);
            var encryptedAuthenticationCookie = FormsAuthentication.Encrypt(authenticationCookie);
            var httpAuthenticationCookie = new HttpCookie(FormsAuthentication.FormsCookieName, encryptedAuthenticationCookie)
            {
                HttpOnly = true,
                Secure = ApplicationEnvironment.IsRunningProduction || ApplicationEnvironment.IsUsingHttps
            };

            // if httpCookie is NOT assigned an expiration date, it will expire as soon as the browser is closed.
            if (persistSession)
            {
                httpAuthenticationCookie.Expires = expiration;
            }

            context.Response.Cookies.Remove(FormsAuthentication.FormsCookieName);
            context.Response.AppendCookie(httpAuthenticationCookie);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        // ==================================================
        #endregion


        private class HttpCookieData
        {
            // HttpCookieData is serialized into a FormsAuthentication cookie (which is encrypted using
            // a machine-specific two-way hash). When the user is retrieved using the cookie, both the
            // user and the token are verified against the database. To forge the cookie, an attacker
            // would have to know the cookie encryption key and a valid user/token combination.

            public long UserId { get; set; }
            public LogInState LogInState { get; set; }
            public long CurrentLocation { get; set; }
            public long Timestamp { get; set; }
        }
    }
}