﻿using CargoAware.Web.Infrastructure.Controllers;
using CargoAware.Web.Infrastructure.Security;
using System.Web.Mvc;
using System.Web.Routing;

namespace CargoAware.WebApp.Infrastructure.Controllers
{
    public abstract class AppMvcController : BaseMvcController<UserPrincipal>
    {
        private RouteValueDictionary _FrontPageRouteValues;

        protected override RouteValueDictionary FrontPageRouteValues
        {
            get
            {
                return _FrontPageRouteValues ??
                       (_FrontPageRouteValues = MVC.Sessions.FrontPage().GetT4MVCResult().RouteValueDictionary);
            }
        }
    }
}