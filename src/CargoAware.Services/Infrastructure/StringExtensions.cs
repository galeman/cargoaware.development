﻿using System;

namespace CargoAware.Services.Infrastructure
{
    public static class StringExtensions
    {
        /// <summary>
        /// Inserts the value if it doesn't already exist at startIndex.
        /// </summary>
        public static String InsertOnce(this String text, int startIndex, String value)
        {
            return String.Compare(text, startIndex, value, 0, value.Length) == 0
                ? text
                : text.Insert(startIndex, value);
        }
    }
}