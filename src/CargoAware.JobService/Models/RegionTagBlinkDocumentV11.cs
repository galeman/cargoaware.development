﻿using CargoAware.Services.Shipment;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace CargoAware.JobService.Models
{
    public class RegionTagBlinkDocument
    {
        [JsonProperty("regionId")]
        public String RegionId { get; set; }

        [JsonProperty("tagBlinks")]
        public JArray TagBlinksData { get; set; }

        public IEnumerable<TagBlinkDocument> GetTagBlinks()
        {
            var tagBlinksArray = (JArray)TagBlinksData[1];
            var result = tagBlinksArray.ToObject<IEnumerable<TagBlinkDocument>>();
            return result;
        }

        public class TagBlinkDocument
        {
            [JsonProperty("tagID")]
            public String Epc { get; set; }

            [JsonProperty("location")]
            public TagBlinkLocationDocument Location { get; set; }

            [JsonProperty("locateTime")]
            public JArray LocateTimeData { get; set; }

            public DateTime GetLocateTime()
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                var result = epoch.AddMilliseconds(LocateTimeData[1].Value<long>());
                return result.ToLocalTime();
            }

            public class TagBlinkLocationDocument
            {
                [JsonProperty("x")]
                public Decimal X { get; set; }

                [JsonProperty("y")]
                public Decimal Y { get; set; }

                [JsonProperty("z")]
                public Decimal Z { get; set; }
            }

            public class VendorSection
            {

            }

            public TagReadDocument ToTagRead(RegionTagBlinkDocument regionUpdate)
            {
                var result = new TagReadDocument
                {
                    Epc = Epc,
                    RegionManagerId = regionUpdate.RegionId,
                    X = Location.X * 3.2808m,
                    Y = Location.Y * 3.2808m,
                    Z = Location.Z * 3.2808m,
                    Rssi = 0, //x.Rssi,
                    Timestamp = GetLocateTime()
                };
                return result;
            }
        }
    }
}