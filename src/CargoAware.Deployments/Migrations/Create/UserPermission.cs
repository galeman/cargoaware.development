﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20140409232700)]
    public sealed class UserPermissions : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("UserPermission")
                  .WithColumn("UserId").AsInt64().Indexed().NotNullable()
                  .WithColumn("Permission").AsAnsiString(255).NotNullable();
        }
    }
}