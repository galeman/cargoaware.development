﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20140411160200)]
    public sealed class Shipment : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Shipment")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("FlightNumber").AsAnsiString(255).NotNullable()
                  .WithColumn("FlightDate").AsCustom("date").NotNullable()
                  .WithColumn("AirWaybill").AsAnsiString(255).NotNullable().Indexed()
                  .WithColumn("AirWaybillTagEpc").AsAnsiString(255).NotNullable()
                  .WithColumn("AirWaybillTagDateTime").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("Uld").AsAnsiString(255).NotNullable()
                  .WithColumn("UldTagEpc").AsAnsiString(255).NotNullable()
                  .WithColumn("UldTagDateTime").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("OpenContainerDateTime").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("CloseContainerDateTime").AsCustom("datetimeoffset(0)").Nullable()
                  .WithColumn("Note").AsAnsiString(int.MaxValue).NotNullable();
        }
    }
}