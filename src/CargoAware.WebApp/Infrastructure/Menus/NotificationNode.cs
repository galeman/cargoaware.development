﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.WebApp.Infrastructure.Menus
{
    public class NotificationNode
    {
        public NotificationNode()
        {
            DayStatusPairs = new List<Tuple<String, String, DateTime>>();
            TimeRange = new List<TimeRangeEntry>();
        }

        public String Title { get; set; }
        public IList<Tuple<String, String, DateTime>> DayStatusPairs { get; private set; }
        public IList<TimeRangeEntry> TimeRange { get; private set; }

        public static Tuple<DateTimeOffset, TimeSpan> CreateTimeRangeEntry(int year, int month, int day, int hour, int timezone, int durationInHours)
        {
            return Tuple.Create(
                new DateTimeOffset(year, month, day, hour, 0, 0, TimeSpan.FromHours(timezone)),
                TimeSpan.FromHours(durationInHours));
        }

        public IEnumerable<IMenuNode> GetMenuNodes()
        {
            var result =
                GetDayStatusMenuNodes()
                .Concat(GetTimeRangeMenuNodes());
            return result;
        }

        private IEnumerable<IMenuNode> GetDayStatusMenuNodes()
        {
            var today = DateTime.Today;
            var availableDayStatusPairs = DayStatusPairs.Where(x => x.Item3 >= today).ToArray();
            if (availableDayStatusPairs.Length == 0)
            {
                return new IMenuNode[0];
            }

            var result = new List<IMenuNode>(availableDayStatusPairs.Length + 1);
            if (!String.IsNullOrWhiteSpace(Title))
            {
                result.Add(new MenuLink(Title, "")
                {
                    PrependHtml = "<span class=\"text-error\"><strong>",
                    AppendHtml = "</strong></span>"
                });
            }
            result.AddRange(availableDayStatusPairs.Select(x => new MenuLink(x.Item1, "")
            {
                AppendHtml = String.Format(" - <span class=\"text-error\">{0}</span>", x.Item2)
            }));
            return result;
        }

        private IEnumerable<IMenuNode> GetTimeRangeMenuNodes()
        {
            var utcNow = new DateTimeOffset(DateTime.UtcNow);
            var displayCutOff = utcNow.AddDays(-5);
            var availableTimeRanges = TimeRange.Where(x => x.Start.AddHours(x.HoursDuration) >= displayCutOff).ToArray();
            if (availableTimeRanges.Length == 0)
            {
                return new IMenuNode[0];
            }

            var result = new List<IMenuNode>(availableTimeRanges.Length + 1);
            if (!String.IsNullOrWhiteSpace(Title))
            {
                result.Add(new MenuLink(Title, "")
                {
                    PrependHtml = "<span class=\"text-error\"><strong>",
                    AppendHtml = "</strong></span>"
                });
            }
            result.AddRange(availableTimeRanges.Select(x =>
            {
                var startDateTime = x.Start.ToLocalTime();
                var endDateTime = startDateTime.AddHours(x.HoursDuration);
                var displayFormat = startDateTime.Date != endDateTime.Date
                    ? "{0:ddd, MMM d} <small>({0:h tt})</small> - {1:ddd, MMM d} <small>({1:h tt})</small> | <small>UTC{0:%z}</small>"
                    : "{0:ddd, MMM d} | <small>{0:h tt} - {1:h tt}</small> | <small>UTC{0:%z}</small>";
                var display = String.Format(displayFormat, startDateTime, endDateTime);

                return new MenuLink("", "")
                {
                    AppendHtml =
                        "<span class=\"text-error\">" +
                        display +
                        "</span>" +
                        (endDateTime < utcNow ? " | <em>(past)</em>" : "")
                };
            }));
            return result;
        }

        public struct TimeRangeEntry
        {
            public TimeRangeEntry(int year, int month, int day, int hour, int timezone, int hoursDuration)
                : this()
            {
                Start = new DateTimeOffset(year, month, day, hour, 0, 0, TimeSpan.FromHours(timezone));
                HoursDuration = hoursDuration;
            }

            public DateTimeOffset Start { get; private set; }
            public int HoursDuration { get; private set; }
        }
    }
}