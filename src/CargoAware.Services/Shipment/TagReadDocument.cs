﻿using System;

namespace CargoAware.Services.Shipment
{
    public sealed class TagReadDocument
    {
        public long Id { get; set; }
        public String Epc { get; set; }
        public long LocationId { get; set; }
        public String RegionManagerId { get; set; }
        public Decimal X { get; set; }
        public Decimal Y { get; set; }
        public Decimal Z { get; set; }
        public Int16 Rssi { get; set; }
        public String Readers { get; set; }
        public DateTimeOffset Timestamp { get; set; }
        public long LocationZoneId { get; set; }
        public String LocationZoneName { get; set; }
        public Boolean FixedLocationRead { get; set; }
        public Boolean IsProcessed { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as TagReadDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}