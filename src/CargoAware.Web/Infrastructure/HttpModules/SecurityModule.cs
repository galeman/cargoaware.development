﻿using CargoAware.Services;
using System;
using System.Text;
using System.Web;
using System.Web.Security;

namespace CargoAware.Web.Infrastructure.HttpModules
{
    public class SecurityModule : IHttpModule
    {
        #region // Constructor + Destructor
        // ==================================================

        public void Init(HttpApplication context)
        {
            context.PreSendRequestHeaders += Application_PreSendRequestHeaders;
        }

        public void Dispose()
        {
        }

        // ==================================================
        #endregion


        #region // Event Methods
        // ==================================================

        private static void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            var context = ((HttpApplication)sender).Context;
            if (context == null)
            {
                return;
            }

            if (AppSettings.Instance.AppendSecurityHeaders)
            {
                AppendSecurityHeaders(context);
            }

            if (AppSettings.Instance.AppendResponseTimestampCookie)
            {
                AppendResponseTimestampCookie(context);
            }

            if (AppSettings.Instance.AppendBasicAuthenticationHeader)
            {
                AppendBasicAuthenticationHeader(context);
            }
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private static void AppendSecurityHeaders(HttpContext context)
        {
            #region // Setup the Content Security Policy String
            // ==================================================

            var contentSecurityPolicyBuilder = new StringBuilder();
            contentSecurityPolicyBuilder.Append("default-src ");
            if ((ApplicationEnvironment.IsRunningProduction) || (ApplicationEnvironment.IsUsingHttps))
            {
                contentSecurityPolicyBuilder.Append("https: ");
            }
            contentSecurityPolicyBuilder.AppendFormat(
                "'self' 'unsafe-inline' 'unsafe-eval' {0}; img-src 'self' data: {0} {1}",
                "https://piwik.metrc.com:443",
                "https://assets.braintreegateway.com:443");
            var contentSecurityPolicyString = contentSecurityPolicyBuilder.ToString();

            // ==================================================
            #endregion

            context.Response.Headers.Remove("Server");

            // Firefox
            context.Response.AppendHeader("X-Content-Security-Policy", contentSecurityPolicyString);
            // Webkit (Chrome, Safari)
            context.Response.AppendHeader("X-WebKit-CSP", contentSecurityPolicyString);
            // W3C
            context.Response.AppendHeader("Content-Security-Policy", contentSecurityPolicyString);

            context.Response.AppendHeader("Strict-Transport-Security", "max-age=31536000");

            context.Response.AppendHeader("X-Frame-Options", "SAMEORIGIN");
            context.Response.AppendHeader("X-XSS-Protection", "1; mode=block");
        }

        private static void AppendResponseTimestampCookie(HttpContext context)
        {
            var expiration = DateTime.UtcNow.Add(FormsAuthentication.Timeout);
            var expirationString = expiration.ToString("yyyy-MM-ddTHH:mm:ssZ");
            var timeoutCookie = new HttpCookie(Constants.TimeoutCookieName, expirationString)
            {
                HttpOnly = false,
                Secure = ApplicationEnvironment.IsRunningProduction || ApplicationEnvironment.IsUsingHttps
            };

            context.Response.AppendCookie(timeoutCookie);
        }

        private static void AppendBasicAuthenticationHeader(HttpContext context)
        {
            context.Response.AppendHeader("WWW-Authenticate", "Basic");
        }

        // ==================================================
        #endregion
    }
}