﻿using CargoAware.JobService.Infrastructure;
using log4net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


namespace CargoAware.JobService.Jobs
{
    [JobDescription("DataProcessingJob", "-a {JobName}")]
    public sealed class DataProcessingJob : BaseJob
    {
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string ProcessTagReadsUrl { get; set; }

        public DataProcessingJob()
        {
            ProcessTagReadsUrl = ConfigurationManager.AppSettings["ProcessTagReadsUrl"];
        }

        protected override void OnExecute()
        {
            //ContinueProcessing = true;
            var taskCancellation = new CancellationTokenSource();
            var token = taskCancellation.Token;
            Task task = null;

            Func<Task, Boolean> isTaskRunning = t => !(t == null || t.IsCanceled || t.IsCompleted || t.IsFaulted);
            Action<CancellationTokenSource> taskWork = (tok) =>
            {

                while (!tok.IsCancellationRequested)
                {
                    try
                    {
                        Task t = Task.Run(() => ProcessTagData());
                        t.Wait();
                        Thread.Sleep(1000);
                    }
                    catch (Exception ex)
                    {
                        Program.WriteOutput(_Log.Error, ex.ToString());
                    }
                }
            };

            while (ContinueProcessing)
            {
                if (!isTaskRunning(task))
                {
                    task = Task.Run(() => taskWork(taskCancellation));
                }
                Thread.Sleep(1000);
            }
        }

        private Boolean ProcessTagData()
        {
            Boolean result;
            try
            {
                using (var client = new HttpClient())
                //using (var content = new StringContent(JsonConvert.SerializeObject(""), Encoding.UTF8, "application/json"))
                using (var response = client.GetAsync(ProcessTagReadsUrl).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        result = true;
                    }
                    else
                    {
                        var errorMessage = response.Content.ReadAsStringAsync().Result;
                        Program.WriteOutput(_Log.Error, errorMessage);
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Program.WriteOutput(_Log.Error, ex.ToString());
                result = false;
            }

            return result;
        }

        protected override void OnPostExecute()
        {
        }
        public override IEnumerable<String> GetMissingParameters()
        {
            var missingParameters = new List<String>();
            return missingParameters;
        }
    }
}
