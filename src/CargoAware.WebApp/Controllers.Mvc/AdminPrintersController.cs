﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.Printer;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("admin/printers")]
    public partial class AdminPrintersController : AppMvcController
    {
        private static Lazy<PrinterRepository> _PrinterRepository;


        #region // Constructor + DI
        // ==================================================

        static AdminPrintersController()
        {
            Inject(Repositories.ForPrinters);
        }

        private static void Inject(Lazy<PrinterRepository> dependency)
        {
            _PrinterRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.AdminPrinters()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        [GET("addedit")]
        public virtual ActionResult AddEdit(Boolean adding, long[] ids)
        {
            if (!User.HasPermission(Permissions.User.AdminPrinters()))
            {
                throw UnauthorizedException();
            }

            var model = new AddEditPrintersModel
            {
                Adding = adding,
                Printers = _PrinterRepository.Value.GetPrinters(User.CurrentLocation.Id).Data
            };

            if (!adding)
            {
                if (ids != null && ids.Length != 0)
                {
                    model.Details = model.Printers
                        .Where(x => ids.Any(id => x.Id == id))
                        .Select(x => new AddEditPrintersDetailsModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            Type = x.Type,
                            Address = x.Address,
                            Port = x.Port,
                            ExternalId = x.ExternalId,
                            UldDefault = x.UldDefault,
                        });
                }
            }

            return View(model);
        }

        // ==================================================
        #endregion
    }
}