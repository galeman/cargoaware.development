﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.DataProcessing
{
    public static class TakacsPointsBased
    {
        /// <summary>
        /// Returns a copy of the processed data ordered by Timestamp, then by Id.
        /// </summary>
        /// <param name="data">Data to filter.</param>
        /// <param name="samples">Number of samples to consider for the Takacs Smoothing calculation.</param>
        /// <param name="standardDeviasionsFactor">Factor by which to multiply the Standard Deviasion when eliminating values outside of the limit.</param>
        public static IEnumerable<TagReadDocument> Filter(IEnumerable<TagReadDocument> data, int samples = 10, Decimal standardDeviasionsFactor = 1m, Boolean applyAverage = false)
        {
            var sortedDataArray = data.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
            var result = new List<TagReadDocument>(sortedDataArray.Length);

            if (samples < 2)
            {
                samples = 2;
            }

            for (var i = 2; i < sortedDataArray.Length; i++)
            {
                //  1 :  1 - 10 = Min(-9|0) = 0, Min(2|10) = 2
                //  2 :  2 - 10 = Min(-8|0) = 0, Min(3|10) = 3
                //  ...
                //  9 :  9 - 10 = Min(-1|0) = 0, Min(10|10) = 10
                // 15 : 15 - 10 = Min(5|0)  = 5, Min(15|10) = 10

                var startingPoint = Math.Max(i - samples, 0);
                var tagReadsInTimeSlice = sortedDataArray
                    .Skip(startingPoint)
                    .Take(Math.Min(i, samples))
                    .Select(x => new Point3D { X = x.X, Y = x.Y, Z = x.Z })
                    .ToArray();
                if (tagReadsInTimeSlice.Length <= 1)
                {
                    continue;
                }

                var averageOfTimeSlice = tagReadsInTimeSlice.Aggregate((r1, r2) =>
                    new Point3D { X = r1.X + r2.X, Y = r1.Y + r2.Y });
                averageOfTimeSlice.X /= tagReadsInTimeSlice.Length;
                averageOfTimeSlice.Y /= tagReadsInTimeSlice.Length;

                var stdDevOfTimeSlice = new Point3D
                {
                    X = (Decimal)Math.Sqrt(tagReadsInTimeSlice.Average(x => Math.Pow((Double)(x.X - averageOfTimeSlice.X), 2))),
                    Y = (Decimal)Math.Sqrt(tagReadsInTimeSlice.Average(y => Math.Pow((Double)(y.Y - averageOfTimeSlice.Y), 2)))
                };

                var lowerLimit = new Point3D
                {
                    X = averageOfTimeSlice.X - (stdDevOfTimeSlice.X * standardDeviasionsFactor),
                    Y = averageOfTimeSlice.Y - (stdDevOfTimeSlice.Y * standardDeviasionsFactor)
                };
                var upperLimit = new Point3D
                {
                    X = averageOfTimeSlice.X + (stdDevOfTimeSlice.X * standardDeviasionsFactor),
                    Y = averageOfTimeSlice.Y + (stdDevOfTimeSlice.Y * standardDeviasionsFactor)
                };

                var entry = sortedDataArray[i];
                if (entry.X >= lowerLimit.X && entry.X <= upperLimit.X &&
                    entry.Y >= lowerLimit.Y && entry.Y <= upperLimit.Y)
                {
                    var newEntry = entry.CopyCommonPropertiesTo<TagReadDocument>();
                    result.Add(newEntry);

                    if (applyAverage)
                    {
                        startingPoint = Math.Max(result.Count - samples, 0);
                        tagReadsInTimeSlice = result
                            .Skip(startingPoint)
                            .Take(Math.Min(result.Count, samples))
                            .Select(x => new Point3D { X = x.X, Y = x.Y, Z = x.Z })
                            .ToArray();
                        if (tagReadsInTimeSlice.Length != 1)
                        {
                            averageOfTimeSlice = tagReadsInTimeSlice.Aggregate((r1, r2) =>
                                new Point3D { X = r1.X + r2.X, Y = r1.Y + r2.Y });
                            newEntry.X = averageOfTimeSlice.X / tagReadsInTimeSlice.Length;
                            newEntry.Y = averageOfTimeSlice.Y / tagReadsInTimeSlice.Length;
                        }
                    }
                }
            }

            return result;
        }
    }
}