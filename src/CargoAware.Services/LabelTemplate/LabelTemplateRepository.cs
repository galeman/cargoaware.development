﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Linq;

namespace CargoAware.Services.LabelTemplate
{
    public sealed class LabelTemplateRepository : RepositoryBase
    {
        public LabelTemplateRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public bool DoesLabelTemplateWithSameNameExist(long locationId, String name, long? excludedId = null)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var sql = "SELECT TOP (1) 1 FROM LabelTemplate WHERE LocationId = @locationId AND Name = @name";

                if (excludedId.HasValue)
                {
                    sql = sql + " AND Id <> @excludedId";
                }

                return db.QueryBoolean(sql, new { locationId, name, excludedId });
            }
        }

        public LabelTemplateDocument GetLabelTemplateById(long id)
        {
            var result = QueryLabelTemplates(builder => builder.Where("lt.Id = @id", new { id })).Data;
            return result.FirstOrDefault();
        }

        public long GetLabelTemplateIdByName(String name)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM LabelTemplate
/**where**/;");

                builder.Where("Name = @name", new { name });

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        public Paged<LabelTemplateDocument> GetLabelTemplates(long locationId, Action<SqlBuilder> query = null, params LabelType[] labelTypes)
        {
            var result = QueryLabelTemplates(builder =>
            {
                builder.Where("lt.LocationId = @locationId", new { locationId });

                if (labelTypes.Length != 0)
                {
                    builder.Where("lt.[Type] IN @labelTypes", new { labelTypes });
                }

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        public LabelTemplateDocument GetActiveLabelTemplate(long locationId, LabelType labelType)
        {
            var result = QueryLabelTemplates(builder =>
                builder.Where("lt.LocationId = @locationId AND lt.[Type] = @labelType", new { locationId, labelType })).Data.FirstOrDefault();
            return result;
        }

        internal void InsertOrUpdate(LabelTemplateDocument entity)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
IF @IsActive = 1
    UPDATE LabelTemplate
    SET IsActive = 0
    WHERE Id <> @Id AND LocationId = @LocationId AND [Type] = @Type;
UPDATE LabelTemplate
SET Name = @Name,
    [Type] = @Type,
    Language = @Language,
    Code = @Code,
    Version = Version + (CASE WHEN Code <> @Code THEN 1 ELSE 0 END),
    IsActive = @IsActive
WHERE Id = @Id;
IF @@ERROR = 0 AND @@ROWCOUNT = 0
    INSERT INTO LabelTemplate (
        LocationId,
        Name,
        [Type],
        Language,
        Code,
        Version,
        IsActive
    ) VALUES (
        @LocationId,
        @Name,
        @Type,
        @Language,
        @Code,
        1,
        @IsActive
    );", entity);

                scope.Complete();
            }
        }

        internal void Archive(long id)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
DELETE FROM LabelTemplate
WHERE Id = @id;", new { id });

                scope.Complete();
            }
        }

        private Paged<LabelTemplateDocument> QueryLabelTemplates(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        lt.Id,
        lt.LocationId,
        lt.Name,
        lt.[Type],
        lt.Language,
        lt.Code,
        lt.Version,
        lt.IsActive,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwLabelTemplate lt
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("lt.Name, lt.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<LabelTemplateDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<LabelTemplateDocument>(data.Distinct(), total);
            }
        }
    }
}