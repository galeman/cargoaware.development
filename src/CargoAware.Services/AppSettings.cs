﻿using System;

namespace CargoAware.Services
{
    public sealed class AppSettings
    {
        private AppSettings()
        {
            AppendSecurityHeaders = true;
            AppendResponseTimestampCookie = true;
            AppendBasicAuthenticationHeader = false;

            AllowedLogInFailures = 2;
            LogInFailureLockOutWindow = TimeSpan.FromMinutes(5);

            MinimumPasscodeLength = 4;

            MinimumPasswordLength = 8;
            PasswordsMustIncludeOneOrMoreLowercaseLetters = true;
            PasswordsMustIncludeOneOrMoreNumbers = true;
            PasswordsMustIncludeOneOrMoreSymbols = true;
            PasswordsMustIncludeOneOrMoreUppercaseLetters = true;
        }

        public Boolean AppendSecurityHeaders { get; set; }
        public Boolean AppendResponseTimestampCookie { get; set; }
        public Boolean AppendBasicAuthenticationHeader { get; set; }

        public int AllowedLogInFailures { get; set; }
        public TimeSpan LogInFailureLockOutWindow { get; set; }

        public int MinimumPasscodeLength { get; set; }

        public int MinimumPasswordLength { get; set; }
        public Boolean PasswordsMustIncludeOneOrMoreLowercaseLetters { get; set; }
        public Boolean PasswordsMustIncludeOneOrMoreNumbers { get; set; }
        public Boolean PasswordsMustIncludeOneOrMoreSymbols { get; set; }
        public Boolean PasswordsMustIncludeOneOrMoreUppercaseLetters { get; set; }

        public static readonly AppSettings Instance = new AppSettings();
    }
}