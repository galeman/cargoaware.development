﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/system")]
    public sealed class SystemApiController : AppApiController
    {
        #region // API Commands
        // ==================================================

        [POST("noop"), HttpPost]
        public HttpResponseMessage Noop()
        {
            // Noop = NOP = No Operation = http://en.wikipedia.org/wiki/NOP
            // It's just a dummy method to return a "HTTP 200 OK" response
            return Request.CreateResponse(HttpStatusCode.OK, true);
        }

        // ==================================================
        #endregion
    }
}