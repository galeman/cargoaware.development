﻿using Microsoft.Security.Application;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Web;

namespace CargoAware.Web.Infrastructure.Extensions
{
    public static class ObjectExtensions
    {
        public static HtmlString AsJson(this Object model)
        {
            var result = Encoder.JavaScriptEncode(JsonConvert.SerializeObject(model, new StringEnumConverter()), false);
            return new HtmlString(result);
        }
    }
}