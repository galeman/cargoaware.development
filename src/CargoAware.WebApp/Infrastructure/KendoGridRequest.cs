﻿using CargoAware.Services.Infrastructure.Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;

namespace CargoAware.WebApp.Infrastructure
{
    [ModelBinder(typeof(ModelBinder))]
    public class KendoGridRequest
    {
        #region // Constructor
        // ==================================================

        public KendoGridRequest()
        {
            Filter = new FilterObject();
            Group = new GroupObject[0];
            Sort = new SortObject[0];
            Skip = 0;
            Take = int.MaxValue;
        }

        // ==================================================
        #endregion


        #region // Properties
        // ==================================================

        public KendoGridRequest Request
        {
            set
            {
                Filter = value.Filter;
                Group = value.Group;
                Sort = value.Sort;
                Skip = value.Skip;
                Take = value.Take;
            }
        }

        public FilterObject Filter { get; set; }
        public GroupObject[] Group { get; set; }
        public SortObject[] Sort { get; set; }
        public int Skip { get; set; }
        public int Take { get; set; }

        // ==================================================
        #endregion


        #region // Public Methods
        // ==================================================

        public void Apply(SqlBuilder builder)
        {
            ApplySorting(builder, Sort);
            ApplyFiltering(builder, Filter);
            ApplySkipTake(builder, Skip, Take);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        private static String GetColumnName(SqlBuilder builder, String fieldName)
        {
            var result = fieldName;

            if (builder != null)
            {
                String value;
                if (builder.ColumnAliases.TryGetValue(fieldName, out value))
                {
                    result = value;
                }
            }

            return result;
        }

        private static void ApplySorting(SqlBuilder builder, IEnumerable<SortObject> sort)
        {
            foreach (var item in sort)
            {
                String sql;
                String sqlAlias;

                if (String.Equals(item.Dir, "asc"))
                {
                    sql = String.Format("{0} ASC", GetColumnName(builder, item.Field));
                    sqlAlias = String.Format("{0} ASC", item.Field);
                }
                else
                {
                    sql = String.Format("{0} DESC", GetColumnName(builder, item.Field));
                    sqlAlias = String.Format("{0} DESC", item.Field);
                }

                builder.OrderBy(sql);
                builder.OrderByAlias(sqlAlias);
            }
        }

        private static void ApplyFiltering(SqlBuilder builder, FilterObject filter)
        {
            if (filter == null || !filter.Filters.Any())
            {
                return;
            }

            var @params = new ExpandoObject();
            var dict = (IDictionary<String, Object>)@params;
            var sql = BuildWhere(builder, filter, dict);
            builder.Where(sql, @params);
        }

        private static void ApplySkipTake(SqlBuilder builder, long skip, long take)
        {
            builder.SkipTake(skip, take);
        }

        private static String BuildWhere(SqlBuilder builder, FilterObject filter, IDictionary<String, Object> @params)
        {
            String result;

            switch (filter.Logic)
            {
                case "and":
                case "or":
                    var predicates = filter.Filters
                        .Select(f => BuildWhere(builder, f, @params))
                        .ToArray();
                    result = String.Format(
                        "({0})",
                        String.Join(String.Format(" {0} ", filter.Logic.ToUpperInvariant()), predicates));
                    break;

                default:
                    result = BuildWhere(builder, filter.Field, filter.Operator, filter.Value, @params);
                    break;
            }

            return result;
        }

        private static String BuildWhere(SqlBuilder builder, String field, String @operator, String value, IDictionary<String, Object> @params)
        {
            if (!Regex.IsMatch(field, @"[A-z0-9_\.]+"))
            {
                throw new ArgumentException("A field used for filtering contains invalid characters.", "field");
            }


            const String DefaultCondition = "1=1";
            var paramName = String.Format("@p{0}", @params.Count);

            field = GetColumnName(builder, field);

            String sql;
            switch (@operator)
            {
                case "eq":
                    sql = String.Format("{0} = {1}", field, paramName);
                    break;
                case "neq":
                    sql = String.Format("{0} <> {1}", field, paramName);
                    break;
                case "contains":
                    sql = String.Format("{0} LIKE {1}", field, paramName);
                    value = String.Format("%{0}%", value);
                    break;
                case "doesnotcontain":
                    sql = String.Format("{0} NOT LIKE {1}", field, paramName);
                    value = String.Format("%{0}%", value);
                    break;
                case "startswith":
                    sql = String.Format("{0} LIKE {1}", field, paramName);
                    value = String.Format("{0}%", value);
                    break;
                case "endswith":
                    sql = String.Format("{0} LIKE {1}", field, paramName);
                    value = String.Format("%{0}", value);
                    break;
                case "gte":
                    sql = String.Format("{0} >= {1}", field, paramName);
                    break;
                case "gt":
                    sql = String.Format("{0} > {1}", field, paramName);
                    break;
                case "lte":
                    sql = String.Format("{0} <= {1}", field, paramName);
                    break;
                case "lt":
                    sql = String.Format("{0} < {1}", field, paramName);
                    break;
                default:
                    sql = DefaultCondition;
                    break;
            }

            if (sql != DefaultCondition)
            {
                @params[paramName] = value;
            }

            return sql;
        }

        // ==================================================
        #endregion


        #region // Supporting Classes
        // ==================================================

        public sealed class FilterObject
        {
            public FilterObject()
            {
                Filters = new FilterObject[0];
            }

            public FilterObject[] Filters { get; set; }
            public String Logic { get; set; }
            public String Field { get; set; }
            public String Operator { get; set; }
            public String Value { get; set; }
        }

        public sealed class GroupObject
        {
        }

        public sealed class SortObject
        {
            public String Field { get; set; }
            public String Dir { get; set; }
        }

        // ==================================================
        #endregion


        #region // Model Binder
        // ==================================================

        public sealed class ModelBinder : IModelBinder
        {
            public Object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
            {
                if (bindingContext == null)
                {
                    throw new ArgumentException("The parameter \"bindingContext\" cannot be null.", "bindingContext");
                }
                var inputStream = controllerContext.HttpContext.Request.InputStream;
                inputStream.Position = 0;
                var content = new StreamReader(controllerContext.HttpContext.Request.InputStream).ReadToEnd();
                return JsonConvert.DeserializeObject<KendoGridRequest>(content);
            }
        }

        // ==================================================
        #endregion
    }

    public class KendoGridRequest<T> : KendoGridRequest
    {
        public T Data { get; set; }
    }

    public static class KendoGridRequestExtensions
    {
        public static Action<SqlBuilder> GetApplyMethod(this KendoGridRequest request)
        {
            Action<SqlBuilder> result;

            if (request != null)
                result = request.Apply;
            else
                result = null;

            return result;
        }
    }
}