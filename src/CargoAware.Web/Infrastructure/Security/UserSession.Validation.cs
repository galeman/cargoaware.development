using CargoAware.Services.User;
using System;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace CargoAware.Web.Infrastructure.Security
{
    public static partial class UserSession
    {
        private const String InvalidCredentials = "Unknown username, or invalid password.";
        private const String InvalidLogInKey = "Invalid, or expired, log-in key.";
        private const String NoPermissions = "Permission to log in denied, or it has been revoked.";


        #region // Public Static Methods
        // ==================================================

        public static ValidateCredentialsResult ValidateCredentials(
            ValidationType validation,
            String username = null,
            String password = null,
            String passcode = null,
            String key = null)
        {
            var validateCredentials = validation != ValidationType.Key;
            var user = validateCredentials
                ? _UserRepository.Value.GetAuthorizationDetailsByUsername(username)
                : _UserRepository.Value.GetAuthorizationDetailsByLogInKey(key);

            var loginSuccess = false;
            String failureReason;

            if (user == null)
            {
                failureReason = validateCredentials ? InvalidCredentials : InvalidLogInKey;
            }
            else if (user.IsLocked())
            {
                failureReason = BuildLockOutNotification(user);
            }
            else
            {
                var permissions = user
                    .Permissions
                    .Select(p => new PermissionKey(p))
                    .ToArray();
                if (permissions.Length == 0)
                {
                    failureReason = NoPermissions;
                }
                // Passcode, Password, and Key verification should always be the last thing
                else if (validation == ValidationType.Passcode && !user.VerifyPasscode(passcode))
                {
                    failureReason = InvalidCredentials;
                }
                else if (validation == ValidationType.Password && !user.VerifyPassword(password))
                {
                    failureReason = InvalidCredentials;
                }
                else if (validation == ValidationType.Key && !user.IsLogInKeyValid())
                {
                    failureReason = InvalidLogInKey;
                }
                else
                {
                    loginSuccess = true;
                    failureReason = "";
                }
            }

            var result = new ValidateCredentialsResult
            {
                Successful = loginSuccess,
                AuthorizationDetails = user,
                ErrorMessage = failureReason
            };
            return result;
        }

        public static ValidateCredentialsResult Authenticate(
            ValidationType validation,
            String username = null,
            String password = null,
            String passcode = null,
            String key = null)
        {
            var credentialResult = ValidateCredentials(validation, username, password, passcode, key);
            var user = credentialResult.AuthorizationDetails;
            var host = HttpContext.Current.Request.UserHostAddress;

            if (credentialResult.Successful)
            {
                if (!user.LoggedInRecently(TimeSpan.FromMinutes(15)))
                {
                    //sender(new ReportLogInSuccess(user.Id, host));
                }
            }
            else
            {
                if (user != null)
                {
                    //sender(new ReportLogInFailure(user.Id, host, credentialResult.ErrorMessage));
                }
            }

            return credentialResult;
        }

        public static void LogOut()
        {
            var user = HttpContext.Current.User as UserPrincipal;
            if (user != null)
            {
                RemoveUserPrincipalCache(user.Id);
            }

            FormsAuthentication.SignOut();
            ApplyPrincipal(UserPrincipal.Anonymous);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        private static String BuildLockOutNotification(AuthorizationDetails user)
        {
            // ReSharper disable PossibleInvalidOperationException
            var lockedOutUntil = user.LockedUntil.Value;
            // ReSharper restore PossibleInvalidOperationException
            var now = DateTime.UtcNow;

            if (lockedOutUntil.Year - now.Year > 1)
            {
                return NoPermissions;
            }

            var diff = lockedOutUntil - now;
            var timer = diff.TotalHours > 1
                ? String.Format("Account locked out until {0:yyyy-MM-dd HH:mm}.", lockedOutUntil)
                : String.Format("Account locked out for {0} minutes.", Math.Ceiling(diff.TotalMinutes));

            return timer;
        }

        // ==================================================
        #endregion


        public class ValidateCredentialsResult
        {
            public Boolean Successful { get; set; }
            public AuthorizationDetails AuthorizationDetails { get; set; }
            public String ErrorMessage { get; set; }
        }
    }
}