﻿using CargoAware.Services.Common;
using FluentMigrator;
using System;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150115160012)]
    public sealed class Location : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Location")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("Name").AsAnsiString(255).NotNullable()
                  .WithColumn("AxisDirection").AsByte().NotNullable()
                  .WithColumn("OriginLocation").AsByte().NotNullable()
                  .WithColumn("ApiKey").AsGuid().NotNullable()
                  .WithColumn("IsArchived").AsBoolean().WithDefaultValue(false);

            Insert.IntoTable("Location").Row(new
            {
                Name = "Default",
                AxisDirection = (byte)AxisDirection.Normal,
                OriginLocation = (byte)OriginLocation.TopLeft,
                ApiKey = Guid.Empty
            });
        }
    }
}