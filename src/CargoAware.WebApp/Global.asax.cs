﻿using AttributeRouting.Web.Http.WebHost;
using AttributeRouting.Web.Mvc;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using CargoAware.Web.Infrastructure;
using CargoAware.Web.Infrastructure.Filters;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Settings;
using Newtonsoft.Json.Converters;
using StackExchange.Profiling;
using StackExchange.Profiling.Mvc;
using StackExchange.Profiling.SqlFormatters;
using System;
using System.Diagnostics;
using System.Web;
using System.Web.Configuration;
using System.Web.Helpers;
using System.Web.Http;
using System.Web.Http.Filters;
using System.Web.Mvc;
using System.Web.Routing;

namespace CargoAware.WebApp
{
    public class MvcApplication : HttpApplication
    {
        private static Boolean _MiniProfilerEnabled;
        private static readonly Object OneTimeInitLock = new Object();
        private static Boolean _OneTimeInitCompleted;


        // ASP.NET Application Life Cycle Overview for IIS 7.0
        // http://msdn.microsoft.com/en-us/library/bb470252.aspx


        #region // HttpApplication Event Handlers
        // ==================================================

        protected void Application_Start()
        {
            MvcHandler.DisableMvcResponseHeader = true;

            // SqlServerTypes was manually changed to "public" from "internal"
            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));

            var stopwatch = Stopwatch.StartNew();
            Bootstrap();
            stopwatch.Stop();
            Debug.WriteLine("Startup took {0:N0} milliseconds.", stopwatch.ElapsedMilliseconds);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
#if (DEBUG)
            var url = Request.Url.PathAndQuery;
            if (!url.ToLower().Contains("public"))
            {
                Debug.WriteLine("Begin Request: {0}", url);
                Context.Items["PageLoadTimer"] = Stopwatch.StartNew();
            }
#endif

            lock (OneTimeInitLock)
            {
                if (!_OneTimeInitCompleted)
                {
                    _OneTimeInitCompleted = true;
                    ApplicationEnvironment.VerifyProduction();
                    //EmailSender.BaseAppUrl = Request.Url.GetLeftPart(UriPartial.Authority);
                    AntiForgeryConfig.CookieName = Constants.AntiForgeryCookieName;
                    AntiForgeryConfig.RequireSsl = ApplicationEnvironment.IsRunningProduction;
                }
            }

            if (_MiniProfilerEnabled)
            {
                MiniProfiler.Start();
            }
        }

        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            UserSession.RestoreFromHttpCookie();
        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            MiniProfiler.Stop();

#if (DEBUG)
            var url = Request.Url.PathAndQuery;
            if (!url.ToLower().Contains("public"))
            {
                var timer = Context.Items["PageLoadTimer"] as Stopwatch;
                if (timer != null)
                {
                    timer.Stop();
                    Debug.WriteLine("Request {0} took {1:N0} milliseconds. ", url, timer.ElapsedMilliseconds);
                }
            }
#endif
        }

        protected void Application_PreSendRequestHeaders(Object sender, EventArgs e)
        {
            UpdateAuthenticationCookie();
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private void UpdateAuthenticationCookie()
        {
            var user = User as UserPrincipal;
            if (user == null)
            {
                return;
            }

            UserSession.UpdateAuthenticationHttpCookie(user, onlyIfNecessary: true);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        private static void Bootstrap()
        {
            var logInStateFilters = new[]
            {
                // listed in the order they will be applied/required
                //Tuple.Create(LogInState.MustAcceptLegal, MVC.Sessions.LoginTermsAndConditions()),
                Tuple.Create(LogInState.MustSetPassword, MVC.UserProfile.Index()),
                //Tuple.Create(LogInState.MustEnterSudoPassword, MVC.Sessions.Sudo())
            };

            ConfigureMiniProfiler();
            ConfigureGlobalFormatters();
            RegisterMvcControllers();
            RegisterApiControllers();
            RegisterViewEngines();
            DeserializeModelsWith(new ImmutableModelBinder());
            FilterEachMvcRequestUsing(new LogInStateFilter(logInStateFilters), new ProfilingActionFilter());
            FilterEachApiRequestUsing(new ExceptionHandlerFilter());
            HostEngineInternally();
        }

        private static void ConfigureMiniProfiler()
        {
            _MiniProfilerEnabled = MiniProfilerSettings
                .ReadFrom(WebConfigurationManager.OpenWebConfiguration("~"))
                .IsEnabled;

            // TODO: Need to add the table creation scripts for MiniProfiler
            MiniProfiler.Settings.SqlFormatter = new SqlServerFormatter();
            MiniProfiler.Settings.PopupToggleKeyboardShortcut = null;
            MiniProfiler.Settings.ShowControls = true;
        }

        private static void ConfigureGlobalFormatters()
        {
            var formatters = GlobalConfiguration.Configuration.Formatters;

            // remove the XML formatter - not necessary
            formatters.Remove(formatters.XmlFormatter);

            // configure JSON formatter
            formatters.JsonFormatter.Indent = false;
            formatters.JsonFormatter.SerializerSettings.Converters.Add(new StringEnumConverter());
        }

        private static void RegisterMvcControllers()
        {
            RouteTable.Routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            RouteTable.Routes.MapAttributeRoutes();
        }

        private static void RegisterApiControllers()
        {
            GlobalConfiguration.Configuration.Routes.MapHttpAttributeRoutes();
        }

        private static void RegisterViewEngines()
        {
            var viewEngine = new RazorViewEngine
            {
                FileExtensions = new[]
                {
                    "cshtml"
                },
                // {0} = Action name
                // {1} = Controller name
                MasterLocationFormats = new[]
                {
                    "~/Views/Shared/{0}.cshtml"
                },
                PartialViewLocationFormats = new[]
                {
                    "~/Views/{1}/{0}.cshtml",
                    "~/Views/{1}/Partials/{0}.cshtml",
                    "~/Views/Shared/{0}.cshtml"
                },
                ViewLocationFormats = new[]
                {
                    "~/Views/{1}/{0}.cshtml"
                }
            };
            var wrappedEngine = new ProfilingViewEngine(viewEngine);

            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(wrappedEngine);
        }

        private static void DeserializeModelsWith(IModelBinder defaultModelBinder)
        {
            ModelBinders.Binders.DefaultBinder = defaultModelBinder;
        }

        private static void FilterEachMvcRequestUsing(params Object[] filters)
        {
            Array.ForEach(filters, GlobalFilters.Filters.Add);
        }

        private static void FilterEachApiRequestUsing(params IFilter[] filters)
        {
            Array.ForEach(filters, GlobalConfiguration.Configuration.Filters.Add);
        }

        private static void HostEngineInternally()
        {
            //#if (DEBUG)
            //            var log = Loggers.GetExceptionLogger();
            //            SystemEvents.Subscribe(Observer.Create<EventMessage>(log.Warn));
            //#endif

            //            var emailSenderHandler = new EmailSender();
            //            Engine.Initialize(emailSenderHandler);
            Client.ViewStore = () => new RepositoryConnectionFactory(ConnectionStrings.ViewStore);
        }

        // ==================================================
        #endregion
    }
}