﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.PrintJob;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using System;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/printjobs")]
    public class PrintJobsApiController : AppApiController
    {
        private static Lazy<PrintJobRepository> _PrintJobRepository;


        #region // Constructor + DI
        // ==================================================

        static PrintJobsApiController()
        {
            Inject(Repositories.ForPrintJobs);
        }

        private static void Inject(Lazy<PrintJobRepository> dependency)
        {
            _PrintJobRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<PrintJobDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.ViewPrintJobs()) &&
                !User.HasPermission(Permissions.User.ManagePrintJobs()))
            {
                throw UnauthorizedException();
            }

            var result = _PrintJobRepository.Value.GetPrintJobs(User.CurrentLocation.Id, query: request.GetApplyMethod());
            return result;
        }

        [POST("archive"), HttpPost]
        public HttpResponseMessage Archive([FromBody] long id)
        {
            if (!User.HasPermission(Permissions.User.ManagePrintJobs()))
            {
                throw UnauthorizedException();
            }

            PrintJobService.CancelPrintJob(new PrintJobDocument
            {
                Id = id
            });

            return SuccessResponse(true);
        }

        // ==================================================
        #endregion
    }
}