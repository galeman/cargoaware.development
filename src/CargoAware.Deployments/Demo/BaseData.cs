﻿using CargoAware.Services;

namespace CargoAware.Deployments.Demo
{
    public static class BaseData
    {
        public static void Setup()
        {
            var developer = Helper.CreateUser(
                "franwell-dev",
                "Franwell Developers",
                "Jesse.Naranjo@Franwell.com",
                new[] { Permissions.User.FriendlyDeveloper },
                0,
                false);
        }
    }
}