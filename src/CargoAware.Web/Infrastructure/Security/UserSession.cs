using CargoAware.Services;
using CargoAware.Services.Location;
using CargoAware.Services.User;
using CargoAware.Web.Infrastructure.Filters;
using System;
using System.Threading;
using System.Web;

namespace CargoAware.Web.Infrastructure.Security
{
    public static partial class UserSession
    {
        private static readonly Lazy<UserRepository> _UserRepository;


        #region // Constructor + DI
        // ==================================================

        static UserSession()
        {
            _UserRepository = Repositories.ForUsers;
        }

        // ==================================================
        #endregion


        #region // Public Static Methods
        // ==================================================

        public static void Update(AuthorizationDetails authorizationDetails, LogInState logInState)
        {
            var user = UserPrincipal.FromAuthorizationDetails(authorizationDetails);
            user.Update(logInState);

            UpdateAuthenticationHttpCookie(user);
            ApplyPrincipal(user);
        }

        public static void Update(
            LogInState? newLogInState = null, LogInState? statesToAdd = null, LogInState? statesToRemove = null)
        {
            var user = HttpContext.Current.User as UserPrincipal;
            if (user == null)
            {
                return;
            }

            if (!newLogInState.HasValue)
            {
                newLogInState = user.LogInState;
            }
            newLogInState = LogInStateFilter.ModifyLogInState(newLogInState.Value, statesToAdd, statesToRemove);
            user.Update(newLogInState.Value);

            UpdateAuthenticationHttpCookie(user);
            ApplyPrincipal(user);
        }

        public static void Update(LocationDocument location)
        {
            var user = HttpContext.Current.User as UserPrincipal;
            if (user == null)
            {
                return;
            }

            user.Update(location);

            UpdateAuthenticationHttpCookie(user);
            ApplyPrincipal(user);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        private static void ApplyPrincipal(UserPrincipal user)
        {
            HttpContext.Current.User = user;
            Thread.CurrentPrincipal = user;
        }

        // ==================================================
        #endregion
    }
}