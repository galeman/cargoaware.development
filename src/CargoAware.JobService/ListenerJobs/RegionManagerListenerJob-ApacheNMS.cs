﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using System.Reflection;
using System.Threading;
using Apache.NMS.Stomp.Commands;
using CargoAware.JobService.Infrastructure;
using CargoAware.JobService.Jobs;
using CargoAware.Services;
using CargoAware.Services.Shipment;
using log4net;
using Apache.NMS;
using Apache.NMS.Stomp;

namespace CargoAware.JobService.ListenerJobs
{
    [JobDescription("RegionManagerListener", "-a {JobName}")]
    public sealed class RegionManagerListenerJob : BaseJob
    {
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RegionManagerListenerJob()
        {
        }

        protected override void OnExecute() // (IJobExecutionContext context)
        {
            var connectionType = typeof(Connection);
            var connectionInfoField = connectionType.GetField("info", BindingFlags.NonPublic | BindingFlags.Instance);

            //var socket = new ClientWebSocket();
            var connectionFactory = new NMSConnectionFactory(new Uri("stomp:tcp://localhost:61614"))
            {
                //AcknowledgementMode = Apache.NMS.AcknowledgementMode.ClientAcknowledge
            };
            using (var connection = connectionFactory.CreateConnection("admin", "admin"))
            using (var session = connection.CreateSession())
            {
                var connectionInfo = (ConnectionInfo)connectionInfoField.GetValue(connection);
                connectionInfo.Host = "/";

                Program.WriteOutput(_Log.Info, "Connection and Session created.");

                connection.ExceptionListener += exception =>
                {
                    Program.WriteOutput(_Log.Error, exception.ToString());
                    Environment.Exit(0);
                };
                
                var topic = session.GetTopic("tagBlinks.00895ac093db4af8b4d47062ad5a6b15");
                Program.WriteOutput(_Log.Info, "Topic created.");

                using (var consumer = session.CreateConsumer(topic))
                {
                    Program.WriteOutput(_Log.Info, "Consumer created.");

                    connection.Start();
                    Program.WriteOutput(_Log.Info, "Connection started.");

                    consumer.Listener += message =>
                    {
                        var text = (ITextMessage)message;
                        message.Acknowledge();

                        Program.WriteOutput(_Log.Info, text.Text);
                    };

                    Program.WriteOutput(_Log.Info, "Entering wait loop.");
                    while (true)
                    {
                        Thread.Sleep(0);
                        Thread.Sleep(1000);
                    }
                }
            }
        }

        public override IEnumerable<String> GetMissingParameters()
        {
            var missingParameters = new List<String>();
            return missingParameters;
        }
    }
}