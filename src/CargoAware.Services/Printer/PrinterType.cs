﻿namespace CargoAware.Services.Printer
{
    public enum PrinterType : byte
    {
        // ReSharper disable InconsistentNaming
        ZebraR110Xi4,
        ZebraRZ600
        // ReSharper restore InconsistentNaming
    }
}