﻿using CargoAware.Web.Infrastructure.Extensions;
using HtmlTags;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Routing;

namespace CargoAware.WebApp.Infrastructure.Menus
{
    public sealed class DropdownMenu : List<IMenuNode>, IMenuNode
    {
        private readonly string _ButtonGroupCssClasses;
        private readonly String _AnchorCssClasses;
        private readonly String _ListCssClasses;


        private enum DropdownType
        {
            ButtonDropdown,
            MenuDropdown,
            SubmenuDropdown
        }


        public DropdownMenu(String text, String buttonGroupCssClasses = "", String anchorCssClasses = "", String listCssClasses = "")
        {
            Text = text;
            _ButtonGroupCssClasses = buttonGroupCssClasses;
            _AnchorCssClasses = anchorCssClasses;
            _ListCssClasses = listCssClasses;
        }


        public String Text { get; set; }
        /// <summary>
        /// Always perform an HTML Escape on all user-input being assigned to this property.
        /// </summary>
        public String PrependHtml { get; set; }
        /// <summary>
        /// Always perform an HTML Escape on all user-input being assigned to this property.
        /// </summary>
        public String AppendHtml { get; set; }
        public Boolean Selected { get { return this.Any(m => m.Selected); } }
        public Boolean Visible { get { return this.Any(x => !(x is SeparatorNode) && x.Visible); } }
        // ReSharper disable ReturnTypeCanBeEnumerable.Global
        public IList<IMenuNode> Items { get { return this; } }
        // ReSharper restore ReturnTypeCanBeEnumerable.Global


        public HtmlTag ToHtmlTag()
        {
            return ToMenuDropdown();
        }

        public HtmlTag ToButtonDropdown()
        {
            return BuildDropdown(DropdownType.ButtonDropdown);
        }

        public HtmlTag ToMenuDropdown()
        {
            return BuildDropdown(DropdownType.MenuDropdown);
        }

        private HtmlTag ToSubmenuDropdown()
        {
            return BuildDropdown(DropdownType.SubmenuDropdown);
        }

        private HtmlTag BuildDropdown(DropdownType type)
        {
            if (!Visible)
            {
                return null;
            }

            var listItems = GetListItems().ToArray();
            if (listItems.Length == 0)
            {
                return null;
            }

            HtmlTag result;

            var dropdownLink = new HtmlTag("a")
                .AddClass(("dropdown-toggle " + _AnchorCssClasses).TrimEnd())
                .Attr("data-toggle", "dropdown")
                .AppendHtml(PrependHtml)
                .AppendHtml(Encoder.HtmlEncode(Text))
                .AppendHtml(AppendHtml);
            var dropdownMenu = BuildDropdownMenu(listItems);

            switch (type)
            {
                case DropdownType.ButtonDropdown:
                    dropdownLink.AddClass("btn");
                    result = new HtmlTag("div")
                        .AddClass(("btn-group " + _ButtonGroupCssClasses).TrimEnd())
                        .Append(dropdownLink)
                        .Append(dropdownMenu);
                    break;
                default:
                    result = new HtmlTag("li")
                        .AddClass(type == DropdownType.MenuDropdown ? "dropdown" : "dropdown-submenu")
                        .Append(dropdownLink)
                        .Append(dropdownMenu);
                    if (Selected)
                    {
                        result.AddClass("active active-page");
                    }
                    break;
            }
            if (type != DropdownType.SubmenuDropdown)
            {
                dropdownLink.AppendHtml(" ");
                dropdownLink.Append(new HtmlTag("i").AddClass("caret"));
            }

            return result;
        }

        private IEnumerable<HtmlTag> GetListItems()
        {
            var first = true;
            var needsSeparator = false;

            foreach (var node in this.Where(x => x.Visible))
            {
                var separator = node as SeparatorNode;
                if (separator != null)
                {
                    if ((!first) && (!needsSeparator))
                    {
                        needsSeparator = true;
                    }
                    continue;
                }

                if (first)
                {
                    first = false;
                }

                HtmlTag yieldItem = null;
                var menu = node as DropdownMenu;
                if (menu != null)
                {
                    var submenuDropdown = menu.ToSubmenuDropdown();
                    if (submenuDropdown != null)
                    {
                        yieldItem = submenuDropdown;
                    }
                }
                else
                {
                    yieldItem = node.ToHtmlTag();
                }
                if (yieldItem == null)
                {
                    continue;
                }

                if (needsSeparator)
                {
                    yield return SeparatorNode.Default.ToHtmlTag();
                    needsSeparator = false;
                }

                yield return yieldItem;
            }
        }

        private HtmlTag BuildDropdownMenu(IEnumerable<HtmlTag> listItems)
        {
            var dropdownMenu = new HtmlTag("ul")
                .AddClass(("dropdown-menu " + _ListCssClasses).TrimEnd());

            foreach (var item in listItems)
            {
                dropdownMenu.Append(item);
            }

            return dropdownMenu;
        }
    }

    public static class DropdownMenuExtensions
    {
        public static DropdownMenu SetActive(this DropdownMenu dropdownMenu, RouteValueDictionary routeValues)
        {
            foreach (var node in dropdownMenu)
            {
                SetActive(node, routeValues);
            }

            return dropdownMenu;
        }

        public static IEnumerable<IMenuNode> SetActive(this IEnumerable<IMenuNode> dropdownMenu, RouteValueDictionary routeValues)
        {
            var dropdownMenuList = dropdownMenu.ToList();
            foreach (var node in dropdownMenuList)
            {
                SetActive(node, routeValues);
            }

            return dropdownMenuList;
        }

        private static void SetActive(IMenuNode node, RouteValueDictionary routeValues)
        {
            DropdownMenu menu;
            MenuLink link;

            if ((menu = node as DropdownMenu) != null)
            {
                menu.SetActive(routeValues);
            }
            else if ((link = node as MenuLink) != null)
            {
                if (String.Equals(
                    link.Url,
                    routeValues.ToUrl(),
                    StringComparison.OrdinalIgnoreCase))
                {
                    link.Selected = true;
                }
            }
        }
    }
}