﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.DataProcessing
{
    public static class ExponentialMovingAverage
    {
        /// <summary>
        /// Returns a copy of the processed data ordered by Timestamp, then by Id.
        /// </summary>
        /// <param name="data">Data to filter.</param>
        /// <param name="nextValueWeight">Weight of the new data point.</param>
        public static IEnumerable<TagReadDocument> Filter(IEnumerable<TagReadDocument> data, Decimal nextValueWeight = 0.2m)
        {
            var sortedDataArray = data.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
            var result = sortedDataArray.Select(x => x.CopyCommonPropertiesTo<TagReadDocument>()).ToArray();

            if (nextValueWeight < 0)
            {
                nextValueWeight = Math.Abs(nextValueWeight);
            }

            // Skip the first value
            for (var i = 1; i < result.Length; i++)
            {
                var previousEntry = result[i - 1];

                var entry = result[i];
                entry.X = previousEntry.X * (1m - nextValueWeight) + nextValueWeight * entry.X;
                entry.Y = previousEntry.Y * (1m - nextValueWeight) + nextValueWeight * entry.Y;
            }

            return result;
        }
    }
}