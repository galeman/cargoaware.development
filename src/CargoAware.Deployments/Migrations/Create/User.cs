﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20140409232600)]
    public sealed class Users : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("User")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("Name").AsAnsiString(255).Nullable()
                  .WithColumn("Email").AsAnsiString(255).Nullable()
                  .WithColumn("Username").AsAnsiString(255).Unique().Nullable()
                  .WithColumn("PasswordBytes").AsBinary(20).Nullable()
                  .WithColumn("TokenBytes").AsBinary(80).Nullable()
                  .WithColumn("SaltBytes").AsBinary(24).Nullable()
                  .WithColumn("SecurityQuestion").AsAnsiString(255).Nullable()
                  .WithColumn("SecurityAnswer").AsAnsiString(255).Nullable()
                  .WithColumn("LogInKey").AsAnsiString(32).Nullable()
                  .WithColumn("LogInKeyValidUntil").AsDateTime().Nullable()
                  .WithColumn("LastLogIn").AsDateTime().Nullable()
                  .WithColumn("LastPasswordChange").AsDateTime().Nullable()
                  .WithColumn("LockedUntil").AsDateTime().Nullable()
                  .WithColumn("LockReason").AsAnsiString(100).Nullable()
                  .WithColumn("IsArchived").AsBoolean().WithDefaultValue(false);
        }
    }
}