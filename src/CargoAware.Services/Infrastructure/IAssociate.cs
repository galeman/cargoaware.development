﻿namespace CargoAware.Services.Infrastructure
{
    public interface IAssociate<in T>
    {
        void Associate(T value);
    }
}