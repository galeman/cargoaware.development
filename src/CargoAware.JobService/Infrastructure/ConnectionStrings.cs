﻿using System;
using System.Configuration;

namespace CargoAware.JobService.Infrastructure
{
    public static class ConnectionStrings
    {
        private const String EventStoreConnectionName = "EventStore";
        private const String ReportStoreConnectionName = "ReportStore";
        private const String ViewStoreConnectionName = "ViewStore";


        public static string EventStore
        {
            get
            {
                var result = ConfigurationManager.ConnectionStrings[EventStoreConnectionName].ConnectionString ?? "";
                return result;
            }
        }

        public static string ReportStore
        {
            get
            {
                var result = ConfigurationManager.ConnectionStrings[ReportStoreConnectionName].ConnectionString ?? "";
                return result;
            }
        }

        public static string ViewStore
        {
            get
            {
                var result = ConfigurationManager.ConnectionStrings[ViewStoreConnectionName].ConnectionString ?? "";
                return result;
            }
        }
    }
}