﻿using System.Web.Mvc;

namespace CargoAware.Web.Infrastructure.Security
{
    public sealed class MvcHttpsEncryptionAttribute : FilterAttribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationContext filterContext)
        {
            var httpsUsage = Helper.ValidateHttpsUsage(filterContext.HttpContext.Request.Url);

            if (httpsUsage.Redirect)
            {
                filterContext.Result = new RedirectResult(httpsUsage.HttpsUrl.ToString(), true);
            }
        }
    }
}