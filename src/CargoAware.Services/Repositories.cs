﻿using CargoAware.Services.Event;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.KeyValue;
using CargoAware.Services.LabelTemplate;
using CargoAware.Services.Location;
using CargoAware.Services.LocationZone;
using CargoAware.Services.Printer;
using CargoAware.Services.PrintJob;
using CargoAware.Services.RegionManager;
using CargoAware.Services.Shipment;
using CargoAware.Services.User;
using System;

namespace CargoAware.Services
{
    public static class Repositories
    {
        public static readonly Lazy<EventRepository> ForEvents =
            new Lazy<EventRepository>(() => new EventRepository(Client.ViewStore()));
        public static readonly Lazy<KeyValueRepository> ForKeyValues =
            new Lazy<KeyValueRepository>(() => new KeyValueRepository(Client.ViewStore()));
        public static readonly Lazy<LabelTemplateRepository> ForLabelTemplates =
            new Lazy<LabelTemplateRepository>(() => new LabelTemplateRepository(Client.ViewStore()));
        public static readonly Lazy<LocationRepository> ForLocations =
            new Lazy<LocationRepository>(() => new LocationRepository(Client.ViewStore()));
        public static readonly Lazy<LocationZoneRepository> ForLocationZones =
            new Lazy<LocationZoneRepository>(() => new LocationZoneRepository(Client.ViewStore()));
        public static readonly Lazy<PrinterRepository> ForPrinters =
            new Lazy<PrinterRepository>(() => new PrinterRepository(Client.ViewStore()));
        public static readonly Lazy<PrintJobRepository> ForPrintJobs =
            new Lazy<PrintJobRepository>(() => new PrintJobRepository(Client.ViewStore()));
        public static readonly Lazy<RegionManagerRepository> ForRegionManager =
            new Lazy<RegionManagerRepository>(() => new RegionManagerRepository(Client.ViewStore()));
        public static readonly Lazy<ShipmentRepository> ForShipments =
            new Lazy<ShipmentRepository>(() => new ShipmentRepository(Client.ViewStore()));
        public static readonly Lazy<UserRepository> ForUsers =
            new Lazy<UserRepository>(() => new UserRepository(Client.ViewStore()));
    }
}