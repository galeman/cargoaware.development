﻿using System;
using System.Xml.Serialization;

namespace CargoAware.WebApp.Models.ExternalServices
{
    [XmlRoot("ECTPBL-OUT")]
    public sealed class LmsPrintXmlModel
    {
        [XmlElement("AUTHENTICATION")]
        public PrintXmlAuthentication Authentication { get; set; }

        [XmlElement("PID")]
        public long PrinterId { get; set; }

        [XmlElement("PRINT-INFO")]
        public PrintInfoModel PrintInfo { get; set; }

        [XmlElement("DETAIL")]
        public AirWaybillInfoModel AirWaybillInfo { get; set; }

        [XmlElement("BKG-INFO")]
        public BookingInfoModel BookingInfo { get; set; }


        public sealed class PrintXmlAuthentication
        {
            [XmlElement("TOKEN")]
            public String Token { get; set; }
        }

        public sealed class PrintInfoModel
        {
            /// <summary>
            /// <para>Must be a String because LMS can specify a NUM-LABEL of "T".</para>
            /// <para>A value of "T" indicates that a label should be printed for each piece.</para>
            /// </summary>
            [XmlElement("NUM-LABEL")]
            public String NumberOfLabels { get; set; }
        }

        public sealed class AirWaybillInfoModel
        {
            [XmlElement("AWB-INFO")]
            public AirWaybillDetailsModel Details { get; set; }
        }

        public sealed class AirWaybillDetailsModel
        {
            [XmlElement("AWB")]
            public String AirWaybillNumber { get; set; }

            [XmlElement("DESCRIPTION")]
            public String Description { get; set; }

            //[XmlElement("PROD")]
            //public String ProductAbbreviation { get; set; }

            /// <summary>
            /// Split Description into two lines on the "/" character
            /// </summary>
            [XmlElement("PROD-DESC")]
            public String ProductDescription { get; set; }

            [XmlElement("SPLS")]
            public HandlingCodesModel HandlingCodes { get; set; }

            [XmlElement("SDCTOTALS")]
            public Pieces Pieces { get; set; }

            [XmlElement("ROUTING")]
            public Routing Routing { get; set; }
        }

        public sealed class HandlingCodesModel
        {
            [XmlElement("SPL")]
            public String[] Codes { get; set; }
        }

        public sealed class Pieces
        {
            [XmlElement("PCS")]
            public int Total { get; set; }

            [XmlElement("WGT")]
            public Double Weight { get; set; }

            [XmlElement("WUNI")]
            public String WeightUnitOfMeasure { get; set; }

            [XmlElement("VOL")]
            public Double? Volume { get; set; }

            [XmlElement("VUNI")]
            public String VolumeUnitOfMeasure { get; set; }
        }

        public sealed class Routing
        {
            [XmlElement("ORIGIN")]
            public RoutingDetails Origin { get; set; }

            [XmlElement("TRANSFER")]
            public RoutingDetails[] Transfers { get; set; }

            [XmlElement("DEST")]
            public RoutingDetails Destination { get; set; }
        }

        public sealed class RoutingDetails
        {
            [XmlElement("STATION")]
            public String Station { get; set; }

            [XmlElement("AIRPORT")]
            public String Airport { get; set; }

            [XmlElement("CARRIER")]
            public String Carrier { get; set; }
        }

        public sealed class BookingInfoModel
        {
            [XmlElement("SEGS")]
            public SegmentsInfo Details { get; set; }
        }

        public sealed class SegmentsInfo
        {
            [XmlElement("SEG")]
            public SegmentDetails[] Segments { get; set; }
        }

        public sealed class SegmentDetails
        {
            [XmlElement("ORIGIN")]
            public RoutingDetails Origin { get; set; }

            [XmlElement("DEST")]
            public RoutingDetails Destination { get; set; }

            [XmlElement("FLTINFO")]
            public FlightInfo FlightInfo { get; set; }
        }

        public sealed class FlightInfo
        {
            [XmlElement("FLIGHT")]
            public String FlightNumber { get; set; }

            [XmlElement("DATE")]
            public String FlightDate { get; set; }

            [XmlElement("DEP")]
            public DepartureModel Departure { get; set; }
        }

        public sealed class DepartureModel
        {
            [XmlElement("TIME")]
            public String Time { get; set; }
        }
    }
}