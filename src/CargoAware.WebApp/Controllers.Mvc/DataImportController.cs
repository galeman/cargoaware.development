﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("data/import")]
    public partial class DataImportController : AppMvcController
    {
        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.DataImport()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        // ==================================================
        #endregion
    }
}