﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Location;
using CargoAware.Services.LocationZone;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/locationzones")]
    public class LocationZonesApiController : AppApiController
    {
        private static Lazy<LocationRepository> _LocationRepository;
        private static Lazy<LocationZoneRepository> _LocationZoneRepository;


        #region // Constructor + DI
        // ==================================================

        static LocationZonesApiController()
        {
            Inject(Repositories.ForLocations);
            Inject(Repositories.ForLocationZones);
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        private static void Inject(Lazy<LocationZoneRepository> dependency)
        {
            _LocationZoneRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<LocationZoneDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.AdminLocationZones()))
            {
                throw UnauthorizedException();
            }

            var result = _LocationZoneRepository.Value.GetLocationZones(User.CurrentLocation.Id, query: request.GetApplyMethod());
            return result;
        }

        [POST("list"), HttpPost]
        public IEnumerable<LocationZoneDocument> GetList()
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw UnauthorizedException();
            }

            var result = _LocationZoneRepository.Value.GetLocationZones(User.CurrentLocation.Id);
            return result.Data;
        }

        [POST("details"), HttpPost]
        public HttpResponseMessage GetDetails(long locationId)
        {
            if (!User.Identity.IsAuthenticated)
            {
                throw UnauthorizedException();
            }

            var location = _LocationRepository.Value.GetLocationById(locationId);
            var zones = _LocationZoneRepository.Value.GetLocationZones(locationId).Data;
            var result = new
            {
                location.AxisDirection,
                location.OriginLocation,
                Zones = zones
            };
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        [POST("create"), HttpPost]
        public HttpResponseMessage Create(IEnumerable<AddEditLocationZonesDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminLocationZones()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (String.IsNullOrWhiteSpace(detail.Name) ||
                    detail.LmsZoneId == null ||
                    detail.LmsUpdateDelaySeconds < 0 ||
                    detail.EnvoyZoneId == null ||
                    detail.Points == null ||
                    !detail.Points.Any())
                {
                    continue;
                }

                LocationZoneService.CreateLocationZone(new LocationZoneDocument
                {
                    LocationId = locationId,
                    Name = detail.Name,
                    LmsZoneId = detail.LmsZoneId,
                    AutoUpdateLms = detail.AutoUpdateLms,
                    LmsUpdateDelaySeconds = detail.LmsUpdateDelaySeconds,
                    EnvoyZoneId = detail.EnvoyZoneId
                }, detail.Points);
            }

            return SuccessResponse(true);
        }

        [POST("update"), HttpPost]
        public HttpResponseMessage Update(IEnumerable<AddEditLocationZonesDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminLocationZones()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (detail.Id == 0 ||
                    String.IsNullOrWhiteSpace(detail.Name) ||
                    detail.LmsZoneId == null ||
                    detail.LmsUpdateDelaySeconds < 0 ||
                    detail.EnvoyZoneId == null ||
                    detail.Points == null ||
                    !detail.Points.Any())
                {
                    continue;
                }

                LocationZoneService.UpdateLocationZone(new LocationZoneDocument
                {
                    Id = detail.Id,
                    LocationId = locationId,
                    Name = detail.Name,
                    LmsZoneId = detail.LmsZoneId,
                    AutoUpdateLms = detail.AutoUpdateLms,
                    LmsUpdateDelaySeconds = detail.LmsUpdateDelaySeconds,
                    EnvoyZoneId = detail.EnvoyZoneId
                }, detail.Points);
            }

            return SuccessResponse(true);
        }

        [POST("archive"), HttpPost]
        public HttpResponseMessage Archive([FromBody] long id)
        {
            if (!User.HasPermission(Permissions.User.AdminLocationZones()))
            {
                throw UnauthorizedException();
            }

            LocationZoneService.ArchiveLocationZone(new LocationZoneDocument
            {
                Id = id
            });

            return SuccessResponse(true);
        }

        // ==================================================
        #endregion
    }
}