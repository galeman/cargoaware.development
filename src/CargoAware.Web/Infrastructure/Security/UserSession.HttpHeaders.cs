using System;
using System.Text;
using System.Web;

namespace CargoAware.Web.Infrastructure.Security
{
    public static partial class UserSession
    {
        #region // Public Static Methods
        // ==================================================

        public static void RestoreFromHttpHeaders()
        {
            var context = HttpContext.Current;

            var authorizationHeader = context.Request.Headers["Authorization"];
            if (authorizationHeader == null)
            {
                LogOut();
                return;
            }

            var authorizationParameters = authorizationHeader.Split(' ');
            if (authorizationParameters.Length != 2)
            {
                LogOut();
                return;
            }

            var authorizationType = authorizationParameters[0];
            var credentialsAsBase64 = authorizationParameters[1];

            ValidateCredentialsResult credentialResult;
            switch (authorizationType)
            {
                case "Basic":
                    credentialResult = ValidateBasicAuthentication(credentialsAsBase64);
                    break;

                case "MetrcPasscode":
                    credentialResult = ValidateMetrcPasscode(credentialsAsBase64);
                    break;

                default:
                    LogOut();
                    return;
            }

            var userPrincipal = UserPrincipal.FromAuthorizationDetails(credentialResult.AuthorizationDetails);
            userPrincipal.Update(0); // LogInState

            ApplyPrincipal(userPrincipal);
        }

        // ==================================================
        #endregion


        #region // Private Static Methods
        // ==================================================

        private static ValidateCredentialsResult ValidateBasicAuthentication(String credentialsAsBase64)
        {
            var failedResult = new ValidateCredentialsResult
            {
                Successful = false
            };

            var credentialsString = Encoding.UTF8.GetString(Convert.FromBase64String(credentialsAsBase64));
            var credentials = credentialsString.Split(':');
            if (credentials.Length != 2 ||
                String.IsNullOrWhiteSpace(credentials[0]) ||
                String.IsNullOrWhiteSpace(credentials[1]))
            {
                return failedResult;
            }

            var credentialResult = ValidateCredentials(ValidationType.Password, credentials[0], credentials[1]);
            return credentialResult;
        }

        private static ValidateCredentialsResult ValidateMetrcPasscode(String credentialsAsBase64)
        {
            var failedResult = new ValidateCredentialsResult
            {
                Successful = false
            };

            var credentialsString = Encoding.UTF8.GetString(Convert.FromBase64String(credentialsAsBase64));
            var credentials = credentialsString.Split(':');
            if (credentials.Length != 2 ||
                String.IsNullOrWhiteSpace(credentials[0]) ||
                String.IsNullOrWhiteSpace(credentials[1]))
            {
                return failedResult;
            }

            var credentialResult = ValidateCredentials(ValidationType.Passcode, credentials[0], credentials[1]);
            return credentialResult;
        }

        // ==================================================
        #endregion
    }
}