﻿using System;

namespace CargoAware.Web.Infrastructure
{
    public static class Constants
    {
        public const String AntiForgeryCookieName = "CargoAwareRequestToken";
        public const String TimeoutCookieName = "CargoAwareTimeout";
    }
}