﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace CargoAware.JobService
{
    [RunInstaller(true)]
    public partial class JobServiceInstaller : Installer
    {
        public JobServiceInstaller()
        {
            InitializeComponent();
        }
    }
}
