﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Event;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Location;
using CargoAware.Services.PrintJob;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api.ExternalServices
{
    [RoutePrefix("ilynx")]
    public sealed class IlynxApiController : ApiController
    {
        private const String StartContainerLoadMessageId = "Open Container";
        private const String StopContainerLoadMessageId = "Stop Load";
        private const String CloseContainerMessageId = "Close Container";
        private const String AssociateAwbRfidMessageId = "Tag Association";

        private static Lazy<EventRepository> _EventRepository;
        private static Lazy<LocationRepository> _LocationRepository;


        #region // Constructor + DI
        // ==================================================

        static IlynxApiController()
        {
            Inject(Repositories.ForEvents);
            Inject(Repositories.ForLocations);
        }

        private static void Inject(Lazy<EventRepository> dependency)
        {
            _EventRepository = dependency;
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [GET(""), HttpGet] // used for testing
        [POST(""), HttpPost]
        public async Task<HttpResponseMessage> CatchAll(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return
                TryProcessStartContainerLoad(requestBody) ||
                TryProcessStopContainerLoad(requestBody) ||
                TryProcessCloseContainer(requestBody) ||
                TryProcessAssociateAwbRfid(requestBody)
                    ? Request.CreateResponse(HttpStatusCode.OK)
                    : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("startcontainerload"), HttpGet] // used for testing
        [POST("startcontainerload"), HttpPost]
        public async Task<HttpResponseMessage> StartContainerLoad(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return TryProcessStartContainerLoad(requestBody)
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("stopcontainerload"), HttpGet] // used for testing
        [POST("stopcontainerload"), HttpPost]
        public async Task<HttpResponseMessage> StopContainerLoad(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return TryProcessStopContainerLoad(requestBody)
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("closecontainer"), HttpGet] // used for testing
        [POST("closecontainer"), HttpPost]
        public async Task<HttpResponseMessage> CloseContainer(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return TryProcessCloseContainer(requestBody)
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        [GET("associateawbrfid"), HttpGet] // used for testing
        [POST("associateawbrfid"), HttpPost]
        public async Task<HttpResponseMessage> AssociateAwbRfid(HttpRequestMessage request)
        {
            var requestBody = await request.Content.ReadAsStringAsync();
            return TryProcessAssociateAwbRfid(requestBody)
                ? Request.CreateResponse(HttpStatusCode.OK)
                : Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Unrecognized message");
        }

        // ==================================================
        #endregion


        #region // Private Methods
        // ==================================================

        private Boolean TryProcessStartContainerLoad(String requestBody)
        {
            try
            {
                var ilynxMessage = JsonConvert.DeserializeObject<IlynxMessageDto>(requestBody);
                if (!String.Equals(ilynxMessage.MessageId, StartContainerLoadMessageId, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                _EventRepository.Value.Insert(new EventDocument
                {
                    Timestamp = DateTimeOffset.Now,
                    EventSource = EventSource.iLynx,
                    EventName = "iLynx/StartContainerLoad",
                    Message = requestBody
                });

                var currentStation = !String.IsNullOrWhiteSpace(ilynxMessage.CurrentStation)
                    ? ilynxMessage.CurrentStation
                    : ilynxMessage.OriginStation;
                var locationId = _LocationRepository.Value.GetLocationIdByName(currentStation);
                if (locationId == 0)
                {
                    throw new DomainException("The Location \"{0}\" does not exist.", currentStation);
                }

                //ShipmentService.InsertTagReads(tagReads.DeviceName, tagReads.Data.Select(x => new TagReadDocument
                //{
                //    Epc = x.Tag,
                //    Rssi = (Int16)x.Rssi,
                //    Timestamp = tagReads.Timestamp,
                //    LocationZoneId = locationZoneId,
                //    FixedLocationRead = true
                //}));
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        private Boolean TryProcessStopContainerLoad(String requestBody)
        {
            try
            {
                var ilynxMessage = JsonConvert.DeserializeObject<IlynxMessageDto>(requestBody);
                if (!String.Equals(ilynxMessage.MessageId, StopContainerLoadMessageId, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                _EventRepository.Value.Insert(new EventDocument
                {
                    Timestamp = DateTimeOffset.Now,
                    EventSource = EventSource.iLynx,
                    EventName = "iLynx/StopContainerLoad",
                    Message = requestBody
                });

                var currentStation = !String.IsNullOrWhiteSpace(ilynxMessage.CurrentStation)
                    ? ilynxMessage.CurrentStation
                    : ilynxMessage.OriginStation;
                var locationId = _LocationRepository.Value.GetLocationIdByName(currentStation);
                if (locationId == 0)
                {
                    throw new DomainException("The Location \"{0}\" does not exist.", currentStation);
                }

                //ShipmentService.InsertTagReads(tagReads.DeviceName, tagReads.Data.Select(x => new TagReadDocument
                //{
                //    Epc = x.Tag,
                //    Rssi = (Int16)x.Rssi,
                //    Timestamp = tagReads.Timestamp,
                //    LocationZoneId = locationZoneId,
                //    FixedLocationRead = true
                //}));
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        private Boolean TryProcessCloseContainer(String requestBody)
        {
            try
            {
                var ilynxMessage = JsonConvert.DeserializeObject<IlynxMessageDto>(requestBody);
                if (!String.Equals(ilynxMessage.MessageId, CloseContainerMessageId, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                _EventRepository.Value.Insert(new EventDocument
                {
                    Timestamp = DateTimeOffset.Now,
                    EventSource = EventSource.iLynx,
                    EventName = "iLynx/CloseContainer",
                    Message = requestBody
                });

                var currentStation = !String.IsNullOrWhiteSpace(ilynxMessage.CurrentStation)
                    ? ilynxMessage.CurrentStation
                    : ilynxMessage.OriginStation;
                var locationId = _LocationRepository.Value.GetLocationIdByName(currentStation);
                if (locationId == 0)
                {
                    throw new DomainException("The Location \"{0}\" does not exist.", currentStation);
                }

                if (String.Equals(ilynxMessage.OffloadStation, "YUL", StringComparison.OrdinalIgnoreCase) ||
                    String.Equals(ilynxMessage.DestinationStation, "YUL", StringComparison.OrdinalIgnoreCase) ||
                    String.Equals(ilynxMessage.OffloadStation, "FRA", StringComparison.OrdinalIgnoreCase) ||
                    String.Equals(ilynxMessage.DestinationStation, "FRA", StringComparison.OrdinalIgnoreCase))
                {
                    var printJob = PrintJobService.PrintUldLabel(new PrintJobDataDocument
                    {
                        Uld = ilynxMessage.UldNumber,
                        Destination = ilynxMessage.DestinationStation,
                        Origin = ilynxMessage.OriginStation
                    }, locationId);
                }

                //ShipmentService.InsertTagReads(tagReads.DeviceName, tagReads.Data.Select(x => new TagReadDocument
                //{
                //    Epc = x.Tag,
                //    Rssi = (Int16)x.Rssi,
                //    Timestamp = tagReads.Timestamp,
                //    LocationZoneId = locationZoneId,
                //    FixedLocationRead = true
                //}));
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        private Boolean TryProcessAssociateAwbRfid(String requestBody)
        {
            try
            {
                var ilynxMessage = JsonConvert.DeserializeObject<IlynxMessageDto>(requestBody);
                if (!String.Equals(ilynxMessage.MessageId, AssociateAwbRfidMessageId, StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                var locationZoneId = _LocationRepository.Value.GetLocationIdByName(ilynxMessage.OriginStation);
                if (locationZoneId == 0)
                {
                    throw new DomainException("The Location \"{0}\" does not exist.", ilynxMessage.OriginStation);
                }

                _EventRepository.Value.Insert(new EventDocument
                {
                    Timestamp = DateTimeOffset.Now,
                    EventSource = EventSource.iLynx,
                    EventName = "iLynx/AssociateAwbRfid",
                    Message = requestBody
                });
                //ShipmentService.InsertTagReads(tagReads.DeviceName, tagReads.Data.Select(x => new TagReadDocument
                //{
                //    Epc = x.Tag,
                //    Rssi = (Int16)x.Rssi,
                //    Timestamp = tagReads.Timestamp,
                //    LocationZoneId = locationZoneId,
                //    FixedLocationRead = true
                //}));
                return true;
            }
            catch (JsonReaderException)
            {
                return false;
            }
        }

        // ==================================================
        #endregion


        #region // DTO Classes
        // ==================================================

        public class IlynxMessageDto
        {
            [JsonProperty("Message ID")]
            public String MessageId { get; set; }
            public DateTime Timestamp { get; set; }
            [JsonProperty("Current Station")]
            public String CurrentStation { get; set; }
            [JsonProperty("Origin Station")]
            public String OriginStation { get; set; }
            [JsonProperty("Offload Station")]
            public String OffloadStation { get; set; }
            [JsonProperty("Destination Station")]
            public String DestinationStation { get; set; }
            [JsonProperty("RFID Tag")]
            public String RfidTag { get; set; }
            [JsonProperty("AWB Information")]
            public String AwbNumber { get; set; }
            [JsonProperty("ULD Number")]
            public String UldNumber { get; set; }
            [JsonProperty("ULD SPL")]
            public IEnumerable<String> UldHandlingCodes { get; set; }
            [JsonProperty("ULD Remarks")]
            public String UldRemarks { get; set; }
            [JsonProperty("Computed Weight")]
            //public Double ComputedWeight { get; set; }
            public String ComputedWeight { get; set; }
            [JsonProperty("Computed Weight Unit")]
            public String ComputedWeightUoM { get; set; }
            [JsonProperty("Input Weight")]
            public Double InputWeight { get; set; }
            [JsonProperty("Input Weight Unit")]
            public String InputWeightUoM { get; set; }
            [JsonProperty("AWB")]
            public IEnumerable<AwbDto> Awb { get; set; }
            [JsonProperty("iLynx Device ID")]
            public String DeviceId { get; set; }
            [JsonProperty("User ID")]
            public String UserId { get; set; }
            public String Location { get; set; }

            public class AwbDto
            {
                [JsonProperty("AWB Number")]
                public String AWbNumber { get; set; }
                [JsonProperty("AWB Pieces")]
                public int AwbPieces { get; set; }
                [JsonProperty("AWB Weight")]
                public Double AwbWeight { get; set; }
                [JsonProperty("AWB Weight Unit")]
                public String AwbWeightUoM { get; set; }
                [JsonProperty("AWB Destination")]
                public String AwbDestination { get; set; }
                [JsonProperty("AWB SPL")]
                public IEnumerable<String> AwbHandlingCodes { get; set; }
                [JsonProperty("AWB Booked Flight Number")]
                public String FlightNumber { get; set; }
                [JsonProperty("AWB Booked Flight Date")]
                public String FlightDate { get; set; }
            }
        }

        // ==================================================
        #endregion
    }
}