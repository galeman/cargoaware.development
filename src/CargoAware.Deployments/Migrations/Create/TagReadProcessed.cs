﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150703103050)]
    public sealed class TagReadProcessed : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("TagReadProcessed")
                  .WithColumn("Id").AsInt64()
                  .WithColumn("Epc").AsAnsiString(255).NotNullable()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("RegionManagerId").AsAnsiString(50).Nullable()
                  .WithColumn("X").AsDouble().NotNullable()
                  .WithColumn("Y").AsDouble().NotNullable()
                  .WithColumn("Z").AsDouble().NotNullable()
                  .WithColumn("Rssi").AsInt16().NotNullable()
                  .WithColumn("Readers").AsAnsiString(512).NotNullable()
                  .WithColumn("Timestamp").AsCustom("datetimeoffset(0)").NotNullable().Indexed()
                  .WithColumn("LocationZoneId").AsInt64().Nullable()
                  .WithColumn("FixedLocationRead").AsBoolean().NotNullable();              

            Execute.Sql(@"
CREATE NONCLUSTERED INDEX [IX_TagReadProcessed_Epc] ON [dbo].[TagReadProcessed]
(
    [Epc] ASC
)
INCLUDE ([LocationId], [Timestamp]);");
        }

    }
}
