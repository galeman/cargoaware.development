﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.PrintJob
{
    public sealed class PrintJobRepository : RepositoryBase
    {
        public PrintJobRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public PrintJobDocument GetPrintJobById(long id)
        {
            var result = QueryPrintJobs(builder => builder.Where("pj.Id = @id", new { id })).Data;
            return result.FirstOrDefault();
        }

        public Paged<PrintJobDocument> GetPrintJobs(
            long locationId, EntitySubmittedState submittedState = EntitySubmittedState.All,
            EntityCancelledState cancelRequestState = EntityCancelledState.All, EntityCancelledState cancelledState = EntityCancelledState.All,
            Action<SqlBuilder> query = null)
        {
            var result = QueryPrintJobs(builder =>
            {
                builder.Where("pj.LocationId = @locationId", new { locationId });

                switch (submittedState)
                {
                    case EntitySubmittedState.NonSubmittedOnly:
                        builder.Where("pj.SubmittedDateTimeUtc IS NULL");
                        break;
                    case EntitySubmittedState.SubmittedOnly:
                        builder.Where("pj.SubmittedDateTimeUtc IS NOT NULL");
                        break;
                }

                switch (cancelRequestState)
                {
                    case EntityCancelledState.NonCancelledOnly:
                        builder.Where("pj.CancelRequestDateTimeUtc IS NULL");
                        break;
                    case EntityCancelledState.CancelledOnly:
                        builder.Where("pj.CancelRequestDateTimeUtc IS NOT NULL");
                        break;
                }

                switch (cancelledState)
                {
                    case EntityCancelledState.NonCancelledOnly:
                        builder.Where("pj.CancelledDateTimeUtc IS NULL");
                        break;
                    case EntityCancelledState.CancelledOnly:
                        builder.Where("pj.CancelledDateTimeUtc IS NOT NULL");
                        break;
                }

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        public PrintJobDocument GetLastPrintJob(String airWaybill)
        {
            var result = QueryPrintJobs(builder =>
            {
                builder.Where("pj.AirWaybill = @airWaybill", new { airWaybill });
                builder.OrderBy("pj.CreatedDateTimeUtc DESC");
            }).Data;
            return result.FirstOrDefault();
        }

        internal void Insert(PrintJobDocument entity, IEnumerable<PrintJobDataDocument> printJobData)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                var printJobId = db.QueryScalar<long>(@"
INSERT INTO PrintJob (
    LocationId,
    CreatedDateTimeUtc,
    LabelType,
    PrinterAddress,
    PrinterPort,
    PrinterExternalId,
    AirWaybill,
    StartPiece,
    EndPiece,
    Uld
) VALUES (
    @LocationId,
    GETUTCDATE(),
    @LabelType,
    @PrinterAddress,
    @PrinterPort,
    @PrinterExternalId,
    @AirWaybill,
    @StartPiece,
    @EndPiece,
    @Uld
);

SELECT @@IDENTITY;", entity);

                foreach (var data in printJobData)
                {
                    db.Execute(@"
INSERT INTO PrintJobData (
    PrintJobId,
    Epc,
    Barcode,
    PieceNumber,
    AirWaybill,
    Uld,
    Destination,
    Pieces,
    Origin,
    Weight,
    UnitOfMeasure,
    Service,
    HandlingCodes,
    TransfersInfo,
    TransfersDateTime
) VALUES (
    @printJobId,
    @Epc,
    @Barcode,
    @PieceNumber,
    @AirWaybill,
    @Uld,
    @Destination,
    @Pieces,
    @Origin,
    @Weight,
    @UnitOfMeasure,
    @Service,
    @HandlingCodes,
    @TransfersInfoAsString,
    @TransfersDateTimeAsString
)", new
                    {
                        printJobId,
                        data.Epc,
                        data.Barcode,
                        data.PieceNumber,
                        data.AirWaybill,
                        data.Uld,
                        data.Destination,
                        data.Pieces,
                        data.Origin,
                        data.Weight,
                        data.UnitOfMeasure,
                        data.Service,
                        data.HandlingCodes,
                        data.TransfersInfoAsString,
                        data.TransfersDateTimeAsString
                    });
                }

                scope.Complete();
            }
        }

        internal void PrintJobsSubmitted(params long[] printJobIds)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE PrintJob
SET SubmittedDateTimeUtc = GETUTCDATE()
WHERE Id IN @printJobIds;", new { printJobIds });
                scope.Complete();
            }
        }

        internal void PrintJobDataPrinted(long printJobDataId, DateTime printedDateTime)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE PrintJobData
SET PrintedDateTime = @printedDateTime
WHERE Id = @printJobDataId;", new { printJobDataId, printedDateTime });
                scope.Complete();
            }
        }

        internal void CancelPrintJob(long id)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE PrintJob
SET CancelRequestDateTimeUtc = GETUTCDATE()
WHERE Id = @id;", new { id });

                scope.Complete();
            }
        }

        internal void PrintJobsCancelled(params long[] printJobIds)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE PrintJob
SET CancelledDateTimeUtc = GETUTCDATE()
WHERE Id IN @printJobIds;", new { printJobIds });

                scope.Complete();
            }
        }

        private Paged<PrintJobDocument> QueryPrintJobs(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        pj.Id,
        pj.LocationId,
        pj.CreatedDateTimeUtc,
        pj.SubmittedDateTimeUtc,
        pj.CancelRequestDateTimeUtc,
        pj.CancelledDateTimeUtc,
        pj.LabelType,
        pj.PrinterAddress,
        pj.PrinterPort,
        pj.PrinterExternalId,
        pj.AirWaybill,
        pj.StartPiece,
        pj.EndPiece,
        pj.Uld,
        pj.PrintJobDataId,
        pj.PrintJobId,
        pj.Epc,
        pj.Barcode,
        pj.PieceNumber,
        pj.AirWaybillDapper,
        pj.UldDapper,
        pj.Destination,
        pj.Pieces,
        pj.Origin,
        pj.Weight,
        pj.UnitOfMeasure,
        pj.Service,
        pj.HandlingCodes,
        pj.TransfersInfoAsString,
        pj.TransfersDateTimeAsString,
        pj.PrintedDateTime,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwPrintJob pj
        
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("pj.CreatedDateTimeUtc DESC, pj.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<PrintJobDocument, PrintJobDataDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "PrintJobDataId,FirstRow,LastRow");

                return new Paged<PrintJobDocument>(data.Distinct(), total);
            }
        }
    }
}