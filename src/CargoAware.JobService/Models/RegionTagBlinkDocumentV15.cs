﻿using CargoAware.Services.Shipment;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.JobService.Models
{
    public class RegionTagBlinkDocumentV15 // Region Manager v1.5
    {
        [JsonProperty("regionId")]
        public String RegionId { get; set; }

        [JsonProperty("tagBlinks")]
        public IEnumerable<TagBlinkDocument> TagBlinks { get; set; }

        public class TagBlinkDocument
        {
            [JsonProperty("tagID")]
            public String Epc { get; set; }

            [JsonProperty("location")]
            public TagBlinkLocationDocument Location { get; set; }

            [JsonProperty("locateTime")]
            public long LocateTime { get; set; }

            public DateTime GetLocateTime()
            {
                var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                var result = epoch.AddMilliseconds(LocateTime);
                return result.ToLocalTime();
            }

            [JsonProperty("readerID")]
            public String Readers { get; set; }

            [JsonProperty("vendorSection")]
            public CustomSectionDocument CustomSection { get; set; }

            public class TagBlinkLocationDocument
            {
                [JsonProperty("x")]
                public Decimal X { get; set; }

                [JsonProperty("y")]
                public Decimal Y { get; set; }

                [JsonProperty("z")]
                public Decimal Z { get; set; }
            }

            public class CustomSectionDocument
            {
                [JsonProperty("any")]
                public IEnumerable<CustomSectionPropertiesDocument> PropertiesData { get; set; }

                public CustomSectionPropertiesDocument GetProperties()
                {
                    return PropertiesData.FirstOrDefault() ?? new CustomSectionPropertiesDocument();
                }

                public class CustomSectionPropertiesDocument
                {
                    [JsonProperty("rssi")]
                    public Double Rssi { get; set; }
                }
            }

            public TagReadDocument ToTagRead(RegionTagBlinkDocumentV15 regionUpdate)
            {
                var result = new TagReadDocument
                {
                    Epc = Epc,
                    RegionManagerId = regionUpdate.RegionId,
                    X = Location.X * 3.2808m,
                    Y = Location.Y * 3.2808m,
                    Z = Location.Z * 3.2808m,
                    Rssi = (short)CustomSection.GetProperties().Rssi,
                    Readers = this.Readers,
                    Timestamp = GetLocateTime()
                };
                return result;
            }
        }
    }
}