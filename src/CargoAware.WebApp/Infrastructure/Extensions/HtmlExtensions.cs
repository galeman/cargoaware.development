﻿using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Menus;
using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace CargoAware.WebApp.Infrastructure.Extensions
{
    public static class HtmlExtensions
    {
        public static HtmlString NavigationMenu(this HtmlHelper html, IPrincipal principal)
        {
            var user = principal as UserPrincipal;
            if (user == null)
            {
                return null;
            }

            var routeValues = html.ViewContext.RouteData.Values;
            var result = PageMenus.NavigationMenu(user)
                .SetActive(routeValues)
                .Select(m => m.ToHtmlTag());
            return new HtmlString(String.Concat(result));
        }

        public static HtmlString ImportExportMenu(this HtmlHelper html, IPrincipal principal)
        {
            var user = principal as UserPrincipal;
            if (user == null)
            {
                return null;
            }

            var routeValues = html.ViewContext.RouteData.Values;
            var result = PageMenus.ImportExportMenu(user)
                .SetActive(routeValues)
                .Select(x => x.ToHtmlTag());
            return new HtmlString(String.Concat(result));
        }

        public static HtmlString SupportMenu(this HtmlHelper html, IPrincipal principal)
        {
            var user = principal as UserPrincipal;
            if (user == null)
            {
                return null;
            }

            var routeValues = html.ViewContext.RouteData.Values;
            var result = PageMenus.SupportMenu()
                .SetActive(routeValues)
                .ToHtmlTag();
            return new HtmlString(result.ToString());
        }

        public static HtmlString UserMenu(this HtmlHelper html, IPrincipal principal)
        {
            var user = principal as UserPrincipal;
            if (user == null)
            {
                return null;
            }

            var routeValues = html.ViewContext.RouteData.Values;
            var result = PageMenus.UserMenu(user)
                .SetActive(routeValues)
                .ToHtmlTag();
            return new HtmlString(result.ToString());
        }

        public static HtmlString LocationsDropdown(this HtmlHelper html, IPrincipal principal)
        {
            var user = principal as UserPrincipal;
            if (user == null)
            {
                return null;
            }

            var routeValues = MVC.Sessions.ChangeLocation(user.CurrentLocation.Id).GetRouteValueDictionary();
            var result = PageMenus.LocationsDropdown(user)
                .SetActive(routeValues)
                .ToButtonDropdown();
            return result == null ? null : new HtmlString(result.ToString());
        }

        public static Boolean IsDebug(this HtmlHelper htmlHelper)
        {
#if (DEBUG)
            return true;
#else
            return false;
#endif
        }
    }
}