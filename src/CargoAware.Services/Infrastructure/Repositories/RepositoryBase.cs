using System.Transactions;

namespace CargoAware.Services.Infrastructure.Repositories
{
    public abstract class RepositoryBase
    {
        public IDbConnectionFactory DbFactory { get; set; }
        //public IAggregateStore AggregateStore { get; set; }

        public static TransactionOptions TransactionOptionsDefault = new TransactionOptions
        {
            IsolationLevel = IsolationLevel.ReadCommitted,
            Timeout = TransactionManager.MaximumTimeout
        };

        public static TransactionScope GetNewTransactionScope()
        {
            var scope = new TransactionScope(TransactionScopeOption.Required, TransactionOptionsDefault);
            return scope;
        }
    }
}