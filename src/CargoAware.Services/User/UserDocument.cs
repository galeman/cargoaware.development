﻿using System;
using System.Collections.Generic;
using CargoAware.Services.Infrastructure;

namespace CargoAware.Services.User
{
    public sealed class UserDocument : IAssociate<PermissionKey>
    {
        public UserDocument()
        {
            Permissions = new HashSet<string>();
        }

        public long Id { get; internal set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HomePage { get; set; }
        public string Username { get; set; }
        public ICollection<string> Permissions { get; set; }
        public DateTime? LockedUntil { get; internal set; }
        public DateTime? LastPasswordChange { get; internal set; }
        public bool IsArchived { get; internal set; }
        public long RecordedById { get; set; }

        #region Special Dapper fields
        //This is very special property to support multimapping with dapper
        //when the IsArchived field might be duplicated in the query.
        //This gives us a way to trick dapper.
        //I strongly suggest you not change how this works 
        //[UsedImplicitly]
        private bool UserIsArchivedDapper
        {
            set
            {
                IsArchived = value;
            }
        }
        //This is very special property to support multimapping with dapper
        //when the HomePage field might be duplicated in the query.
        //This gives us a way to trick dapper.
        //I strongly suggest you not change how this works 
        //[UsedImplicitly]
        private string UserHomePageDapper
        {
            set
            {
                HomePage = value;
            }
        }

        //This is very special property to support multimapping with dapper
        //when the Id field might be duplicated in the query.
        //This gives us a way to trick dapper.
        //I strongly suggest you not change how this works 
        //THIS HAS TO BE PUBLIC FOR SPLITON TO WORK PROPERLY
        //[UsedImplicitly]
        private long UserIdDapper
        {
            set
            {
                Id = value;
            }
        }
        #endregion

        public void Associate(PermissionKey value)
        {
            if (value != null)
            {
                Permissions.Add(value.Uri);
            }
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as UserDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}