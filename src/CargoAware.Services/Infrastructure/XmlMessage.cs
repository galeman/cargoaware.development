﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;

namespace CargoAware.Services.Infrastructure
{
    public static class XmlMessage
    {
        public static Boolean TryParse<T>(String xml, out T output) where T : class
        {
            try
            {
                var xmlSerializer = new XmlSerializer(typeof(T));
                using (var memoryStream = new MemoryStream(Encoding.Default.GetBytes(xml)))
                {
                    output = xmlSerializer.Deserialize(memoryStream) as T;
                }
                return true;
            }
            catch
            {
                output = null;
                return false;
            }
        }
    }
}