﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using CargoAware.Services.Location;
using Dapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoAware.Services.User
{
    public sealed class UserRepository : RepositoryBase
    {
        public UserRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        internal void AddPermissions(long userId, IEnumerable<string> permissions)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                foreach (var permission in permissions)
                {
                    db.Execute(
                        "INSERT INTO UserPermission (UserId, Permission) VALUES (@userId, @permission)",
                        new { userId, permission });
                }
            }
        }

        internal void Archive(long userId)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute("UPDATE [User] SET IsArchived = 1 WHERE Id = @userId", new { userId });
            }
        }

        public bool CheckSecurityAnswer(long key, string answer)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var result = db.Query<int>(
                    "SELECT COUNT(*) FROM [User] WHERE Id = @key AND SecurityAnswer = @answer", new { key, answer });
                return result.FirstOrDefault() == 1;
            }
        }

        public bool DoesUserExist(long userId)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                return db.QueryBoolean("SELECT TOP (1) 1 FROM [User] WHERE Id = @userId", new { userId });
            }
        }

        public bool DoesUserWithSameUsernameExist(
            string username, long? excludedId = null, EntityArchivalState archivalState = EntityArchivalState.ActiveOnly)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var sql = "SELECT TOP (1) 1 FROM [User] WHERE Username = @username";

                if (excludedId.HasValue)
                {
                    sql = sql + " AND Id <> @excludedId";
                }

                switch (archivalState)
                {
                    case EntityArchivalState.ActiveOnly:
                        sql = sql + " AND IsArchived = 0";
                        break;
                    case EntityArchivalState.ArchivedOnly:
                        sql = sql + " AND IsArchived = 1";
                        break;
                }

                return db.QueryBoolean(sql, new { username, excludedId });
            }
        }

        public AuthorizationDetails GetAuthorizationDetailsById(long id)
        {
            var result = QueryAuthorizationDetail(builder => builder.Where("u.Id = @id", new { id }));
            return result;
        }

        public AuthorizationDetails GetAuthorizationDetailsByLogInKey(string logInKey)
        {
            var result = QueryAuthorizationDetail(builder => builder.Where("u.LogInKey = @logInKey", new { logInKey }));
            return result;
        }

        public AuthorizationDetails GetAuthorizationDetailsByUsername(string username)
        {
            var result = QueryAuthorizationDetail(builder => builder.Where("u.Username = @username", new { username }));
            return result;
        }

        public Paged<UserDocument> GetUsers(
            UserLockedState lockedState, EntityArchivalState entityArchivalState, Action<SqlBuilder> query)
        {
            return QueryUsers(
                builder =>
                {
                    switch (entityArchivalState)
                    {
                        case EntityArchivalState.ActiveOnly:
                            builder.Where("u.IsArchived = 0");
                            break;
                        case EntityArchivalState.ArchivedOnly:
                            builder.Where("u.IsArchived = 1");
                            break;
                    }

                    switch (lockedState)
                    {
                        case UserLockedState.NotLockedOnly:
                            builder.Where("u.LockedUntil IS NULL");
                            break;
                        case UserLockedState.LockedOnly:
                            builder.Where("u.LockedUntil IS NOT NULL");
                            break;
                    }

                    if (query != null)
                    {
                        query(builder);
                    }
                });
        }

        public string GetSecurityQuestion(long key)
        {
            var user = GetAuthorizationDetailsById(key);
            return user.SecurityQuestion;
        }

        public UserDocument GetUserById(long key)
        {
            var result = QueryUsers(builder => builder.Where("u.Id = @key", new { key })).Data;
            return result.FirstOrDefault();
        }

        public long GetUserIdByUsername(String username)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM [User] u
/**where**/");
                builder.Where("u.Username = @username", new { username });
                builder.Where("u.IsArchived = 0");

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        internal void InsertUser(
            string name, string email, string username,
            byte[] tokenBytes, string salt, string logInKey, DateTime validUntilUtc)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
INSERT INTO [User] (
    Name,
    Email,
    Username,
    TokenBytes,
    SaltBytes,
    LogInKey,
    LogInKeyValidUntil
) VALUES (
    @name,
    @email,
    @username,
    @tokenBytes,
    @saltBytes,
    @logInKey,
    @validUntilUtc
)", new
                    {
                        name,
                        email,
                        username,
                        tokenBytes,
                        saltBytes = Encoding.UTF8.GetBytes(salt),
                        logInKey,
                        validUntilUtc
                    });
            }
        }

        internal void Lock(long userId, DateTime lockedUntil, string lockReason)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    "UPDATE [User] SET LockedUntil = @lockedUntil, LockReason = @lockReason WHERE Id = @userId",
                    new { userId, lockedUntil, lockReason });
            }
        }

        internal void RemovePermissions(long userId, IEnumerable<string> permissions)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                foreach (var permission in permissions)
                {
                    db.Execute(
                        "DELETE FROM UserPermission WHERE UserId = @userId AND Permission = @permission",
                        new { userId, permission });
                }
            }
        }

        internal void ReportLogInAttempt(
            string username, string host, string reason, DateTime attemptDateTime, bool succeeded)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    "INSERT INTO LogInAttempt (Username, Host, Reason, AttemptDateTime, Succeeded) VALUES (@username, @host, @reason, @attemptDateTime, @succeeded)",
                    new { username, host, reason, attemptDateTime, succeeded });

                if (succeeded)
                {
                    db.Execute(
                        "UPDATE [User] SET LastLogIn = @attemptDateTime WHERE Username = @username",
                        new { username, attemptDateTime });
                }
            }
        }

        internal void ResetPassword(long userId, string logInKey, DateTime validUntilUtc, string salt, byte[] tokenBytes)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    "UPDATE [User] SET LogInKey = @logInKey, LogInKeyValidUntil = @validUntilUtc, SaltBytes = @saltBytes, TokenBytes = @tokenBytes, LockedUntil = NULL, LockReason = NULL WHERE Id = @userId",
                    new { userId, logInKey, validUntilUtc, saltBytes = Encoding.UTF8.GetBytes(salt), tokenBytes });
            }
        }

        internal void SetEmail(long userId, string email)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute("UPDATE [User] SET Email = @email WHERE Id = @userId", new { userId, email });
            }
        }

        internal void SetPassword(
            long userId, byte[] passwordHash, DateTime lastPasswordChange, string salt, byte[] tokenBytes)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    @"
UPDATE [User]
SET LogInKey = NULL,
    LogInKeyValidUntil = NULL,
    PasswordBytes = @passwordHash,
    SaltBytes = @saltBytes,
    LastPasswordChange = @lastPasswordChange,
    TokenBytes = @tokenBytes
WHERE Id = @userId",
                    new
                    {
                        userId,
                        passwordHash,
                        saltBytes = Encoding.UTF8.GetBytes(salt),
                        lastPasswordChange,
                        tokenBytes
                    });
            }
        }

        internal void SetSecurityQuestionAndAnswer(long userId, string securityQuestion, string securityAnswer)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    "UPDATE [User] SET SecurityQuestion = @securityQuestion, SecurityAnswer = @securityAnswer WHERE Id = @userId",
                    new { userId, securityQuestion, securityAnswer });
            }
        }

        internal void SetToken(long userId, string salt, byte[] tokenBytes)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    "UPDATE [User] SET SaltBytes = @saltBytes, TokenBytes = @tokenBytes WHERE Id = @userId",
                    new { userId, saltBytes = Encoding.UTF8.GetBytes(salt), tokenBytes });
            }
        }

        internal void Unlock(long userId)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute("UPDATE [User] SET LockedUntil = NULL, LockReason = NULL WHERE Id = @userId", new { userId });
            }
        }

        internal void UpdateUser(
            long userId, string name, string email, string homePage, string username, string logInKey,
            DateTime? validUntilUtc)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(
                    @"
UPDATE [User]
SET Name = @name,
    Email = @email,
    HomePage = @homePage,
    Username = @username,
    LogInKey = @logInKey,
    LogInKeyValidUntil = @validUntilUtc
WHERE Id = @userId",
                    new { userId, name, email, homePage, username, logInKey, validUntilUtc });
            }
        }

        private AuthorizationDetails QueryAuthorizationDetail(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var select = builder.AddTemplate(@"
SELECT
    u.Id,
    u.Name,
    u.Email,
    u.Username,
    u.PasswordBytes,
    u.TokenBytes,
    u.SaltBytes,
    u.SecurityQuestion,
    u.SecurityAnswer,
    u.LogInKey,
    u.LogInKeyValidUntil,
    u.LastLogIn,
    u.LastPasswordChange,
    u.LockedUntil,
    u.LockReason,
    u.IsArchived,
    u.Uri,
    u.LocationId,
    u.LocationName AS Name,
    u.AxisDirection,
    u.OriginLocation,
    u.LocationIsArchived AS IsArchived
FROM vwAuthorizationDetail u
/**where**/");

                builder.Where("u.IsArchived = 0 AND u.LocationIsArchived = 0");

                if (query != null)
                {
                    query(builder);
                }

                var records = db.Query(
                    @select.RawSql,
                    DapperMapper.CreateMap<AuthorizationDetails, PermissionKey, LocationDocument>(),
                    @select.Parameters,
                    splitOn: "Uri,LocationId");

                return records.FirstOrDefault();
            }
        }

        private Paged<UserDocument> QueryUsers(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        u.Id,
        u.Name,
        u.Email,
        u.Username,
        u.LastLogIn,
        u.LastPasswordChange,
        u.LockedUntil,
        u.LockReason,
        u.IsArchived,
        u.Uri,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwUser u
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("u.Id ASC");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<UserDocument, PermissionKey>(i => total = i),
                    selector.Parameters,
                    splitOn: "Uri,FirstRow,LastRow");

                return new Paged<UserDocument>(data.Distinct(), total);
            }
        }
    }
}