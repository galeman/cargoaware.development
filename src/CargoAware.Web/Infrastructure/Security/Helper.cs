﻿using System;

namespace CargoAware.Web.Infrastructure.Security
{
    public static class Helper
    {
        public static HttpsUsageResult ValidateHttpsUsage(Uri requestUrl)
        {
            var uri = new UriBuilder(requestUrl);
            var host = uri.Host;
            var validHost = host.EndsWith("cargoaware.com", StringComparison.OrdinalIgnoreCase);

            if (requestUrl.IsLoopback || !validHost)
            {
                return new HttpsUsageResult
                {
                    Redirect = false,
                    HttpsUrl = uri.Uri
                };
            }

            var redirect = false;

            // If not using https, redirect to it.
            if (requestUrl.Scheme != Uri.UriSchemeHttps)
            {
                uri.Scheme = Uri.UriSchemeHttps;
                uri.Port = 443;
                redirect = true;
            }

            return new HttpsUsageResult
            {
                Redirect = redirect,
                HttpsUrl = uri.Uri
            };
        }

        public struct HttpsUsageResult
        {
            public Boolean Redirect { get; set; }
            public Uri HttpsUrl { get; set; }
        }
    }
}