﻿using System;
using System.Reflection;

namespace CargoAware.Services.Infrastructure
{
    public static class ObjectExtensions
    {
        internal static T CopyCommonPropertiesTo<T>(this Object source, params String[] propertiesToSkip) where T : new()
        {
            const BindingFlags Flags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic;
            // ReSharper disable InconsistentNaming
            Func<String, Boolean> SkipProperty = name => Array.Exists(propertiesToSkip, propName => String.Equals(propName, name));
            // ReSharper restore InconsistentNaming


            //if (source is IDictionary<String, Object>)
            //{
            //    CopyFromDictionary<String, Object>(
            //        (source as IDictionary<String, Object>),
            //        target,
            //        Flags,
            //        SkipProperty);
            //    return;
            //}


            var target = new T();
            var targetType = target.GetType();
            var targetProperties = targetType.GetProperties(Flags);

            var sourceType = source.GetType();

            foreach (var writeProperty in targetProperties)
            {
                if ((!writeProperty.CanWrite) || (SkipProperty(writeProperty.Name)))
                {
                    continue;
                }

                var readProperty = sourceType.GetProperty(writeProperty.Name, Flags);
                if ((readProperty != null) && (readProperty.CanRead))
                {
                    writeProperty.SetValue(target, readProperty.GetValue(source, null), null);
                }
            }

            return target;
        }
    }
}