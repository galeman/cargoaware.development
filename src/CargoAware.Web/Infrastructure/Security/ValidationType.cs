﻿namespace CargoAware.Web.Infrastructure.Security
{
    public enum ValidationType
    {
        Key,
        Passcode,
        Password,
        Username,
    }
}