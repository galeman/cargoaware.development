﻿// map-display.js
// (c) 2012-2015 Franwell, Inc.

// requires - jQuery.js, franwell.format.js

(function () {
    'use strict';


    var root = window,
        franwell = root.franwell = root.franwell || {},
        getMapConfiguration = function (axis, origin) {
            return {
                normalAxis: axis === 'Normal',
                invertX: origin.indexOf('Right') !== -1,
                invertY: origin.indexOf('Bottom') !== -1
            };
        },
        traversePoints = function (zones, zoneWorker, pointWorker) {
            var z, zcnt, p, pcnt, zone;

            for (z = 0, zcnt = zones.length; z < zcnt; z++) {
                zone = zones[z];

                zoneWorker(zone, z, 0);
                for (p = 0, pcnt = zone.Points.length; p < pcnt; p++) {
                    pointWorker(zone.Points[p], p);
                }
                zoneWorker(zone, z, 1);
            }
        },
        calculateMapSize = function (zones) {
            var maxX = 0, maxY = 0;

            traversePoints(
                zones,
                function () { },
                function (point) {
                    if (point.X > maxX) maxX = point.X;
                    if (point.Y > maxY) maxY = point.Y;
                });

            return {
                width: maxX,
                height: maxY
            };
        },
        getScale = function (mapConfig, mapSize, canvasWidth) {
            if (!mapConfig.normalAxis) {
                var temp = mapSize.width;
                mapSize.width = mapSize.height;
                mapSize.height = temp;
            }
            return canvasWidth / mapSize.width;
        },
        getCanvasPoint = function (canvasWidth, canvasHeight, mapConfig, scale, point) {
            var xCoord, yCoord;

            if (mapConfig.normalAxis) {
                xCoord = point.X * scale;
                yCoord = point.Y * scale;
            } else {
                xCoord = point.Y * scale;
                yCoord = point.X * scale;
            }
            if (mapConfig.invertX) { xCoord = canvasWidth - xCoord; }
            if (mapConfig.invertY) { yCoord = canvasHeight - yCoord; }

            return {
                x: xCoord,
                y: yCoord
            };
        };


    franwell.mapDisplay = franwell.mapDisplay || {};


    franwell.mapDisplay.setCanvasDimensions = franwell.mapDisplay.setCanvasDimensions || function (canvasElement, locationData) {
        var canvasWidth = canvasElement.width,
            canvasHeight,
            mapConfig = getMapConfiguration(locationData.AxisDirection, locationData.OriginLocation),
            mapSize = calculateMapSize(locationData.Zones),
            scale = getScale(mapConfig, mapSize, canvasWidth);

        canvasHeight = Math.round(mapSize.height * scale);
        if (canvasElement.height !== canvasHeight) {
            canvasElement.height = canvasHeight;
        }
    };


    franwell.mapDisplay.getPanZoomCanvasContext = franwell.mapDisplay.getPanZoomCanvasContext || function (canvasElement, redraw) {
        var $canvasElement = $(canvasElement),
            canvasContext = canvasElement.getContext('2d'),
            lastX = canvasElement.width / 2,
            lastY = canvasElement.height / 2,
            scaleFactor = 1.1,
            dragStart,
            trackTransforms = function (ctx) {
                var svgElement = document.createElementNS('http://www.w3.org/2000/svg', 'svg'),
                    svgMatrix = svgElement.createSVGMatrix(),
                    savedTransforms = [],
                    baseSave = ctx.save,
                    baseRestore = ctx.restore,
                    baseScale = ctx.scale,
                    baseRotate = ctx.rotate,
                    baseTranslate = ctx.translate,
                    baseTransform = ctx.transform,
                    baseSetTransform = ctx.setTransform,
                    svgPoint = svgElement.createSVGPoint();
                ctx.ca_CurrentTransform = {
                    actualX: 0, actualY: 0, scaleX: 1, scaleY: 1,
                    updateActual: function (x, y) {
                        this.actualX += x;
                        this.actualY += y;
                    },
                    updateScale: function (factor) {
                        this.scaleX *= factor;
                        this.scaleY *= factor;
                    }
                };
                ctx.getTransform = function () { return svgMatrix; };
                ctx.save = function () {
                    savedTransforms.push(svgMatrix.translate(0, 0));
                    return baseSave.call(ctx);
                };
                ctx.restore = function () {
                    svgMatrix = savedTransforms.pop();
                    return baseRestore.call(ctx);
                };
                ctx.scale = function (sx, sy) {
                    svgMatrix = svgMatrix.scaleNonUniform(sx, sy);
                    return baseScale.call(ctx, sx, sy);
                };
                ctx.rotate = function (radians) {
                    svgMatrix = svgMatrix.rotate(radians * 180 / Math.PI);
                    return baseRotate.call(ctx, radians);
                };
                ctx.translate = function (dx, dy) {
                    svgMatrix = svgMatrix.translate(dx, dy);
                    return baseTranslate.call(ctx, dx, dy);
                };
                ctx.transform = function (a, b, c, d, e, f) {
                    var m2 = svgElement.createSVGMatrix();
                    m2.a = a; m2.b = b; m2.c = c; m2.d = d; m2.e = e; m2.f = f;
                    svgMatrix = svgMatrix.multiply(m2);
                    return baseTransform.call(ctx, a, b, c, d, e, f);
                };
                ctx.setTransform = function (a, b, c, d, e, f) {
                    svgMatrix.a = a; svgMatrix.b = b; svgMatrix.c = c; svgMatrix.d = d; svgMatrix.e = e; svgMatrix.f = f;
                    return baseSetTransform.call(ctx, a, b, c, d, e, f);
                };
                ctx.transformedPoint = function (x, y) {
                    svgPoint.x = x;
                    svgPoint.y = y;
                    return svgPoint.matrixTransform(svgMatrix.inverse());
                }
            },
            zoom = function (clicks) {
                var pt = canvasContext.transformedPoint(lastX, lastY),
                    factor = Math.pow(scaleFactor, clicks);
                canvasContext.ca_CurrentTransform.updateScale(factor);
                canvasContext.translate(pt.x, pt.y);
                canvasContext.scale(factor, factor);
                canvasContext.translate(-pt.x, -pt.y);
                redraw();
            },
            handleScroll = function (e) {
                var delta = e.wheelDelta ? e.wheelDelta / 40 : e.detail ? -e.detail : 0;
                if (delta) zoom(delta);
                return e.preventDefault() && false;
            };

        if (!canvasContext.ca_CurrentTransform) {
            trackTransforms(canvasContext);
            $canvasElement
                .mousedown(function (e) {
                    //document.body.style.mozUserSelect = document.body.style.webkitUserSelect = document.body.style.userSelect = 'none';
                    lastX = e.pageX - this.offsetLeft;
                    lastY = e.pageY - this.offsetTop;
                    dragStart = canvasContext.transformedPoint(lastX, lastY);
                })
                .mousemove(function (e) {
                    lastX = e.pageX - this.offsetLeft;
                    lastY = e.pageY - this.offsetTop;
                    if (dragStart) {
                        var pt = canvasContext.transformedPoint(lastX, lastY),
                            deltaX = pt.x - dragStart.x,
                            deltaY = pt.y - dragStart.y;
                        canvasContext.ca_CurrentTransform.updateActual(deltaX, deltaY);
                        canvasContext.translate(deltaX, deltaY);
                        redraw();
                    }
                })
                .mouseup(function () { dragStart = null; });
            //.on('DOMMouseScroll', handleScroll)
            //.on('mousewheel', handleScroll);
            canvasElement.addEventListener('DOMMouseScroll', handleScroll, false);
            canvasElement.addEventListener('mousewheel', handleScroll, false);
        }

        return canvasContext;
    };


    franwell.mapDisplay.resetPanZoom = franwell.mapDisplay.resetPanZoom || function (canvasContext) {
        canvasContext.scale(1, 1);
        canvasContext.setTransform(1, 0, 0, 1, 0, 0);
    };


    franwell.mapDisplay.prepareMapZones = franwell.mapDisplay.prepareMapZones || function (canvasElement, locationData) {
        var canvasWidth = canvasElement.width,
            canvasHeight = canvasElement.height,
            canvasContext = locationData.panZoomCanvasContext,
            zones = locationData.Zones,
            topLeft = canvasContext.transformedPoint(0, 0),
            bottomRight = canvasContext.transformedPoint(canvasWidth, canvasHeight),
            mapConfig = getMapConfiguration(locationData.AxisDirection, locationData.OriginLocation),
            mapSize = calculateMapSize(zones),
            scale = getScale(mapConfig, mapSize, canvasWidth),
            canvasPoint;

        canvasContext.fillStyle = '#FFF';
        canvasContext.fillRect(topLeft.x, topLeft.y, bottomRight.x - topLeft.x, bottomRight.y - topLeft.y);

        canvasContext.lineJoin = 'round';
        canvasContext.lineWidth = 1;
        canvasContext.strokeStyle = '#DDD';
        traversePoints(
            zones,
            function (zone, zoneIndex, step) {
                if (step === 0) {
                    canvasContext.beginPath();
                } else {
                    canvasContext.closePath();
                    canvasContext.stroke();
                }
            },
            function (point, pointIndex) {
                canvasPoint = getCanvasPoint(canvasWidth, canvasHeight, mapConfig, scale, point);
                if (pointIndex === 0) {
                    canvasContext.moveTo(canvasPoint.x, canvasPoint.y);
                } else {
                    canvasContext.lineTo(canvasPoint.x, canvasPoint.y);
                }
            });

        canvasContext.lineJoin = 'miter';
        canvasContext.strokeStyle = '#888';
        canvasContext.strokeRect(0, 0, canvasWidth, canvasHeight);

        canvasContext.save();
        canvasContext.scale(1, 1);
        canvasContext.setTransform(1, 0, 0, 1, 0, 0);
        canvasContext.strokeRect(0, 0, canvasWidth, canvasHeight);
        canvasContext.restore();
    };


    franwell.mapDisplay.plotTagReads = franwell.mapDisplay.plotTagReads || function (canvasElement, locationData, shipments) {
        if (!(shipments instanceof Array) || shipments.length === 0) return;

        var canvasWidth = canvasElement.width,
            canvasHeight = canvasElement.height,
            canvasContext = locationData.panZoomCanvasContext,
            mapConfig = getMapConfiguration(locationData.AxisDirection, locationData.OriginLocation),
            mapSize = calculateMapSize(locationData.Zones),
            scale = getScale(mapConfig, mapSize, canvasWidth),
            t, tcnt, tagReads, r, rcnt, read, canvasPoint,

            moveToCanvasPoint = function (pointColor, pointSize) {
                if (pointColor) canvasContext.fillStyle = pointColor;
                canvasContext.beginPath();
                canvasContext.moveTo(canvasPoint.x, canvasPoint.y);
                canvasContext.arc(canvasPoint.x, canvasPoint.y, pointSize || 2, 0, Math.PI * 2, true);
                canvasContext.closePath();
                canvasContext.fill();
            },
            lineToCanvasPoint = function (lineColor, pointColor) {
                if (lineColor) canvasContext.strokeStyle = lineColor;
                if (pointColor) canvasContext.fillStyle = pointColor;
                canvasContext.lineTo(canvasPoint.x, canvasPoint.y);
                canvasContext.stroke();
                canvasContext.beginPath();
                canvasContext.arc(canvasPoint.x, canvasPoint.y, 2, 0, Math.PI * 2, true);
                canvasContext.closePath();
                canvasContext.fill();
            };

        canvasContext.lineJoin = 'round';
        canvasContext.lineWidth = 2;

        for (t = 0, tcnt = shipments.length; t < tcnt; t++) {
            tagReads = shipments[t].tagReads;
            if (!(tagReads instanceof Array) || tagReads.length === 0) continue;

            read = tagReads[0];
            canvasPoint = getCanvasPoint(canvasWidth, canvasHeight, mapConfig, scale, read);
            moveToCanvasPoint('#555');

            for (r = 1, rcnt = tagReads.length; r < rcnt; r++) {
                read = tagReads[r];
                canvasPoint = getCanvasPoint(canvasWidth, canvasHeight, mapConfig, scale, read);
                lineToCanvasPoint('#777', '#333');
            }
        }

        for (t = 0, tcnt = shipments.length; t < tcnt; t++) {
            tagReads = shipments[t].tagReads;
            if (!(tagReads instanceof Array) || tagReads.length === 0) continue;

            read = tagReads[tagReads.length - 1];
            canvasPoint = getCanvasPoint(canvasWidth, canvasHeight, mapConfig, scale, read);
            moveToCanvasPoint('#00D', 4);
        }
    };


    franwell.mapDisplay.prepareMouseInteractions = franwell.mapDisplay.prepareMouseInteractions || function (canvasElement, xCoordSelector, yCoordSelector, locationData, redraw) {
        var $canvasElement = $(canvasElement),
            $xCoord = $(xCoordSelector),
            $yCoord = $(yCoordSelector),
            canvasWidth = canvasElement.width,
            canvasHeight = canvasElement.height,
            canvasContext = locationData.panZoomCanvasContext,
            mapConfig = getMapConfiguration(locationData.AxisDirection, locationData.OriginLocation),
            mapSize = calculateMapSize(locationData.Zones),
            scale = getScale(mapConfig, mapSize, canvasWidth);

        $canvasElement
            .dblclick(function () {
                franwell.mapDisplay.resetPanZoom(canvasContext);
                redraw();
            })
            .mousemove(function (e) {
                var point = { X: e.pageX - this.offsetLeft, Y: e.pageY - this.offsetTop },
                    canvasPoint = getCanvasPoint(canvasWidth, canvasHeight, mapConfig, 1, point),
                    panZoomPoint = {
                        x: ((canvasPoint.x / canvasContext.ca_CurrentTransform.scaleX) + canvasContext.ca_CurrentTransform.actualX) / scale,
                        y: ((canvasPoint.y / canvasContext.ca_CurrentTransform.scaleY) + canvasContext.ca_CurrentTransform.actualY) / scale
                    };
                $xCoord.text(franwell.formatNumber(panZoomPoint.x, 2));
                $yCoord.text(franwell.formatNumber(panZoomPoint.y, 2));
                //$xCoord.text(point.X);
                //$yCoord.text(point.Y);
            })
            .mouseleave(function () {
                $xCoord.text('-');
                $yCoord.text('-');
            });
    };


    franwell.mapDisplay.prepareTimeSlider = franwell.mapDisplay.prepareTimeSlider || function (templateSelector, classToRemove, sliderInputClass, timestamps, changeEventHandler) {
        var $template = $(templateSelector),
            $existingSlider = $template.siblings('.k-slider').find('input.' + sliderInputClass),
            $inputElement = $template.clone().css('display', '').removeClass(classToRemove).insertAfter($template);

        if ($existingSlider.length !== 0) {
            $existingSlider.data('kendoSlider').destroy();
            $existingSlider.closest('.k-slider').remove();
        }

        franwell.kendo.slider($inputElement, { min: 0, max: timestamps.length - 1, tickPlacement: 'none' }, changeEventHandler);
    };
})();