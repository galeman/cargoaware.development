﻿using System;

namespace CargoAware.Services.RegionManager
{
    public sealed class RegionManagerRegionDocument
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public String Name { get; set; }
        public Guid RegionId { get; set; }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as RegionManagerRegionDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}