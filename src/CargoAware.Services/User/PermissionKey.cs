using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;

namespace CargoAware.Services.User
{
    /// <remarks>
    /// Permission keys are case insensitive.
    /// </remarks>
    [TypeConverter(typeof(PermissionKeyTypeConverter))]
    public sealed class PermissionKey
    {
        public PermissionKey()
        {
        }

        public PermissionKey(string uri)
        {
            Uri = uri;
        }

        public string Uri { get; private set; }

        private string[] Parts
        {
            get { return GetCachedParts(); }
        }

        public bool Permits(PermissionKey request)
        {
            if (Parts.Length > request.Parts.Length)
            {
                return false;
            }

            var result = !Parts
                .Where((part, i) =>
                    !String.Equals(part, MatchesAll) &&
                    !String.Equals(request.Parts[i], MatchesAll) &&
                    !String.Equals(part, request.Parts[i]))
                .Any();
            return result;
        }

        private string[] GetCachedParts()
        {
            if (!parsed)
            {
                ParseUri();
            }
            return cachedParts;
        }

        private void ParseUri()
        {
            cachedParts = new Regex(@"([^:/]+)")
                .Matches(Uri)
                .Cast<Match>()
                .Select(m => m.Groups[1].Value.ToLowerInvariant())
                .ToArray();
            parsed = true;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }
            if (ReferenceEquals(this, obj))
            {
                return true;
            }
            var key = obj as PermissionKey;
            return key != null && key.Uri.Equals(Uri);
        }

        public override int GetHashCode()
        {
            return Uri.GetHashCode();
        }

        public override string ToString()
        {
            return Uri;
        }

        public static implicit operator PermissionKey(String uri)
        {
            return new PermissionKey(uri);
        }

        private const string MatchesAll = "*";
        private string[] cachedParts;
        private bool parsed;
    }

    public sealed class PermissionKeyTypeConverter : TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            return (sourceType == typeof(string)) || base.CanConvertFrom(context, sourceType);
        }

        public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
        {
            var url = value as string;
            return url == null ? base.ConvertFrom(context, culture, value) : new PermissionKey(url);
        }
    }

    public static class PermissionKeyExtensions
    {
        public static bool AnyPermits(this IEnumerable<PermissionKey> permissions, PermissionKey request)
        {
            return permissions.Any(k => k.Permits(request));
        }
    }
}