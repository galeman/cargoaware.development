﻿namespace CargoAware.JobService
{
    partial class JobServiceInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tasksProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.tasksInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // tasksProcessInstaller
            // 
            this.tasksProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.tasksProcessInstaller.Password = null;
            this.tasksProcessInstaller.Username = null;
            // 
            // tasksInstaller
            // 
            this.tasksInstaller.Description = "CargoAware Job Service";
            this.tasksInstaller.DisplayName = "CargoAware Job Service";
            this.tasksInstaller.ServiceName = "CargoAwareJobService";
            this.tasksInstaller.StartType = System.ServiceProcess.ServiceStartMode.Automatic;
            // 
            // JobServiceInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.tasksProcessInstaller,
            this.tasksInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller tasksProcessInstaller;
        private System.ServiceProcess.ServiceInstaller tasksInstaller;
    }
}