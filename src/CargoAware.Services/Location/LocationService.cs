﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using System;

namespace CargoAware.Services.Location
{
    public static class LocationService
    {
        private static Lazy<LocationRepository> _LocationRepository;


        static LocationService()
        {
            Inject(Repositories.ForLocations);
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }


        /// <param name="message">Required: Name, AxisDirection, OriginLocation, ApiKey</param>
        public static LocationDocument CreateLocation(LocationDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            message.Id = 0;

            // Step 3: Does this command pass business rules?

            var sameUsernameUserExists = _LocationRepository.Value.DoesLocationWithSameNameExist(message.Name);
            if (sameUsernameUserExists)
            {
                throw new DomainException("The Location \"{0}\" already exists.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            long locationId;
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LocationRepository.Value.InsertOrUpdate(message);
                locationId = _LocationRepository.Value.GetLocationIdByName(message.Name);
                scope.Complete();
            }

            var result = _LocationRepository.Value.GetLocationById(locationId);
            return result;
        }

        /// <param name="message">Required: Id, Name, AxisDirection, OriginLocation, ApiKey</param>
        public static void UpdateLocation(LocationDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var existingLocation = _LocationRepository.Value.GetLocationById(message.Id);
            if (existingLocation == null)
            {
                throw new DomainException("The specified Location, \"{0}\", does not exist.", message.Name);
            }

            var sameUsernameUserExists = _LocationRepository.Value.DoesLocationWithSameNameExist(message.Name, message.Id);
            if (sameUsernameUserExists)
            {
                throw new DomainException("A Location named \"{0}\" already exists.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LocationRepository.Value.InsertOrUpdate(message);
                scope.Complete();
            }
        }

        /// <param name="message">Required: Id</param>
        public static void ArchiveLocation(LocationDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var doesLocationExist = _LocationRepository.Value.GetLocationById(message.Id);
            if (doesLocationExist == null)
            {
                throw new DomainException("The specified Location does not exist.", message.Name);
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _LocationRepository.Value.Archive(message.Id);
                scope.Complete();
            }
        }
    }
}