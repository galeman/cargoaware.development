﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630151519)]
    public sealed class vwAuthorizationDetail : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwAuthorizationDetail') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwAuthorizationDetail AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwAuthorizationDetail 
                AS
                   SELECT
                        u.Id,
                        u.Name,
                        u.Email,
                        u.Username,
                        u.PasswordBytes,
                        u.TokenBytes,
                        u.SaltBytes,
                        u.SecurityQuestion,
                        u.SecurityAnswer,
                        u.LogInKey,
                        u.LogInKeyValidUntil,
                        u.LastLogIn,
                        u.LastPasswordChange,
                        u.LockedUntil,
                        u.LockReason,
                        u.IsArchived,
                        up.Permission AS Uri,
                        l.Id AS LocationId,
                        l.Name AS LocationName,
                        l.AxisDirection,
                        l.OriginLocation,
                        l.IsArchived AS LocationIsArchived
                    FROM dbo.[User] u
                        LEFT JOIN dbo.UserPermission up ON up.UserId = u.Id
                        CROSS JOIN dbo.Location l;");
        }
    }
}
