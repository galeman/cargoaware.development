﻿using System;
using HtmlTags;

namespace CargoAware.WebApp.Infrastructure.Menus
{
    public sealed class SeparatorNode : IMenuNode
    {
        private static readonly SeparatorNode _DefaultSeparatorNode = new SeparatorNode();


        private SeparatorNode()
        {
        }

        public String PrependHtml
        {
            get { throw new NotImplementedException(); }
        }

        public String AppendHtml
        {
            get { throw new NotImplementedException(); }
        }

        public Boolean Selected
        {
            get { return false; }
        }

        public Boolean Visible
        {
            get { return true; }
        }

        public HtmlTag ToHtmlTag()
        {
            var separator = new HtmlTag("div").Attr("style", "border-bottom: 2px solid #DDD; margin: 4px 15px;");
            var listItem = new HtmlTag("li").Append(separator);
            return listItem;
        }


        public static SeparatorNode Default { get { return _DefaultSeparatorNode; } }
    }
}