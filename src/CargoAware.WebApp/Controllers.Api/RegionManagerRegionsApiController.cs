﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.RegionManager;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api
{
    [ApiAuthorize]
    [RoutePrefix("api/regionmanager/regions")]
    public class RegionManagerRegionsApiController : AppApiController
    {
        private static Lazy<RegionManagerRepository> _RegionManagerRepository;


        #region // Constructor + DI
        // ==================================================

        static RegionManagerRegionsApiController()
        {
            Inject(Repositories.ForRegionManager);
        }

        private static void Inject(Lazy<RegionManagerRepository> dependency)
        {
            _RegionManagerRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [POST(""), HttpPost]
        public Paged<RegionManagerRegionDocument> GetPagedList(KendoGridRequest request)
        {
            if (!User.HasPermission(Permissions.User.AdminRegionManagerRegions()))
            {
                throw UnauthorizedException();
            }

            var result = _RegionManagerRepository.Value.GetRegions(User.CurrentLocation.Id, request.GetApplyMethod());
            return result;
        }

        [POST("create"), HttpPost]
        public HttpResponseMessage Create(IEnumerable<AddEditRegionManagerRegionsDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminRegionManagerRegions()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (String.IsNullOrWhiteSpace(detail.Name) ||
                    detail.RegionId == Guid.Empty)
                {
                    continue;
                }

                RegionManagerService.CreateRegion(new RegionManagerRegionDocument
                {
                    LocationId = locationId,
                    Name = detail.Name,
                    RegionId = detail.RegionId
                });
            }

            UserSession.RemoveFromCache();

            return SuccessResponse(true);
        }

        [POST("update"), HttpPost]
        public HttpResponseMessage Update(IEnumerable<AddEditRegionManagerRegionsDetailsModel> model)
        {
            if (!User.HasPermission(Permissions.User.AdminRegionManagerRegions()))
            {
                throw UnauthorizedException();
            }

            //var userId = User.Id;
            var locationId = User.CurrentLocation.Id;
            foreach (var detail in model)
            {
                if (detail.Id == 0 ||
                    String.IsNullOrWhiteSpace(detail.Name) ||
                    detail.RegionId == Guid.Empty)
                {
                    continue;
                }

                RegionManagerService.UpdateRegion(new RegionManagerRegionDocument
                {
                    Id = detail.Id,
                    LocationId = locationId,
                    Name = detail.Name,
                    RegionId = detail.RegionId
                });
            }

            UserSession.RemoveFromCache();

            return SuccessResponse(true);
        }

        [POST("archive"), HttpPost]
        public HttpResponseMessage Archive([FromBody] long id)
        {
            if (!User.HasPermission(Permissions.User.AdminRegionManagerRegions()))
            {
                throw UnauthorizedException();
            }

            RegionManagerService.ArchiveRegion(new RegionManagerRegionDocument
            {
                Id = id
            });

            UserSession.RemoveFromCache();

            return SuccessResponse(true);
        }

        // ==================================================
        #endregion
    }
}