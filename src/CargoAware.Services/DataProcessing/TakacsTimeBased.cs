﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.DataProcessing
{
    public static class TakacsTimeBased
    {
        /// <summary>
        /// Returns a copy of the processed data ordered by Timestamp, then by Id.
        /// </summary>
        /// <param name="data">Data to filter.</param>
        /// <param name="timeLength">Number of seconds to consider for the Takacs Smoothing calculation.</param>
        /// <param name="standardDeviasionsFactor">Factor by which to multiply the Standard Deviasion when eliminating values outside of the limit.</param>
        public static IEnumerable<TagReadDocument> Filter(IEnumerable<TagReadDocument> data, Double timeLength = -20, Decimal standardDeviasionsFactor = 1, Boolean applyAverage = false)
        {
            var sortedDataArray = data.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
            var result = new List<TagReadDocument>(sortedDataArray.Length);

            if (timeLength > 0)
            {
                timeLength = -timeLength;
            }

            foreach (var entry in sortedDataArray)
            {
                var startingTime = entry.Timestamp.AddSeconds(timeLength);
                var tagReadsInTimeSlice = sortedDataArray
                    .SkipWhile(x => x.Timestamp <= startingTime)
                    .TakeWhile(x => x.Timestamp < entry.Timestamp)
                    .Select(x => new Point3D { X = x.X, Y = x.Y, Z = x.Z })
                    .ToArray();
                if (tagReadsInTimeSlice.Length <= 1)
                {
                    continue;
                }

                var averageOfTimeSlice = tagReadsInTimeSlice.Aggregate((r1, r2) =>
                    new Point3D { X = r1.X + r2.X, Y = r1.Y + r2.Y });
                averageOfTimeSlice.X /= tagReadsInTimeSlice.Length;
                averageOfTimeSlice.Y /= tagReadsInTimeSlice.Length;

                var stdDevOfTimeSlice = new Point3D
                {
                    X = (Decimal)Math.Sqrt(tagReadsInTimeSlice.Average(x => Math.Pow((Double)(x.X - averageOfTimeSlice.X), 2))),
                    Y = (Decimal)Math.Sqrt(tagReadsInTimeSlice.Average(y => Math.Pow((Double)(y.Y - averageOfTimeSlice.Y), 2)))
                };

                var lowerLimit = new Point3D
                {
                    X = averageOfTimeSlice.X - (stdDevOfTimeSlice.X * standardDeviasionsFactor),
                    Y = averageOfTimeSlice.Y - (stdDevOfTimeSlice.Y * standardDeviasionsFactor)
                };
                var upperLimit = new Point3D
                {
                    X = averageOfTimeSlice.X + (stdDevOfTimeSlice.X * standardDeviasionsFactor),
                    Y = averageOfTimeSlice.Y + (stdDevOfTimeSlice.Y * standardDeviasionsFactor)
                };

                if (entry.X >= lowerLimit.X && entry.X <= upperLimit.X &&
                    entry.Y >= lowerLimit.Y && entry.Y <= upperLimit.Y)
                {
                    var newEntry = entry.CopyCommonPropertiesTo<TagReadDocument>();
                    result.Add(newEntry);

                    if (applyAverage)
                    {
                        startingTime = newEntry.Timestamp.AddSeconds(timeLength);
                        tagReadsInTimeSlice = result
                            .SkipWhile(x => x.Timestamp <= startingTime)
                            .TakeWhile(x => x.Timestamp <= newEntry.Timestamp)
                            .Select(x => new Point3D { X = x.X, Y = x.Y, Z = x.Z })
                            .ToArray();
                        if (tagReadsInTimeSlice.Length != 1)
                        {
                            averageOfTimeSlice = tagReadsInTimeSlice.Aggregate((r1, r2) =>
                                new Point3D { X = r1.X + r2.X, Y = r1.Y + r2.Y });
                            newEntry.X = averageOfTimeSlice.X / tagReadsInTimeSlice.Length;
                            newEntry.Y = averageOfTimeSlice.Y / tagReadsInTimeSlice.Length;
                        }
                    }
                }
            }

            return result;
        }
    }
}