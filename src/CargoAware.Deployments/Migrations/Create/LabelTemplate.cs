﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150228213567)]
    public sealed class LabelTemplate : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("LabelTemplate")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("Name").AsAnsiString(255).NotNullable()
                  .WithColumn("Type").AsByte().NotNullable()
                  .WithColumn("Language").AsByte().NotNullable()
                  .WithColumn("Code").AsAnsiString(int.MaxValue).NotNullable()
                  .WithColumn("Version").AsInt64().NotNullable()
                  .WithColumn("IsActive").AsBoolean().NotNullable();
        }
    }
}