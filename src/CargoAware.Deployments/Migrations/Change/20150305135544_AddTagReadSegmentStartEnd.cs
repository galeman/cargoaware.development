﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150305135544)]
    public class AddTagReadSegmentStartEnd_20150305135544 : AutoReversingMigration
    {
        public override void Up()
        {
            Alter.Table("Shipment")
                 .AddColumn("TagReadSegmentStart").AsCustom("datetimeoffset(0)").Nullable()
                 .AddColumn("TagReadSegmentEnd").AsCustom("datetimeoffset(0)").Nullable();
        }
    }
}