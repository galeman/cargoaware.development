﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.LabelTemplate;
using System;
using System.Collections.Generic;

namespace CargoAware.Services.PrintJob
{
    public class PrintJobDocument : IAssociate<PrintJobDataDocument>
    {
        public PrintJobDocument()
        {
            LabelData = new HashSet<PrintJobDataDocument>();
        }

        public long Id { get; set; }
        public long LocationId { get; set; }
        public DateTimeOffset CreatedDateTimeUtc { get; set; }
        public DateTimeOffset? SubmittedDateTimeUtc { get; set; }
        public DateTimeOffset? CancelRequestDateTimeUtc { get; set; }
        public DateTimeOffset? CancelledDateTimeUtc { get; set; }
        public LabelType LabelType { get; set; }
        public ISet<PrintJobDataDocument> LabelData { get; set; }
        public String PrinterAddress { get; set; }
        public Int16 PrinterPort { get; set; }
        public String PrinterExternalId { get; set; }
        public String AirWaybill { get; set; }
        public Int16 StartPiece { get; set; }
        public Int16 EndPiece { get; set; }
        public String Uld { get; set; }

        public void Associate(PrintJobDataDocument value)
        {
            LabelData.Add(value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var other = obj as PrintJobDocument;
            return other != null && other.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}