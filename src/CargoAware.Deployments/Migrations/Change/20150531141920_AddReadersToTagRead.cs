﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150531141920)]
    public class AddReadersToTagRead_20150531141920 : AutoReversingMigration
    {
        public override void Up()
        {
            if (!Schema.Table("TagRead").Column("Readers").Exists())
            {
                Alter.Table("TagRead")
                     .AddColumn("Readers").AsAnsiString(512).Nullable().SetExistingRowsTo("");
            }
        }
    }
}