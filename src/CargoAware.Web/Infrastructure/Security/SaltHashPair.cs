﻿using System;

namespace CargoAware.Web.Infrastructure.Security
{
    public class SaltHashPair : Tuple<String, Byte[]>
    {
        public SaltHashPair(String item1, Byte[] item2)
            : base(item1, item2)
        {
        }

        public String Salt
        {
            get { return Item1; }
        }

        public Byte[] Hash
        {
            get { return Item2; }
        }
    }
}