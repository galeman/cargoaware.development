﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure.Dapper;
using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Linq;

namespace CargoAware.Services.Location
{
    public sealed class LocationRepository : RepositoryBase
    {
        public LocationRepository(IDbConnectionFactory dbFactory)
        {
            DbFactory = dbFactory;
        }

        public bool DoesLocationWithSameNameExist(
            string name, long? excludedId = null, EntityArchivalState archivalState = EntityArchivalState.ActiveOnly)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var sql = "SELECT TOP (1) 1 FROM Location WHERE Name = @name";

                if (excludedId.HasValue)
                {
                    sql = sql + " AND Id <> @excludedId";
                }

                switch (archivalState)
                {
                    case EntityArchivalState.ActiveOnly:
                        sql = sql + " AND IsArchived = 0";
                        break;
                    case EntityArchivalState.ArchivedOnly:
                        sql = sql + " AND IsArchived = 1";
                        break;
                }

                return db.QueryBoolean(sql, new { name, excludedId });
            }
        }

        public LocationDocument GetLocationById(long id)
        {
            var result = QueryLocations(builder => builder.Where("l.Id = @id", new { id })).Data;
            return result.FirstOrDefault();
        }

        public LocationDocument GetLocationByApiKey(String apiKey, EntityArchivalState archivalState = EntityArchivalState.ActiveOnly)
        {
            var result = QueryLocations(builder =>
            {
                builder.Where("l.ApiKey = @apiKey", new { apiKey });

                switch (archivalState)
                {
                    case EntityArchivalState.ActiveOnly:
                        builder.Where("l.IsArchived = 0");
                        break;
                    case EntityArchivalState.ArchivedOnly:
                        builder.Where("l.IsArchived = 1");
                        break;
                }
            }).Data;
            return result.FirstOrDefault();
        }

        public long GetLocationIdByName(String name, EntityArchivalState archivalState = EntityArchivalState.ActiveOnly)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();
                var selector = builder.AddTemplate(@"
SELECT TOP (1) Id
FROM Location
/**where**/;");

                builder.Where("Name = @name", new { name });

                switch (archivalState)
                {
                    case EntityArchivalState.ActiveOnly:
                        builder.Where("IsArchived = 0");
                        break;
                    case EntityArchivalState.ArchivedOnly:
                        builder.Where("IsArchived = 1");
                        break;
                }

                var records = db.Query<long>(
                    selector.RawSql,
                    selector.Parameters);
                return records.FirstOrDefault();
            }
        }

        public Paged<LocationDocument> GetLocations(
            EntityArchivalState archivalState = EntityArchivalState.ActiveOnly, Action<SqlBuilder> query = null)
        {
            var result = QueryLocations(builder =>
            {
                switch (archivalState)
                {
                    case EntityArchivalState.ActiveOnly:
                        builder.Where("l.IsArchived = 0");
                        break;
                    case EntityArchivalState.ArchivedOnly:
                        builder.Where("l.IsArchived = 1");
                        break;
                }

                if (query != null)
                {
                    query(builder);
                }
            });
            return result;
        }

        internal void InsertOrUpdate(LocationDocument entity)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE Location
SET Name = @Name,
    AxisDirection = @AxisDirection,
    OriginLocation = @OriginLocation,
    IsArchived = @IsArchived
WHERE Id = @Id;
IF @@ERROR = 0 AND @@ROWCOUNT = 0
    INSERT INTO Location (
        Name,
        AxisDirection,
        OriginLocation,
        ApiKey
    ) VALUES (
        @Name,
        @AxisDirection,
        @OriginLocation,
        @ApiKey
    );", entity);

                scope.Complete();
            }
        }

        internal void Archive(long id)
        {
            using (var scope = GetNewTransactionScope())
            using (var db = DbFactory.OpenDbConnection())
            {
                db.Execute(@"
UPDATE Location
SET IsArchived = 1
WHERE Id = @id;", new { id });

                scope.Complete();
            }
        }

        private Paged<LocationDocument> QueryLocations(Action<SqlBuilder> query)
        {
            using (var db = DbFactory.OpenDbConnection())
            {
                var builder = new SqlBuilder();

                var selector = builder.AddTemplate(@"
WITH cte AS (
    SELECT
        l.Id,
        l.Name,
        l.AxisDirection,
        l.OriginLocation,
        l.ApiKey,
        l.IsArchived,
        CAST(1 AS BIGINT) AS FirstRow,
        DENSE_RANK() OVER (/**orderby**/) AS _row
    FROM dbo.vwLocation l
    /**where**/
),
_totals AS (
	SELECT COUNT_BIG(DISTINCT _row) AS LastRow
	FROM cte 
)

SELECT * FROM cte
CROSS APPLY _totals
/**skiptake**/
ORDER BY _row;");

                if (query != null)
                {
                    query(builder);
                }

                builder.OrderBy("l.Name, l.Id");

                long total = 0;
                var data = db.Query(
                    selector.RawSql,
                    DapperMapper.CreatePagedMap<LocationDocument>(i => total = i),
                    selector.Parameters,
                    splitOn: "FirstRow,LastRow");

                return new Paged<LocationDocument>(data.Distinct(), total);
            }
        }
    }
}