﻿using System;
using System.ComponentModel;

namespace CargoAware.Services.KeyValue
{
    public class KeyValueDocument
    {
        public String Application { get; set; }
        public String Key { get; set; }
        public String Reference { get; set; }
        public String Value { get; set; }
        public byte[] BinaryValue { get; set; }
        public DateTimeOffset LastWriteDateTime { get; private set; }

        public DateTime LastWriteDateTimeSetter
        {
            set
            {
                LastWriteDateTime = new DateTimeOffset(value, TimeSpan.Zero);
            }
        }

        public override String ToString()
        {
            return String.Format(
                "Application:{0}|Key:{1}|Reference:{2}|Value:{3}|BinaryCount:{4}|LastWriteDateTime:{5}", Application,
                Key, Reference, Value, BinaryValue != null ? BinaryValue.Length : 0, LastWriteDateTime);
        }

        public DateTime? GetValueAsDateTime()
        {
            return ConvertValueTo<DateTime?>();
        }

        public int? GetValueAsInteger()
        {
            return ConvertValueTo<int?>();
        }
        public long? GetValueAsLong()
        {
            return ConvertValueTo<long?>();
        }

        public T ConvertValueTo<T>()
        {
            if (String.IsNullOrEmpty(Value))
            {
                return default(T);
            }

            var converter = TypeDescriptor.GetConverter(typeof(T));
            return (T)converter.ConvertFromString(Value);
        }
    }
}