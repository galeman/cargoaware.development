﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Change
{
    [Migration(20150703131540)]
    public class AddTagReadProcessedTable_20150703131540 : AutoReversingMigration
    {
        public override void Up()
        {
            if (!Schema.Table("TagReadProcessed").Exists())
            {
                Create.Table("TagReadProcessed")
                  .WithColumn("Id").AsInt64()
                  .WithColumn("Epc").AsAnsiString(255).NotNullable()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("RegionManagerId").AsAnsiString(50).Nullable()
                  .WithColumn("X").AsDouble().NotNullable()
                  .WithColumn("Y").AsDouble().NotNullable()
                  .WithColumn("Z").AsDouble().NotNullable()
                  .WithColumn("Rssi").AsInt16().NotNullable()
                  .WithColumn("Readers").AsAnsiString(512).NotNullable()
                  .WithColumn("Timestamp").AsCustom("datetimeoffset(0)").NotNullable().Indexed()
                  .WithColumn("LocationZoneId").AsInt64().Nullable()
                  .WithColumn("FixedLocationRead").AsBoolean().NotNullable();
                
                
                Execute.Sql(@"
CREATE NONCLUSTERED INDEX [] ON [dbo].[TagReadProcessed]
(
    [Epc] ASC
)
INCLUDE ([LocationId], [Timestamp]);");
            }

        }
            
    }
}
