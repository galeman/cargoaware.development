﻿using CargoAware.Services.Infrastructure;
using System;
using System.Configuration;

namespace CargoAware.WebApp.Infrastructure.Settings
{
    public sealed class MiniProfilerSettings
    {
        private const String EnabledKey = "EnableMiniProfiler";

        public Boolean IsEnabled { get; set; }


        public void WriteTo(Configuration config)
        {
            config.AppSettings.Settings.Set(EnabledKey, IsEnabled);
            config.Save();
        }

        public static MiniProfilerSettings ReadFrom(Configuration config)
        {
            return new MiniProfilerSettings
            {
                IsEnabled = config.AppSettings.Settings.GetOrDefault(EnabledKey, false)
            };
        }
    }
}