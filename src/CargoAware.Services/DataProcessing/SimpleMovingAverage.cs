﻿using CargoAware.Services.Common;
using CargoAware.Services.Infrastructure;
using CargoAware.Services.Shipment;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.Services.DataProcessing
{
    public static class SimpleMovingAverage
    {
        /// <summary>
        /// Returns a copy of the processed data ordered by Timestamp, then by Id.
        /// </summary>
        /// <param name="data">Data to filter.</param>
        /// <param name="timeLength">Number of seconds to consider for the Moving Average calculation.</param>
        public static IEnumerable<TagReadDocument> Filter(IEnumerable<TagReadDocument> data, Double timeLength = -10)
        {
            var sortedDataArray = data.OrderBy(x => x.Timestamp).ThenBy(x => x.Id).ToArray();
            var result = sortedDataArray.Select(x => x.CopyCommonPropertiesTo<TagReadDocument>()).ToArray();

            if (timeLength > 0)
            {
                timeLength = -timeLength;
            }

            foreach (var entry in result)
            {
                var startingTime = entry.Timestamp.AddSeconds(timeLength);
                var tagReadsInTimeSlice = sortedDataArray
                    .SkipWhile(x => x.Timestamp <= startingTime)
                    .TakeWhile(x => x.Timestamp <= entry.Timestamp) // include current entry
                    .Select(x => new Point3D { X = x.X, Y = x.Y, Z = x.Z })
                    .ToArray();
                if (tagReadsInTimeSlice.Length == 1)
                {
                    continue;
                }

                var averageOfTimeSlice = tagReadsInTimeSlice.Aggregate((r1, r2) =>
                    new Point3D { X = r1.X + r2.X, Y = r1.Y + r2.Y });
                averageOfTimeSlice.X /= tagReadsInTimeSlice.Length;
                averageOfTimeSlice.Y /= tagReadsInTimeSlice.Length;

                entry.X = averageOfTimeSlice.X;
                entry.Y = averageOfTimeSlice.Y;
            }

            return result;
        }
    }
}