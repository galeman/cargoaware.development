﻿using CargoAware.Services.Location;
using CargoAware.Services.User;
using CargoAware.Web.Infrastructure.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;

namespace CargoAware.Web.Infrastructure.Security
{
    public sealed class UserPrincipal : IPrincipal
    {
        private UserPrincipal(
            long userId,
            String username,
            String name,
            String email,
            LogInState logInState,
            IEnumerable<String> permissions,
            String securityQuestion,
            String securityAnswer,
            IEnumerable<LocationDocument> locations,
            LocationDocument currentLocation,
            DateTime? whenPasswordLastChanged)
        {
            Id = userId;
            Username = username;
            Name = name;
            Email = email;
            LogInState = logInState;
            Permissions = permissions.Select(x => new PermissionKey(x)).ToArray();
            SecurityQuestion = securityQuestion;
            SecurityAnswer = securityAnswer;
            Locations = locations.OrderBy(x => x.Name).ToArray();
            CurrentLocation = currentLocation;
            WhenPasswordLastChanged = whenPasswordLastChanged;

            Identity = new UserIdentity(this);
        }

        public long Id { get; private set; }
        public String Username { get; private set; }
        public String Name { get; private set; }
        public String Email { get; private set; }
        public LogInState LogInState { get; private set; }
        public IList<PermissionKey> Permissions { get; private set; }
        public String SecurityQuestion { get; private set; }
        public String SecurityAnswer { get; private set; }
        public IList<LocationDocument> Locations { get; set; }
        public LocationDocument CurrentLocation { get; set; }
        public DateTime? WhenPasswordLastChanged { get; private set; }

        public Boolean HasPermission(String request)
        {
            var permission = new PermissionKey(request);
            return Permissions.AnyPermits(permission);
        }

        public void Update(LogInState logInState)
        {
            LogInState = logInState;
        }

        public void Update(LocationDocument location)
        {
            CurrentLocation = location;
        }

        #region // IPrincipal Members
        // ==================================================

        public IIdentity Identity { get; private set; }

        public Boolean IsInRole(String role)
        {
            return HasPermission(role);
        }

        private sealed class UserIdentity : IIdentity
        {
            private readonly UserPrincipal _User;

            public UserIdentity(UserPrincipal user)
            {
                _User = user;
            }

            public String AuthenticationType
            {
                get { return "Custom"; }
            }

            public Boolean IsAuthenticated { get { return _User.Id != 0; } }

            public String Name { get { return _User.Username; } }
        }

        // ==================================================
        #endregion


        #region // Static Methods
        // ==================================================

        public static UserPrincipal Anonymous
        {
            get
            {
                return new UserPrincipal(
                    0,
                    "Anonymous",
                    "",
                    "",
                    0,
                    new String[0],
                    "",
                    "",
                    new LocationDocument[0],
                    null,
                    null);
            }
        }

        public static UserPrincipal FromPrincipal(IPrincipal principal)
        {
            var result = (principal as UserPrincipal) ?? Anonymous;
            return result;
        }

        public static UserPrincipal FromAuthorizationDetails(AuthorizationDetails user)
        {
            if (user == null)
            {
                return Anonymous;
            }

            return new UserPrincipal(
                user.Id,
                user.Username,
                user.Name,
                user.Email,
                0,
                user.Permissions,
                user.SecurityQuestion,
                user.SecurityAnswer,
                user.Locations,
                null,
                user.LastPasswordChange);
        }

        // ==================================================
        #endregion
    }
}