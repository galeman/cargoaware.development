﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;

namespace CargoAware.Services.User
{
    public static class UserService
    {
        private static Lazy<UserRepository> _UserRepository;


        static UserService()
        {
            Inject(Repositories.ForUsers);
        }

        private static void Inject(Lazy<UserRepository> dependency)
        {
            _UserRepository = dependency;
        }


        /// <param name="message">Required: Username, Name, Email, Permissions, RecordedById</param>
        public static UserDocument CreateUser(UserDocument message, Boolean verifyRecordedById = true)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            message.Id = 0;
            var logInKey = Generate.NextLogInKey();
            var validUntilUtc = DateTime.UtcNow.Add(TimeSpan.FromDays(1));
            var salt = Generate.NextSalt();
            var encryptedToken = Security.Encrypt(Generate.NextToken(), salt);

            // Step 3: Does this command pass business rules?

            var sameUsernameUserExists = _UserRepository.Value.DoesUserWithSameUsernameExist(message.Username, 0);
            if (sameUsernameUserExists)
            {
                throw new DomainException("The user \"{0}\" already exists.", message.Username);
            }

            if (verifyRecordedById)
            {
                var recordedByUserExists = _UserRepository.Value.DoesUserExist(message.RecordedById);
                if (!recordedByUserExists)
                {
                    throw new DomainException("The specified RecordedById user does not exist.");
                }
            }

            // Step 4: Update the view in a single transaction.

            long userId;
            using (var scope = new TransactionScope(TransactionScopeOption.Required, RepositoryBase.TransactionOptionsDefault))
            {
                _UserRepository.Value.InsertUser(
                    message.Name, message.Email, message.Username, encryptedToken, salt, logInKey, validUntilUtc);

                userId = _UserRepository.Value.GetUserIdByUsername(message.Username);
                _UserRepository.Value.AddPermissions(
                    userId,
                    message.Permissions.Select(Permissions.User.FriendlyToUri));

                scope.Complete();
            }

            var result = _UserRepository.Value.GetUserById(userId);
            return result;
        }

        public static void SetPassword(long userId, String password)
        {
            // Step 1: Does the command pass validation rules?

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            var salt = Generate.NextSalt();
            var encryptedToken = Security.Encrypt(Generate.NextToken(), salt);
            var passwordHash = Security.Hash(password, salt);

            // Step 3: Does this command pass business rules?

            if (password.Length < AppSettings.Instance.MinimumPasswordLength)
            {
                throw new DomainException(PasswordValidation.GetMinimumLengthMessage());
            }

            if (!new Regex(PasswordValidation.GetComplexityRegex()).IsMatch(password))
            {
                throw new DomainException(PasswordValidation.GetSimpleMessage());
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = new TransactionScope(TransactionScopeOption.Required, RepositoryBase.TransactionOptionsDefault))
            {
                _UserRepository.Value.SetPassword(userId, passwordHash, DateTime.UtcNow, salt, encryptedToken);
                scope.Complete();
            }
        }

        public static void SetEmail(long userId, String email)
        {
            // Step 1: Does the command pass validation rules?

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            // Step 4: Update the view in a single transaction.

            using (var scope = new TransactionScope(TransactionScopeOption.Required, RepositoryBase.TransactionOptionsDefault))
            {
                _UserRepository.Value.SetEmail(userId, email);
                scope.Complete();
            }
        }

        public static void SetSecurityQuestionAndAnswer(long userId, String question, String answer)
        {
            // Step 1: Does the command pass validation rules?

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            // Step 4: Update the view in a single transaction.

            using (var scope = new TransactionScope(TransactionScopeOption.Required, RepositoryBase.TransactionOptionsDefault))
            {
                _UserRepository.Value.SetSecurityQuestionAndAnswer(userId, question, answer);
                scope.Complete();
            }
        }
    }
}