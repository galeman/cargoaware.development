﻿using System;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Web.Http;
using CargoAware.Web.Infrastructure.Security;

namespace CargoAware.Web.Infrastructure.Controllers
{
    [ApiHttpsEncryption]
    public abstract class BaseApiController<TPrincipal> : ApiController where TPrincipal : class, IPrincipal
    {
        protected new TPrincipal User
        {
            get { return base.User as TPrincipal; }
        }


        #region // Response Helpers
        // ==================================================

        protected HttpResponseMessage SuccessResponse<T>(T result)
        {
            return Request.CreateResponse(HttpStatusCode.OK, result);
        }

        protected HttpResponseMessage ErrorResponse(String message)
        {
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, message);
        }

        protected HttpResponseMessage ErrorResponse(Exception exception)
        {
            return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, exception);
        }

        protected HttpResponseException ResponseException(String message)
        {
            return new HttpResponseException(ErrorResponse(message));
        }

        protected HttpResponseException ResponseException(Exception exception)
        {
            return new HttpResponseException(ErrorResponse(exception));
        }

        protected HttpResponseException UnauthorizedException()
        {
            return new HttpResponseException(HttpStatusCode.Unauthorized);
        }

        // ==================================================
        #endregion
    }
}