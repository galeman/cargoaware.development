﻿using CargoAware.JobService.Infrastructure;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.JobService.Jobs
{
    public abstract class BaseJob : IJobEx
    {
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public Boolean ContinueProcessing { get; set; }
        public string Description { get; set; }
        public int IntervalSeconds { get; set; }

        //public void Execute()
        //{
        //    Execute(null);
        //}

        public void Execute() // (IJobExecutionContext context)
        {
            ContinueProcessing = true;

            try
            {
                OnPreExecute(); // (context);
                OnExecute(); // (context);
                OnPostExecute(); // (context);
            }
            catch (Exception ex)
            {
                Program.WriteOutput(_Log.Error, ex.ToString());
            }
        }

        protected virtual void OnPreExecute() // (IJobExecutionContext context)
        {
            CheckParameters();
        }

        protected virtual void OnExecute() // (IJobExecutionContext context)
        {
            throw new NotImplementedException();
        }

        protected virtual void OnPostExecute() // (IJobExecutionContext context)
        {
        }

        public virtual void CheckParameters()
        {
            var missingParameters = GetMissingParameters().ToArray();
            if (missingParameters.Length == 0)
            {
                return;
            }

            throw new InvalidOperationException("Missing parameters: " + String.Join(", ", missingParameters));
        }

        public abstract IEnumerable<String> GetMissingParameters();

        public static string GetActionName<T>()
        {
            foreach (var t in typeof(T).GetCustomAttributes(typeof(JobDescriptionAttribute), false))
            {
                var attribute = t as JobDescriptionAttribute;

                if (attribute != null)
                {
                    return attribute.ActionName;
                }
            }

            return "";
        }
    }
}