﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630150100)]
    public sealed class vwPrintJob : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwPrintJob') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwPrintJob AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwPrintJob
                AS
                   SELECT
                        pj.Id,
                        pj.LocationId,
                        pj.CreatedDateTimeUtc,
                        pj.SubmittedDateTimeUtc,
                        pj.CancelRequestDateTimeUtc,
                        pj.CancelledDateTimeUtc,
                        pj.LabelType,
                        pj.PrinterAddress,
                        pj.PrinterPort,
                        pj.PrinterExternalId,
                        pj.AirWaybill,
                        pj.StartPiece,
                        pj.EndPiece,
                        pj.Uld,
                        pjd.Id AS PrintJobDataId,
                        pjd.PrintJobId,
                        pjd.Epc,
                        pjd.Barcode,
                        pjd.PieceNumber,
                        pjd.AirWaybill AS AirWaybillDapper,
                        pjd.Uld AS UldDapper,
                        pjd.Destination,
                        pjd.Pieces,
                        pjd.Origin,
                        pjd.Weight,
                        pjd.UnitOfMeasure,
                        pjd.Service,
                        pjd.HandlingCodes,
                        pjd.TransfersInfo AS TransfersInfoAsString,
                        pjd.TransfersDateTime AS TransfersDateTimeAsString,
                        pjd.PrintedDateTime
                    FROM dbo.PrintJob pj
                        LEFT JOIN dbo.PrintJobData pjd ON pj.Id = pjd.PrintJobId;");
        }
    }
}
