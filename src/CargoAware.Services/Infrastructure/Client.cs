﻿using CargoAware.Services.Infrastructure.Repositories;
using Dapper;
using System;
using System.Data;

namespace CargoAware.Services.Infrastructure
{
    public static class Client
    {
        static Client()
        {
            SqlMapper.AddTypeMap(typeof(String), DbType.AnsiString);
        }

        public static Func<IDbConnectionFactory> ReportStore =
            () => { throw new InvalidOperationException("Set the Client.ReportStore builder."); };

        public static Func<IDbConnectionFactory> ViewStore =
            () => { throw new InvalidOperationException("Set the Client.ViewStore builder."); };
    }
}