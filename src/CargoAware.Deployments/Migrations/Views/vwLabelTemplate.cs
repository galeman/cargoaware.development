﻿using FluentMigrator;

namespace CargoAware.Deployment.Migrations.Views
{
    [Migration(20150630143936)]
    public sealed class vwLabelTemplate : ForwardOnlyMigration
    {
        public override void Up()
        {
            Execute.Sql(@"
                IF NOT EXISTS (SELECT TABLE_NAME FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'vwLabelTemplate') BEGIN 
                    EXEC sp_executesql N'CREATE VIEW vwLabelTemplate AS SELECT 1 AS PlaceHolder'
                END 
                GO
                ALTER VIEW dbo.vwLabelTemplate 
                AS
                    SELECT
                        lt.Id,
                        lt.LocationId,
                        lt.Name,
                        lt.[Type],
                        lt.Language,
                        lt.Code,
                        lt.Version,
                        lt.IsActive
                    FROM dbo.LabelTemplate lt;");
        }
    }
}
