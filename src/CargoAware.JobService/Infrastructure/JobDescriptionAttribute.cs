﻿using System;

namespace CargoAware.JobService.Infrastructure
{
    public class JobDescriptionAttribute : Attribute
    {
        public JobDescriptionAttribute(string actionName, string usageExample)
        {
            ActionName = actionName;
            UsageExample = usageExample;
        }

        public string ActionName { get; set; }
        public string UsageExample { get; set; }
    }
}