﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Infrastructure.Repositories;
using System;

namespace CargoAware.Services.RegionManager
{
    public static class RegionManagerService
    {
        private static Lazy<RegionManagerRepository> _RegionManagerRepository;


        static RegionManagerService()
        {
            Inject(Repositories.ForRegionManager);
        }

        private static void Inject(Lazy<RegionManagerRepository> dependency)
        {
            _RegionManagerRepository = dependency;
        }


        /// <param name="message">Required: LocationId, Name, RegionId</param>
        public static RegionManagerRegionDocument CreateRegion(RegionManagerRegionDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            message.Id = 0;

            // Step 3: Does this command pass business rules?

            var sameRegionIdExists = _RegionManagerRepository.Value.DoesRegionWithSameRegionIdExist(message.LocationId, message.RegionId);
            if (sameRegionIdExists)
            {
                throw new DomainException("The Region \"{0:D}\" already exists.", message.RegionId);
            }

            // Step 4: Update the view in a single transaction.

            long regionId;
            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _RegionManagerRepository.Value.InsertOrUpdate(message);
                regionId = _RegionManagerRepository.Value.GetIdByRegionId(message.RegionId);
                scope.Complete();
            }

            var result = _RegionManagerRepository.Value.GetRegionById(regionId);
            return result;
        }

        /// <param name="message">Required: Id, LocationId, Name, RegionId</param>
        public static void UpdateRegion(RegionManagerRegionDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var existingRegion = _RegionManagerRepository.Value.GetRegionById(message.Id);
            if (existingRegion == null)
            {
                throw new DomainException("The specified Region, \"{0:D}\", does not exist.", message.RegionId);
            }

            var sameRegionIdExists = _RegionManagerRepository.Value.DoesRegionWithSameRegionIdExist(message.LocationId, message.RegionId, message.Id);
            if (sameRegionIdExists)
            {
                throw new DomainException("A Region \"{0:D}\" already exists.", message.RegionId);
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _RegionManagerRepository.Value.InsertOrUpdate(message);
                scope.Complete();
            }
        }

        /// <param name="message">Required: Id</param>
        public static void ArchiveRegion(RegionManagerRegionDocument message)
        {
            // Step 1: Does the command pass validation rules?

            //Requires.False(message.Name.IsBlank(), "Name must not be blank.");
            //Requires.False(message.Email.IsBlank(), "Email must not be blank.");

            //var validationResult = ValidatorFactory.GetValidator(message.GetType()).Validate(message);
            //if (!validationResult.IsValid)
            //{
            //    throw new ValidationException(validationResult.Errors);
            //}

            // Step 2: Set up required objects.

            // Step 3: Does this command pass business rules?

            var doesRegionExist = _RegionManagerRepository.Value.GetRegionById(message.Id);
            if (doesRegionExist == null)
            {
                throw new DomainException("The specified Region does not exist.");
            }

            // Step 4: Update the view in a single transaction.

            using (var scope = RepositoryBase.GetNewTransactionScope())
            {
                _RegionManagerRepository.Value.Archive(message.Id);
                scope.Complete();
            }
        }
    }
}