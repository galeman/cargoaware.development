﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.LocationZone;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("admin/locationzones")]
    public partial class AdminLocationZonesController : AppMvcController
    {
        private static Lazy<LocationZoneRepository> _LocationZoneRepository;


        #region // Constructor + DI
        // ==================================================

        static AdminLocationZonesController()
        {
            Inject(Repositories.ForLocationZones);
        }

        private static void Inject(Lazy<LocationZoneRepository> dependency)
        {
            _LocationZoneRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.AdminLocationZones()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        [GET("addedit")]
        public virtual ActionResult AddEdit(Boolean adding, long[] ids)
        {
            if (!User.HasPermission(Permissions.User.AdminLocationZones()))
            {
                throw UnauthorizedException();
            }

            var model = new AddEditLocationZonesModel
            {
                Adding = adding,
                LocationZones = _LocationZoneRepository.Value.GetLocationZones(User.CurrentLocation.Id).Data
            };

            if (!adding)
            {
                if (ids != null && ids.Length != 0)
                {
                    model.Details = model.LocationZones
                        .Where(x => ids.Any(id => x.Id == id))
                        .Select(x => new AddEditLocationZonesDetailsModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            LmsZoneId = x.LmsZoneId,
                            AutoUpdateLms = x.AutoUpdateLms,
                            LmsUpdateDelaySeconds = x.LmsUpdateDelaySeconds,
                            EnvoyZoneId = x.EnvoyZoneId,
                            Points = x.Points
                        });
                }
            }

            return View(model);
        }

        // ==================================================
        #endregion
    }
}