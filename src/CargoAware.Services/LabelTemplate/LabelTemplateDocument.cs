﻿using System;

namespace CargoAware.Services.LabelTemplate
{
    public class LabelTemplateDocument
    {
        public long Id { get; set; }
        public long LocationId { get; set; }
        public String Name { get; set; }
        public LabelType Type { get; set; }
        public TemplateLanguage Language { get; set; }
        public String Code { get; set; }
        public long Version { get; set; }
        public Boolean IsActive { get; set; }
    }
}