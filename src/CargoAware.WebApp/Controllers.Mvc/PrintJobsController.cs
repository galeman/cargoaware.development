﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("admin/printjobs")]
    public partial class PrintJobsController : AppMvcController
    {
        #region // Constructor + DI
        // ==================================================

        static PrintJobsController()
        {
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.ViewPrintJobs()) &&
                !User.HasPermission(Permissions.User.ManagePrintJobs()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        // ==================================================
        #endregion
    }
}