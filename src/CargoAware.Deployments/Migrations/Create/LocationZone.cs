﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20150119205566)]
    public sealed class LocationZone : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("LocationZone")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("Name").AsAnsiString(255).NotNullable()
                  .WithColumn("LmsZoneId").AsAnsiString(50).NotNullable()
                  .WithColumn("AutoUpdateLms").AsBoolean().NotNullable()
                  .WithColumn("LmsUpdateDelaySeconds").AsInt16().NotNullable()
                  .WithColumn("EnvoyZoneId").AsAnsiString(50).NotNullable()
                  .WithColumn("SpatialData").AsCustom("geometry").NotNullable()
                  .WithColumn("ArchivedDateTimeUtc").AsCustom("datetimeoffset(0)").Nullable();
            Execute.Sql(@"
CREATE SPATIAL INDEX IX_LocationZone_SpatialData
ON LocationZone(SpatialData)
WITH (BOUNDING_BOX = (xmin=-1000, ymin=-1000, xmax=1000, ymax=1000));");
        }
    }
}