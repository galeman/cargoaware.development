﻿namespace CargoAware.Services.Common
{
    public enum EntitySubmittedState : byte
    {
        All,
        NonSubmittedOnly,
        SubmittedOnly
    }
}