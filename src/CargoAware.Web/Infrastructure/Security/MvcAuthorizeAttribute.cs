using System;
using System.Web.Mvc;

namespace CargoAware.Web.Infrastructure.Security
{
    public class MvcAuthorizeAttribute : AuthorizeAttribute
    {
        public Boolean RequirePasswordValidation { get; set; }


        public MvcAuthorizeAttribute()
        {
            RequirePasswordValidation = false;
        }


        //protected override Boolean AuthorizeCore(HttpContextBase httpContext)
        //{
        //    if (!RequirePasswordValidation)
        //    {
        //        return base.AuthorizeCore(httpContext);
        //    }

        //    if (!SudoSession.IsActive(httpContext))
        //    {
        //        var user = httpContext.User as UserPrincipal;
        //        if (user == null)
        //        {
        //            return false;
        //        }

        //        UserSession.ApplyNewLogInState(logInStatesToAdd: LogInState.MustEnterSudoPassword);
        //        SudoSession.SetRedirectUrl(httpContext, httpContext.Request.RawUrl);
        //        return false;
        //    }

        //    SudoSession.Extend(httpContext);
        //    return true;
        //}
    }
}