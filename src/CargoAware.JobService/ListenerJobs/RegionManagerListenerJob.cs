﻿using CargoAware.JobService.Infrastructure;
using CargoAware.JobService.Jobs;
using CargoAware.JobService.Models;
using CargoAware.Services.Shipment;
using log4net;
using Newtonsoft.Json;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CargoAware.JobService.ListenerJobs
{
    [JobDescription("RegionManagerListener", "-a {JobName}")]
    public sealed class RegionManagerListenerJob : BaseJob
    {
        private static readonly ILog _Log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public RegionManagerListenerJob()
        {
            RabbitMqUri = ConfigurationManager.AppSettings["RabbitMQ"];
            QueueName = ConfigurationManager.AppSettings["QueueName"] ?? "CargoAware.JobService";
            DequeueCount = int.Parse(ConfigurationManager.AppSettings["DequeueCount"] ?? "10");
            TagPrefixes = (ConfigurationManager.AppSettings["TagPrefixes"] ?? "")
                .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Where(x => !String.IsNullOrWhiteSpace(x))
                .ToArray();
            UploadTagReadsUrl = ConfigurationManager.AppSettings["UploadTagReadsUrl"];
        }

        public String RabbitMqUri { get; set; }
        public String QueueName { get; set; }
        public int DequeueCount { get; set; }
        public String[] TagPrefixes { get; set; }
        public String UploadTagReadsUrl { get; set; }

        protected override void OnExecute() // (IJobExecutionContext context)
        {
            var connectionFactory = new ConnectionFactory { Uri = RabbitMqUri };
            var taskCancellation = new CancellationTokenSource();
            var token = taskCancellation.Token;
            Task task = null;

            Func<Task, Boolean> isTaskRunning = t => !(t == null || t.IsCanceled || t.IsCompleted || t.IsFaulted);
            Action taskWork = () =>
            {
                try
                {
                    using (var connection = connectionFactory.CreateConnection())
                    using (var channel = connection.CreateModel())
                    {
                        SubscribeToRegions(channel, token);
                    }
                }
                catch (Exception ex)
                {
                    Program.WriteOutput(_Log.Error, ex.ToString());
                }
            };
            Action<CancellationTokenSource, Task> cancelTask = (cts, t) =>
            {
                if (cts == null || !isTaskRunning(t))
                {
                    return;
                }

                cts.Cancel();
                // ReSharper disable MethodSupportsCancellation
                t.Wait();
                // ReSharper restore MethodSupportsCancellation
            };

            while (ContinueProcessing)
            {
                if (!isTaskRunning(task))
                {
                    task = Task.Run(taskWork, token);
                }
                Thread.Sleep(5000);
            }

            cancelTask(taskCancellation, task);
        }

        //private IEnumerable<RegionManagerRegionDocument> GetRegions()
        //{
        //    ICollection<RegionManagerRegionDocument> result;

        //    try
        //    {
        //        using (var client = new HttpClient())
        //        using (var response = client.GetAsync(RegionsUrl, HttpCompletionOption.ResponseContentRead).Result)
        //        {
        //            var contentString = response.Content.ReadAsStringAsync().Result;

        //            if (response.IsSuccessStatusCode)
        //            {
        //                result = JsonConvert.DeserializeObject<ICollection<RegionManagerRegionDocument>>(contentString);
        //            }
        //            else
        //            {
        //                Program.WriteOutput(_Log.Error, contentString);
        //                result = null;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Program.WriteOutput(_Log.Error, ex.ToString());
        //        result = null;
        //    }

        //    return result;
        //}

        private void SubscribeToRegions(IModel channel, CancellationToken token)
        {
            var queue = channel.QueueDeclare(QueueName, true, false, false, null);
            Program.WriteOutput(_Log.Info, String.Format("Successfully created \"{0}\" Queue.", QueueName));
            channel.QueueBind(queue, "amq.topic", "tagBlinks.*");
            Program.WriteOutput(_Log.Info, "Successfully subscribed to Tag Blinks Topic.");

            var consumer = new QueueingBasicConsumer(channel);
            channel.BasicConsume(queue, false, consumer);
            Program.WriteOutput(_Log.Info, "Started processing tags from Queue...");

            while (!token.IsCancellationRequested)
            {
                var messages = new List<BasicDeliverEventArgs>(DequeueCount);

                while (messages.Count < DequeueCount)
                {
                    BasicDeliverEventArgs message;
                    if (consumer.Queue.Dequeue(1000, out message))
                    {
                        messages.Add(message);
                    }
                    else if (messages.Count != 0)
                    {
                        break;
                    }
                }

                TagReadDocument[] validTagReads;
                TagReadDocument[] invalidTagReads;
                FilterTagBlinks(messages, out validTagReads, out invalidTagReads);

                OutputTagBlinks(validTagReads, true);
                OutputTagBlinks(invalidTagReads, false);

                if (validTagReads.Length != 0)
                {
                    while (!UploadTagBlinks(validTagReads))
                    {
                        Thread.Sleep(5000);
                    }
                }

                foreach (var message in messages)
                {
                    channel.BasicAck(message.DeliveryTag, false);
                }

                Thread.Sleep(10);
            }
        }

        private void FilterTagBlinks(
            IEnumerable<BasicDeliverEventArgs> messages,
            out TagReadDocument[] validTagBlinks,
            out TagReadDocument[] invalidTagBlinks)
        {
            var validTagBlinksList = new List<TagReadDocument>();
            var invalidTagBlinksList = new List<TagReadDocument>();

            foreach (var message in messages)
            {
                var body = Encoding.UTF8.GetString(message.Body);
                var regionUpdate = JsonConvert.DeserializeObject<RegionTagBlinkDocumentV15>(body);
                var tagBlinks = regionUpdate.TagBlinks; //GetTagBlinks().ToArray();

                if (TagPrefixes.Length == 0)
                {
                    validTagBlinksList.AddRange(tagBlinks.Select(x => x.ToTagRead(regionUpdate)));
                    continue;
                }

                foreach (var tagBlink in tagBlinks)
                {
                    if (TagPrefixes.Any(x => tagBlink.Epc.StartsWith(x, StringComparison.OrdinalIgnoreCase)))
                    {
                        validTagBlinksList.Add(tagBlink.ToTagRead(regionUpdate));
                    }
                    else
                    {
                        invalidTagBlinksList.Add(tagBlink.ToTagRead(regionUpdate));
                    }
                }
            }

            validTagBlinks = validTagBlinksList.ToArray();
            invalidTagBlinks = invalidTagBlinksList.ToArray();
        }

        private void OutputTagBlinks(IEnumerable<TagReadDocument> tagReads, Boolean processed)
        {
            var messageFormat =
                (processed ? "Processed" : "Ignored") +
                " {1:yyyy-MM-dd|HH:mm:ss.fff} {2} | {3:N0},{4:N0},{5:N0}{0}    {6:D} | {7:N2} | {8}";

            foreach (var tagRead in tagReads)
            {
                Program.WriteOutput(
                    _Log.Info,
                    String.Format(
                        messageFormat,
                        Environment.NewLine,
                        tagRead.Timestamp,
                        tagRead.Epc,
                        tagRead.X,
                        tagRead.Y,
                        tagRead.Z,
                        tagRead.RegionManagerId,
                        tagRead.Rssi,
                        tagRead.Readers));
            }
        }

        private Boolean UploadTagBlinks(IEnumerable<TagReadDocument> tagReads)
        {
            Boolean result;

            try
            {
                using (var client = new HttpClient())
                using (var content = new StringContent(JsonConvert.SerializeObject(tagReads), Encoding.UTF8, "application/json"))
                using (var response = client.PostAsync(UploadTagReadsUrl, content).Result)
                {
                    if (response.IsSuccessStatusCode)
                    {
                        result = true;
                    }
                    else
                    {
                        var errorMessage = response.Content.ReadAsStringAsync().Result;
                        Program.WriteOutput(_Log.Error, errorMessage);
                        result = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Program.WriteOutput(_Log.Error, ex.ToString());
                result = false;
            }

            return result;
        }

        public override IEnumerable<String> GetMissingParameters()
        {
            var missingParameters = new List<String>();
            return missingParameters;
        }
    }
}