﻿using AttributeRouting;
using AttributeRouting.Web.Http;
using CargoAware.Services;
using CargoAware.Services.Common;
using CargoAware.Services.Event;
using CargoAware.Services.Location;
using CargoAware.Services.RegionManager;
using CargoAware.Services.Shipment;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CargoAware.WebApp.Controllers.Api.ExternalServices
{
    [RoutePrefix("jobservice")]
    public sealed class JobServiceApiController : ApiController
    {
        private static Lazy<EventRepository> _EventRepository;
        private static Lazy<LocationRepository> _LocationRepository;
        private static Lazy<RegionManagerRepository> _RegionManagerRepository;


        #region // Constructor + DI
        // ==================================================

        static JobServiceApiController()
        {
            Inject(Repositories.ForEvents);
            Inject(Repositories.ForLocations);
            Inject(Repositories.ForRegionManager);
        }

        private static void Inject(Lazy<EventRepository> dependency)
        {
            _EventRepository = dependency;
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        private static void Inject(Lazy<RegionManagerRepository> dependency)
        {
            _RegionManagerRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // API Commands
        // ==================================================

        [GET("regions"), HttpGet]
        public IEnumerable<RegionManagerRegionDocument> Regions([FromUri] String apiKey)
        {
            var location = _LocationRepository.Value.GetLocationByApiKey(apiKey);
            var result = _RegionManagerRepository.Value.GetRegions(location.Id).Data;
            return result;
        }

        [POST("tagreads"), HttpPost]
        public HttpResponseMessage TagReads([FromUri] String apiKey, TagReadDocument[] tagReads)
        {
            _EventRepository.Value.Insert(new EventDocument
            {
                Timestamp = DateTimeOffset.Now,
                EventSource = EventSource.JobService,
                EventName = "JobService/TagReads",
                Message = JsonConvert.SerializeObject(new
                {
                    ApiKey = apiKey,
                    TagReads = tagReads
                }, Formatting.None)
            });
            ShipmentService.InsertTagReads(apiKey, tagReads);

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [GET("tagreads/processdata"), HttpGet]
        public HttpResponseMessage TagReads([FromUri] String apiKey)
        {
            _EventRepository.Value.Insert(new EventDocument
            {
                Timestamp = DateTimeOffset.Now,
                EventSource = EventSource.JobService,
                EventName = "JobService/ProcessTagReads",
                Message = JsonConvert.SerializeObject(new
                {
                    ApiKey = apiKey,
                }, Formatting.None)
            });

            ShipmentService.InsertTagReadsProcessed(apiKey);
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // ==================================================
        #endregion
    }
}