﻿using FluentMigrator;

namespace CargoAware.Deployments.Migrations.Create
{
    [Migration(20140411160300)]
    public sealed class TagRead : ForwardOnlyMigration
    {
        public override void Up()
        {
            Create.Table("TagRead")
                  .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                  .WithColumn("Epc").AsAnsiString(255).NotNullable()
                  .WithColumn("LocationId").AsInt64().NotNullable().Indexed()
                  .WithColumn("RegionManagerId").AsAnsiString(50).Nullable()
                  .WithColumn("X").AsDouble().NotNullable()
                  .WithColumn("Y").AsDouble().NotNullable()
                  .WithColumn("Z").AsDouble().NotNullable()
                  .WithColumn("Rssi").AsInt16().NotNullable()
                  .WithColumn("Readers").AsAnsiString(512).NotNullable()
                  .WithColumn("Timestamp").AsCustom("datetimeoffset(0)").NotNullable().Indexed()
                  .WithColumn("LocationZoneId").AsInt64().Nullable()
                  .WithColumn("FixedLocationRead").AsBoolean().NotNullable()
                  .WithColumn("IsProcessed").AsBoolean().NotNullable();

            Execute.Sql(@"
CREATE NONCLUSTERED INDEX [IX_TagRead_Epc] ON [dbo].[TagRead]
(
    [Epc] ASC
)
INCLUDE ([LocationId], [Timestamp]);");
        }
    }
}