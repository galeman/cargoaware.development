using FluentMigrator.Runner;
using FluentMigrator.Runner.Announcers;
using FluentMigrator.Runner.Generators.SqlServer;
using FluentMigrator.Runner.Initialization;
using FluentMigrator.Runner.Processors;
using FluentMigrator.Runner.Processors.SqlServer;
using System.Data.SqlClient;
using System.Reflection;

namespace CargoAware.Deployments.Infrastructure
{
    public sealed class MigrateRelationalDb
    {
        public MigrateRelationalDb(string connectionString, params Assembly[] migrations)
        {
            _ConnectionString = connectionString;
            _Migrations = migrations;
        }

        public void Execute()
        {
            var announcer = new NullAnnouncer();
            var runnerContext = new RunnerContext(announcer);
            var options = new ProcessorOptions
            {
                PreviewOnly = runnerContext.PreviewOnly,
                Timeout = runnerContext.Timeout
            };

            var connection = new SqlConnection(_ConnectionString);
            var generator = new SqlServer2008Generator();
            var factory = new SqlServerDbFactory();
            var processor = new SqlServerProcessor(connection, generator, announcer, options, factory);

            foreach (var assembly in _Migrations)
            {
                new MigrationRunner(assembly, runnerContext, processor).MigrateUp();
            }
        }

        private readonly string _ConnectionString;
        private readonly Assembly[] _Migrations;
    }
}