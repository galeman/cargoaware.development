﻿using CargoAware.JobService.Infrastructure;
using CargoAware.JobService.Jobs;
using CargoAware.JobService.ListenerJobs;
using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;
using System.Threading.Tasks;

namespace CargoAware.JobService
{
    public class JobService : ServiceBase
    {
        private static readonly ILog _Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        private static Dictionary<String, IJobEx> _ValidJobs;

        private IJobEx _CurrentJob;

        public JobService()
        {
            ServiceName = "CargoAware.JobService";
        }

        public void Start(String[] args)
        {
            OnStart(args);
        }

        protected override void OnStart(String[] args)
        {
            Program.WriteOutput(
                _Log.Info,
                String.Format(
                    "Metrc Job Service OnStart called at ... {0}",
                    DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss.ttt")));

            if (Program.CommandLineOptions.Verbose)
            {
                Program.WriteOutput(
                    _Log.Debug,
                    String.Format("Action = {0}", Program.CommandLineOptions.ActionToPerform));
                Program.WriteOutput(
                    _Log.Debug,
                    String.Format("Input file = {0}", Program.CommandLineOptions.InputFile));
            }

            try
            {
                if (Program.IsRunningAsService || String.Equals(Program.CommandLineOptions.ActionToPerform, "RunJobs", StringComparison.OrdinalIgnoreCase))
                {
                    //// construct a scheduler factory
                    //_ScheduleFactory = new StdSchedulerFactory();

                    //// get a scheduler
                    //_Scheduler = _ScheduleFactory.GetScheduler();
                    //_Scheduler.Start();

                    //Program.WriteOutput(
                    //    _Log.Info,
                    //    "Number of quartz job groups: " + _Scheduler.GetJobGroupNames().Count);

                    var regionManagerlistener = Convert.ToBoolean(ConfigurationManager.AppSettings["RunRegionManagerListenerJob"]);
                    var processDataJob = Convert.ToBoolean(ConfigurationManager.AppSettings["RunDataProcessingJob"]);

                    if (regionManagerlistener)
                    {
                        var regionManagerListenerName = BaseJob.GetActionName<RegionManagerListenerJob>();
                        var regionManagerListenerJob = ValidJobs[regionManagerListenerName];
                        Task.Run(() => regionManagerListenerJob.Execute());
                    }
                    if (processDataJob)
                    {
                        var dataProcessingName = BaseJob.GetActionName<DataProcessingJob>();
                        var dataProcessingJob = ValidJobs[dataProcessingName];
                        Task.Run(() => dataProcessingJob.Execute());
                    }

                }
                else
                {
                    SetCurrentJob();

                    if (_CurrentJob != null)
                    {
                        ExecuteJob((dynamic)_CurrentJob);
                    }
                    else
                    {
                        Program.WriteOutput(_Log.Info, "No job to execute.");
                    }
                }
            }
            catch (Exception ex)
            {
                Program.WriteOutput(_Log.Error, "Error starting service: " + ex.Message);
            }
        }

        public new void Stop()
        {
            StopJobs();
            Thread.Sleep(5000);
            OnStop();
        }

        public void StopJobs()
        {
            foreach (var validJob in _ValidJobs.Values)
            {
                validJob.ContinueProcessing = false;
            }
        }

        protected override void OnStop()
        {
            //if (_Scheduler != null)
            //{
            //    _Scheduler.Shutdown();
            //}

            //Program.Engine.Stop();

            Program.WriteOutput(
                _Log.Info,
                String.Format(
                    "Metrc Job Service OnStop called at ... {0}",
                    DateTime.UtcNow.ToString("MM/dd/yyyy HH:mm:ss.ttt")));
        }

        private void SetCurrentJob()
        {
            ValidJobs.TryGetValue(Program.CommandLineOptions.ActionToPerform.ToLower(), out _CurrentJob);
        }

        #region ExecuteJob(s)

        //private static void ExecuteJob(CompareProjectionsJob job)
        //{
        //    job.CommandLineParameters = Program.CommandLineOptions.Parameter;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(ExtractMyLoDailyFeedJob job)
        //{
        //    var postAction = ImportMyLoData.PostProcessingFileAction.CopyToProcessedFolder;

        //    if (!string.IsNullOrEmpty(Program.CommandLineOptions.Parameter))
        //    {
        //        postAction = (ImportMyLoData.PostProcessingFileAction)
        //            Enum.Parse(
        //                typeof(ImportMyLoData.PostProcessingFileAction), Program.CommandLineOptions.Parameter,
        //                true);
        //    }

        //    job.ZipPath = Program.CommandLineOptions.InputFile;
        //    job.PostExtractingAction = postAction;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(GetPaymentTransactionDetailsJob job)
        //{
        //    if (!string.IsNullOrEmpty(Program.CommandLineOptions.Parameter))
        //    {
        //        job.PaymentNumber = Program.CommandLineOptions.Parameter;
        //    }

        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(MyLoBusinessDataImportJob job)
        //{
        //    job.ImportPath = Program.CommandLineOptions.InputFile;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(MyLoOccupationalDataImportJob job)
        //{
        //    job.ImportPath = Program.CommandLineOptions.InputFile;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(OutputEventStreamJob job)
        //{
        //    job.CommandLineParameters = Program.CommandLineOptions.Parameter;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(ProcessMyLoDailyFeedJob job)
        //{
        //    var postAction = ImportMyLoData.PostProcessingFileAction.CopyToProcessedFolder;

        //    if (!string.IsNullOrEmpty(Program.CommandLineOptions.Parameter))
        //    {
        //        postAction = (ImportMyLoData.PostProcessingFileAction)
        //            Enum.Parse(
        //                typeof(ImportMyLoData.PostProcessingFileAction), Program.CommandLineOptions.Parameter,
        //                true);
        //    }

        //    job.PostExtractingAction = postAction;
        //    job.FilePath = Program.CommandLineOptions.InputFile;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(ReplayAggregatesJob job)
        //{
        //    job.CommandLineParameters = Program.CommandLineOptions.Parameter;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(ReplayAllEventStreamsJob job)
        //{
        //    job.CommandLineParameters = Program.CommandLineOptions.Parameter;
        //    DoExecuteJob(job);
        //}

        //private static void ExecuteJob(UpdateFacilityActivitySummaryJob job)
        //{
        //    job.CommandLineParameters = Program.CommandLineOptions.Parameter;
        //    DoExecuteJob(job);
        //}

        // Default catch-all:
        private static void ExecuteJob(IJobEx job)
        {
            DoExecuteJob(job);
        }

        #endregion

        private static void DoExecuteJob(IJobEx job)
        {
            if (job == null)
            {
                return;
            }

            job.Execute();
        }

        private static IDictionary<String, IJobEx> ValidJobs
        {
            get
            {
                if (_ValidJobs == null)
                {
                    _ValidJobs = new Dictionary<String, IJobEx>(StringComparer.OrdinalIgnoreCase);

                    var job = typeof(IJobEx);

                    foreach (var type in Assembly.GetExecutingAssembly().GetTypes())
                    {
                        if (!job.IsAssignableFrom(type) || type.IsInterface || type.IsAbstract)
                        {
                            continue;
                        }

                        var jobDescription = GetJobDescription(type);
                        if (jobDescription == null)
                        {
                            continue;
                        }

                        var instance = (IJobEx)Activator.CreateInstance(type);
                        _ValidJobs.Add(jobDescription.ActionName, instance);
                    }
                }

                return _ValidJobs;
            }
        }

        internal static Dictionary<String, String> GetJobUsageExamples()
        {
            var jobs = new Dictionary<string, string>();
            var appName = Assembly.GetExecutingAssembly().GetName().Name;

            foreach (var j in ValidJobs)
            {
                var job = GetJobDescription(j.Value.GetType());
                jobs[job.ActionName] = appName + " " + job.UsageExample.Replace("{JobName}", job.ActionName);
            }

            return jobs;
        }

        private static JobDescriptionAttribute GetJobDescription(Type jobClass)
        {
            MemberInfo info = jobClass;
            var attributes = info.GetCustomAttributes(true);

            foreach (var t in attributes)
            {
                var attribute = t as JobDescriptionAttribute;

                if (attribute != null)
                {
                    return attribute;
                }
            }

            return null;
        }
    }
}