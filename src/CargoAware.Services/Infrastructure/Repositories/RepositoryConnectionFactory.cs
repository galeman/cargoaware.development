﻿using StackExchange.Profiling;
using StackExchange.Profiling.Data;
using System;
using System.Data.Common;

namespace CargoAware.Services.Infrastructure.Repositories
{
    public class RepositoryConnectionFactory : IDbConnectionFactory
    {
        public RepositoryConnectionFactory(string connectionString)
            : this(connectionString, DbProviderFactories.GetFactory("System.Data.SqlClient"))
        {
        }

        public RepositoryConnectionFactory(string connectionString, DbProviderFactory dbProviderFactory)
        {
            ConnectionString = connectionString;
            DbProviderFactory = dbProviderFactory;
        }

        public string ConnectionString { get; set; }
        public DbProviderFactory DbProviderFactory { get; set; }

        public DbConnection OpenDbConnection()
        {
            var connection = CreateDbConnection();
            connection.Open();
            return connection;
        }

        public DbConnection CreateDbConnection()
        {
            if (ConnectionString == null)
            {
                throw new InvalidOperationException("ConnectionString must be set");
            }

            var connection = DbProviderFactory.CreateConnection();
            if (connection == null)
            {
                throw new InvalidOperationException("DbProviderFactory was not able to create a DbConnection");
            }

            if (MiniProfiler.Current != null)
            {
                connection = new ProfiledDbConnection(connection, MiniProfiler.Current);
            }

            connection = new RepositoryDbConnection(connection)
            {
                ConnectionString = ConnectionString
            };
            return connection;
        }
    }
}