﻿using CargoAware.Services.Infrastructure;
using CargoAware.Services.Location;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CargoAware.Services.User
{
    public sealed class AuthorizationDetails :
        IAssociate<PermissionKey>,
        IAssociate<LocationDocument>
    {
        public AuthorizationDetails()
        {
            Permissions = new HashSet<String>();
            Locations = new HashSet<LocationDocument>();
        }

        public long Id { get; internal set; }
        public String Name { get; set; }
        public String Email { get; set; }
        public String Username { get; set; }
        public ICollection<String> Permissions { get; internal set; }

        public ISet<LocationDocument> Locations { get; internal set; }

        public DateTime? LockedUntil { get; internal set; }
        public String LockReason { get; internal set; }
        public DateTime? LastLogIn { get; internal set; }
        public DateTime? LastPasswordChange { get; internal set; }
        public DateTime? LogInKeyValidUntil { get; internal set; }

        public String SecurityQuestion { get; internal set; }
        public String SecurityAnswer { get; internal set; }

        // These private properties are deserialized by Dapper
        private Byte[] PasswordBytes { get; set; }
        private Byte[] SaltBytes { get; set; }
        private String SaltBytesString { get { return Encoding.UTF8.GetString(SaltBytes); } }
        private Byte[] TokenBytes { get; set; }

        private Byte[] PasscodeBytes { get; set; }
        private Byte[] PasscodeSaltBytes { get; set; }
        private String PasscodeSaltBytesString { get { return Encoding.UTF8.GetString(PasscodeSaltBytes); } }

        public Boolean IsArchived { get; internal set; }

        public Boolean IsLocked()
        {
            return LockedUntil.HasValue && LockedUntil.Value > DateTime.UtcNow.AddSeconds(2);
        }

        public Boolean IsLogInKeyValid()
        {
            return LogInKeyValidUntil.HasValue &&
                   LogInKeyValidUntil.Value > DateTime.UtcNow;
        }

        public Boolean IsPasswordExpired()
        {
            if (!LastPasswordChange.HasValue)
            {
                return false;
            }

            var passwordExpirationDate = LastPasswordChange.Value.AddDays(60);
            return passwordExpirationDate <= DateTime.UtcNow;
        }

        public Boolean LoggedInRecently(TimeSpan logInThreshold)
        {
            return LastLogIn.HasValue && (DateTime.UtcNow - LastLogIn.Value) < logInThreshold;
        }

        public Boolean VerifyPasscode(String passcode)
        {
            if (PasscodeBytes == null)
            {
                return false;
            }

            var hash = Security.Hash(passcode, PasscodeSaltBytesString);
            return PasscodeBytes.SequenceEqual(hash);
        }

        public Boolean VerifyPassword(String password)
        {
            if (PasswordBytes == null)
            {
                return false;
            }

            var hash = Security.Hash(password, SaltBytesString);
            return PasswordBytes.SequenceEqual(hash);
        }

        public Boolean VerifyToken(String token)
        {
            var storedToken = Security.Decrypt(TokenBytes, SaltBytesString);
            return String.Equals(storedToken, token);
        }

        public void Associate(PermissionKey value)
        {
            if (value != null)
            {
                Permissions.Add(value.Uri);
            }
        }

        public void Associate(LocationDocument value)
        {
            Locations.Add(value);
        }

        public override Boolean Equals(Object obj)
        {
            if (ReferenceEquals(null, obj))
            {
                return false;
            }

            if (ReferenceEquals(this, obj))
            {
                return true;
            }

            var entity = obj as AuthorizationDetails;
            return entity != null && entity.Id.Equals(Id);
        }

        public override int GetHashCode()
        {
            var result = Id.GetHashCode();
            return result;
        }
    }
}