using System.Web.Mvc;

namespace CargoAware.WebApp.Models
{
    public sealed class LogInModel
    {
        public string LogIn { get; set; }
        [AllowHtml]
        public string Password { get; set; }
    }
}