﻿using CargoAware.Services.User;
using System;

namespace CargoAware.Deployments.Demo
{
    public static class Helper
    {
        public static UserDocument CreateUser(
            String username,
            String name,
            String email,
            String[] friendlyPermissions,
            long recordedById,
            Boolean verifyRecordedById = true
        )
        {
            const String DefaultPassword = "Cargo2014!";

            var user = UserService.CreateUser(new UserDocument
            {
                Username = username,
                Name = name,
                Email = email,
                Permissions = friendlyPermissions,
                RecordedById = recordedById
            }, verifyRecordedById);
            UserService.SetPassword(user.Id, DefaultPassword);

            return user;
        }
    }
}