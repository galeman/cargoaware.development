﻿using CargoAware.Services;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Menus;
using Microsoft.Security.Application;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CargoAware.WebApp.Infrastructure
{
    public static class PageMenus
    {
        public static IEnumerable<IMenuNode> NavigationMenu(UserPrincipal user)
        {
            var result = ForUser(user)
                //.Concat(ForFranwell(user))
                .Where(m =>
                {
                    var link = (m as MenuLink);
                    return ((link == null) || (link.Visible));
                });
            return result;
        }

        private static IEnumerable<IMenuNode> ForUser(UserPrincipal user)
        {
            if (!user.Identity.IsAuthenticated)
            {
                yield break;
            }


            var permissions = user.Permissions;


            //yield return new MenuLink(
            //    "Dashboard",
            //    MVC.Dashboard.Index(),
            //    permissions,
            //    Permissions.User.ViewDashboard());

            yield return new MenuLink(
                "Print Jobs",
                MVC.PrintJobs.Index(),
                permissions,
                Permissions.User.ViewPrintJobs(),
                Permissions.User.ManagePrintJobs());

            yield return new MenuLink(
                "Diagnostics",
                MVC.Diagnostics.Index(),
                permissions,
                Permissions.User.ViewShipments(),
                Permissions.User.ManageShipments());

            yield return new DropdownMenu("Admin")
            {
                new DropdownMenu("Locations")
                {
                    new MenuLink(
                        "Locations",
                        MVC.AdminLocations.Index(),
                        permissions,
                        Permissions.User.AdminLocations()),
                    new MenuLink(
                        "Location Zones",
                        MVC.AdminLocationZones.Index(),
                        permissions,
                        Permissions.User.AdminLocationZones()),
                },
                new DropdownMenu("Printers")
                {
                    new MenuLink(
                        "Printers",
                        MVC.AdminPrinters.Index(),
                        permissions,
                        Permissions.User.AdminPrinters()),
                    new MenuLink(
                        "Label Templates",
                        MVC.AdminLabelTemplates.Index(),
                        permissions,
                        Permissions.User.AdminLabelTemplates()),
                },
                new DropdownMenu("Region Manager")
                {
                    new MenuLink(
                        "Regions",
                        MVC.AdminRegionManagerRegions.Index(),
                        permissions,
                        Permissions.User.AdminRegionManagerRegions()),
                }
            //                new MenuLink(
            //                    "Employees",
            //                    MVC.IndustryAdminEmployees.Index(),
            //                    permissions,
            //                    Permissions.Industry.Administration(facilityId)),
            };
        }

        public static IEnumerable<IMenuNode> ImportExportMenu(UserPrincipal user)
        {
            yield return new MenuLink("", MVC.DataImport.Index())
            {
                PrependHtml =
                    "<span class=\"icon-file icon-white\"></span>" +
                    "<span class=\"icon-arrow-up icon-white\"></span>",
                Visible =
                    user.HasPermission(Permissions.User.DataImport())
            };

            //yield return new MenuLink("", MVC.DataExport.Index())
            //{
            //    PrependHtml =
            //        "<span class=\"icon-file icon-white\"></span>" +
            //        "<span class=\"icon-arrow-down icon-white\"></span>",
            //    Visible =
            //        //user.HasPermission(Permissions.Industry.ManagePlants(currentFacilityId)) ||
            //        //user.HasPermission(Permissions.Industry.ManagePackages(currentFacilityId)) ||
            //        user.HasPermission(Permissions.User.Administration())
            //};
        }

        public static DropdownMenu SupportMenu()
        {
            var result = new DropdownMenu("Support")
            {
                new MenuLink("Quick Reference Guide", "")
                {
                    PrependHtml = "<strong>",
                    AppendHtml = "</strong>",
                    OpenNewPage = true
                },
                new MenuLink("Manual / User Guide", "")
                {
                    PrependHtml = "<strong>",
                    AppendHtml = "</strong>",
                    OpenNewPage = true
                },
                //SeparatorNode.Default,
                //new DropdownMenu("Additional User Guides")
                //{
                //    new MenuLink("Beginning Inventory", MVC.Documents.GetBeginningInventory())
                //    {
                //        OpenNewPage = true
                //    },
                //    new MenuLink("Medical to Retail Transfer", MVC.Documents.GetMedToRetailTransfer())
                //    {
                //        OpenNewPage = true
                //    },
                //    new MenuLink("Sales Reporting", MVC.Documents.GetSaleReporting())
                //    {
                //        OpenNewPage = true
                //    },
                //    new MenuLink("Sales Import Memo", MVC.Documents.GetSalesImportMemo())
                //    {
                //        OpenNewPage = true
                //    },
                //    new MenuLink("Plants Search Features", MVC.Documents.GetPlantsSearchFeatures())
                //    {
                //        OpenNewPage = true
                //    },
                //    new MenuLink("Test Labs Transfer Process", MVC.Documents.GetTestLabsTransferProcess())
                //    {
                //        OpenNewPage = true
                //    },
                //},
                //new DropdownMenu("Integrator User Guides")
                //{
                //    new MenuLink("Integrator Sales Support Contacts", MVC.Documents.GetIntegratorSalesSupportContacts())
                //    {
                //        OpenNewPage = true
                //    },
                //    new MenuLink("adilas FIFO and Multi-Package Selling", MVC.Documents.GetIntegratorAdilasFifoAndMultiPackageSelling())
                //    {
                //        OpenNewPage = true
                //    },
                //},
                SeparatorNode.Default,
                new MenuLink("Customer Support:", ""),
                new MenuLink("- http://support.cargoaware.com/", "http://support.cargoaware.com/")
                {
                    PrependHtml = "<small>",
                    AppendHtml = "</small>",
                    OpenNewPage = true
                },
                new MenuLink("- support@cargoaware.com", "mailto://support@cargoaware.com")
                {
                    PrependHtml = "<small>",
                    AppendHtml = "</small>"
                },
                new MenuLink("- 1-877-123-4567", "")
                {
                    PrependHtml = "<small>",
                    AppendHtml = "</small>"
                },
                SeparatorNode.Default,
                new DropdownMenu("Technical Support")
                {
                    PrependHtml = "<small class=\"muted\"><strong>",
                    AppendHtml = "</strong></small>",
                    Items =
                    {
                        new MenuLink("Forced Page Reload", "")
                        {
                            AnchorAttributes = { { "id", "forced_page_reload" } },
                            PrependHtml = "<small class=\"muted\"><strong>",
                            AppendHtml = "</strong></small>"
                        },
                    },
                },
            };
            result.PrependHtml = "<span class=\"icon-question-sign icon-white\"></span> ";
            return result;
        }

        public static DropdownMenu UserMenu(UserPrincipal user)
        {
            var result = new DropdownMenu(user.Username)
            {
                new MenuLink("User Profile", MVC.UserProfile.Index()),
                new MenuLink("Log out", MVC.Sessions.LogOut())
            };
            result.PrependHtml = "<span class=\"icon-user icon-white\"></span> ";
            return result;
        }

        public static DropdownMenu LocationsDropdown(UserPrincipal user)
        {
            const String SelectedNameFormat = "<strong>{0}</strong> | ";
            const String SelectedLicenseFormat = "<small>{0}</small>";
            const String MenuNameFormat = "<div style=\"position: relative; padding-right: 85px\"><strong>{0}</strong>";
            const String MenuLicenseFormat = "<small style=\"position: absolute; right: 0;\">{0}</small></div>";

            var currentLocation = user.CurrentLocation;

            var result = new DropdownMenu("", "locations-dropdown", "btn-inverse shadow", "pull-right")
            {
                PrependHtml = String.Format(SelectedNameFormat, Encoder.HtmlEncode(currentLocation.Name)),
                AppendHtml = String.Format(SelectedLicenseFormat, Encoder.HtmlEncode(currentLocation.Id.ToString()))
            };
            result.AddRange(user.Locations.Select(x => new MenuLink("", MVC.Sessions.ChangeLocation(x.Id))
            {
                PrependHtml = String.Format(MenuNameFormat, Encoder.HtmlEncode(x.Name)),
                AppendHtml = String.Format(MenuLicenseFormat, Encoder.HtmlEncode(x.Id.ToString()))
            }));
            return result;
        }
    }
}