﻿using AttributeRouting;
using AttributeRouting.Web.Mvc;
using CargoAware.Services;
using CargoAware.Services.Location;
using CargoAware.Web.Infrastructure.Security;
using CargoAware.WebApp.Infrastructure.Controllers;
using CargoAware.WebApp.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CargoAware.WebApp.Controllers.Mvc
{
    [MvcAuthorize]
    [RoutePrefix("admin/locations")]
    public partial class AdminLocationsController : AppMvcController
    {
        private static Lazy<LocationRepository> _LocationRepository;


        #region // Constructor + DI
        // ==================================================

        static AdminLocationsController()
        {
            Inject(Repositories.ForLocations);
        }

        private static void Inject(Lazy<LocationRepository> dependency)
        {
            _LocationRepository = dependency;
        }

        // ==================================================
        #endregion


        #region // MVC Commands
        // ==================================================

        [GET("")]
        public virtual ActionResult Index()
        {
            if (!User.HasPermission(Permissions.User.AdminLocations()))
            {
                throw UnauthorizedException();
            }

            return View();
        }

        [GET("addedit")]
        public virtual ActionResult AddEdit(Boolean adding, long[] ids)
        {
            if (!User.HasPermission(Permissions.User.AdminLocations()))
            {
                throw UnauthorizedException();
            }

            var model = new AddEditLocationsModel
            {
                Adding = adding,
                Locations = _LocationRepository.Value.GetLocations().Data
            };

            if (!adding)
            {
                if (ids != null && ids.Length != 0)
                {
                    model.Details = model.Locations
                        .Where(x => ids.Any(id => x.Id == id))
                        .Select(x => new AddEditLocationsDetailsModel
                        {
                            Id = x.Id,
                            Name = x.Name,
                            AxisDirection = x.AxisDirection,
                            OriginLocation = x.OriginLocation,
                            ApiKey = x.ApiKey,
                            GenerateNewApiKey = false
                        });
                }
            }

            return View(model);
        }

        // ==================================================
        #endregion
    }
}