﻿namespace CargoAware.Services.Common
{
    public enum EntityCancelledState : byte
    {
        All,
        NonCancelledOnly,
        CancelledOnly
    }
}